# Changelog
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [1.1.9083] - 2024-11-13 Preview 202304
* Update packages.
* Upgrade Forms libraries and Test and Demo apps to .Net 9.0.
* Increment version to 9083.

## [1.1.8956] - 2024-07-09 Preview 202304
* Reference updated core projects.

## [1.1.8949] - 2024-06-26 Preview 202304
* Update to .Net 8.
* Implement MS Test SDK project format.
* Condition auto generating binding redirects on .NET frameworks 472 and 480.
* Apply code analysis rules.

## [1.1.8546] - 2023-05-26 Preview 202304
* Update packages.
* Reorganize folders. 
* Implement nullable. 
* Add trace log projects. 
* Use simplified test settings and test logging.
* Update revisions
* Add command line demo using MVVM splash.

## [1.1.8544] - 2023-05-24 Preview 202304
* Set projects framework to .NET 7.
* Rearrange folders.
* Set MSTest namespace to cc.isr.

## [1.1.8535] - 2023-05-15 Preview 202304
* Use cc.isr.Json.AppSettings.ViewModels project for settings I/O.

## [1.1.8518] - 2023-04-28 Preview 202304
* Split README.MD to attribution, cloning, open-source and read me files.
* Add code of conduct, contribution and security documents.
* Increment version.

## [1.0.8127] - 2022-04-02
* Model View Form: Rename Model View to Model View Logger and 
Property Notify Control to Model View. 
* Fix handling of the model view logger events.
* Repackage Forms.

## [1.0.8125] - 2022-03-31
* Pass tests in project reference mode. 

## [1.0.8119] - 2022-03-25
* Use the new Json application settings base class.
* Use logging trace log windows forms.

## [1.0.8109] - 2022-03-15
* Use the ?. operator, without making a copy of the delegate, 
to check if a delegate is non-null and invoke it in a thread-safe way.

## [1.0.8105] - 2022-03-11
* Forked from [dn.core].

&copy; 2012 Integrated Scientific Resources, Inc. All rights reserved.

[dn.core]: https://www.bitbucket.org/davidhary/dn.core
[1.1.9083]: https://bitbucket.org/davidhary/dn.win.forms/src/main/
