# Open Source

Support libraries for Windows Forms.
This is a fork of the [dn.core] repository.

* [Open Source](#Open-Source)
* [Closed Software](#Closed-software)

<a name="Open-Source"></a>
## Open Source
Open source used by this software is described and licensed at the
following sites:  
[win.forms]  
[json]  
[logging]  
[std]  
[tracing]  
[win.controls]  

<a name="Closed-software"></a>
### Closed software
Closed software used by this software are described and licensed on
the following sites:  
[SQL Exception Message]
 
[MEGA packages folder]: https://mega.nz/folder/KEcVxC5a#GYnmvMcwP4yI4tsocD31Pg
[win.forms]: https://www.bitbucket.org/davidhary/dn.win.forms
[win.controls]: https://www.bitbucket.org/davidhary/dn.win.controls
[json]: https://www.bitbucket.org/davidhary/dn.json
[logging]: https://www.bitbucket.org/davidhary/dn.logging
[std]: https://www.bitbucket.org/davidhary/dn.std
[tracing]: https://www.bitbucket.org/davidhary/dn.tracing
[SQL Exception Message]: https://msdn.microsoft.com/en-us/library/ms365274.aspx

[IDE Repository]: https://www.bitbucket.org/davidhary/vs.ide
[external repositories]: ExternalReposCommits.csv

