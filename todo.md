### TODO

#### Bug fixes

##### Metro
* Figure out why the blue splash resource fails to get the objects when running under .Net Framework 4.72 or 4.8.

#### MVVM
* Support MVVM in relevant libraries.
* Support MVVM in Windows forms project.
* Add MAUI forms and demo projects;
* Add UNO forms and demo projects;
* Replace Notify Propery Change calls with Set Property

#### Package

* Add the solution to GitHub.
* Add a GitHub folder with actions to build the package.
* Use GitHub actions to unit test and build the package.
