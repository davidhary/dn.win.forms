using System;
using System.Windows.Forms;
using cc.isr.WinForms.Forma;
using System.ComponentModel;

namespace cc.isr.WinForms.LogForms;

public partial class LoggerUserFormBase
{
    #region " class style "

    /// <summary> The enable drop shadow version. </summary>
#pragma warning disable CA1707
    public const int ENABLE_DROP_SHADOW_VERSION = 5;
#pragma warning restore CA1707

    /// <summary> Gets the class style. </summary>
    /// <value> The class style. </value>
    protected ClassStyleConstants ClassStyle { get; set; } = ClassStyleConstants.None;

    /// <summary> Adds a drop shadow parameter. </summary>
    /// <remarks>
    /// From <see href="http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx">Code Project</see>.
    /// </remarks>
    /// <value>
    /// A <see cref="CreateParams" /> that contains the required creation
    /// parameters when the handle to the control is created.
    /// </value>
    protected override CreateParams CreateParams
    {
        get
        {
            CreateParams cp = base.CreateParams;
            cp.ClassStyle |= ( int ) this.ClassStyle;
            return cp;
        }
    }

    #endregion

    #region " setting events "

    /// <summary>   Gets or sets information describing the user form. </summary>
    /// <value> Information describing the user form. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public UserForms.UserFormInfo UserFormInfo { get; private set; }

    /// <summary> Controls if settings are saved when closing. </summary>
    /// <remarks> Set this property to false to disable saving form settings on closing. </remarks>
    /// <value> <c>SaveSettingsOnClosing</c>is a Boolean property. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public bool SaveSettingsOnClosing { get; set; }

    /// <summary> This is called when the form is loaded before it is visible. </summary>
    /// <remarks> Use this method to set form elements before the form is visible. </remarks>
    protected virtual void OnLoadSettings()
    {
        if ( this.SaveSettingsOnClosing )
        {
            this.UserFormInfo.ReadSettings();
            FormStartPosition startPos = this.UserFormInfo.StartPosition;
            if ( !this.StartPosition.Equals( startPos ) )
            {
                this.StartPosition = startPos;
            }
        }
    }

    /// <summary> This is called when the form is shown after it is visible. </summary>
    /// <remarks> Use this method to set form elements after the form is visible. </remarks>
    protected virtual void OnShowSettings()
    {
        if ( this.SaveSettingsOnClosing )
        {
            if ( this.StartPosition == FormStartPosition.Manual )
            {
                this.UserFormInfo.ReadSettings();
                this.WindowState = this.UserFormInfo.WindowsState;
                if ( this.WindowState == FormWindowState.Normal )
                {
                    System.Drawing.Size size = this.UserFormInfo.Size;
                    System.Drawing.Point loc = this.UserFormInfo.Location;
                    if ( size.Width < this.MinimumSize.Width && size.Height < this.MinimumSize.Height )
                    {
                        size = new System.Drawing.Size( this.MinimumSize.Width, this.MinimumSize.Height );
                    }
                    else if ( size.Width < this.MinimumSize.Width )
                    {
                        size = new System.Drawing.Size( this.MinimumSize.Width, size.Height );
                    }
                    else if ( size.Height < this.MinimumSize.Height )
                    {
                        size = new System.Drawing.Size( size.Width, this.MinimumSize.Height );
                    }

                    if ( Screen.PrimaryScreen is not null
                        && !this.Location.Equals( loc ) && loc.X < Screen.PrimaryScreen.WorkingArea.Width && loc.X + size.Width > 0 && loc.Y < Screen.PrimaryScreen.WorkingArea.Height && loc.Y + size.Height > 0 )
                    {
                        this.Location = loc;
                    }

                    if ( !this.Size.Equals( size ) )
                    {
                        this.Size = size;
                    }
                }
            }
        }
    }

    /// <summary> Is called when the form unloads. </summary>
    /// <remarks> Must be overridden by the sub-class form at the top level of the
    ///           application handling the storage of the Json settings file. </remarks>
    protected virtual void OnSaveSettings()
    {
    }

    #endregion

    #region " form events "

    /// <summary>
    /// Gets the sentinel indicating that the form loaded without an exception. Should be set only if
    /// load did not fail.
    /// </summary>
    /// <value> The is loaded. </value>
    protected bool IsLoaded { get; set; }

    /// <summary>
    /// Raises the <see cref="Form.Load" /> event after reading the start
    /// position from the configuration file.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnLoad( EventArgs e )
    {
        try
        {
            if ( !this.DesignMode )
            {
                this.OnLoadSettings();
            }

            this.IsLoaded = true;
        }
        catch
        {
            throw;
        }
        finally
        {
            base.OnLoad( e );
        }
    }

    /// <summary>
    /// Raises the <see cref="Form.Shown" /> event after positioning the form
    /// based on the configuration settings.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnShown( EventArgs e )
    {
        if ( !this.IsLoaded )
        {
            return;
        }

        try
        {
            if ( !this.DesignMode )
            {
                this.OnShowSettings();
            }
        }
        catch
        {
            throw;
        }
        finally
        {
            base.OnShown( e );
        }

        this.OnShownCompleted( e );
    }

    /// <summary>
    /// Raises the <see cref="Form.OnClosing" /> event after saving the form
    /// location settings.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="System.ComponentModel.CancelEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnClosing( System.ComponentModel.CancelEventArgs e )
    {
        try
        {
            if ( this.SaveSettingsOnClosing && !this.DesignMode && this.IsLoaded && e is not null && !e.Cancel )
            {
                this.OnSaveSettings();
            }
        }
        catch
        {
            throw;
        }
        finally
        {
            base.OnClosing( e );
        }
    }

    /// <summary> Event queue for all listeners interested in ShownCompleted events. </summary>
    public event EventHandler<EventArgs>? ShownCompleted;

    /// <summary> Raises the shown completed event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> Event information to send to registered event handlers. </param>
    protected virtual void OnShownCompleted( EventArgs e )
    {
        ShownCompleted?.Invoke( this, EventArgs.Empty );
    }

    #endregion
}
