using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using cc.isr.Logging.TraceLog;
using Microsoft.Extensions.Logging;

namespace cc.isr.WinForms.LogForms;

public partial class LoggerUserFormBase
{

    /// <summary>   Gets or sets the trace logger. </summary>
    /// <value> The trace logger. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public static TraceLogger<LoggerUserFormBase> TraceLogger { get; set; } = new();

    #region " text  writer "

    /// <summary>   Adds a display trace event writer. </summary>
    /// <remarks>   David, 2021-02-23. </remarks>
    /// <param name="textWriter">   The trace Event tWriter. </param>
    public void AddDisplayTextWriter( cc.isr.Tracing.ITraceEventWriter textWriter )
    {
        textWriter.TraceLevel = this.DisplayTraceEventType;
        Tracing.TracingPlatform.Instance.AddTraceEventWriter( textWriter );
    }

    /// <summary>   Removes the display text writer described by <paramref name="textWriter"/>. </summary>
    /// <remarks>   David, 2021-02-23. </remarks>
    /// <param name="textWriter">   The trace Event tWriter. </param>
#pragma warning disable CA1822
    public void RemoveDisplayTextWriter( Tracing.ITraceEventWriter textWriter )
    {
        Tracing.TracingPlatform.Instance.RemoveTraceEventWriter( textWriter );
    }
#pragma warning restore CA1822

    #endregion

    #region " display trace event type "

    /// <summary>
    /// Gets or sets the <see cref="TraceEventType"/> value name pair for the global trace event
    /// writer. This level determines the level of all the
    /// <see cref="Tracing.ITextWriter"/>s Trace Listeners. Each trace listener can still listen at a
    /// lower level.
    /// </summary>
    /// <value> The trace event writer trace event value name pair. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public KeyValuePair<TraceEventType, string> TraceEventWriterTraceEventValueNamePair
    {
        get => TraceLogger.TraceEventWriterTraceEventValueNamePair;
        set
        {
            if ( !KeyValuePair<TraceEventType, string>.Equals( value, this.TraceEventWriterTraceEventValueNamePair ) )
            {
                TraceLogger.TraceEventWriterTraceEventValueNamePair = value;
            }
        }
    }

    /// <summary>
    /// Gets or sets the <see cref="TraceEventType"/> for the global trace event writer. This level
    /// determines the level of all the
    /// <see cref="Tracing.ITextWriter"/>s Trace Listeners. Each trace listener can still listen at a
    /// lower level.
    /// </summary>
    /// <value> The type of the trace event writer trace event. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public TraceEventType TraceEventWriterTraceEventType
    {
        get => TraceLogger.TraceEventWriterTraceEventType;
        set
        {
            if ( value != this.TraceEventWriterTraceEventType )
            {
                TraceLogger.TraceEventWriterTraceEventType = value;
            }
        }
    }

    /// <summary>
    /// Gets or sets the <see cref="TraceEventType"/> value name pair for display.
    /// </summary>
    /// <value> The log level value name pair. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public KeyValuePair<TraceEventType, string> DisplayTraceEventTypeValueNamePair { get; set; }

    /// <summary>
    /// Gets or sets the <see cref="TraceEventType"/> for display.
    /// </summary>
    /// <value> The trace event writer log level. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public TraceEventType DisplayTraceEventType { get; set; }

    #endregion

    #region " logging log level "

#pragma warning disable IDE0001
    /// <summary>
    /// Gets or sets the <see cref="Microsoft.Extensions.Logging.LogLevel"/> value name pair for logging. This level
    /// determines the level of the <see cref="ILogger"/>.
    /// </summary>
    /// <value> The log trace event value name pair. </value>
#pragma warning restore IDE0001
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public KeyValuePair<LogLevel, string> LoggingLevelValueNamePair
    {
        get => TraceLogger.LoggingLevelValueNamePair;
        set
        {
            if ( !KeyValuePair<LogLevel, string>.Equals( value, this.LoggingLevelValueNamePair ) )
            {
                TraceLogger.LoggingLevelValueNamePair = value;
            }
        }
    }
#pragma warning disable IDE0001
    /// <summary>   Gets or sets <see cref="Microsoft.Extensions.Logging.LogLevel"/> value for logging. </summary>
    /// <value> The trace event type for logging. </value>
#pragma warning restore IDE0001
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public LogLevel LoggingLogLevel
    {
        get => TraceLogger.MinimumLogLevel;
        set
        {
            if ( value != this.LoggingLogLevel )
            {
                TraceLogger.MinimumLogLevel = value;
            }
        }
    }

    #endregion

    #region " log exception "

    /// <summary>   Adds an exception data. </summary>
    /// <remarks>   David, 2021-07-02. </remarks>
    /// <param name="ex">   The exception. </param>
    /// <returns>   True if exception data was added for this exception; otherwise, false. </returns>
    protected virtual bool AddExceptionData( Exception ex )
    {
        return false;
    }

    /// <summary>   Log exception. </summary>
    /// <remarks>
    /// Declared as must override so that the calling method could add exception data before
    /// recording this exception.
    /// </remarks>
    /// <param name="ex">               The ex. </param>
    /// <param name="activity">         The activity. </param>
    /// <param name="memberName">       (Optional) Name of the caller member. </param>
    /// <param name="sourceFilePath">   (Optional) Full pathname of the caller source file. </param>
    /// <param name="sourceLineNumber"> (Optional) Line number in the caller source file. </param>
    /// <returns>   A <see cref="string" />. </returns>
    protected string LogError( Exception ex, string activity, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
                                                              [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
                                                              [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0 )
    {
        _ = this.AddExceptionData( ex );
        return TraceLogger.LogException( ex, activity, memberName, sourceFilePath, sourceLineNumber );
    }

    #endregion
}
