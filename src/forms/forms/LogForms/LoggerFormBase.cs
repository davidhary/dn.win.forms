using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using cc.isr.WinForms.Forma;

namespace cc.isr.WinForms.LogForms;

/// <summary> A form listening to trace messages. </summary>
/// <remarks>
/// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2015-12-26, 2.1.5836. </para>
/// </remarks>
public partial class LoggerFormBase : Form
{
    #region " construction and cleanup "

    /// <summary> Gets the initializing components sentinel. </summary>
    /// <value> The initializing components sentinel. </value>
    protected bool InitializingComponents { get; set; }

    /// <summary> Specialized default constructor for use only by derived classes. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    protected LoggerFormBase() : base()
    {
        this.InitializingComponents = true;
        this.InitializeComponent();
        this.InitializingComponents = false;
    }

    /// <summary> Initializes the component. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private void InitializeComponent()
    {
        this.SuspendLayout();
        this.AutoScaleDimensions = new SizeF( 7.0f, 17.0f );
        this.AutoScaleMode = AutoScaleMode.Font;
        this.BackColor = SystemColors.Control;
        this.ClientSize = new Size( 331, 341 );
        this.Cursor = Cursors.Default;
        this.Icon = Properties.Resources.favicon;
        this.Font = new Font( SystemFonts.DefaultFont.FontFamily, 9.75f, FontStyle.Regular, GraphicsUnit.Point );
        this.Name = nameof( LoggerFormBase );
        this.ResumeLayout( false );
    }

    /// <summary>
    /// Disposes of the resources (other than memory) used by the
    /// <see cref="Form" />.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="disposing"> true to release both managed and unmanaged resources; false to
    /// release only unmanaged resources. </param>
    [System.Diagnostics.DebuggerNonUserCode()]
    protected override void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }

    #region " class style "

    /// <summary> The enable drop shadow version. </summary>
#pragma warning disable CA1707
    public const int ENABLE_DROP_SHADOW_VERSION = 5;
#pragma warning restore CA1707

    /// <summary> Gets the class style. </summary>
    /// <value> The class style. </value>
    protected ClassStyleConstants ClassStyle { get; set; } = ClassStyleConstants.None;

    /// <summary> Adds a drop shadow parameter. </summary>
    /// <remarks>
    /// From <see href="http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx">Code Project</see>.
    /// </remarks>
    /// <value> Options that control the create. </value>
    protected override CreateParams CreateParams
    {
        get
        {
            CreateParams cp = base.CreateParams;
            cp.ClassStyle |= ( int ) this.ClassStyle;
            return cp;
        }
    }

    #endregion

    #endregion

    #region " show "

    /// <summary>
    /// Shows the <see cref="RichTextBox">rich text box</see> form with these messages.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="mdiForm"> The MDI form. </param>
    /// <param name="owner">   The owner. </param>
    public void Show( Form mdiForm, IWin32Window owner )
    {
        if ( mdiForm is not null && mdiForm.IsMdiContainer )
        {
            this.MdiParent = mdiForm;
            mdiForm.Show();
        }

        this.Show( owner );
    }

    /// <summary>
    /// Shows the <see cref="RichTextBox">rich text box</see> form with these messages.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="mdiForm"> The MDI form. </param>
    public void ShowDialog( Form mdiForm )
    {
        if ( mdiForm is not null && mdiForm.IsMdiContainer )
        {
            this.MdiParent = mdiForm;
            mdiForm.Show();
        }

        _ = this.ShowDialog();
    }

    #endregion
}
/// <summary> Collection of logger forms. </summary>
/// <remarks>
/// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2016-01-04 </para>
/// </remarks>
public class LoggerFormCollection : List<LoggerFormBase>
{
    /// <summary> Adds and shows a new form,. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="form"> The form. </param>
    public void ShowNew( LoggerFormBase form )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( form, nameof( form ) );
#else
        if ( form is null ) throw new ArgumentNullException( nameof( form ) );
#endif
        this.Add( form );
        form.FormClosed += this.OnClosed;
        form.Show();
    }

    /// <summary> Adds a form. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="form"> The form. </param>
    /// <returns> A LoggerFormBase. </returns>
    public LoggerFormBase AddForm( LoggerFormBase form )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( form, nameof( form ) );
#else
        if ( form is null ) throw new ArgumentNullException( nameof( form ) );
#endif
        this.Add( form );
        form.FormClosed += this.OnClosed;
        return form;
    }

    /// <summary> Handles a member form closed event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information to send to registered event handlers. </param>
    private void OnClosed( object? sender, EventArgs e )
    {
        if ( sender is LoggerFormBase f )
        {
            f.FormClosed -= this.OnClosed;
            _ = this.Remove( f );
            f?.Dispose();
        }
    }

    /// <summary> Removes the dispose described by value. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The value. </param>
    public void RemoveDispose( LoggerFormBase value )
    {
        LoggerFormBase? f = value;
        if ( f is not null )
        {
            f.FormClosed -= this.OnClosed;
            _ = this.Remove( f );
            f.Dispose();
            f = null;
        }
    }

    /// <summary>
    /// Removes all items from the <see cref="System.Collections.ICollection" />.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public void ClearDispose()
    {
        while ( this.Any() )
        {
            this.RemoveDispose( this[0] );
            Application.DoEvents();
        }
    }
}
