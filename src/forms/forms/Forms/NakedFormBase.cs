using System;
using System.Drawing;
using System.Windows.Forms;

namespace cc.isr.WinForms.Forma;

/// <summary> Form with drop shadow. </summary>
/// <remarks>
/// (c) 2007 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2007-08-13 1.0.2781. from Nicholas Seward </para><para>
/// http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx.
/// http://www.codeproject.com/Articles/1108900/Resize-and-Drag-a-FormBorderStyle-None-Form-in-NET
/// David, 2007-08-13, 1.0.2781 Convert from C#. </para>
/// </remarks>
public partial class NakedFormBase : Form
{
    #region " construction and cleanup "

    /// <summary> Specialized default constructor for use only by derived classes. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    protected NakedFormBase() : base() => this.InitializeComponent();

    /// <summary> Initializes the component. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private void InitializeComponent()
    {
        this.SuspendLayout();
        this.AutoScaleDimensions = new SizeF( 7.0f, 17.0f );
        this.AutoScaleMode = AutoScaleMode.Font;
        this.BackColor = SystemColors.Control;
        this.ClientSize = new Size( 331, 341 );
        this.Cursor = Cursors.Default;
        this.Font = new Font( SystemFonts.DefaultFont.FontFamily, 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0 );
        this.Icon = Properties.Resources.favicon;
        this.Name = nameof( NakedFormBase );
        this.ResumeLayout( false );
    }

    #region " class style "

#pragma warning disable CA1707
    /// <summary> The enable drop shadow version. </summary>
    public const int ENABLE_DROP_SHADOW_VERSION = 5;
#pragma warning restore CA1707    

    /// <summary> Gets the class style. </summary>
    /// <value> The class style. </value>
    protected ClassStyleConstants ClassStyle { get; set; } = ClassStyleConstants.None;

    /// <summary> Adds a drop shadow parameter. </summary>
    /// <remarks>
    /// From <see href="http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx">Code Project</see>.
    /// </remarks>
    /// <value> Options that control the create. </value>
    protected override CreateParams CreateParams
    {
        get
        {
            CreateParams cp = base.CreateParams;
            cp.ClassStyle |= ( int ) this.ClassStyle;
            return cp;
        }
    }

    #endregion

    #endregion

    #region " form event handlers "

    /// <summary> The test on border. </summary>
    private OnBorder _testOnBorder;

    /// <summary> Width of the border. </summary>
    private readonly int _borderWidth = 8;

    /// <summary> In panel border. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="pos"> The position. </param>
    /// <returns> An OnBorder. </returns>
    private OnBorder InPanelBorder( Point pos )
    {
        Point pointMe = new( 0, 0 );
        OnBorder result = OnBorder.None;
        if ( pos.Y < pointMe.Y + this._borderWidth )
        {
            result |= OnBorder.Top;
        }

        if ( pos.Y > pointMe.Y + this.Height - this._borderWidth )
        {
            result |= OnBorder.Bottom;
        }

        if ( pos.X < pointMe.X + this._borderWidth )
        {
            result |= OnBorder.Left;
        }

        if ( pos.X > pointMe.X + this.Width - this._borderWidth )
        {
            result |= OnBorder.Right;
        }

        return result;
    }

    /// <summary> Raises the <see cref="System.Windows.Forms.Control.MouseMove" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="MouseEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnMouseMove( MouseEventArgs e )
    {
        if ( e is null ) return;
        this._testOnBorder = this.InPanelBorder( e.Location );
        switch ( this._testOnBorder )
        {
            case OnBorder.None:
                {
                    this.Cursor = Cursors.Arrow;
                    break;
                }

            case OnBorder.Top:
                {
                    this.Cursor = Cursors.SizeNS;
                    break;
                }

            case OnBorder.Right:
                {
                    this.Cursor = Cursors.SizeWE;
                    break;
                }

            case OnBorder.TopRight:
                {
                    this.Cursor = Cursors.SizeNESW;
                    break;
                }

            case OnBorder.RightBottom:
                {
                    this.Cursor = Cursors.SizeNWSE;
                    break;
                }

            case OnBorder.Bottom:
                {
                    this.Cursor = Cursors.SizeNS;
                    break;
                }

            case OnBorder.BottomLeft:
                {
                    this.Cursor = Cursors.SizeNESW;
                    break;
                }

            case OnBorder.Left:
                {
                    this.Cursor = Cursors.SizeWE;
                    break;
                }

            case OnBorder.LeftTop:
                {
                    this.Cursor = Cursors.SizeNWSE;
                    break;
                }

            default:
                this.Cursor = Cursors.Arrow;
                break;
        }
    }

    /// <summary> Raises the <see cref="System.Windows.Forms.Control.MouseDown" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="MouseEventArgs" /> that contains the
    /// event data. </param>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0010:Add missing cases", Justification = "<Pending>" )]
    protected override void OnMouseDown( MouseEventArgs e )
    {
        UIntPtr dir;
        switch ( this._testOnBorder )
        {
            case OnBorder.Top:
                {
                    dir = this.HTTOP;
                    break;
                }

            case OnBorder.TopRight:
                {
                    dir = this.HTTOPRIGHT;
                    break;
                }

            case OnBorder.Right:
                {
                    dir = this.HTRIGHT;
                    break;
                }

            case OnBorder.RightBottom:
                {
                    dir = this.HTBOTTOMRIGHT;
                    break;
                }

            case OnBorder.Bottom:
                {
                    dir = this.HTBOTTOM;
                    break;
                }

            case OnBorder.BottomLeft:
                {
                    dir = this.HTBOTTOMLEFT;
                    break;
                }

            case OnBorder.Left:
                {
                    dir = this.HTLEFT;
                    break;
                }

            case OnBorder.LeftTop:
                {
                    dir = this.HTTOPLEFT;
                    break;
                }

            default:
                {
                    dir = this.HT_CAPTION;
                    break;
                }
        }

        _ = SafeNativeMethods.ReleaseCapture();
        _ = SafeNativeMethods.SendMessage( this.Handle, this.WM_NCLBUTTONDOWN, dir, new IntPtr( 0 ) );
    }

    #endregion
}
