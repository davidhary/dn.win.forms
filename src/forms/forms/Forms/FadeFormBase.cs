using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using System.ComponentModel;

namespace cc.isr.WinForms.Forma;

/// <summary> Form with fade in and out capabilities. </summary>
/// <remarks>
/// Features: <para>
/// fades in on open; </para><para>
/// fades out on close; </para><para>
/// partially fades out on focus lost;</para><para>
/// fades in on focus; </para><para>
/// fades out on minimize; </para><para>
/// fades in on restore;</para><para>
/// must not annoy the user. </para><para>
/// Usage: To use FadeForm, just extend it instead of Form and you are ready to go. It is
/// defaulted to use the fade-in from nothing on open and out to nothing on close. It will also
/// fade to 85% opacity when not the active window. Fading can be disabled and enabled with two
/// methods setting the default enable and disable modes.</para> (c) 2007 Integrated Scientific
/// Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2007-08-13. 1.0.2781. from Nicholas Seward  </para><para>
/// http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx.  </para><para>
/// David, 2007-08-13, 1.0.2781. Convert from C#. </para>
/// </remarks>
public partial class FadeFormBase : Form
{
    #region " construction and cleanup "

    /// <summary> Specialized default constructor for use only by derived class. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    protected FadeFormBase() : base()
    {
        this._fadeTime = 0.35d;
        this._activeOpacity = 1d;
        this._inactiveOpacity = 0.85d;
        this.InitializeComponent();
    }

    /// <summary> Initializes the component. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    [MemberNotNull( nameof( _timerThis ) )]
    private void InitializeComponent()
    {
        this.SuspendLayout();
        this.AutoScaleDimensions = new SizeF( 7.0f, 17.0f );
        this.AutoScaleMode = AutoScaleMode.Font;
        this.ClientSize = new Size( 331, 341 );
        this.Font = new Font( SystemFonts.DefaultFont.FontFamily, 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0 );
        this.Icon = Properties.Resources.favicon;
        this.Name = nameof( FadeFormBase );
        this._timerThis = new Timer() { Interval = 25 };
        this._timerThis.Tick += new EventHandler( this.Timer_Tick );
        this.ResumeLayout( false );
    }

    /// <summary>
    /// Releases the unmanaged resources used by the <see cref="Control" />
    /// and its child controls and optionally releases the managed resources.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="disposing"> true to release both managed and unmanaged resources; false to
    /// release only unmanaged resources. </param>
    [DebuggerNonUserCode()]
    protected override void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this.TimerThis?.Dispose();
            }
        }
        catch ( Exception ex )
        {
            Trace.TraceError( ex.ToString() );
            Debug.Assert( !Debugger.IsAttached, ex.ToString() );
        }
        finally
        {
            base.Dispose( disposing );
        }
    }

    #region " class style "

#pragma warning disable CA1707
    /// <summary> The enable drop shadow version. </summary>
    public const int ENABLE_DROP_SHADOW_VERSION = 5;
#pragma warning restore CA1707    

    /// <summary> Gets the class style. </summary>
    /// <value> The class style. </value>
    protected ClassStyleConstants ClassStyle { get; set; } = ClassStyleConstants.None;

    /// <summary> Adds a drop shadow parameter. </summary>
    /// <remarks>
    /// From <see href="http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx">Code Project</see>.
    /// </remarks>
    /// <value> Options that control the create. </value>
    protected override CreateParams CreateParams
    {
        get
        {
            CreateParams cp = base.CreateParams;
            cp.ClassStyle |= ( int ) this.ClassStyle;
            return cp;
        }
    }

    #endregion

    #endregion

    #region " fading properties "

    /// <summary> The active opacity. </summary>
    private double _activeOpacity;

    /// <summary>
    /// Gets or sets the opacity that the form will transition to when the form gets focus.
    /// </summary>
    /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    /// the required range. </exception>
    /// <value> The active opacity. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public double ActiveOpacity
    {
        get => this._activeOpacity;
        set
        {
            this._activeOpacity = value is >= 0d and <= 1d
                ? value
                : throw new ArgumentOutOfRangeException( nameof( value ), "The Active Opacity must be between 0 and 1." );

            if ( this.ContainsFocus )
            {
                this.TargetOpacity = this._activeOpacity;
            }
        }
    }

    /// <summary> The fade time. </summary>
    private double _fadeTime;

    /// <summary>
    /// Gets or sets the time it takes to fade from 1 to 0 or the other way around.
    /// </summary>
    /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    /// the required range. </exception>
    /// <value> The fade time. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public double FadeTime
    {
        get => this._fadeTime;
        set => this._fadeTime = value > 0d ? value : throw new ArgumentOutOfRangeException( nameof( value ), "The FadeTime must be a positive value." );
    }

    /// <summary> The inactive opacity. </summary>
    private double _inactiveOpacity;

    /// <summary>
    /// Gets or sets the opacity that the form will transition to when the form doesn't have focus.
    /// </summary>
    /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    /// the required range. </exception>
    /// <value> The inactive opacity. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public double InactiveOpacity
    {
        get => this._inactiveOpacity;
        set
        {
            this._inactiveOpacity = value is >= 0d and <= 1d
                ? value
                : throw new ArgumentOutOfRangeException( nameof( value ), "The InactiveOpacity must be between 0 and 1." );

            if ( !this.ContainsFocus && this.WindowState != FormWindowState.Minimized )
            {
                this.TargetOpacity = this._inactiveOpacity;
            }
        }
    }

    /// <summary> The minimized opacity. </summary>
    private double _minimizedOpacity;

    /// <summary>
    /// Gets or sets the opacity that the form will transition to when the form is minimized.
    /// </summary>
    /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    /// the required range. </exception>
    /// <value> The minimized opacity. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public double MinimizedOpacity
    {
        get => this._minimizedOpacity;
        set
        {
            this._minimizedOpacity = value is >= 0d and <= 1d
                ? value
                : throw new ArgumentOutOfRangeException( nameof( value ), "The MinimizedOpacity must be between 0 and 1." );

            if ( !this.ContainsFocus && this.WindowState != FormWindowState.Minimized )
            {
                this.TargetOpacity = this._inactiveOpacity;
            }
        }
    }

    /// <summary> Target opacity. </summary>
    private double _targetOpacity;

    /// <summary> Gets or sets the opacity the form is transitioning to. </summary>
    /// <remarks>
    /// Setting this value amounts also to a one-time fade to any value. The opacity is never
    /// actually 1. This is hack to keep the window from flashing black. The cause of this is not
    /// clear.
    /// </remarks>
    /// <value> The target opacity. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public double TargetOpacity
    {
        get => this._targetOpacity;
        set
        {
            this._targetOpacity = value;
            if ( !this.TimerThis.Enabled )
            {
                this.TimerThis.Start();
            }
        }
    }

    #endregion

    #region " fading control "

    /// <summary> Turns off opacity fading. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public void DisableFade()
    {
        this._activeOpacity = 1d;
        this._inactiveOpacity = 1d;
        this._minimizedOpacity = 1d;
    }

    /// <summary> Turns on opacity fading. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public void EnableFadeDefaults()
    {
        this._activeOpacity = 1d;
        this._inactiveOpacity = 0.85d;
        this._minimizedOpacity = 0d;
        this._fadeTime = 0.35d;
    }

    #endregion

    #region " form event handlers "

    /// <summary>
    /// Gets or sets the sentinel indicating that the form loaded without an exception. Should be set
    /// only if load did not fail.
    /// </summary>
    /// <value> The is loaded. </value>
    protected bool IsLoaded { get; set; }

    /// <summary>
    /// Raises the <see cref="System.Windows.Forms.Form.Load" /> event after reading the start
    /// position from the configuration file.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnLoad( EventArgs e )
    {
        try
        {
            this.Opacity = this.MinimizedOpacity;
            this.TargetOpacity = this.ActiveOpacity;
            this.IsLoaded = true;
        }
        catch
        {
            throw;
        }
        finally
        {
            base.OnLoad( e );
        }
    }

    /// <summary>
    /// Raises the <see cref="System.Windows.Forms.Form.Activated" /> event. Fades in the form when
    /// it gains focus.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnActivated( EventArgs e )
    {
        this.TargetOpacity = this.ActiveOpacity;
        base.OnActivated( e );
    }

    /// <summary>
    /// Raises the <see cref="System.Windows.Forms.Form.Deactivate" /> event. Fades out the form
    /// when it losses focus.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> The <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnDeactivate( EventArgs e )
    {
        this.TargetOpacity = this.InactiveOpacity;
        base.OnDeactivate( e );
    }

    #endregion

    #region " opacity timer "

    /// <summary> Timer to aid in fade effects. </summary>
    private Timer _timerThis;

    private Timer TimerThis
    {
        [MethodImpl( MethodImplOptions.Synchronized )]
        get => this._timerThis;

        [MethodImpl( MethodImplOptions.Synchronized )]
        set
        {
            if ( this._timerThis is not null )
            {
                this._timerThis.Tick -= this.Timer_Tick;
            }

            this._timerThis = value;
            if ( this._timerThis is not null )
            {
                this._timerThis.Tick += this.Timer_Tick;
            }
        }
    }

    /// <summary> Performs fade increment. </summary>
    /// <remarks>
    /// The timer is stopped after reaching the target opacity. The timer gets started whenever the
    /// <see cref="TargetOpacity">target opacity</see> is set method. The timer is stopped to
    /// conserve processor power when not needed.
    /// </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void Timer_Tick( object? sender, EventArgs e )
    {
        double fadeChangePerTick = this.TimerThis.Interval * 1.0d / 1000d / this._fadeTime;
        // Check to see if it is time to stop the timer
        if ( Math.Abs( this._targetOpacity - this.Opacity ) < fadeChangePerTick )
        {
            // Stop the timer to save processor.
            this.TimerThis.Stop();
            // There is an ugly black flash if you set the Opacity to 1.0
            this.Opacity = this._targetOpacity == 1d ? 0.999d : this.TargetOpacity;
            // Process held Windows Message.
            base.WndProc( ref this.heldMessage );
            this.heldMessage = new Message();
        }
        else if ( this.TargetOpacity > this.Opacity )
        {
            this.Opacity += fadeChangePerTick;
        }
        else if ( this.TargetOpacity < this.Opacity )
        {
            this.Opacity -= fadeChangePerTick;
        }
    }

    #endregion
}
