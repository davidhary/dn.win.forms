using System;
using System.Drawing;

namespace cc.isr.WinForms.UserForms;

/// <summary> A form that persists user settings in the Application Settings file. </summary>
/// <remarks>
/// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License. </para><para>
/// David, 2013-01-31, 6.1.4779 </para>
/// </remarks>
[CLSCompliant( false )]
public partial class UserFormBase : System.Windows.Forms.Form
{
    /// <summary> Specialized default constructor for use only by derived classes. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    protected UserFormBase() : base()
    {
        this.InitializeComponent();
        this.SaveSettingsOnClosing = true;
        this.UserFormInfo = UserFormInfo.CreateInstance( this.Name );
    }

    /// <summary> Initializes the component. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private void InitializeComponent()
    {
        this.SuspendLayout();
        this.AutoScaleDimensions = new SizeF( 7.0f, 17.0f );
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.ClientSize = new Size( 331, 341 );
        this.Font = new Font( SystemFonts.DefaultFont.FontFamily, 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0 );
        this.Icon = Properties.Resources.favicon;
        this.Name = nameof( UserFormBase );
        this.ResumeLayout( false );
    }

}
