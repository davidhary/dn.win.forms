using System;
using System.Diagnostics;
using CommunityToolkit.Mvvm.ComponentModel;
using cc.isr.Json.AppSettings.Models;

namespace cc.isr.WinForms.UserForms;
/// <summary>
/// Reads and writes form start information to a Json configuration file.
/// </summary>
/// <remarks>
/// (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License.</para><para>
/// David, 2009-10-21, 1.0.3581 </para>
/// </remarks>
[CLSCompliant( false )]
public class UserFormInfo : ObservableObject
{
    #region " construction "

    /// <summary>
    /// Constructor that prevents a default instance of this class from being created.
    /// </summary>
    /// <remarks>   2023-04-24. </remarks>
    public UserFormInfo()
    {
    }

    /// <summary>
    /// Constructor that prevents a default instance of this class from being created.
    /// </summary>
    /// <remarks>   2023-05-23. </remarks>
    /// <param name="settingsPath">         Full pathname of the settings file. </param>
    /// <param name="settingsSectionName">  Name of the settings section. </param>
    public UserFormInfo( string settingsPath, string settingsSectionName ) : this()
    {
        this._settingsPath = settingsPath;
        this._settingsSectionName = settingsSectionName;
    }

    #endregion

    #region " singleton "

    /// <summary>
    /// Creates an instance of the <see cref="UserFormInfo"/> after restoring the application context
    /// settings to both the user and all user files.
    /// </summary>
    /// <remarks>   2023-05-15. </remarks>
    /// <param name="uniqueFormName">   Name of the unique form. </param>
    /// <param name="allUsers">         (Optional) True to use the settings file for all users [true]. </param>
    /// <returns>   The new instance. </returns>
    public static UserFormInfo CreateInstance( string uniqueFormName, bool allUsers = true )
    {
        // get assembly files using the .Logging suffix.

        AssemblyFileInfo ai = new( typeof( UserFormInfo ).Assembly, null, ".Settings", ".json" );

        // must copy application context settings here to clear any bad settings files.

        AppSettingsScribe.CopySettings( ai.AppContextAssemblyFilePath!, ai.AllUsersAssemblyFilePath! );
        AppSettingsScribe.CopySettings( ai.AppContextAssemblyFilePath!, ai.ThisUserAssemblyFilePath! );

        string settingsFilePath = allUsers ? ai.AllUsersAssemblyFilePath! : ai.ThisUserAssemblyFilePath!;
        UserFormInfo ti = new( settingsFilePath, uniqueFormName );
        ti.ReadSettings();
        return ti;
    }

    #endregion

    #region " i/o "

    private readonly string _settingsPath = string.Empty;
    private readonly string _settingsSectionName = string.Empty;

    /// <summary>   Reads the settings. </summary>
    /// <remarks>   2023-05-23. </remarks>
    public void ReadSettings()
    {
        AppSettingsScribe.ReadSettings( this._settingsPath, this._settingsSectionName, this );
    }

    /// <summary>   Saves the settings. </summary>
    /// <remarks>   2023-05-23. </remarks>
    public void SaveSettings()
    {
        AppSettingsScribe.ReadSettings( this._settingsPath, this._settingsSectionName, this );
    }

    #endregion

    #region " configuration information "

    private TraceLevel _traceLevel = TraceLevel.Verbose;

    /// <summary>   Gets or sets the trace level. </summary>
    /// <value> The trace level. </value>
    [System.ComponentModel.Description( "Sets the message level" )]
    public TraceLevel TraceLevel
    {
        get => this._traceLevel;
        set => _ = this.SetProperty( ref this._traceLevel, value );
    }

    private bool _enabled = true;

    /// <summary>   Gets or sets a value indicating whether this object is enabled. </summary>
    /// <value> True if enabled, false if not. </value>
    [System.ComponentModel.Description( "True if testing is enabled for this test class" )]
    public bool Enabled
    {
        get => this._enabled;
        set => this.SetProperty( ref this._enabled, value );
    }

    private bool _all = true;

    /// <summary> Gets or sets all. </summary>
    /// <value> all. </value>
    [System.ComponentModel.Description( "True if all testing is enabled for this test class" )]
    public bool All
    {
        get => this._all;
        set => this.SetProperty( ref this._all, value );
    }

    #endregion

    /// <summary>   Gets or sets the start position. </summary>
    /// <value> The start position. </value>
	public System.Windows.Forms.FormStartPosition StartPosition { get; set; }

    /// <summary>   Gets or sets the state of the windows. </summary>
    /// <value> The windows state. </value>
	public System.Windows.Forms.FormWindowState WindowsState { get; set; }

    /// <summary>   Gets or sets the form size. </summary>
    /// <value> The size of the form. </value>
	public System.Drawing.Size Size { get; set; }

    /// <summary>   Gets or sets the location. </summary>
    /// <value> The location. </value>
	public System.Drawing.Point Location { get; set; }

}

