using System;
using System.Diagnostics;
using System.Windows.Forms;
using cc.isr.WinControls.BindingExtensions;

namespace cc.isr.WinForms.UserForms;

public partial class UserFormBase
{
    #region " complete event handlers "

    /// <summary> Takes the binding failed actions. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="binding"> <see cref="object"/> instance of this
    /// <see cref="Control"/> </param>
    /// <param name="e">       Binding complete event information. </param>
    protected virtual void OnBindingFailed( Binding binding, BindingCompleteEventArgs e )
    {
        string activity = string.Empty;
        if ( binding is null || e is null ) return;
        try
        {
            activity = "setting cancel state";
            e.Cancel = e.BindingCompleteState != BindingCompleteState.Success;
            activity = $"binding {e.Binding?.BindingMemberInfo.BindingField}:{e.BindingCompleteContext}:{e.Binding?.BindableComponent}:{e.BindingCompleteState}";
            if ( e.BindingCompleteState == BindingCompleteState.DataError )
            {
                activity = $"data error; {activity}";
                Debug.Assert( !Debugger.IsAttached, $"{activity};. {e.ErrorText}" );
            }
            else if ( e.BindingCompleteState == BindingCompleteState.Exception )
            {
                if ( !string.IsNullOrWhiteSpace( e.ErrorText ) )
                {
                    activity = $"{activity}; {e.ErrorText}";
                }
                Trace.TraceError( $"{activity};. {e.Exception}" );
                Debug.Assert( !Debugger.IsAttached, $"{activity};. {e.Exception}" );
            }
        }
        catch ( Exception ex )
        {
            Trace.TraceError( $"{activity};. {ex}" );
            Debug.Assert( !Debugger.IsAttached, $"{activity};. {ex}" );
        }
    }

    /// <summary> Takes the binding succeeded actions. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="binding"> <see cref="object"/> instance of this
    /// <see cref="Control"/> </param>
    /// <param name="e">       Event information to send to registered event handlers. </param>
    protected virtual void OnBindingSucceeded( Binding binding, BindingCompleteEventArgs e )
    {
    }

    /// <summary> Handles the binding complete event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> <see cref="object"/> instance of this
    /// <see cref="Control"/> </param>
    /// <param name="e">      Binding complete event information. </param>
    private void HandleBindingCompleteEvent( object? sender, BindingCompleteEventArgs e )
    {
        string activity = string.Empty;
        if ( sender is not Binding binding || e is null )
        {
            return;
        }

        try
        {
            if ( e.BindingCompleteState == BindingCompleteState.Success )
            {
                activity = $"handling the binding {e.BindingCompleteState} event";
                this.OnBindingSucceeded( binding, e );
            }
            else if ( e.Exception is InvalidOperationException && binding.Control is not null && binding.Control.InvokeRequired )
            {
                activity = $"attempting to handle binding cross thread exception";
                // try to handle the cross thread situation
                // _ = binding.Control.BeginInvoke( new Action ( () => binding.ReadValue() ) );
                _ = binding.Control.BeginInvoke( new MethodInvoker( binding.ReadValue ) );
            }
            else
            {
                activity = $"handling the binding {e.BindingCompleteState} event";
                this.OnBindingFailed( binding, e );
            }
        }
        catch ( Exception ex )
        {
            Trace.TraceError( $"{activity};. {ex}" );
            Debug.Assert( !Debugger.IsAttached, $"{activity};. {ex}" );
        }
    }

    #endregion

    #region " add and remove "

    /// <summary>
    /// Adds or removes binding from a <see cref="IBindableComponent">bindable component</see>
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="component"> The bindable control. </param>
    /// <param name="add">       True to add; otherwise, remove. </param>
    /// <param name="binding">   The binding. </param>
    /// <returns> A Binding. </returns>
    public Binding AddRemoveBinding( IBindableComponent component, bool add, Binding binding )
    {
        _ = component.AddRemoveBinding( add, binding, this.HandleBindingCompleteEvent );
        return binding;
    }

    /// <summary>
    /// Adds a formatted binding to a <see cref="IBindableComponent">bindable component</see>
    /// </summary>
    /// <remarks> Enabling formatting also enables the binding complete event. </remarks>
    /// <param name="component">    The bindable control. </param>
    /// <param name="add">          True to add; otherwise, remove. </param>
    /// <param name="propertyName"> Name of the property. </param>
    /// <param name="dataSource">   The data source. </param>
    /// <param name="dataMember">   The data member. </param>
    /// <returns> A Binding. </returns>
    public Binding AddRemoveBinding( IBindableComponent component, bool add, string propertyName, object dataSource, string dataMember )
    {
        // must set formatting enabled to true to enable the binding complete event.
        return this.AddRemoveBinding( component, add, new Binding( propertyName, dataSource, dataMember, true )
        {
            ControlUpdateMode = ControlUpdateMode.OnPropertyChanged,
            DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged
        } );
    }

    /// <summary>
    /// Adds a formatted binding to a <see cref="IBindableComponent">bindable component</see>
    /// </summary>
    /// <remarks> Enabling formatting also enables the binding complete event. </remarks>
    /// <param name="component">            The bindable control. </param>
    /// <param name="add">                  True to add; otherwise, remove. </param>
    /// <param name="propertyName">         Name of the property. </param>
    /// <param name="dataSource">           The data source. </param>
    /// <param name="dataMember">           The data member. </param>
    /// <param name="dataSourceUpdateMode"> The data source update mode. </param>
    /// <returns> A Binding. </returns>
    public Binding AddRemoveBinding( IBindableComponent component, bool add, string propertyName, object dataSource, string dataMember, DataSourceUpdateMode dataSourceUpdateMode )
    {
        // must set formatting enabled to true to enable the binding complete event.
        return this.AddRemoveBinding( component, add, new Binding( propertyName, dataSource, dataMember, true, dataSourceUpdateMode ) );
    }

    /// <summary>
    /// Adds a formatted binding to a <see cref="IBindableComponent">bindable component</see>
    /// </summary>
    /// <remarks> Enabling formatting also enables the binding complete event. </remarks>
    /// <param name="component">            The bindable control. </param>
    /// <param name="add">                  True to add; otherwise, remove. </param>
    /// <param name="propertyName">         Name of the property. </param>
    /// <param name="dataSource">           The data source. </param>
    /// <param name="dataMember">           The data member. </param>
    /// <param name="dataSourceUpdateMode"> The data source update mode. </param>
    /// <param name="controlUpdateMode">    The control update mode. </param>
    /// <returns> A Binding. </returns>
    public Binding AddRemoveBinding( IBindableComponent component, bool add, string propertyName, object dataSource, string dataMember, DataSourceUpdateMode dataSourceUpdateMode, ControlUpdateMode controlUpdateMode )
    {
        // must set formatting enabled to true to enable the binding complete event.
        return this.AddRemoveBinding( component, add, new Binding( propertyName, dataSource, dataMember, true, dataSourceUpdateMode ) { ControlUpdateMode = controlUpdateMode } );
    }

    /// <summary>
    /// Adds a formatted binding to a <see cref="IBindableComponent">bindable component</see>
    /// </summary>
    /// <remarks> Enabling formatting also enables the binding complete event. </remarks>
    /// <param name="component">    The bindable control. </param>
    /// <param name="propertyName"> Name of the property. </param>
    /// <param name="dataSource">   The data source. </param>
    /// <param name="dataMember">   The data member. </param>
    /// <returns> A Binding. </returns>
    public Binding AddBinding( IBindableComponent component, string propertyName, object dataSource, string dataMember )
    {
        return this.AddRemoveBinding( component, true, new Binding( propertyName, dataSource, dataMember, true )
        {
            ControlUpdateMode = ControlUpdateMode.OnPropertyChanged,
            DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged
        } );
    }

    /// <summary>
    /// Adds a formatted binding to a <see cref="IBindableComponent">bindable component</see>
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="component"> The bindable control. </param>
    /// <param name="binding">   The binding. </param>
    /// <returns> A Binding. </returns>
    public Binding AddBinding( IBindableComponent component, Binding binding )
    {
        return this.AddRemoveBinding( component, true, binding );
    }

    /// <summary>
    /// removes binding from a <see cref="IBindableComponent">bindable component</see>
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="component">    The bindable component. </param>
    /// <param name="propertyName"> Name of the property. </param>
    /// <param name="dataSource">   The data source. </param>
    /// <param name="dataMember">   The data member. </param>
    /// <returns> A Binding. </returns>
    public Binding RemoveBinding( IBindableComponent component, string propertyName, object dataSource, string dataMember )
    {
        return this.AddRemoveBinding( component, false, new Binding( propertyName, dataSource, dataMember, true ) );
    }

    /// <summary>
    /// removes binding from a <see cref="IBindableComponent">bindable component</see>
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="component"> The bindable component. </param>
    /// <param name="binding">   The binding. </param>
    /// <returns> A Binding. </returns>
    public Binding RemoveBinding( IBindableComponent component, Binding binding )
    {
        return this.AddRemoveBinding( component, false, binding );
    }

    #endregion

    #region " convert event handlers "

    /// <summary> Displays universal time value as local time. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> <see cref="object"/> instance of this
    /// <see cref="Control"/> </param>
    /// <param name="e">      Convert event information. </param>
    protected void DisplayLocalTime( object? sender, ConvertEventArgs e )
    {
        e.DisplayLocalTime();
    }

    /// <summary> Parse a local time string to universal time. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> <see cref="object"/> instance of this
    /// <see cref="Control"/> </param>
    /// <param name="e">      Convert event information. </param>
    protected void ParseLocalTime( object? sender, ConvertEventArgs e )
    {
        e.ParseLocalTime();
    }

    #endregion
}
