namespace cc.isr.WinForms.Demo.UI;

public partial class RichTextEditorForm : Form
{
    public RichTextEditorForm()
    {
        this.InitializeComponent();
        this.RichTextEditor.ExitRequested += new EventHandler( this.Editor_ExitRequested );
    }

    private void Editor_ExitRequested( object? sender, EventArgs e )
    {
        this.Close();
    }

}
