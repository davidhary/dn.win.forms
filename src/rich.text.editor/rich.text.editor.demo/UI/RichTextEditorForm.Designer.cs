﻿
namespace cc.isr.WinForms.Demo.UI
{
    partial class RichTextEditorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing )
            {
                components?.Dispose();
            }
            base.Dispose( disposing );
        }

        #region " windows form designer generated code "

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RichTextEditor = new cc.isr.WinControls.RichTextEditControl();
            this.SuspendLayout();
            // 
            // RichTextEditor
            // 
            this.RichTextEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RichTextEditor.Location = new System.Drawing.Point(0, 0);
            this.RichTextEditor.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.RichTextEditor.Name = "RichTextEditor";
            this.RichTextEditor.Size = new System.Drawing.Size(800, 450);
            this.RichTextEditor.TabIndex = 0;
            // 
            // RichTextEditorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.RichTextEditor);
            this.Name = "RichTextEditorForm";
            this.Text = "Rich Text Editor";
            this.ResumeLayout(false);

        }

        #endregion

        private WinControls.RichTextEditControl RichTextEditor;
    }
}