namespace cc.isr.WinForms.Demo;

internal static class Program
{
    /// <summary>
    ///  The main entry point for the application.
    /// </summary>
    [STAThread]
    private static void Main()
    {
        _ = Application.SetHighDpiMode( HighDpiMode.SystemAware );
        Application.EnableVisualStyles();
        Application.SetCompatibleTextRenderingDefault( false );
        Application.Run( new UI.RichTextEditorForm() );
    }

    /// <summary>   Creates a form. This requires having an Json settings file for logging. </summary>
    /// <remarks>   David, 2021-03-16. </remarks>
    /// <returns>   The new form. </returns>
    public static ModelViewForm.ModelViewForm CreateForm()
    {
        ModelViewForm.ModelViewForm? result = null;
        try
        {
            result = new ModelViewForm.ModelViewForm() { Text = "Rich Text Editor" };
            result.AddUserControl( "Editor", new WinControls.RichTextEditControl(), true );
        }
        catch
        {
            result?.Dispose();

            throw;
        }

        return result;
    }

    /// <summary> Creates a form. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="caption">        The caption. </param>
    /// <param name="tabTitle">       The tab title. </param>
    /// <param name="richTextEditor"> The rich text editor. </param>
    /// <param name="logListener">    The log listener. </param>
    /// <returns> The new form. </returns>
    public static ModelViewForm.ModelViewForm CreateForm( string caption, string tabTitle, UserControl richTextEditor )
    {
        ModelViewForm.ModelViewForm? result = null;
        try
        {
            result = new ModelViewForm.ModelViewForm() { Text = caption };
            result.AddUserControl( tabTitle, richTextEditor, true );
        }
        catch
        {
            result?.Dispose();
            throw;
        }

        return result;
    }

}
