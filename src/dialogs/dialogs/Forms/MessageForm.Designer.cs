using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace cc.isr.WinForms.Dialogs;

public partial class MessageForm
{
    #region "windows form designer generated code "

    // Required by the Windows Form Designer
    private System.ComponentModel.IContainer components;
    // NOTE: The following procedure is required by the Windows Form Designer
    // It can be modified using the Windows Form Designer.
    // Do not modify it using the code editor.
    [DebuggerStepThrough()]
    private void InitializeComponent()
    {
        var resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageForm));
        _richTextBox = new System.Windows.Forms.RichTextBox();
        _statusStrip = new System.Windows.Forms.StatusStrip();
        _customButton = new System.Windows.Forms.ToolStripDropDownButton();
        _customButton.Click += new EventHandler(CustomButton_Click);
        _statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
        _progressBar = new System.Windows.Forms.ToolStripProgressBar();
        this._statusStrip.SuspendLayout();
        SuspendLayout();
        // 
        // _richTextBox
        // 
        _richTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
        _richTextBox.Location = new System.Drawing.Point(0, 0);
        _richTextBox.Name = "_RichTextBox";
        _richTextBox.ReadOnly = true;
        _richTextBox.Size = new System.Drawing.Size(386, 133);
        _richTextBox.TabIndex = 0;
        _richTextBox.Text = string.Empty;
        _richTextBox.WordWrap = false;
        // 
        // StatusStrip1
        // 
        _statusStrip.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
        _statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _statusLabel, _progressBar, _customButton });
        _statusStrip.Location = new System.Drawing.Point(0, 133);
        _statusStrip.Name = "StatusStrip1";
        _statusStrip.Size = new System.Drawing.Size(386, 23);
        _statusStrip.TabIndex = 2;
        _statusStrip.Text = "StatusStrip1";
        // 
        // _customButton
        // 
        _customButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
        _customButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
        _customButton.Image = (System.Drawing.Image)resources.GetObject("_CustomButton.Image");
        _customButton.ImageTransparentColor = System.Drawing.Color.Magenta;
        _customButton.Name = "_CustomButton";
        _customButton.ShowDropDownArrow = false;
        _customButton.Size = new System.Drawing.Size(45, 21);
        _customButton.Text = "Abort";
        // 
        // _statusLabel
        // 
        _statusLabel.Name = "_StatusLabel";
        _statusLabel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
        _statusLabel.Size = new System.Drawing.Size(193, 18);
        _statusLabel.Spring = true;
        _statusLabel.Text = "Status";
        // 
        // _progressBar
        // 
        _progressBar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
        _progressBar.Name = "_ProgressBar";
        _progressBar.RightToLeft = System.Windows.Forms.RightToLeft.No;
        _progressBar.Size = new System.Drawing.Size(100, 17);
        // 
        // MessageForm
        // 
        AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
        ClientSize = new System.Drawing.Size(386, 156);
        Controls.Add(_richTextBox);
        Controls.Add(_statusStrip);
        Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
        MaximizeBox = false;
        MinimizeBox = false;
        Name = "MessageForm";
        Text = "Status";
        _statusStrip.ResumeLayout(false);
        _statusStrip.PerformLayout();
        ResumeLayout(false);
        PerformLayout();
    }

    private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
    private System.Windows.Forms.ToolStripDropDownButton _customButton;
    private System.Windows.Forms.ToolStripProgressBar _progressBar;
    private System.Windows.Forms.RichTextBox _richTextBox;
    private System.Windows.Forms.StatusStrip _statusStrip;

    #endregion
}
