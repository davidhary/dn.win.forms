using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;

namespace cc.isr.WinForms.Dialogs;

/// <summary> Keeps dialog form on top. </summary>
/// <remarks>
/// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License. </para><para>
/// David, 2013-01-31, 6.1.4779 </para>
/// </remarks>
public class TopDialogBase : System.Windows.Forms.Form
{
    #region " windows form designer generated code "

    /// <summary> Specialized default constructor for use only by derived classes. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    protected TopDialogBase() : base() =>

        // This call is required by the Windows Form Designer.
        this.InitializeComponent();// Add any initialization after the InitializeComponent() call

    /// <summary>
    /// Releases the unmanaged resources used by the <see cref="System.Windows.Forms.Control" />
    /// and its child controls and optionally releases the managed resources.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="disposing"> true to release both managed and unmanaged resources; false to
    /// release only unmanaged resources. </param>
    [DebuggerNonUserCode()]
    protected override void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this._components?.Dispose();
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }

    private System.ComponentModel.Container _components;

    private System.Windows.Forms.Timer _onTopTimer;

    private System.Windows.Forms.Timer OnTopTimer
    {
        [MethodImpl( MethodImplOptions.Synchronized )]
        get => this._onTopTimer;

        [MethodImpl( MethodImplOptions.Synchronized )]
        set
        {
            if ( this._onTopTimer is not null )
            {
                this._onTopTimer.Tick -= this.OnTopTimer_Tick;
            }

            this._onTopTimer = value;
            if ( this._onTopTimer is not null )
            {
                this._onTopTimer.Tick += this.OnTopTimer_Tick;
            }
        }
    }

    // NOTE: The following procedure is required by the Windows Form Designer
    // It can be modified using the Windows Form Designer.  
    // Do not modify it using the code editor.
    [DebuggerStepThrough()]
    [MemberNotNull( nameof( _components ) )]
    [MemberNotNull( nameof( _onTopTimer ) )]
    private void InitializeComponent()
    {
        this._components = new System.ComponentModel.Container();
        this._onTopTimer = new System.Windows.Forms.Timer( this._components );
        this._onTopTimer.Tick += new EventHandler( this.OnTopTimer_Tick );
        this.SuspendLayout();
        // 
        // _onTopTimer
        // 
        this._onTopTimer.Interval = 1000;
        // 
        // TopDialogBase
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF( 7.0f, 17.0f );
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.ClientSize = new System.Drawing.Size( 331, 343 );
        this.Font = new System.Drawing.Font( System.Drawing.SystemFonts.DefaultFont.FontFamily, 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0 );
        this.Icon = Properties.Resources.favicon;
        this.Name = "TopDialogBase";
        this.Text = "Top Dialog Base";
        this.ResumeLayout( false );
    }

    #endregion

    /// <summary>
    /// Gets or sets the sentinel indicating that the form loaded without an exception. Should be set
    /// only if load did not fail.
    /// </summary>
    /// <value> The is loaded. </value>
    protected bool IsLoaded { get; set; }

    /// <summary> Raises the <see cref="System.Windows.Forms.Form.OnLoad" /> event . </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnLoad( EventArgs e )
    {
        try
        {
            this.IsLoaded = true;
        }
        catch
        {
            throw;
        }
        finally
        {
            base.OnLoad( e );
        }
    }

    /// <summary>
    /// Raises the <see cref="System.Windows.Forms.Form.Shown" /> event after enabling the top most
    /// timer.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnShown( EventArgs e )
    {
        this.OnTopTimer.Enabled = true;
        base.OnShown( e );
    }

    /// <summary>
    /// Raises the <see cref="System.Windows.Forms.Form.Closing" /> event after Disabling the top
    /// most timer if not canceled.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="System.ComponentModel.CancelEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnClosing( System.ComponentModel.CancelEventArgs e )
    {
        if ( e is null || !e.Cancel )
        {
            this.OnTopTimer.Enabled = false;
        }
        if ( e is not null )
            base.OnClosing( e );
    }

    /// <summary>
    /// Raises the <see cref="System.Windows.Forms.Form.Closed" /> event after disabling the top
    /// most timer.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> The <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnClosed( EventArgs e )
    {
        if ( this.OnTopTimer is not null )
        {
            this.OnTopTimer.Enabled = false;
        }
        if ( e is not null )
            base.OnClosed( e );
    }

    /// <summary>
    /// Handles the Tick event of the <see cref="OnTopTimer"/> control. 
    /// Sets <see cref="System.Windows.Forms.Form.TopMost"></see> true if not already true.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> The source of the event. </param>
    /// <param name="e">      The <see cref="EventArgs" /> instance containing the event data. </param>
    private void OnTopTimer_Tick( object? sender, EventArgs e )
    {
        if ( sender is not null )
        {
            if ( !this.TopMost )
            {
                this.TopMost = true;
            }
        }
    }
}
