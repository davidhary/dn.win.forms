using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ComponentModel;

namespace cc.isr.WinForms.Dialogs;

/// <summary> Form for displaying a message. </summary>
/// <remarks>
/// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2014-09-17 </para>
/// </remarks>
public sealed partial class MessageForm : FormBase
{
    #region " construction and cleanup "

    /// <summary>
    /// Prevents a default instance of the <see cref="MessageForm" /> class from being created.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private MessageForm() : base() =>

        // This method is required by the Windows Form Designer.
        this.InitializeComponent();

    /// <summary>
    /// Releases the unmanaged resources used by the <see cref="Control" />
    /// and its child controls and optionally releases the managed resources.
    /// </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <param name="disposing">    true to release both managed and unmanaged resources; false to
    ///                             release only unmanaged resources. </param>
    [DebuggerNonUserCode()]
    protected override void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this.components?.Dispose();
                this.components = null;
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }

    #endregion

    #region " singleton "

    /// <summary>   Gets the instance. </summary>
    /// <value> The instance. </value>
    public static MessageForm Instance => _instance.Value;

    private static readonly Lazy<MessageForm> _instance = new( () => new(), true );

    /// <summary> Instantiates the class. </summary>
    /// <remarks> Use this property to instantiate a single instance of this class. This class uses
    /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    /// <returns> A new or existing instance of the class. </returns>
    public static MessageForm Get()
    {
        return Instance;
    }

    /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
    /// <value> <c>true</c> if instantiated; otherwise, <c>false</c>. </value>
    internal static bool Instantiated => _instance.IsValueCreated;

    /// <summary> Dispose instance. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public static void DisposeInstance()
    {
        if ( _instance.IsValueCreated && !(Instance?.IsDisposed ?? true) )
        {
            Instance?.Dispose();
        }
    }

    #endregion

    #region " form events "

    /// <summary> Raises the <see cref="Form.Shown" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnShown( EventArgs e )
    {
        base.OnShown( e );
    }

    /// <summary> Displays this dialog with title 'illegal call'. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> One of the <see cref="DialogResult" /> values. </returns>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
    private new DialogResult ShowDialog()
    {
        this.Text = "Illegal Call";
        return base.ShowDialog();
    }

    /// <summary> Shows the message box with 'illegal call' in the caption. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
    private new void Show()
    {
        this.Status = string.Empty;
        this.Text = "Illegal Call";
        this.RichTextBox.Text = "Illegal Call";
        this.DialogResult = DialogResult.None;
        base.Show();
    }

    /// <summary> Shows the message box with these messages. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="mdiForm"> The MDI form. </param>
    /// <param name="caption"> The caption. </param>
    /// <param name="details"> The details. </param>
    public void Show( Form mdiForm, string caption, string details )
    {
        if ( mdiForm is not null && mdiForm.IsMdiContainer )
        {
            this.MdiParent = mdiForm;
            mdiForm.Show();
        }

        this.ProgressBar.Visible = false;
        this.Status = string.Empty;
        this.Text = caption;
        this.RichTextBox.Text = details;
        this.DialogResult = DialogResult.None;
        Application.DoEvents();
        base.Show();
        Application.DoEvents();
    }

    /// <summary> Shows the message box with these messages. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="mdiForm">  The MDI form. </param>
    /// <param name="caption">  The caption. </param>
    /// <param name="details">  The details. </param>
    /// <param name="duration"> The duration. </param>
    public void Show( Form mdiForm, string caption, string details, TimeSpan duration )
    {
        this.Show( mdiForm, caption, details );
        Stopwatch sw = Stopwatch.StartNew();
        this.ProgressBar.Value = 100;
        this.ProgressBar.Visible = true;
        while ( sw.Elapsed <= duration )
        {
            Application.DoEvents();
            int value = ( int ) (100d * (1d - (sw.Elapsed.Ticks / ( double ) duration.Ticks)));
            value = value < 0 ? 0 : value > 100 ? 100 : value;
            this.ProgressBar.Value = value;
            Application.DoEvents();
        }

        this.ProgressBar.Visible = false;
        this.Close();
    }

    /// <summary>
    /// Shows the message box with these messages until the task completes or times out.
    /// </summary>
    /// <remarks>   David, 2021-08-10. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="mdiForm">  The MDI form. </param>
    /// <param name="caption">  The caption. </param>
    /// <param name="details">  The details. </param>
    /// <param name="duration"> The duration. </param>
    /// <param name="task">     The task. </param>
    public void Show( Form mdiForm, string caption, string details, TimeSpan duration, Task task )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( task, nameof( task ) );
#else
        if ( task is null ) throw new ArgumentNullException( nameof( task ) );
#endif

        this.Show( mdiForm, caption, details );
        _ = task.Wait( duration );
    }

    /// <summary> Gets the rich text box. </summary>
    /// <value> The rich text box. </value>
    [System.ComponentModel.Browsable( false )]
    [System.ComponentModel.DesignerSerializationVisibility( System.ComponentModel.DesignerSerializationVisibility.Hidden )]
    public RichTextBox RichTextBox => this._richTextBox;

    /// <summary> Gets or sets the status. </summary>
    /// <value> The status. </value>
    [System.ComponentModel.Browsable( false )]
    [System.ComponentModel.DesignerSerializationVisibility( System.ComponentModel.DesignerSerializationVisibility.Hidden )]
    public string Status
    {
        get => this._statusLabel.Text ?? string.Empty;
        set
        {
            this._statusLabel.Text = WinForms.Dialogs.CompactExtensions.CompactExtensionMethods.Compact( value, this._statusLabel );
            this._statusLabel.ToolTipText = value;
            Application.DoEvents();
        }
    }

    /// <summary> Gets the progress. </summary>
    /// <value> The progress. </value>
    [System.ComponentModel.Browsable( false )]
    [System.ComponentModel.DesignerSerializationVisibility( System.ComponentModel.DesignerSerializationVisibility.Hidden )]
    public ToolStripProgressBar ProgressBar => this._progressBar;

    /// <summary> Gets or sets the custom button text. </summary>
    /// <value> The custom button text. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string CustomButtonText
    {
        get => this._customButton?.Text ?? string.Empty;
        set
        {
            this._customButton.Text = value;
            Application.DoEvents();
        }
    }

    /// <summary> Custom button click. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void CustomButton_Click( object? sender, EventArgs e )
    {
        this.DialogResult = DialogResult.OK;
        Application.DoEvents();
        this.Status = $"{this._customButton.Text} requested";
    }

    #endregion
}
