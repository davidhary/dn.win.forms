namespace cc.isr.WinForms.Dialogs;

/// <summary> Dialog for selecting a file to open. </summary>
/// <remarks> David, 2019-01-22. </remarks>
public class OpenFileDialog : FileDialogBase
{
    /// <summary> Constructs this class. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    public OpenFileDialog() : base()
    {
    }

    /// <summary> Opens the File Open dialog box and selects a file name. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <returns> A Windows.Forms.DialogResult. </returns>
    public virtual bool TrySelectFile()
    {
        using System.Windows.Forms.OpenFileDialog fileDialog = new()
        {
            // Use the common dialog box
            CheckFileExists = true,
            CheckPathExists = true,
            Title = this.FileDialogTitle,
            FileName = this.FullFileName
        };
        System.IO.FileInfo fi = new( this.FullFileName );
        if ( fi.Directory?.Exists ?? false )
        {
            fileDialog.InitialDirectory = fi.DirectoryName;
        }

        fileDialog.RestoreDirectory = true;
        fileDialog.Filter = this.FileDialogFilter;
        fileDialog.DefaultExt = this.FileExtension;
        bool result = System.Windows.Forms.DialogResult.OK == fileDialog.ShowDialog();
        if ( result )
        {
            this.FullFileName = fileDialog.FileName;
        }

        return result;
    }

    /// <summary> Try find file. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="fileName"> Filename of the file. </param>
    /// <returns> A <see cref="string" /> if file found or empty if not. </returns>
    public static string TryFindFile( string fileName )
    {
        OpenFileDialog dialog = new() { FullFileName = fileName };
        if ( !dialog.FileExists() )
        {
            fileName = dialog.TrySelectFile() ? dialog.FullFileName : string.Empty;
        }

        return fileName;
    }

}
