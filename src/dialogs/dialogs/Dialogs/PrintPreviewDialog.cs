using System;
using System.Windows.Forms;

namespace cc.isr.WinForms.Dialogs;

/// <summary> Dialog for print preview and print. </summary>
/// <remarks> David, 2020-09-10. </remarks>
public class PrintPreviewDialog : FileDialogBase
{
    /// <summary> Constructs this class. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    public PrintPreviewDialog() : base()
    {
    }

    /// <summary> Opens the print preview dialog. </summary>
    /// <remarks> David, 2020-09-10. </remarks>
    /// <param name="document"> The document. </param>
    public virtual void ShowDialog( System.Drawing.Printing.PrintDocument document )
    {
        string activity = string.Empty;
        try
        {
            activity = "preparing print preview dialog";
            using MyPrintPreviewDialog ppd = new() { Document = document };
            activity = "showing print preview dialog";
            _ = ppd.ShowDialog();
        }
        catch ( Exception ex )
        {
            _ = MessageBox.Show( $"Exception {activity};. {ex}", "Print Dialog Exception", MessageBoxButtons.OK, MessageBoxIcon.Error );
        }
    }

}
