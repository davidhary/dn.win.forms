using System;

namespace cc.isr.WinForms.Dialogs;

/// <summary>   A file dialog base. </summary>
/// <remarks>   David, 202-09-12. </remarks>
public class FileDialogBase
{
    #region " construction and cleanup "

    /// <summary> Constructs this class. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    public FileDialogBase() : base() =>
        // set default file name if empty.
        this._fullFileName = System.IO.Path.Combine( DefaultFolderPath(), System.Reflection.Assembly.GetCallingAssembly().GetName().Name + this.FileExtension );

    #endregion 

    #region " dialog information "

    /// <summary> Gets or sets the file dialog filter. </summary>
    /// <value> The file dialog filter. </value>
    public string? FileDialogFilter { get; set; }

    /// <summary> Gets or sets the file dialog title. </summary>
    /// <value> The file dialog title. </value>
    public string? FileDialogTitle { get; set; }

    #endregion 

    #region " file information "

    /// <summary> Gets or sets the filename of the file. </summary>
    /// <value> The name of the file. </value>
    public string? FileName { get; set; }

    /// <summary> Queries if a given file exists. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    public bool FileExists()
    {
        System.IO.FileInfo fi = new( this.FullFileName );
        return fi.Exists;
    }

    /// <summary> Gets or sets the file extension. </summary>
    /// <value> The file extension. </value>
    public string? FileExtension { get; set; }

    /// <summary> Full pathname of the file. </summary>
    private string _fullFileName;

    /// <summary> Gets or sets the file name. </summary>
    /// <remarks> Use this property to get or set the file name. </remarks>
    /// <value> <c>FullFileName</c> is a String property. </value>
    public string FullFileName
    {
        get
        {
            if ( string.IsNullOrEmpty( this._fullFileName ) )
            {
                // set default file name if empty.
                this._fullFileName = System.IO.Path.Combine( DefaultFolderPath(), System.Reflection.Assembly.GetCallingAssembly().GetName().Name + this.FileExtension );
            }
            return this._fullFileName!;
        }
        set => this._fullFileName = value;
    }

    #endregion

    #region " io "

    /// <summary> Determines whether the specified folder path is writable. </summary>
    /// <remarks>
    /// Uses a temporary random file name to test if the file can be created. The file is deleted
    /// thereafter.
    /// </remarks>
    /// <param name="path"> The path. </param>
    /// <returns> <c>true</c> if the specified path is writable; otherwise, <c>false</c>. </returns>
    public static bool IsFolderWritable( string path )
    {
        string filePath = string.Empty;
        bool affirmative = false;
        try
        {
            filePath = System.IO.Path.Combine( path, System.IO.Path.GetRandomFileName() );
            using ( System.IO.FileStream s = System.IO.File.Open( filePath, System.IO.FileMode.OpenOrCreate ) )
            {
            }

            affirmative = true;
        }
        catch
        {
        }
        finally
        {
            // SS reported an exception from this test possibly indicating that Windows allowed writing the file 
            // by failed report deletion. Or else, Windows raised another exception type.
            try
            {
                if ( System.IO.File.Exists( filePath ) )
                {
                    System.IO.File.Delete( filePath );
                }
            }
            catch
            {
            }
        }

        return affirmative;
    }

    /// <summary> Selects the default file path. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <returns>
    /// The default file path: either the application folder or the user documents folder.
    /// </returns>
    public static string DefaultFolderPath()
    {
        string candidatePath = AppContext.BaseDirectory;
        if ( !IsFolderWritable( candidatePath ) )
        {
            candidatePath = Environment.GetFolderPath( Environment.SpecialFolder.MyDocuments );
        }

        return candidatePath;
    }

    #endregion 

}
