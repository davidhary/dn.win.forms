using System;
using System.Windows.Forms;

namespace cc.isr.WinForms.Dialogs;

/// <summary> Dialog for setting the print preview. </summary>
/// <remarks>
/// David, 2020-09-10.
/// https://StackOverflow.com/questions/40236241/how-to-add-print-dialog-to-the-printpreviewdialog.
/// </remarks>
public class MyPrintPreviewDialog : System.Windows.Forms.PrintPreviewDialog

{
    /// <summary>
    /// Initializes a new instance of the <see cref="PrintPreviewDialog" />
    /// class.
    /// </summary>
    /// <remarks> David, 2020-09-08. </remarks>
    public MyPrintPreviewDialog() : base() => this.ReplacePrintDialogButton();

    /// <summary> Replace print dialog button. </summary>
    /// <remarks> David, 2020-09-08. </remarks>
    private void ReplacePrintDialogButton()
    {
        if ( this.Controls[1] is ToolStrip printToolStrip && printToolStrip is not null )
        {
            ToolStripButton b = new() { DisplayStyle = ToolStripItemDisplayStyle.Image };
            b.Click += this.PrintDialog_PrintClick;
            if ( printToolStrip is not null )
            {
                b.Image = printToolStrip.ImageList?.Images[0];
                printToolStrip.Items.RemoveAt( 0 );
                printToolStrip.Items.Insert( 0, b );
            }
        }
    }

    /// <summary> Event handler. Called by PrintDialog for print click events. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void PrintDialog_PrintClick( object? sender, EventArgs e )
    {
        if ( this.Document is null ) return;
        string activity = string.Empty;
        try
        {
            activity = "instantiating the print dialog";
            using PrintDialog pd = new() { Document = this.Document };
            activity = "showing the print dialog";
            if ( pd.ShowDialog() == DialogResult.OK )
            {
                activity = "setting printer settings";
                this.Document.PrinterSettings = pd.PrinterSettings;
                activity = "printing";
                this.Document.Print();
            }
        }
        catch ( Exception ex )
        {
            _ = MessageBox.Show( $"Exception {activity};. {ex}", "Print Dialog Exception", MessageBoxButtons.OK, MessageBoxIcon.Error );
        }
    }
}
