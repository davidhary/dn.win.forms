using System.Collections.Generic;
using System.Windows.Forms;

namespace cc.isr.WinForms.Dialogs;

/// <summary> my text input box. </summary>
/// <remarks> David, 2019-09-13. </remarks>
public sealed class MyTextInputBox
{
    #region " construction and cleanup "

    /// <summary>
    /// Constructor that prevents a default instance of this class from being created.
    /// </summary>
    /// <remarks> David, 202-09-12. </remarks>
    private MyTextInputBox() : base()
    {
    }

    #endregion 

    #region " static "

    /// <summary> Enter dialog. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="caption"> The caption. </param>
    /// <returns> A (DialogResult As MyDialogResult, DialogValue As String) </returns>
    public static (MyDialogResult DialogResult, string DialogValue) EnterDialog( string caption )
    {
        (DialogResult DialogResult, string DialogValue) result = TextInputBox.EnterDialog( caption );
        return (result.DialogResult.ToDialogResult(), result.DialogValue);
    }

    /// <summary> Enter dialog. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="caption">               The caption. </param>
    /// <param name="useSystemPasswordChar"> True to use system password character. </param>
    /// <returns> A (DialogResult As MyDialogResult, DialogValue As String) </returns>
    public static (MyDialogResult DialogResult, string DialogValue) EnterDialog( string caption, bool useSystemPasswordChar )
    {
        (DialogResult DialogResult, string DialogValue) result = TextInputBox.EnterDialog( caption, useSystemPasswordChar );
        return (result.DialogResult.ToDialogResult(), result.DialogValue);
    }

    /// <summary> Enter dialog. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="caption">               The caption. </param>
    /// <param name="prompt">                The prompt. </param>
    /// <param name="defaultValue">          The default value. </param>
    /// <param name="useSystemPasswordChar"> True to use system password character. </param>
    /// <returns> A (DialogResult As MyDialogResult, DialogValue As String) </returns>
    public static (MyDialogResult DialogResult, string DialogValue) EnterDialog( string caption, string prompt, string defaultValue, bool useSystemPasswordChar )
    {
        (DialogResult DialogResult, string DialogValue) result = TextInputBox.EnterDialog( caption, prompt, defaultValue, useSystemPasswordChar );
        return (result.DialogResult.ToDialogResult(), result.DialogValue);
    }

    #endregion 

}
/// <summary> Values that represent dialog results. </summary>
/// <remarks> David, 202-09-12. </remarks>
public enum MyDialogResult
{
    /// <summary> An enum constant representing the none option. </summary>
    None = System.Windows.Forms.DialogResult.None,

    /// <summary> An enum constant representing the okay option. </summary>
    Ok = System.Windows.Forms.DialogResult.OK,

    /// <summary> An enum constant representing the cancel option. </summary>
    Cancel = System.Windows.Forms.DialogResult.Cancel,

    /// <summary> An enum constant representing the abort option. </summary>
    Abort = System.Windows.Forms.DialogResult.Abort,

    /// <summary> An enum constant representing the retry option. </summary>
    Retry = System.Windows.Forms.DialogResult.Retry,

    /// <summary> An enum constant representing the ignore option. </summary>
    Ignore = System.Windows.Forms.DialogResult.Ignore,

    /// <summary> An enum constant representing the yes option. </summary>
    Yes = System.Windows.Forms.DialogResult.Yes,

    /// <summary> An enum constant representing the no option. </summary>
    No = System.Windows.Forms.DialogResult.No
}
/// <summary>   A dialog extension methods. </summary>
/// <remarks>   David, 2021-03-11. </remarks>
internal static class DialogExtensionMethods
{
    /// <summary>   Converts a value to a dialog result. </summary>
    /// <remarks>   David, 2021-03-11. </remarks>
    /// <param name="value">    The value. </param>
    /// <returns>   Value as a DialogResult. </returns>
    internal static DialogResult ToDialogResult( this MyDialogResult value )
    {
        return ( DialogResult ) ( int ) value;
    }

    /// <summary>
    /// A MyDialogResult[] extension method that converts the values to a dialog results.
    /// </summary>
    /// <remarks>   David, 2021-03-11. </remarks>
    /// <param name="values">   The values to act on. </param>
    /// <returns>   Values as a DialogResult[]. </returns>
    internal static DialogResult[] ToDialogResults( this MyDialogResult[] values )
    {
        List<DialogResult> results = [];
        foreach ( MyDialogResult dr in values )
        {
            results.Add( dr.ToDialogResult() );
        }
        return [.. results];
    }

    /// <summary>   Converts a value to a dialog result. </summary>
    /// <remarks>   David, 202-09-12. </remarks>
    /// <param name="value">    The value. </param>
    /// <returns>   Value as a DialogResult. </returns>
    internal static MyDialogResult ToDialogResult( this DialogResult value )
    {
        return ( MyDialogResult ) ( int ) value;
    }

}
