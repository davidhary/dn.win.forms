using System;
using System.Windows.Forms;
using System.ComponentModel;

namespace cc.isr.WinForms.Dialogs;

/// <summary> A data entry form for text with a numeric validator. </summary>
/// <remarks> David, 2006-02-20. </remarks>
public partial class TextInputBox : FormBase
{
    #region " construction and cleanup "

    /// <summary> Initializes a new instance of this class. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    public TextInputBox() : base()
    {
        // This call is required by the Windows Form Designer.
        this.InitializeComponent();

        // Add any initialization after the InitializeComponent() call
        // this.ClassStyle = ClassStyleConstants.DropShadow;

        this.NumberStyle = System.Globalization.NumberStyles.None;
        this._enteredValueTextBox.Name = "_EnteredValueTextBox";
    }

    /// <summary>
    /// Disposes of the resources (other than memory) used by the
    /// <see cref="Form" />.
    /// </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="disposing"> true to release both managed and unmanaged resources; false to
    /// release only unmanaged resources. </param>
    protected override void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this.components?.Dispose();
                this.components = null;
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }

    #endregion 

    #region " properties "

    private readonly char _multilinePasswordCharacter = '*';

    /// <summary> Gets or sets the use system password character. </summary>
    /// <value> The use system password character. </value>
    /// <summary> Gets or sets the use system password character. </summary>
    /// <value> The use system password character. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public bool UseSystemPasswordChar
    {
        get => this._enteredValueTextBox.UseSystemPasswordChar;
        set
        {
            this._enteredValueTextBox.UseSystemPasswordChar = value;
            this._enteredValueTextBox.Multiline = !value;
            this._enteredValueTextBox.PasswordChar = this._enteredValueTextBox.Multiline ? this._multilinePasswordCharacter : '*';
        }
    }

    /// <summary> Gets or sets the prompt. </summary>
    /// <value> The prompt. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string Prompt
    {
        get => this._enteredValueTextBoxLabel.Text;
        set => this._enteredValueTextBoxLabel.Text = value;
    }

    /// <summary> Returns the entered value. </summary>
    /// <value> The entered value. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string EnteredValue
    {
        get => this._enteredValueTextBox.Text;
        set => this._enteredValueTextBox.Text = value;
    }

    /// <summary> Gets or sets the default value. </summary>
    /// <value> The default value. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string DefaultValue { get; set; } = string.Empty;

    /// <summary> Gets or sets the number style requested. </summary>
    /// <value> The total number of style. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public System.Globalization.NumberStyles NumberStyle { get; set; }

    #endregion

    #region " form and control event handlers "

    /// <summary> Raises the <see cref="Form.Shown" /> event. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="e"> A <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnShown( EventArgs e )
    {
        try
        {
            _ = this._enteredValueTextBox.Focus();
        }
        catch
        {
        }
        finally
        {
            base.OnShown( e );
        }
    }

    /// <summary>
    /// Closes and returns the <see cref="DialogResult.OK">OK</see>
    /// dialog result.
    /// </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void AcceptButtonClick( object? sender, EventArgs e )
    {
        this.DialogResult = System.Windows.Forms.DialogResult.OK;
        this.Close();
    }

    /// <summary>
    /// Closes and returns the <see cref="DialogResult.Cancel">Cancel</see>
    /// dialog result.
    /// </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void CancelButtonClick( object? sender, EventArgs e )
    {
        this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        this.Close();
    }

    /// <summary> Event handler. Called by _enteredValueTextBox for text changed events. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void EnteredValueTextBoxTextChanged( object? sender, EventArgs e )
    {
        if ( this.NumberStyle == System.Globalization.NumberStyles.None )
        {
            this._acceptButton.Enabled = !string.IsNullOrEmpty( this._enteredValueTextBox.Text );
        }
    }

    /// <summary> Validates the entered value. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Cancel event information. </param>
    private void EnteredValueTextBoxValidating( object? sender, System.ComponentModel.CancelEventArgs e )
    {
        this._acceptButton.Enabled = false;
        if ( sender is not null and Control )
        {
            this._validationErrorProvider.SetError( ( Control ) sender, string.Empty );
        }
        if ( this.NumberStyle != System.Globalization.NumberStyles.None )
        {
            e.Cancel = !double.TryParse( this._enteredValueTextBox.Text, this.NumberStyle, System.Globalization.CultureInfo.CurrentCulture, out _ );
        }
    }

    /// <summary> Enables the OK button. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void EnteredValueTextBoxValidated( object? sender, EventArgs e )
    {
        this._acceptButton.Enabled = true;
    }

    #endregion

    #region " static "

    /// <summary> Enter dialog. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="caption"> The caption. </param>
    /// <returns> A (DialogResult As DialogResult, DialogValue As String) </returns>
    public static (DialogResult DialogResult, string DialogValue) EnterDialog( string caption )
    {
        return EnterDialog( caption, "Enter a value:", string.Empty, false );
    }

    /// <summary> Enter dialog. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="caption">               The caption. </param>
    /// <param name="useSystemPasswordChar"> True to use system password character. </param>
    /// <returns> A (DialogResult As DialogResult, DialogValue As String) </returns>
    public static (DialogResult DialogResult, string DialogValue) EnterDialog( string caption, bool useSystemPasswordChar )
    {
        return EnterDialog( caption, "Enter a value:", string.Empty, useSystemPasswordChar );
    }

    /// <summary> Enter dialog. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="caption">               The caption. </param>
    /// <param name="prompt">                The prompt. </param>
    /// <param name="defaultValue">          The default value. </param>
    /// <param name="useSystemPasswordChar"> True to use system password character. </param>
    /// <returns> A (DialogResult As DialogResult, DialogValue As String) </returns>
    public static (DialogResult DialogResult, string DialogValue) EnterDialog( string caption, string prompt, string defaultValue, bool useSystemPasswordChar )
    {
        using TextInputBox box = new();
        box.UseSystemPasswordChar = useSystemPasswordChar;
        box.DefaultValue = defaultValue;
        box.Prompt = prompt;
        box.Text = caption;
        // toggling visibility ensures that the form displays in unit testing debug mode
        box.Visible = true;
        System.Windows.Forms.Application.DoEvents();
        box.Visible = false;
        _ = box.ShowDialog();
        return (box.DialogResult, box.EnteredValue);
    }

    /// <summary>   Enter password. </summary>
    /// <remarks>   David, 2021-03-11. </remarks>
    /// <param name="caption">      The caption. </param>
    /// <param name="prompt">       The prompt. </param>
    /// <returns>   A Tuple. </returns>
    public static (DialogResult DialogResult, System.Security.SecureString Password) EnterPasswordDialog( string caption, string prompt )
    {
        using TextInputBox box = new();
        box.UseSystemPasswordChar = true;
        box.DefaultValue = string.Empty;
        box.Prompt = prompt;
        box.Text = caption;
        // toggling visibility ensures that the form displays in unit testing debug mode
        box.Visible = true;
        System.Windows.Forms.Application.DoEvents();
        box.Visible = false;
        _ = box.ShowDialog();
        return (box.DialogResult, new System.Net.NetworkCredential( string.Empty, box.EnteredValue ).SecurePassword);
    }

    /// <summary>   Enter input. </summary>
    /// <remarks>   David, 2021-03-11. </remarks>
    /// <param name="caption">                  (Optional) The caption. </param>
    /// <param name="prompt">                   (Optional) The prompt. </param>
    /// <param name="defaultValue">             (Optional) The default value. </param>
    /// <param name="useSystemPasswordChar">    (Optional) True to use system password character. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public static string EnterInput( string caption = "Enter Value", string prompt = "Value:", string defaultValue = "", bool useSystemPasswordChar = false )
    {
        return TextInputBox.EnterDialog( caption, prompt, defaultValue, useSystemPasswordChar ).DialogValue;
    }

    /// <summary>   Enter password. </summary>
    /// <remarks>   David, 2021-03-11. </remarks>
    /// <param name="caption">  (Optional) The caption. </param>
    /// <param name="prompt">   (Optional) The prompt. </param>
    /// <returns>   A System.Security.SecureString. </returns>
    public static System.Security.SecureString EnterPassword( string caption = "Enter Password", string prompt = "Password:" )
    {
        return TextInputBox.EnterPasswordDialog( caption, prompt ).Password;
    }

    #endregion
}
