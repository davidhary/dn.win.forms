using System;

namespace cc.isr.WinForms.Dialogs;

/// <summary>   A numeric input box. </summary>
/// <remarks>   David, 2006-02-20. </remarks>
public partial class NumericInputBox : FormBase
{
    #region " construction and cleanup "

    /// <summary> Initializes a new instance of this class. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    public NumericInputBox() : base()
    {
        // This call is required by the Windows Form Designer.
        this.InitializeComponent();

        // Add any initialization after the InitializeComponent() call
        this.ClassStyle = ClassStyleConstants.DropShadow;

        this._acceptButton.Enabled = true;
    }

    /// <summary>
    /// Disposes of the resources (other than memory) used by the
    /// <see cref="System.Windows.Forms.Form" />.
    /// </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="disposing"> true to release both managed and unmanaged resources; false to
    /// release only unmanaged resources. </param>
    protected override void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this.components?.Dispose();
                this.components = null;
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }

    #endregion 

    #region " form and control event handlers "

    /// <summary>
    /// Closes and returns the <see cref="System.Windows.Forms.DialogResult.OK">OK</see>
    /// dialog result.
    /// </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void AcceptButton_Click( object? sender, EventArgs e )
    {
        this.DialogResult = System.Windows.Forms.DialogResult.OK;
        this.Close();
    }

    /// <summary>
    /// Closes and returns the <see cref="System.Windows.Forms.DialogResult.Cancel">Cancel</see>
    /// dialog result.
    /// </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void CancelButton_Click( object? sender, EventArgs e )
    {
        this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        this.Close();
    }

    #endregion 

}
