using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace cc.isr.WinForms.Dialogs;

public partial class NumericInputBox
{
    // Required by the Windows Form Designer
    private System.ComponentModel.IContainer components;

    // NOTE: The following procedure is required by the Windows Form Designer
    // It can be modified using the Windows Form Designer.  
    // Do not modify it using the code editor.
    [DebuggerStepThrough()]
    private void InitializeComponent()
    {
        _cancelButton = new System.Windows.Forms.Button();
        _cancelButton.Click += new EventHandler(CancelButton_Click);
        _acceptButton = new System.Windows.Forms.Button();
        _acceptButton.Click += new EventHandler(AcceptButton_Click);
        _numericUpDownLabel = new System.Windows.Forms.Label();
        NumericUpDown = new System.Windows.Forms.NumericUpDown();
        ((System.ComponentModel.ISupportInitialize)NumericUpDown).BeginInit();
        SuspendLayout();
        // 
        // _cancelButton
        // 
        _cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        _cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
        _cancelButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
        _cancelButton.Location = new System.Drawing.Point(11, 59);
        _cancelButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
        _cancelButton.Name = "_CancelButton";
        _cancelButton.Size = new System.Drawing.Size(87, 30);
        _cancelButton.TabIndex = 27;
        _cancelButton.Text = "&Cancel";
        // 
        // _acceptButton
        // 
        _acceptButton.Enabled = false;
        _acceptButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
        _acceptButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
        _acceptButton.Location = new System.Drawing.Point(108, 59);
        _acceptButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
        _acceptButton.Name = "_acceptButton";
        _acceptButton.Size = new System.Drawing.Size(87, 30);
        _acceptButton.TabIndex = 26;
        _acceptButton.Text = "&OK";
        // 
        // _numericUpDownLabel
        // 
        _numericUpDownLabel.BackColor = System.Drawing.SystemColors.Control;
        _numericUpDownLabel.Cursor = System.Windows.Forms.Cursors.Default;
        _numericUpDownLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
        _numericUpDownLabel.ForeColor = System.Drawing.SystemColors.WindowText;
        _numericUpDownLabel.Location = new System.Drawing.Point(9, 5);
        _numericUpDownLabel.Name = "_NumericUpDownLabel";
        _numericUpDownLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
        _numericUpDownLabel.Size = new System.Drawing.Size(156, 21);
        _numericUpDownLabel.TabIndex = 25;
        _numericUpDownLabel.Text = "Enter a Value: ";
        _numericUpDownLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
        // 
        // NumericUpDown
        // 
        NumericUpDown.Location = new System.Drawing.Point(11, 27);
        NumericUpDown.Name = "NumericUpDown";
        NumericUpDown.Size = new System.Drawing.Size(183, 25);
        NumericUpDown.TabIndex = 28;
        // 
        // NumericInputBox
        // 
        AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
        ClientSize = new System.Drawing.Size(209, 98);
        ControlBox = false;
        Controls.Add(NumericUpDown);
        Controls.Add(_cancelButton);
        Controls.Add(_acceptButton);
        Controls.Add(_numericUpDownLabel);
        Name = "NumericInputBox";
        Text = "Numeric Input Box";
        ((System.ComponentModel.ISupportInitialize)NumericUpDown).EndInit();
        ResumeLayout(false);
    }

    private System.Windows.Forms.Button _cancelButton;
    private System.Windows.Forms.Button _acceptButton;
    private System.Windows.Forms.Label _numericUpDownLabel;

    /// <summary>   The numeric up down control. </summary>
    public System.Windows.Forms.NumericUpDown NumericUpDown;
}
