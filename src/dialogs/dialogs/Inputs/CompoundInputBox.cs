using System;

namespace cc.isr.WinForms.Dialogs;

/// <summary> A data entry form. </summary>
/// <remarks> David, 2006-02-20. </remarks>
public partial class CompoundInputBox : FormBase
{
    #region " construction and cleanup "

    /// <summary> Initializes a new instance of this class. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    public CompoundInputBox() : base()
    {
        // This call is required by the Windows Form Designer.
        this.InitializeComponent();

        // Add any initialization after the InitializeComponent() call
        this.ClassStyle = ClassStyleConstants.DropShadow;
        this._acceptButton.Enabled = true;
        this.TextBoxLabel.Text = string.Empty;
        this.NumericUpDownLabel.Text = string.Empty;
        this.DropDownBoxLabel.Text = string.Empty;
    }

    /// <summary>
    /// Disposes of the resources (other than memory) used by the
    /// <see cref="System.Windows.Forms.Form" />.
    /// </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="disposing"> true to release both managed and unmanaged resources; false to
    /// release only unmanaged resources. </param>
    protected override void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this.components?.Dispose();
                this.components = null;
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }

    #endregion 


    #region " form events "

    /// <summary> Handles the shown event of the control. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="e"> Event information. </param>
    protected override void OnShown( EventArgs e )
    {
        try
        {
            if ( string.IsNullOrWhiteSpace( this.TextBoxLabel.Text ) )
            {
                this.TextBoxLabel.Visible = false;
                this._textBox.Visible = false;
            }

            if ( string.IsNullOrWhiteSpace( this.NumericUpDownLabel.Text ) )
            {
                this.NumericUpDownLabel.Visible = false;
                this.NumericUpDown.Visible = false;
            }

            if ( string.IsNullOrWhiteSpace( this.DropDownBoxLabel.Text ) )
            {
                this.DropDownBoxLabel.Visible = false;
                this.DropDownBox.Visible = false;
            }
        }
        catch
        {
            throw;
        }
        finally
        {
            base.OnShown( e );
        }
    }

    #endregion 


    #region " form and control event handlers "

    /// <summary>
    /// Closes and returns the <see cref="System.Windows.Forms.DialogResult.OK">OK</see>
    /// dialog result.
    /// </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void AcceptButton_Click( object? sender, EventArgs e )
    {
        this.DialogResult = System.Windows.Forms.DialogResult.OK;
        this.Close();
    }

    /// <summary>
    /// Closes and returns the <see cref="System.Windows.Forms.DialogResult.Cancel">Cancel</see>
    /// dialog result.
    /// </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void CancelButton_Click( object? sender, EventArgs e )
    {
        this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        this.Close();
    }

    /// <summary> Enables the OK button. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void TextBox_Validated( object? sender, EventArgs e )
    {
        this._acceptButton.Enabled = true;
    }

    #endregion 

}
