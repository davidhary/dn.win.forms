using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace cc.isr.WinForms.Dialogs;

public partial class CompoundInputBox
{
    // Required by the Windows Form Designer
    private System.ComponentModel.IContainer components;

    // NOTE: The following procedure is required by the Windows Form Designer
    // It can be modified using the Windows Form Designer.  
    // Do not modify it using the code editor.
    [DebuggerStepThrough()]
    private void InitializeComponent()
    {
        _textBox = new System.Windows.Forms.TextBox();
        _textBox.Validated += new EventHandler(TextBox_Validated);
        _cancelButton = new System.Windows.Forms.Button();
        _cancelButton.Click += new EventHandler(CancelButton_Click);
        _acceptButton = new System.Windows.Forms.Button();
        _acceptButton.Click += new EventHandler(AcceptButton_Click);
        TextBoxLabel = new System.Windows.Forms.Label();
        _layout = new System.Windows.Forms.TableLayoutPanel();
        _buttonLayout = new System.Windows.Forms.TableLayoutPanel();
        NumericUpDownLabel = new System.Windows.Forms.Label();
        DropDownBoxLabel = new System.Windows.Forms.Label();
        NumericUpDown = new System.Windows.Forms.NumericUpDown();
        DropDownBox = new System.Windows.Forms.ComboBox();
        _layout.SuspendLayout();
        _buttonLayout.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)NumericUpDown).BeginInit();
        SuspendLayout();
        // 
        // TextBox
        // 
        _textBox.Dock = System.Windows.Forms.DockStyle.Top;
        _textBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
        _textBox.Location = new System.Drawing.Point(9, 35);
        _textBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
        _textBox.Name = "_TextBox";
        _textBox.Size = new System.Drawing.Size(252, 25);
        _textBox.TabIndex = 28;
        _textBox.Text = "TextBox1";
        // 
        // _cancelButton
        // 
        _cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        _cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
        _cancelButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
        _cancelButton.Location = new System.Drawing.Point(36, 4);
        _cancelButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
        _cancelButton.Name = "_CancelButton";
        _cancelButton.Size = new System.Drawing.Size(87, 30);
        _cancelButton.TabIndex = 27;
        _cancelButton.Text = "&Cancel";
        // 
        // _acceptButton
        // 
        _acceptButton.Enabled = false;
        _acceptButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
        _acceptButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
        _acceptButton.Location = new System.Drawing.Point(129, 4);
        _acceptButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
        _acceptButton.Name = "_acceptButton";
        _acceptButton.Size = new System.Drawing.Size(87, 30);
        _acceptButton.TabIndex = 26;
        _acceptButton.Text = "&OK";
        // 
        // TextBoxLabel
        // 
        TextBoxLabel.AutoSize = true;
        TextBoxLabel.BackColor = System.Drawing.SystemColors.Control;
        TextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default;
        TextBoxLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
        TextBoxLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
        TextBoxLabel.ForeColor = System.Drawing.SystemColors.WindowText;
        TextBoxLabel.Location = new System.Drawing.Point(9, 14);
        TextBoxLabel.Name = "TextBoxLabel";
        TextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
        TextBoxLabel.Size = new System.Drawing.Size(252, 17);
        TextBoxLabel.TabIndex = 25;
        TextBoxLabel.Text = "Enter text: ";
        TextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
        // 
        // _layout
        // 
        _layout.ColumnCount = 3;
        _layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 6.0f));
        _layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
        _layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 6.0f));
        _layout.Controls.Add(DropDownBoxLabel, 1, 7);
        _layout.Controls.Add(_buttonLayout, 1, 10);
        _layout.Controls.Add(_textBox, 1, 2);
        _layout.Controls.Add(TextBoxLabel, 1, 1);
        _layout.Controls.Add(NumericUpDownLabel, 1, 4);
        _layout.Controls.Add(NumericUpDown, 1, 5);
        _layout.Controls.Add(DropDownBox, 1, 8);
        _layout.Dock = System.Windows.Forms.DockStyle.Fill;
        _layout.Location = new System.Drawing.Point(0, 0);
        _layout.Name = "_Layout";
        _layout.RowCount = 12;
        _layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0f));
        _layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
        _layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
        _layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0f));
        _layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
        _layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
        _layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0f));
        _layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
        _layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
        _layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0f));
        _layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44.0f));
        _layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0f));
        _layout.Size = new System.Drawing.Size(270, 261);
        _layout.TabIndex = 29;
        // 
        // _buttonLayout
        // 
        _buttonLayout.ColumnCount = 4;
        _buttonLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
        _buttonLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
        _buttonLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
        _buttonLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
        _buttonLayout.Controls.Add(_acceptButton, 2, 1);
        _buttonLayout.Controls.Add(_cancelButton, 1, 1);
        _buttonLayout.Dock = System.Windows.Forms.DockStyle.Fill;
        _buttonLayout.Location = new System.Drawing.Point(9, 205);
        _buttonLayout.Name = "_ButtonLayout";
        _buttonLayout.RowCount = 3;
        _buttonLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
        _buttonLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
        _buttonLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
        _buttonLayout.Size = new System.Drawing.Size(252, 38);
        _buttonLayout.TabIndex = 30;
        // 
        // NumericUpDownLabel
        // 
        NumericUpDownLabel.AutoSize = true;
        NumericUpDownLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
        NumericUpDownLabel.Location = new System.Drawing.Point(9, 78);
        NumericUpDownLabel.Name = "NumericUpDownLabel";
        NumericUpDownLabel.Size = new System.Drawing.Size(252, 17);
        NumericUpDownLabel.TabIndex = 31;
        NumericUpDownLabel.Text = "Enter a number:";
        // 
        // DropDownBoxLabel
        // 
        DropDownBoxLabel.AutoSize = true;
        DropDownBoxLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
        DropDownBoxLabel.Location = new System.Drawing.Point(9, 140);
        DropDownBoxLabel.Name = "DropDownBoxLabel";
        DropDownBoxLabel.Size = new System.Drawing.Size(252, 17);
        DropDownBoxLabel.TabIndex = 30;
        DropDownBoxLabel.Text = "Select item:";
        // 
        // NumericUpDown
        // 
        NumericUpDown.Dock = System.Windows.Forms.DockStyle.Top;
        NumericUpDown.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 0);
        NumericUpDown.Location = new System.Drawing.Point(9, 98);
        NumericUpDown.Name = "NumericUpDown";
        NumericUpDown.Size = new System.Drawing.Size(252, 25);
        NumericUpDown.TabIndex = 32;
        // 
        // DropDownBox
        // 
        DropDownBox.Dock = System.Windows.Forms.DockStyle.Top;
        DropDownBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 0);
        DropDownBox.FormattingEnabled = true;
        DropDownBox.Location = new System.Drawing.Point(9, 160);
        DropDownBox.Name = "DropDownBox";
        DropDownBox.Size = new System.Drawing.Size(252, 25);
        DropDownBox.TabIndex = 33;
        // 
        // CompoundInputBox
        // 
        AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
        ClientSize = new System.Drawing.Size(270, 261);
        ControlBox = false;
        Controls.Add(_layout);
        Name = "CompoundInputBox";
        Text = "Compound Input Box";
        _layout.ResumeLayout(false);
        _layout.PerformLayout();
        _buttonLayout.ResumeLayout(false);
        ((System.ComponentModel.ISupportInitialize)NumericUpDown).EndInit();
        ResumeLayout(false);
    }

    private System.Windows.Forms.TextBox _textBox;
    private System.Windows.Forms.Button _cancelButton;
    private System.Windows.Forms.Button _acceptButton;
    private System.Windows.Forms.TableLayoutPanel _layout;
    private System.Windows.Forms.TableLayoutPanel _buttonLayout;

    /// <summary>   The text box label. </summary>
    public System.Windows.Forms.Label TextBoxLabel;

    /// <summary>   The drop down box label. </summary>
    public System.Windows.Forms.Label DropDownBoxLabel;

    /// <summary>   The numeric up down label. </summary>
    public System.Windows.Forms.Label NumericUpDownLabel;

    /// <summary>   The numeric up down control. </summary>
    public System.Windows.Forms.NumericUpDown NumericUpDown;

    /// <summary>   The drop down box. </summary>
    public System.Windows.Forms.ComboBox DropDownBox;

}
