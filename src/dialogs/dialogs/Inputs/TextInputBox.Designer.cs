using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;

namespace cc.isr.WinForms.Dialogs
{
    public partial class TextInputBox
    {
        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _enteredValueTextBox = new System.Windows.Forms.TextBox();
            _enteredValueTextBox.TextChanged += new EventHandler(EnteredValueTextBoxTextChanged);
            _enteredValueTextBox.Validating += new System.ComponentModel.CancelEventHandler(EnteredValueTextBoxValidating);
            _enteredValueTextBox.Validated += new EventHandler(EnteredValueTextBoxValidated);
            _cancelButton = new System.Windows.Forms.Button();
            _cancelButton.Click += new EventHandler(CancelButtonClick);
            _acceptButton = new System.Windows.Forms.Button();
            _acceptButton.Click += new EventHandler(AcceptButtonClick);
            _enteredValueTextBoxLabel = new System.Windows.Forms.Label();
            _validationErrorProvider = new System.Windows.Forms.ErrorProvider(components);
            ((System.ComponentModel.ISupportInitialize)_validationErrorProvider).BeginInit();
            SuspendLayout();
            // 
            // _enteredValueTextBox
            // 
            _enteredValueTextBox.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;

            _enteredValueTextBox.Font = new Font("Segoe UI", 9.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _enteredValueTextBox.Location = new Point(9, 27);
            _enteredValueTextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _enteredValueTextBox.Multiline = true;
            _enteredValueTextBox.Name = "_EnteredValueTextBox";
            _enteredValueTextBox.Size = new Size(186, 20);
            _enteredValueTextBox.TabIndex = 28;
            // 
            // _cancelButton
            // 
            _cancelButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            _cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            _cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            _cancelButton.Font = new Font("Segoe UI", 9.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _cancelButton.Location = new Point(9, 59);
            _cancelButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _cancelButton.Name = "_CancelButton";
            _cancelButton.Size = new Size(87, 30);
            _cancelButton.TabIndex = 27;
            _cancelButton.Text = "&Cancel";
            // 
            // _acceptButton
            // 
            _acceptButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            _acceptButton.Enabled = false;
            _acceptButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            _acceptButton.Font = new Font("Segoe UI", 9.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _acceptButton.Location = new Point(108, 59);
            _acceptButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _acceptButton.Name = "_acceptButton";
            _acceptButton.Size = new Size(87, 30);
            _acceptButton.TabIndex = 26;
            _acceptButton.Text = "&OK";
            // 
            // _enteredValueTextBoxLabel
            // 
            _enteredValueTextBoxLabel.AutoSize = true;
            _enteredValueTextBoxLabel.BackColor = SystemColors.Control;
            _enteredValueTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _enteredValueTextBoxLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            _enteredValueTextBoxLabel.ForeColor = SystemColors.WindowText;
            _enteredValueTextBoxLabel.Location = new Point(9, 7);
            _enteredValueTextBoxLabel.Name = "_EnteredValueTextBoxLabel";
            _enteredValueTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _enteredValueTextBoxLabel.Size = new Size(91, 17);
            _enteredValueTextBoxLabel.TabIndex = 25;
            _enteredValueTextBoxLabel.Text = "Enter a Value: ";
            _enteredValueTextBoxLabel.TextAlign = ContentAlignment.BottomLeft;
            // 
            // _validationErrorProvider
            // 
            _validationErrorProvider.ContainerControl = this;
            // 
            // TextInputBox
            // 
            AcceptButton = _acceptButton;
            AutoScaleDimensions = new SizeF(7.0f, 17.0f);
            CancelButton = _cancelButton;
            ClientSize = new Size(203, 95);
            ControlBox = false;
            Controls.Add(_enteredValueTextBox);
            Controls.Add(_cancelButton);
            Controls.Add(_acceptButton);
            Controls.Add(_enteredValueTextBoxLabel);
            Name = "TextInputBox";
            Text = "InputBox";
            ((System.ComponentModel.ISupportInitialize)_validationErrorProvider).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        private System.Windows.Forms.TextBox _enteredValueTextBox;
        private System.Windows.Forms.Button _cancelButton;
        private System.Windows.Forms.Button _acceptButton;
        private System.Windows.Forms.Label _enteredValueTextBoxLabel;
        private System.Windows.Forms.ErrorProvider _validationErrorProvider;
    }
}
