using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;

namespace cc.isr.WinForms;
/// <summary>
/// Serves a wrapper for items being viewed in a <see cref="BindingListView{T}"/>. This class
/// implements <see cref="INotifyingEditableObject"/> so will raise the necessary events during
/// the item edit life-cycle.
/// </summary>
/// <remarks>
/// If <typeparamref name="T"/> implements <see cref="IEditableObject"/>
/// this class will call BeginEdit/CancelEdit/EndEdit on the <typeparamref name="T"/> editableObject as
/// well. If <typeparamref name="T"/> implements
/// <see cref="IDataErrorInfo"/> this class will use that implementation as
/// its own. <para>
/// (c) 2006 Andrew Davey. All rights reserved.</para><para>
/// Licensed under The MIT License.</para><para>
/// David, 2018-12-15. Source: </para><para>
/// https://blogs.warwick.ac.uk/andrewdavey and </para><para>
/// https://sourceforge.net/projects/blw/. </para>
/// </remarks>
public class ObjectView<T> : INotifyingEditableObject, IDataErrorInfo, INotifyPropertyChanged, ICustomTypeDescriptor where T : notnull
{
    /// <summary>
    /// Creates a new <see cref="ObjectView{T}"/> wrapper for a <typeparamref name="T"/>
    /// editableObject.
    /// </summary>
    /// <remarks>   David, 2020-09-17. </remarks>
    /// <param name="editableObject">   Gets the editableObject being edited. </param>
    /// <param name="parent">           The view containing this ObjectView. </param>
    public ObjectView( T editableObject, AggregateBindingListView<T> parent )
    {
        this._parent = parent;
        this._editableObject = editableObject;
        this.EditableObject = editableObject;
        if ( editableObject is INotifyPropertyChanged changed )
        {
            changed.PropertyChanged += this.ObjectPropertyChanged;
        }

        if ( typeof( ICustomTypeDescriptor ).IsAssignableFrom( typeof( T ) ) )
        {
            this._isCustomTypeDescriptor = true;
            this._customTypeDescriptor = editableObject as ICustomTypeDescriptor;
            Debug.Assert( this._customTypeDescriptor is object );
        }

        this.ProvidedViews = [];
        this.CreateProvidedViews();
    }

    /// <summary>
    /// The view containing this ObjectView.
    /// </summary>
    [NonSerialized]
    private readonly AggregateBindingListView<T> _parent;

    /// <summary>
    /// Flag that signals if we are currently editing the editableObject.
    /// </summary>
    private bool _editing;

    /// <summary>
    /// Flag set to true if type of T implements ICustomTypeDescriptor
    /// </summary>
    private readonly bool _isCustomTypeDescriptor;

    /// <summary>
    /// Holds the EditableObject pre-casted ICustomTypeDescriptor (if supported).
    /// </summary>
    private readonly ICustomTypeDescriptor? _customTypeDescriptor;

    /// <summary>
    /// The actual editableObject being edited.
    /// </summary>
    private T _editableObject;

    /// <summary>
    /// Gets the editableObject being edited.
    /// </summary>
    public T EditableObject
    {
        get => this._editableObject;

        private set
        {
            if ( value is null )
            {
                throw new ArgumentNullException( nameof( value ), Properties.Resources.ObjectCannotBeNull );
            }

            this._editableObject = value;
        }
    }

    /// <summary>
    /// A collection of BindingListView objects, indexed by name, for views auto-provided for any
    /// generic IList members.
    /// </summary>
    /// <value> The provided views. </value>
    private Dictionary<string, object?> ProvidedViews { get; set; }

    /// <summary> Gets provided view. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="name"> The name. </param>
    /// <returns> The provided view. </returns>
    public object? GetProvidedView( string name )
    {
        return this.ProvidedViews[name];
    }

    /// <summary> Casts an ObjectView{T} to a T by getting the wrapped T editableObject. </summary>
    /// <param name="value"> The ObjectView{T} to cast to a T. </param>
    /// <returns> The editableObject that is wrapped. </returns>
    public static explicit operator T( ObjectView<T> value )
    {
        return value is null ? ( T ) new object() : value.EditableObject;
    }

    /// <summary> Determines whether the specified editableObject is equal to the current editableObject. </summary>
    /// <param name="obj"> The editableObject to compare with the current editableObject. </param>
    /// <returns>
    /// true if the specified editableObject  is equal to the current editableObject; otherwise, false.
    /// </returns>
    public override bool Equals( object? obj )
    {
        return obj is not ObjectView<T> &&
                (obj is T
                    ? this.EditableObject.Equals( obj )
                    : obj is ObjectView<T> ov
                        ? this.EditableObject.Equals( ov.EditableObject )
                        : base.Equals( obj ));
    }

    /// <summary> Serves as the default hash function. </summary>
    /// <returns> A hash code for the current editableObject. </returns>
    public override int GetHashCode()
    {
        return this.EditableObject.GetHashCode();
    }

    /// <summary> Returns a string that represents the current editableObject. </summary>
    /// <returns> A <see cref="string" /> that represents the current editableObject. </returns>
    public override string? ToString()
    {
        return this.EditableObject.ToString();
    }

    /// <summary> EditableObject property changed. </summary>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Property changed event information. </param>
    private void ObjectPropertyChanged( object? sender, PropertyChangedEventArgs e )
    {
        // Raise our own event
        this.OnPropertyChanged( sender, new PropertyChangedEventArgs( e.PropertyName ) );
    }

    /// <summary> Should provide view of the given list property. </summary>
    /// <param name="listProp"> The list property. </param>
    /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    private bool ShouldProvideViewOf( PropertyDescriptor listProp )
    {
        return this._parent.ShouldProvideView( listProp );
    }

    /// <summary> Gets provided view name. </summary>
    /// <param name="listProp"> The list property. </param>
    /// <returns> The provided view name. </returns>
    private string GetProvidedViewName( PropertyDescriptor listProp )
    {
        return this._parent.GetProvidedViewName( listProp );
    }

    /// <summary> Creates provided views. </summary>
    private void CreateProvidedViews()
    {
        foreach ( PropertyDescriptor prop in (this as ICustomTypeDescriptor).GetProperties() )
        {
            if ( this.ShouldProvideViewOf( prop ) )
            {
                object? view = this._parent.CreateProvidedView( this, prop );
                string viewName = this.GetProvidedViewName( prop );
                this.ProvidedViews.Add( viewName, view );
            }
        }
    }

    #region " inotifyeditableobject members "

    /// <summary>
    /// Indicates an edit has just begun.
    /// </summary>
    public event EventHandler? EditBegun;

    /// <summary>
    /// Indicates the edit was canceled.
    /// </summary>
    public event EventHandler? EditCanceled;

    /// <summary>
    /// Indicated the edit was ended.
    /// </summary>
    public event EventHandler? EditEnded;

    /// <summary> Executes the edit begun action. </summary>
    protected virtual void OnEditBegun()
    {
        EditBegun?.Invoke( this, EventArgs.Empty );
    }

    /// <summary> Executes the edit canceled action. </summary>
    protected virtual void OnEditCanceled()
    {
        EditCanceled?.Invoke( this, EventArgs.Empty );
    }

    /// <summary> Executes the edit ended action. </summary>
    protected virtual void OnEditEnded()
    {
        EditEnded?.Invoke( this, EventArgs.Empty );
    }

    #endregion

    #region " ieditableobject members "

    /// <summary> Begins an edit. </summary>
    public void BeginEdit()
    {
        // As per documentation, this method may get called multiple times for a single edit.
        // So we set a flag to only honor the first call.
        if ( !this._editing )
        {
            this._editing = true;
            // If possible call the editableObject's BeginEdit() method
            // to let it do what ever it needs e.g. save state
            if ( this.EditableObject is IEditableObject editableObject )
            {
                editableObject.BeginEdit();
            }
            // Raise the EditBegun event.                
            this.OnEditBegun();
        }
    }

    /// <summary> Cancel edit. </summary>
    public void CancelEdit()
    {
        // We can only cancel if currently editing
        if ( this._editing )
        {
            // If possible call the editableObject's CancelEdit() method
            // to let it do what ever it needs e.g. rollback state
            if ( this.EditableObject is IEditableObject editableObject )
            {
                editableObject.CancelEdit();
            }
            // Raise the EditCancelled event.
            this.OnEditCanceled();
            // No longer editing now.
            this._editing = false;
        }
    }

    /// <summary> Ends an edit. </summary>
    public void EndEdit()
    {
        // We can only end if currently editing. 
        if ( this._editing )
        {
            // If possible call the editableObject's EndEdit() method
            // to let it do what ever it needs e.g. commit state
            if ( this.EditableObject is IEditableObject editableObject )
            {
                editableObject.EndEdit();
            }
            // Raise the EditEnded event.
            this.OnEditEnded();
            // No longer editing now.
            this._editing = false;
        }
    }

    #endregion

    #region " idataerrorinfo members "

    /// <summary> Gets the data error information error. </summary>
    /// <remarks>
    /// If the wrapped EditableObject support IDataErrorInfo we forward calls to it. Otherwise, we just
    /// return empty strings that signal "no error".
    /// </remarks>
    /// <value> The i data error information error. </value>
    string IDataErrorInfo.Error => this.EditableObject is IDataErrorInfo info ? info.Error : string.Empty;

    /// <summary> Gets the data error information item. </summary>
    /// <value> The i data error information item. </value>
    public string this[string columnName] => this.EditableObject is IDataErrorInfo info ? info[columnName] : string.Empty;

    #endregion

    #region " inotifypropertychanged members "

    /// <summary> Event queue for all listeners interested in PropertyChanged events. </summary>
    public event PropertyChangedEventHandler? PropertyChanged;

    /// <summary> Raises the property changed event. </summary>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="args">   Event information to send to registered event handlers. </param>
    protected virtual void OnPropertyChanged( object? sender, PropertyChangedEventArgs args )
    {
        PropertyChanged?.Invoke( sender, args );
    }

    #endregion

    #region " icustomtypedescriptor members "

    AttributeCollection ICustomTypeDescriptor.GetAttributes()
    {
        return this._isCustomTypeDescriptor
            ? this._customTypeDescriptor!.GetAttributes()
            : TypeDescriptor.GetAttributes( this.EditableObject );
    }

    string? ICustomTypeDescriptor.GetClassName()
    {
        return this._isCustomTypeDescriptor
            ? this._customTypeDescriptor!.GetClassName()
            : typeof( T ).FullName;
    }

    string? ICustomTypeDescriptor.GetComponentName()
    {
        return this._isCustomTypeDescriptor
            ? this._customTypeDescriptor!.GetComponentName()
            : TypeDescriptor.GetFullComponentName( this.EditableObject );
    }

    TypeConverter? ICustomTypeDescriptor.GetConverter()
    {
        return this._isCustomTypeDescriptor
            ? this._customTypeDescriptor!.GetConverter()
            : TypeDescriptor.GetConverter( this.EditableObject );
    }

    EventDescriptor? ICustomTypeDescriptor.GetDefaultEvent()
    {
        return this._isCustomTypeDescriptor
            ? this._customTypeDescriptor!.GetDefaultEvent()
            : TypeDescriptor.GetDefaultEvent( this.EditableObject );
    }

    PropertyDescriptor? ICustomTypeDescriptor.GetDefaultProperty()
    {
        return this._isCustomTypeDescriptor
            ? this._customTypeDescriptor!.GetDefaultProperty()
            : TypeDescriptor.GetDefaultProperty( this.EditableObject );
    }

    object? ICustomTypeDescriptor.GetEditor( Type editorBaseType )
    {
        return this._isCustomTypeDescriptor
            ? this._customTypeDescriptor!.GetEditor( editorBaseType )
            : null;
    }

    EventDescriptorCollection ICustomTypeDescriptor.GetEvents( Attribute[]? attributes )
    {
        return this._isCustomTypeDescriptor
            ? this._customTypeDescriptor!.GetEvents()
            : TypeDescriptor.GetEvents( this.EditableObject, attributes! );
    }

    EventDescriptorCollection ICustomTypeDescriptor.GetEvents()
    {
        return this._isCustomTypeDescriptor
            ? this._customTypeDescriptor!.GetEvents()
            : TypeDescriptor.GetEvents( this.EditableObject );
    }

    PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties( Attribute[]? attributes )
    {
        return this._isCustomTypeDescriptor
                ? this._customTypeDescriptor!.GetProperties()
                : TypeDescriptor.GetProperties( this.EditableObject, attributes );
    }

    /// <summary> Custom type descriptor get properties. </summary>
    /// <returns> A PropertyDescriptorCollection. </returns>
    PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties()
    {
        return this._isCustomTypeDescriptor
            ? this._customTypeDescriptor!.GetProperties()
            : TypeDescriptor.GetProperties( this.EditableObject );
    }

    /// <summary> Custom type descriptor get property owner. </summary>
    /// <param name="pd"> The pd. </param>
    /// <returns> An EditableObject. </returns>
    object? ICustomTypeDescriptor.GetPropertyOwner( PropertyDescriptor? pd )
    {
        return this._isCustomTypeDescriptor
            ? this._customTypeDescriptor!.GetPropertyOwner( pd )
            : this.EditableObject;
    }

    #endregion
}
