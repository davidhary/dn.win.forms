using System.ComponentModel;

namespace cc.isr.WinForms;

public partial class AggregateBindingListView<T>
{
    /// <summary>   Event queue for all listeners interested in Adding New events. </summary>
    public event AddingNewEventHandler? AddingNew;

    /// <summary>
    /// Notifies Adding New synchronously. Not thread save.
    /// </summary>
    /// <param name="e"> Adding New event information. </param>
    protected virtual void NotifyAddingNew( AddingNewEventArgs e )
    {
        this.AddingNew?.Invoke( this, e );
    }

    /// <summary> Raises the AddingNew event with the given event arguments. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="e"> The AddingNewEventArgs to raise the event with. </param>
    protected virtual void OnAddingNew( AddingNewEventArgs e )
    {
        this.NotifyAddingNew( e );
    }

}
