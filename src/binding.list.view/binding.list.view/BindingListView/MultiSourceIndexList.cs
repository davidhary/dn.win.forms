using System;
using System.Collections;
using System.Collections.Generic;

namespace cc.isr.WinForms;

/// <summary> Multi source index list. </summary>
/// <remarks>
/// (c) 2006 Andrew Davey. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2018-12-15.  </para><para>
/// David, 2018-12-15, 1.2.*. https://blogs.warwick.ac.uk/andrewdavey and
/// https://sourceforge.net/projects/blw/. </para>
/// </remarks>
internal class MultiSourceIndexList<T> : List<KeyValuePair<ListItemPair<T>, int>> where T : notnull
{
    /// <summary> Adds sourceList. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="sourceList"> List of sources. </param>
    /// <param name="item">       The <see cref="ObjectView{T}"/> to search for. </param>
    /// <param name="index">      Zero-based index of the. </param>
    public void Add( IList sourceList, ObjectView<T> item, int index )
    {
        this.Add( new KeyValuePair<ListItemPair<T>, int>( new ListItemPair<T>( sourceList, item ), index ) );
    }

    /// <summary>
    /// Searches for a given source index value, returning the list index of the value.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="sourceList">  List of sources. </param>
    /// <param name="sourceIndex"> The source index to find. </param>
    /// <returns> Returns the index in this list of the source index, or -1 if not found. </returns>
    public int IndexOfSourceIndex( IList sourceList, int sourceIndex )
    {
        for ( int i = 0, loopTo = this.Count - 1; i <= loopTo; i++ )
        {
            if ( ReferenceEquals( this[i].Key.List, sourceList ) && this[i].Value == sourceIndex )
            {
                return i;
            }
        }

        return -1;
    }

    /// <summary> Searches for a given item, returning the index of the value in this list. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="item"> The <typeparamref name="T"/> item to search for. </param>
    /// <returns> Returns the index in this list of the item, or -1 if not found. </returns>
    public int IndexOfItem( T item )
    {
        for ( int i = 0, loopTo = this.Count - 1; i <= loopTo; i++ )
        {
            KeyValuePair<ListItemPair<T>, int> kvp = this[i];
            if ( kvp.Value > -1 && object.Equals( item, this[i].Key.Item.EditableObject ) )
            {
                return i;
            }
        }

        return -1;
    }

    /// <summary>
    /// Searches for a given item's <see cref="ObjectView{T}"/> wrapper, returning the index of
    /// the value in this list.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="item"> The <see cref="ObjectView{T}"/> to search for. </param>
    /// <returns> Returns the index in this list of the item, or -1 if not found. </returns>
    public int IndexOfKey( ObjectView<T> item )
    {
        for ( int i = 0, loopTo = this.Count - 1; i <= loopTo; i++ )
        {
            if ( this[i].Key.Item.Equals( item ) && this[i].Value > -1 )
            {
                return i;
            }
        }

        return -1;
    }

    /// <summary> Checks if the list contains a given item. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="item"> The <typeparamref name="T"/> item to check for. </param>
    /// <returns> True if the item is contained in the list, otherwise false. </returns>
    public bool ContainsItem( T item )
    {
        return this.IndexOfItem( item ) != -1;
    }

    /// <summary> Checks if the list contains a given <see cref="ObjectView{T}"/> key. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="key"> The key to search for. </param>
    /// <returns> True if the key is contained in the list, otherwise false. </returns>
    public bool ContainsKey( ObjectView<T> key )
    {
        return this.IndexOfKey( key ) != -1;
    }

    /// <summary>
    /// Returns an array of all the <see cref="ObjectView{T}"/> keys in the list.
    /// </summary>
    /// <value> The keys. </value>
    public ObjectView<T>[] Keys => [.. this.ConvertAll( new Converter<KeyValuePair<ListItemPair<T>, int>, ObjectView<T>>( ( kvp ) => kvp.Key.Item ) )];

    /// <summary>
    /// Returns an <see cref="IEnumerator{T}"/> to iterate over all the keys in this list.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <returns> The <see cref="IEnumerator{T}"/> to use. </returns>
    public IEnumerator<ObjectView<T>> GetKeyEnumerator()
    {
        foreach ( KeyValuePair<ListItemPair<T>, int> kvp in this )
        {
            yield return kvp.Key.Item;
        }
    }
}
/// <summary> A t. </summary>
/// <remarks>
/// <para>
/// David, 2018-12-15 </para>
/// </remarks>
internal class ListItemPair<T> where T : notnull
{
    /// <summary> Constructor. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="list"> The list. </param>
    /// <param name="item"> The item. </param>
    public ListItemPair( IList list, ObjectView<T> item )
    {
        this.List = list;
        this.Item = item;
    }

    /// <summary> Gets or sets the list. </summary>
    /// <value> The list. </value>
    public IList List { get; private set; }

    /// <summary> Gets or sets the item. </summary>
    /// <value> The item. </value>
    public ObjectView<T> Item { get; private set; }
}
