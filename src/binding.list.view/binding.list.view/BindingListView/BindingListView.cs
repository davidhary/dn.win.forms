using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace cc.isr.WinForms;
/// <summary>
/// A searchable, sortable, filterable, data bindable view of a list of objects.
/// </summary>
/// <remarks>
/// The BindingListView .NET library provides a type-safe, sortable, filterable, data-bindable
/// view of one or more lists of objects. It is the business objects equivalent of using a
/// DataView on a DataTable in ADO.NET. If you have a list of objects to display on a Windows
/// Forms UI (e.g. in a DataGridView) and want to allow your user to sort and filter, then this
/// is the library to use! For more information see
/// https://siderite.blogspot.com/2016/01/bindinglist-vs-observablecollection.html#at3175648015
/// <para>
/// The BindingListView works by creating a wrapper object around each item in a source list.
/// This is just like how a DataView contains DataRowViews, which wrap DataRow objects in
/// ADO.NET. </para>
/// <para>
/// Here is a very simple example of creating a view of a list of objects: </para>
/// <c>
/// Dim customers as IList(Of Customer) = GetCustomers()</c><c>
/// Dim view as New BindingListView(Of Customer)(customers)</c><c>
/// dataGridView1.DataSource = view</c>
/// <para>
/// The new view is passed the customers list as the "source" list. The view is then data bound
/// to a DataGridView control.</para>
/// <para>
/// In the grid you can now sort by clicking on the headers. This was not possible if we had
/// bound directly to the list object instead. </para>
/// <para>
/// You can programatically sort the view Using the 
/// <see cref="AggregateBindingListView{T}.ApplySort(IComparer{T})"/> method.</para>
/// <para>
/// There are a number Of Overloads, the simplest Of which takes a String In the form Of an SQL
/// "order by" clause. For example,</para>
/// <c>
/// view.ApplySort("Balance DESC, Surname")</c>
/// <para>
/// would first sort by the Balance Property (putting the highest first) and then sort by Surname
/// (Is normal ascending order). </para>
/// <para>
/// You can specify zero or more properties To sort by. With Each Property you can enter "DESC"
/// To sort In descending order. </para><para>The restrict the items displayed by the view
/// </para>
/// <para>
/// You can Set a filter. A simple filter Is a Function that takes an EditableObject And returns True Or
/// False depending On If the EditableObject should appear In the view. More advanced filters can be
/// created by creating a Class that Implements the <c>IItemFilter{T}</c> Interface. </para>
/// <para>
/// This example shows creating a filter Using an anonymous method In C#.</para>
/// <c>
/// view.ApplyFilter(delegate(Customer customer) {return customer.Balance .gt. 1000; });
/// In VB.NET you will have to explicitly create the function.
/// </c>
/// <c>
/// view.ApplyFilter(AddressOf BalanceFilter)
/// ...
/// Function BalanceFilter(ByVal customer As Customer) As Boolean
/// Return customer.Balance .gt. 1000
/// End Function
/// </c>
/// A filter will Not actually remove items from the source list, they are just Not visible When
/// bound To a grid For example.
/// <para>
/// An Important Detail
/// </para><para>
/// The BindingListView works by creating a wrapper EditableObject around Each item In a source list.
/// This Is just Like how a DataView contains DataRowViews, which wrap DataRow objects, In
/// ADO.NET. The wrapper EditableObject In a BindingListView Is Of type ObjectView{T}. The only time
/// you usually need to interact with an object view is when retrieving items from the view in
/// code:</para>
/// <c>
/// ObjectView(Of Customer)
/// CustomerView = view[0] ' Get first item in view Customer c = customerView.EditableObject;
/// </c>
/// The example above uses the EditableObject Property Of the ObjectView To Return the original EditableObject.
/// This important detail impacts the most often When you are Using a BindingSource component On
/// a Form, As outlined In this following example.
/// <c>
/// Upon a customer being selected (in a grid or listbox for example)
/// we want to do something with that object.
/// <code>
/// Private Sub customersBindingSource_PositionChanged(EditableObject sender, EventArgs e)
/// ' This cast will fail at runtime. Customer c = (Customer)customersBindingSource.Current '
/// ' The correct code Is this
/// Customer c = ((ObjectView(Of customer)(customersBindingSource.Current).EditableObject
/// </code>
/// </c>
/// BindingSource.Current Is typed as just EditableObject, so the invalid cast to Customer cannot be
/// caught at compile time. This causes a runtime cast exception instead. <para>
/// (c) 2006 Andrew Davey. All rights reserved.</para><para>
/// Licensed under The MIT License.</para><para>
/// David, 2019-05-14, </para><para>
/// David, 2018-12-15, https://blogs.warwick.ac.uk/andrewdavey and
/// https://sourceforge.net/projects/blw/. </para>
/// </remarks>
[System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1010:Generic interface should also be implemented", Justification = "<Pending>" )]
public class BindingListView<T> : AggregateBindingListView<T> where T : notnull
{
    /// <summary>
    /// Creates a new <see cref="BindingListView{T}"/> of a given IBindingList. All items in the
    /// list must be of type <typeparamref name="T"/>.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="list"> The list of objects to base the view on. </param>
    public BindingListView( IList list ) : base() => this.DataSource = list;

    /// <summary>
    /// Creates a new <see cref="BindingListView{T}"/> of a given IBindingList. All items in the
    /// list must be of type <typeparamref name="T"/>.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="container"> The container. </param>
    public BindingListView( IContainer container ) : base( container ) => this.DataSource = null;

    /// <summary> Clears the current data. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    private void ClearCurrentData()
    {
        this.SourceLists = new BindingList<IList<T>>();
        this.NewItemsList = null;
        this.FilterAndSort();
        this.OnListChanged( new ListChangedEventArgs( ListChangedType.Reset, -1 ) );
    }

    /// <summary> Gets or sets the data source. </summary>
    /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    /// illegal values. </exception>
    /// <value> The data source. </value>
    [AttributeProvider( typeof( IListSource ) )]
    public IList? DataSource
    {
        get
        {
            IEnumerator<IList?> e = this.EnumerateSourceLists().GetEnumerator();
            _ = e.MoveNext();
            return e.Current;
        }

        set
        {
            if ( value is null )
            {
                // Clear all current data
                this.SourceLists = new BindingList<IList<T>>();
                this.NewItemsList = null;
                this.FilterAndSort();
                this.OnListChanged( new ListChangedEventArgs( ListChangedType.Reset, -1 ) );
                return;
            }

            if ( value is not ICollection<T> )
            {
                // list is not a strongly-type collection.
                // Check that items in list are all of type T
                foreach ( object item in value )
                {
                    if ( item is not T )
                    {
                        throw new ArgumentException( string.Format( System.Globalization.CultureInfo.CurrentCulture,
                            Properties.Resources.InvalidListItemType ?? string.Empty, typeof( T ).FullName ), nameof( value ) );
                    }
                }
            }

            this.SourceLists = new object[] { value };
            this.NewItemsList = value;
        }
    }

    /// <summary> Determine if we should serialize data source. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    private bool ShouldSerializeDataSource()
    {
        return this.SourceLists.Count > 0;
    }

    /// <summary> Event handler for when SourceLists is changed. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    /// <param name="sender"> The <see cref="object" /> that raised the event. </param>
    /// <param name="e">      List changed event information. </param>
    protected override void SourceListsChanged( object? sender, ListChangedEventArgs e )
    {
        if ( sender is null || e is null )
        {
            return;
        }

        if ( (this.SourceLists.Count > 1 && e.ListChangedType == ListChangedType.ItemAdded) || e.ListChangedType == ListChangedType.ItemDeleted )
        {
            throw new InvalidOperationException( $"{nameof( cc.isr.WinForms.BindingListView<T> )} allows strictly one source list." );
        }
        else
        {
            base.SourceListsChanged( sender, e );
        }
    }
}
