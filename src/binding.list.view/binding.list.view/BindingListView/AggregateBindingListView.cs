using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using System.Reflection.Emit;

namespace cc.isr.WinForms;
/// <summary>
/// A searchable, sortable, filterable, data bindable view of a list of objects.
/// </summary>
/// <remarks>
/// David, 2018-12-15, <para>
/// David, 2018-12-15, 1.2.*, https://blogs.warwick.ac.uk/andrewdavey and
/// https://sourceforge.net/projects/blw/. (c) 2006 Andrew Davey. All rights
/// reserved.</para><para>
/// Licensed under The MIT License.</para><para>
/// </para>
/// </remarks>
[System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1010:Generic interface should also be implemented", Justification = "<Pending>" )]
public partial class AggregateBindingListView<T> : Component, IBindingListView, IList, IRaiseItemChangedEvents, ICancelAddNew, ITypedList where T : notnull
{
    #region " construction "

    /// <summary> Default constructor. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    public AggregateBindingListView()
    {
        this._sourceLists = new BindingList<IList>();
        (this._sourceLists as IBindingList)!.ListChanged += this.SourceListsChanged;
        this.SavedSourceLists = [];
        this._sourceIndices = [];

        // Start with a filter that includes all items.
        this._filter = IncludeAllItemFilter<T>.Instance;

        // Start with no sorts applied.
        this._sorts = new ListSortDescriptionCollection();
        this.ObjectViewCache = [];
    }

    /// <summary> Constructor. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="container"> The container. </param>
    public AggregateBindingListView( IContainer container ) : this()
    {
        container?.Add( this );
        if ( base.Site is ISynchronizeInvoke )
        {
            this.SynchronizingObject = (base.Site as ISynchronizeInvoke)!;
        }
    }

    #endregion

    #region " private member fields "

    /// <summary>
    /// The sorted, filtered list of item indices in _sourceList.
    /// </summary>
    private MultiSourceIndexList<T> _sourceIndices;

    /// <summary>
    /// The current filter applied to the view.
    /// </summary>
    private IItemFilter<T> _filter;

    /// <summary>
    /// The current sorts applied to the view.
    /// </summary>
    private ListSortDescriptionCollection _sorts;

    /// <summary>
    /// The <see cref="IComparer{T}">IComparer</see> used to compare items when sorting.
    /// </summary>
    private IComparer<KeyValuePair<ListItemPair<T>, int>>? _comparer;

    /// <summary>
    /// The item in the process of being added to the view.
    /// </summary>
    private ObjectView<T>? _newItem;

    /// <summary>
    /// A copy of the source lists so when a list is removed from SourceLists we still have a
    /// reference to use for unhooking events, etc.
    /// </summary>
    /// <exception cref="ArgumentNullException">      Thrown when one or more required arguments are
    /// null. </exception>
    /// <exception cref="InvalidSourceListException"> Thrown when an Invalid Source List error
    /// condition occurs. </exception>
    /// <value> The saved source lists. </value>
    private List<IList> SavedSourceLists { get; set; }

    /// <summary>
    /// ObjectView cache used to prevent re-creation of existing objectVu wrappers when in
    /// FilterAndSort().
    /// </summary>
    /// <exception cref="ArgumentNullException">      Thrown when one or more required arguments are
    /// null. </exception>
    /// <exception cref="InvalidSourceListException"> Thrown when an Invalid Source List error
    /// condition occurs. </exception>
    /// <value> The objectVu view cache. </value>
    private Dictionary<T, ObjectView<T>> ObjectViewCache { get; set; }

    #endregion

    /// <summary>
    /// The list of underlying list of items on which this view is based.
    /// </summary>
    private IList _sourceLists;

    /// <summary> Gets or sets the list of source lists used by this view. </summary>
    /// <exception cref="ArgumentNullException">      Thrown when one or more required arguments are
    /// null. </exception>
    /// <exception cref="InvalidSourceListException"> Thrown when an Invalid Source List error
    /// condition occurs. </exception>
    /// <value> The source lists. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public IList SourceLists
    {
        get => this._sourceLists;
        set
        {
            if ( value is null )
            {
                throw new ArgumentNullException( nameof( value ), Properties.Resources.SourceListsNull );
            }

            // Check that every item in each list is of type T.
            foreach ( object obj in value )
            {
                if ( obj is null )
                {
                    throw new InvalidSourceListException();
                }

                IList? list = null;
                if ( !string.IsNullOrEmpty( this.DataMember ) )
                {
                    foreach ( PropertyDescriptor pd in TypeDescriptor.GetProperties( obj ) )
                    {
                        if ( (pd.Name ?? "") == (this.DataMember ?? "") )
                        {
                            list = pd.GetValue( obj ) as IList;
                            break;
                        }
                    }
                }
                else if ( obj is IListSource src )
                {
                    list = src.ContainsListCollection ? src.GetList()[0] as IList : src.GetList();
                }
                else if ( obj is not ICollection<T> )
                {
                    list = obj as IList;
                }
                else
                {
                    // We have a typed collection, so can skip the item-by-item check.
                    continue;
                }

                if ( list is null )
                {
                    throw new InvalidSourceListException();
                }

                foreach ( object item in list )
                {
                    if ( item is not T )
                    {
                        throw new InvalidSourceListException( string.Format( System.Globalization.CultureInfo.CurrentCulture,
                            Properties.Resources.InvalidListItemType ?? string.Empty, typeof( T ).FullName ) );
                    }
                }
            }


            // Unhook old list changed event.
            if ( this._sourceLists is IBindingList bindingList && bindingList.SupportsChangeNotification )
            {
                bindingList.ListChanged -= this.SourceListsChanged;
            }

            foreach ( object list in this._sourceLists )
            {
                if ( list is IBindingList bl && bl.SupportsChangeNotification )
                {
                    bl.ListChanged -= this.SourceListChanged;
                }
            }

            this._sourceLists = value;
            if ( this._sourceLists is IBindingList bindingList1 && bindingList1.SupportsChangeNotification )
            {
                // Hook new list changed event
                bindingList1.ListChanged += this.SourceListsChanged;
            }

            foreach ( object list in this._sourceLists )
            {
                if ( list is IBindingList bl && bl.SupportsChangeNotification )
                {
                    bl.ListChanged += this.SourceListChanged;
                }
            }

            // save new lists
            this.BuildSavedList();
            this.FilterAndSort();
            this.OnListChanged( ListChangedType.Reset, -1 );
        }
    }

    /// <summary> Gets the ObjectView{T} of the item at the given index in the view. </summary>
    /// <value> The ObjectView{T} of the item. </value>
    public ObjectView<T> this[int index] => this._sourceIndices[index].Key.Item;

    private string? _dataMember;

    /// <summary>
    /// Gets or sets the property on a source list item that contains the actual list to view.
    /// If null or empty then the source list item is used instead.
    /// </summary>
    /// <value> The data member. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string? DataMember
    {
        get => this._dataMember;
        set
        {
            this._dataMember = value;
            this.FilterAndSort();
            this.OnListChanged( ListChangedType.Reset, -1 );
        }
    }

    #region " adding new items "

    /// <summary>
    /// Attempts to get a new <typeparamref name="T"/> objectVu to add to the list, first by raising
    /// the AddingNew event and then (if no new objectVu was assigned) by using the default public
    /// constructor.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    /// <returns> The new objectVu to add to the list. </returns>
    protected virtual T OnAddingNew()
    {
        // We allow users of this class to provide the objectVu to add
        // by raising the AddingNew event.
        AddingNewEventHandler? handler = this.AddingNew;
        if ( handler is not null )
        {
            AddingNewEventArgs args = new();
            this.OnAddingNew( args );
            // Check if we were given an objectVu (and it's the correct type)
            if ( args.NewObject is not null and T t )
            {
                return t;
            }
        }
        // Otherwise, try the default public constructor instead.
        // Use reflection to find it. Note: We're not using the generic new() constraint since
        // we do not want to force the need for a public default constructor when the user
        // can simply handle the AddingNew event called above.
        ConstructorInfo? ci = typeof( T ).GetConstructor( Type.EmptyTypes );
        if ( ci is not null )
        {
            // Invoke the constructor to create the objectVu.
            return ( T ) ci.Invoke( null );
        }
        else
        {
            throw new InvalidOperationException( Properties.Resources.CannotAddNewItem );
        }
    }

    /// <summary>
    /// Adds a new item to the view. Note that EndNew must be called to commit the item to the to the
    /// source list.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <returns> The new item, wrapped in an ObjectView<typeparamref name="T"/>. </returns>
    public ObjectView<T> AddNew()
    {
        // Are we currently adding another item?
        if ( this._newItem is not null )
        {
            // Need to commit previous new item before adding another.
            this.EndNew( this._sourceIndices.Count - 1 );
        }

        // Get the new item to add.
        T item = this.OnAddingNew();

        // Create the ObjectView<T> wrapper for the item.
        ObjectView<T> objectView = new( item, this );
        this.ObjectViewCache[item] = objectView;
        this.HookPropertyChangedEvent( objectView );

        // Set the _newItem reference so we know what to use when ending/canceling this add operation.
        this._newItem = objectView;

        // Add to indexes list, but index of -1 means it's not in the source list yet.

        this._sourceIndices.Add( this._newItemsList!, objectView, -1 );

        // Tell any data binders that we've added an item to the view.
        // Put it at the end of the list.

        this.OnListChanged( ListChangedType.ItemAdded, this._sourceIndices.Count - 1 );
#pragma warning disable CS8619 // Nullability of reference types in value doesn't match target type.
        return objectView;
#pragma warning restore CS8619
    }

    /// <summary>
    /// Cancels the pending addition of a new item to the source list and remove the item from the
    /// view.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="itemIndex"> The index of the new item. </param>
    public void CancelNew( int itemIndex )
    {
        // We must take special care that the item index does refer to the new item.
        if ( itemIndex > -1 && itemIndex < this._sourceIndices.Count && this._newItem is not null && ReferenceEquals( this._sourceIndices[itemIndex].Key.Item, this._newItem ) )
        {
            // We no longer need to listen to any events from the objectVu.
            this.UnHookPropertyChangedEvent( this._newItem );
            // Remove the item from the view.
            this._sourceIndices.RemoveAt( itemIndex );
            // Data binders need to know the item has gone from the view.
            this.OnListChanged( ListChangedType.ItemDeleted, itemIndex );
            // Done with this adding operation, so clear the _newItem reference.
            this._newItem = null;
        }
    }

    /// <summary> Commits the pending addition of a new item to the source list. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="itemIndex"> The index of the new item. </param>
    public void EndNew( int itemIndex )
    {
        // The binding infrastructure tends to call the method
        // more times than needed and often with itemIndex not even pointing to the 
        // new objectVu! So we have to take special care to check.
        if ( this._newItemsList is not null && itemIndex > -1 && itemIndex < this._sourceIndices.Count && this._newItem is not null
            && ReferenceEquals( this._sourceIndices[itemIndex].Key.Item, this._newItem ) )
        {
            // In order to reuse the SourceListChanged code for adding a new item
            // we have to first remove all knowledge of the item, then add it 
            // to the source list.

            // We no longer need to listen to any events from the objectVu.
            this.UnHookPropertyChangedEvent( this._newItem );

            // Remove the item from the view.
            this._sourceIndices.RemoveAt( itemIndex );

            // Add the actual data objectVu to the source list.
            // The SourceListChanged event handler will take care of correctly inserting this
            // objectVu into the view (if newItemsList is a IBindingList).
            _ = this._newItemsList.Add( this._newItem.EditableObject );

            // If it is not an IBindingList (or not SupportsChangeNotification) 
            // then we must force the update ourselves.
            if ( this._newItemsList is not IBindingList || !((this._newItemsList as IBindingList)?.SupportsChangeNotification ?? false) )
            {
                this.FilterAndSort();
                this.OnListChanged( ListChangedType.Reset, -1 );
            }

            // Done with this adding operation, so clear the _newItem reference.
            this._newItem = null;
        }
    }

    /// <summary>
    /// The IList we will add new items to.
    /// </summary>
    private IList? _newItemsList;

    /// <summary> Gets or sets the source list to which new items are added. </summary>
    /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    /// illegal values. </exception>
    /// <value> A List of new items. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public IList? NewItemsList
    {
        get => this._newItemsList;
        set
        {
            if ( value is object && !this._sourceLists.Contains( value ) )
            {
                throw new ArgumentException( Properties.Resources.SourceListNotFound );
            }

            this._newItemsList = value;
        }
    }

    #endregion

    /// <summary> Re-applies any current filter and sorts to refresh the current view. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    public void Refresh()
    {
        this.FilterAndSort();
        // Get any bound objects to refresh everything as well.
        this.OnListChanged( ListChangedType.Reset, -1 );
    }

    /// <summary>
    /// Gets the objectVu used to marshal event-handler calls that are invoked on a non-UI thread.
    /// </summary>
    /// <exception cref="InvalidSourceListException"> Thrown when an Invalid Source List error
    /// condition occurs. </exception>
    /// <exception cref="ArgumentNullException">      Thrown when one or more required arguments are
    /// null. </exception>
    /// <value> The synchronizing objectVu. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public ISynchronizeInvoke? SynchronizingObject { get; set; }

    /// <summary>
    /// Updates the _sourceIndices list to contain the items that are current viewed according to
    /// applied filter and sorts.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    protected void FilterAndSort()
    {
        // The view contains items from the source list
        // and possibly a new items that are not yet committed.
        // Therefore we can't just clear the list and start over
        // as we would lose the new items. So we have to to insert
        // filtered source list items into a new list first.
        // New items can then be pulled out of the current view
        // and appended to the new list.
        MultiSourceIndexList<T> newList = [];

        // Get items from the source list that are included by the current filter.
        foreach ( IList? sourceList in this.EnumerateSourceLists() )
        {
            ObjectView<T>? editableObject;
            if ( sourceList is null ) continue;
            for ( int i = 0, loopTo = sourceList.Count - 1; i <= loopTo; i++ )
            {
                if ( sourceList[i] is not T item ) continue;
                if ( this._filter.Include( item ) )
                {
                    if ( this.ObjectViewCache.TryGetValue( item, out editableObject ) )
                    {
                    }
                    else
                    {
                        editableObject = new ObjectView<T>( item, this );
                        this.ObjectViewCache.Add( item, editableObject );
                        // Listen to the editing notification and property changed events.
                        this.HookEditableObjectEvents( editableObject );
                        this.HookPropertyChangedEvent( editableObject );
                    }

                    // Add the editable objectVu along with the index of the item in the source list.
                    if ( editableObject is not null )
                        newList.Add( sourceList, editableObject, i );
                }
                else if ( this.ObjectViewCache.TryGetValue( item, out editableObject ) )
                {
                    this.UnHookEditableObjectEvents( editableObject );
                    this.UnHookPropertyChangedEvent( editableObject );
                    _ = this.ObjectViewCache.Remove( item );
                }
            }
        }

        // If we have sorts to apply, do them now
        if ( this._comparer is object )
        {
            newList.Sort( this._comparer );
        }

        // Now we can append any new items to the end of the view.
        foreach ( KeyValuePair<ListItemPair<T>, int> kvp in this._sourceIndices )
        {
            // New items have a source list index of -1 since they are not
            // yet in the source list.
            if ( kvp.Value == -1 )
            {
                newList.Add( kvp );
            }
        }

        // Set our view now
        this._sourceIndices = newList;

        // Note: We do not raise the ListChanged event with ListChangeType.Reset
        // since the view may not have changed that much. It is better to let
        // the calling code decide what has happened and raise events accordingly.
    }

    #region " editing items event handlers "

    /// <summary> Begun item edit. </summary>
    /// <remarks>
    /// Currently unused. Here in case we want to perform actions when an item edit begins.
    /// </remarks>
    /// <param name="sender"> The <see cref="ObjectView{T}"/> that raised the event. </param>
    /// <param name="e">      Event information. </param>
    protected virtual void OnItemEditBegun( object? sender, EventArgs e )
    {
    }

    /// <summary> Canceled item edit. </summary>
    /// <remarks>
    /// Currently unused. Here in case we want to perform actions when an item edit is canceled.
    /// </remarks>
    /// <param name="sender"> The <see cref="ObjectView{T}"/> that raised the event. </param>
    /// <param name="e">      Event information. </param>
    protected virtual void OnItemEditCanceled( object? sender, EventArgs e )
    {
    }

    /// <summary> Handles the <see cref="ObjectView{T}"/> EndedEdit event. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="sender"> The <see cref="ObjectView{T}"/> that raised the event. </param>
    /// <param name="e">      Event information. </param>
    protected virtual void OnItemEditEnded( object? sender, EventArgs e )
    {
        if ( sender is ObjectView<T> editableObject )
        {
            // Check if filtering removed the item from view
            // by getting the index before and after
            int oldIndex = this._sourceIndices.IndexOfItem( editableObject.EditableObject );
            this.FilterAndSort();
            int newIndex = this._sourceIndices.IndexOfItem( editableObject.EditableObject );
            // if item was filtered out then the newIndex == -1
            if ( newIndex > -1 )
            {
                if ( oldIndex == newIndex )
                {
                    this.OnListChanged( ListChangedType.ItemChanged, newIndex );
                }
                else
                {
                    this.OnListChanged( ListChangedType.ItemMoved, newIndex, oldIndex );
                }
            }
            else
            {
                this.OnListChanged( ListChangedType.ItemDeleted, oldIndex );
            }
        }
    }

    #endregion

    /// <summary> Event handler for when SourceLists is changed. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <exception cref="InvalidSourceListException"> Thrown when an Invalid Source List error
    /// condition occurs. </exception>
    /// <param name="sender"> The <see cref="ObjectView{T}"/> that raised the event. </param>
    /// <param name="e">      List changed event information. </param>
    protected virtual void SourceListsChanged( object? sender, ListChangedEventArgs e )
    {
        if ( sender is null || e is null )
        {
            return;
        }

        if ( e.ListChangedType == ListChangedType.ItemAdded )
        {
            if ( this.SourceLists[e.NewIndex] is not IList list )
            {
                this.SourceLists.RemoveAt( e.NewIndex );
                throw new InvalidSourceListException();
            }

            if ( list is IBindingList l )
            {
                // We need to know when the source list changes
                l.ListChanged += this.SourceListChanged;
            }

            this.SavedSourceLists.Add( list );
            this.FilterAndSort();
            this.OnListChanged( ListChangedType.Reset, -1 );
        }
        else if ( e.ListChangedType == ListChangedType.ItemDeleted )
        {
            if ( this.SavedSourceLists[e.NewIndex] is IList list )
            {
                if ( list is IBindingList l )
                {
                    l.ListChanged -= this.SourceListChanged;
                }

                this.SavedSourceLists.RemoveAt( e.NewIndex );
                this.FilterAndSort();
                this.OnListChanged( ListChangedType.Reset, -1 );
            }
        }
        else if ( e.ListChangedType == ListChangedType.Reset )
        {
            this.BuildSavedList();
            this.FilterAndSort();
            this.OnListChanged( ListChangedType.Reset, -1 );
        }
    }

    /// <summary> Event handler for when a source list changes. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="sender"> The <see cref="ObjectView{T}"/> that raised the event. </param>
    /// <param name="e">      List changed event information. </param>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0010:Add missing cases", Justification = "<Pending>" )]
    private void SourceListChanged( object? sender, ListChangedEventArgs e )
    {
        int oldIndex;
        int newIndex;
        if ( sender is not IBindingList sourceList ) return;

        switch ( e.ListChangedType )
        {
            case ListChangedType.ItemAdded:
                {
                    this.FilterAndSort();
                    // Get the index of the newly sorted item
                    newIndex = this._sourceIndices.IndexOfSourceIndex( sourceList, e.NewIndex );
                    if ( newIndex > -1 )
                    {
                        this.OnListChanged( ListChangedType.ItemAdded, newIndex );
                        // Other items have moved down the list
                        for ( int i = newIndex + 1, loopTo = this.Count - 1; i <= loopTo; i++ )
                        {
                            this.OnListChanged( ListChangedType.ItemMoved, i - 1, i );
                        }
                    }
                    else
                    {
                        // The item was excluded by the filter,
                        // so to the viewer the item has been "deleted".
                        // The new item will have been added at the end of the view
                        this.OnListChanged( ListChangedType.ItemDeleted, Math.Max( this.Count - 1, 0 ) );
                    }

                    break;
                }

            case ListChangedType.ItemChanged:
                {
                    // Check if filtering will remove the item from view
                    // by getting the index before and after
                    oldIndex = this._sourceIndices.IndexOfSourceIndex( sourceList, e.NewIndex );

                    // Is the objectVu in our view?
                    if ( oldIndex < 0 )
                    {
                        return;
                    }

                    this.FilterAndSort();
                    newIndex = this._sourceIndices.IndexOfSourceIndex( sourceList, e.NewIndex );
                    // if item was filtered out then the newIndex == -1
                    // otherwise we can say that the item was changed.
                    if ( newIndex > -1 )
                    {
                        if ( newIndex == oldIndex )
                        {
                            this.OnListChanged( ListChangedType.ItemChanged, newIndex );
                        }
                        else
                        {
                            // Two items will have changed places
                            this.OnListChanged( ListChangedType.ItemMoved, newIndex, oldIndex );
                        }
                    }
                    else
                    {
                        this.OnListChanged( ListChangedType.ItemDeleted, oldIndex );
                    }

                    break;
                }

            case ListChangedType.ItemDeleted:
                {
                    // Find the deleted index
                    newIndex = this._sourceIndices.IndexOfSourceIndex( sourceList, e.NewIndex );

                    // Did we have the objectVu in our view?
                    if ( newIndex < 0 )
                    {
                        return;
                    }

                    // Stop listening to it's events
                    this.UnHookEditableObjectEvents( this._sourceIndices[newIndex].Key.Item );
                    this.UnHookPropertyChangedEvent( this._sourceIndices[newIndex].Key.Item );
                    // Remove its index
                    this._sourceIndices.RemoveAt( newIndex );
                    // Move up indexes after removed item
                    for ( int i = 0, loopTo1 = this._sourceIndices.Count - 1; i <= loopTo1; i++ )
                    {
                        if ( this._sourceIndices[i].Value > e.NewIndex )
                        {
                            this._sourceIndices[i] = new KeyValuePair<ListItemPair<T>, int>( this._sourceIndices[i].Key, this._sourceIndices[i].Value - 1 );
                        }
                    }
                    // Inform listeners that an item has been deleted from this view
                    this.OnListChanged( ListChangedType.ItemDeleted, newIndex );
                    break;
                }

            case ListChangedType.ItemMoved:
                {
                    if ( !this.IsSorted && this.Filter is IncludeAllItemFilter<T> )
                    {
                        // We can move the item in the view
                        // note indexes match those in _sourceList
                        this.OnListChanged( ListChangedType.ItemMoved, e.NewIndex, e.OldIndex );
                    }

                    break;
                }
            // Otherwise it makes no sense to move due to sort and/or filter

            case ListChangedType.Reset:
                {
                    // Most of the source list has changed
                    // so re-sort and filter
                    this.FilterAndSort();
                    // The view is most likely to have changed lots as well
                    this.OnListChanged( ListChangedType.Reset, -1 );
                    break;
                }

            default:
                break;
        }
    }

    /// <summary> Event handler for when an item in the view changes. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="sender"> The item that changed. </param>
    /// <param name="e">      Property changed event information. </param>
    private void ItemPropertyChanged( object? sender, PropertyChangedEventArgs e )
    {
        if ( sender is not T tItem ) return;
        // The changed item may not actually be present in the view
        int index = this._sourceIndices.IndexOfItem( tItem );
        // Test the returned index, -1 => not in the view.
        if ( index > -1 )
        {
            // Tell listeners that an item has changed.
            // This is inline with the IRaiseItemChangedEvents implementation.
            this.OnListChanged( ListChangedType.ItemChanged, index );
        }
    }

    #region " filtering "

    /// <summary> Applies the filter described by includeItem. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="filter"> The current filter applied to the view. </param>
    public void ApplyFilter( IItemFilter<T> filter )
    {
        this.Filter = filter;
    }

    /// <summary> Applies the filter described by includeItem. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="includeItem"> The include item. </param>
    public void ApplyFilter( Predicate<T> includeItem )
    {
        if ( includeItem is null )
        {
            throw new ArgumentNullException( nameof( includeItem ), Properties.Resources.IncludeDelegateCannotBeNull );
        }

        this.Filter = CreateItemFilter( includeItem );
    }

    /// <summary> Gets if this view supports filtering of items. Always returns true. </summary>
    /// <value> The i binding list view supports filtering. </value>
    bool IBindingListView.SupportsFiltering => true;

    /// <summary> Gets or sets the binding list view filter. </summary>
    /// <remarks> Explicitly implemented to expose the stronger Filter property instead. </remarks>
    /// <exception cref="NotSupportedException"> Thrown when the requested operation is not supported. </exception>
    /// <value> The i binding list view filter. </value>
    string? IBindingListView.Filter
    {
        get => this.Filter.ToString();

        // TODO: Re-instate this line once we have an expression filter// Filter = new ExpressionItemFilter<T>(value);
        set => throw new NotSupportedException( "Cannot set filter from string expression." );
    }

    /// <summary> Gets or sets the filter currently applied to the view. </summary>
    /// <value> The filter. </value>
    public IItemFilter<T> Filter
    {
        get => this._filter;
        set
        {
            // Do not allow a null filter. Instead, use the "include all items" filter.
            value ??= IncludeAllItemFilter<T>.Instance;

            if ( !ReferenceEquals( this._filter, value ) )
            {
                this._filter = value;
                this.FilterAndSort();
                // The list has probably changed a lot, so get bound controls to reset.
                this.OnListChanged( ListChangedType.Reset, -1 );
            }
        }
    }

    /// <summary> Determine if we should serialize filter. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    private bool ShouldSerializeFilter()
    {
        return !ReferenceEquals( this.Filter, IncludeAllItemFilter<T>.Instance );
    }

    /// <summary> Creates item filter. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="predicate"> The predicate. </param>
    /// <returns> The new item filter. </returns>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1000:Do not declare static members on generic types", Justification = "<Pending>" )]
    public static IItemFilter<T> CreateItemFilter( Predicate<T> predicate )
    {
        return predicate is null ? throw new ArgumentNullException( nameof( predicate ) ) : new PredicateItemFilter<T>( predicate );
    }

    /// <summary>
    /// Removes any currently applied filter so that all items are displayed by the view.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    public void RemoveFilter()
    {
        // Set filter back to including all items.
        this.Filter = IncludeAllItemFilter<T>.Instance;
    }

    #endregion

    #region " sorting "

    /// <summary>
    /// Used to signal that a sort on a property is to be descending, not ascending.
    /// </summary>
    /// <value> The sort descending modifier. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string SortDescendingModifier { get; private set; } = "DESC";

    /// <summary> The character used to separate sorts by multiple properties. </summary>
    /// <value> The sort delimiter. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public char SortDelimiter { get; private set; } = ',';

    /// <summary> Gets if this view supports sorting. Always returns true. </summary>
    /// <value> The i binding list supports sorting. </value>
    bool IBindingList.SupportsSorting => true;

    /// <summary> Gets if this view supports advanced sorting. Always returns true. </summary>
    /// <value> The i binding list view supports advanced sorting. </value>
    bool IBindingListView.SupportsAdvancedSorting => true;

    /// <summary>
    /// Sorts the view by a single property in a given direction. This will remove any existing sort.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="property">   A property of <typeparamref name="T"/> to sort by. </param>
    /// <param name="direction">  The direction to sort in. </param>
#if NET5_0_OR_GREATER
    // sort not working under .NET 5 at this time. The IL Generator emits incorrect IL code.
    [Obsolete( "Stopped working after porting to NET 5.0; TODO: construct the Comparer from Comparison functions." )]
#else
#endif
    public void ApplySort( PropertyDescriptor property, ListSortDirection direction )
    {
        // Apply sort by setting the current sort descriptions
        // to be a collection containing just one SortDescription.
        this.SortDescriptions = new ListSortDescriptionCollection( [new( property, direction )] );
    }

    /// <summary> Sorts the view by the given collection of sort descriptions. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="sorts"> The sorts to apply. </param>
#if NET5_0_OR_GREATER
    // sort not working under .NET 5 at this time. The IL Generator emits incorrect IL code.
    [Obsolete( "Stopped working after porting to NET 5.0; TODO: construct the Comparer from Comparison functions." )]
#else
#endif
    public void ApplySort( ListSortDescriptionCollection sorts )
    {
        this.SortDescriptions = sorts;
    }

    /// <summary>
    /// Sorts the view according to the properties and directions given in the SQL style sort
    /// parameter.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="sort"> The SQL ORDER BY clause style sort. A comma separated list of properties
    /// to sort by. Use "DESC" after a property name to sort descending. The
    /// default direction is ascending. </param>
    /// <example> <code>view.ApplySort("Surname, FirstName, Age DESC");</code> </example>
#if NET5_0_OR_GREATER
    // sort not working under .NET 5 at this time. The IL Generator emits incorrect IL code.
    [Obsolete( "Stopped working after porting to NET 5.0; TODO: construct the Comparer from Comparison functions." )]
#else
#endif
    public void ApplySort( string sort )
    {
        if ( string.IsNullOrEmpty( sort ) )
        {
            this.RemoveSort();
            return;
        }

        // Parse string for sort descriptions
        string[] sorts = sort.Split( this.SortDelimiter );
        ListSortDescription[] col = new ListSortDescription[sorts.Length];
        for ( int i = 0, loopTo = sorts.Length - 1; i <= loopTo; i++ )
        {
            // Get the sort description.
            // This will be a name optionally followed by a direction.
            sort = sorts[i].Trim();
            // A space will separate name from direction.
            int pos = sort.IndexOf( ' ' );
            string name;
            ListSortDirection direction;
            if ( pos == -1 )
            {
                // No direction specified, default to ascending.
                name = sort;
                direction = ListSortDirection.Ascending;
            }
            else
            {
                // Name is everything before the space.
                // name = sort.Substring( 0, pos );
                name = sort[..pos];
                // direction is everything after the space.
                string dir = sort[(pos + 1)..].Trim();

                // Check what kind of direction is specified.
                // (Ignoring case and culture.)
                if ( string.Equals( dir, this.SortDescendingModifier, StringComparison.OrdinalIgnoreCase ) )
                {
                    direction = ListSortDirection.Descending;
                }
                else
                {
                    // Default to ascending.
                    direction = ListSortDirection.Ascending;
                }
            }

            // Put the sort description into the collection.
            col[i] = this.CreateListSortDescription( name, direction );
        }

        this.ApplySort( new ListSortDescriptionCollection( col ) );
    }

    /// <summary> Sorts the view by the given collection of sort descriptions. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="comparer"> The <see cref="IComparer{T}">IComparer</see>
    /// used to compare items when sorting. </param>
    public void ApplySort( IComparer<T> comparer )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( comparer, nameof( comparer ) );
#else
        if ( comparer is null ) throw new ArgumentNullException( nameof( comparer ) );
#endif

        // Clear any current sorts
        this._sorts = new ListSortDescriptionCollection();
        // Sort with this new comparer
        this._comparer = new ExternalSortComparer<T>( comparer );
        this.FilterAndSort();
        this.OnListChanged( ListChangedType.Reset, -1 );
    }

    /// <summary> Sorts the view by the given collection of sort descriptions. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="comparison"> The comparison. </param>
    public void ApplySort( Comparison<T> comparison )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( comparison, nameof( comparison ) );
#else
        if ( comparison is null ) throw new ArgumentNullException( nameof( comparison ) );
#endif

        // Clear any current sorts
        this._sorts = new ListSortDescriptionCollection();
        // Sort with this new comparer
        this._comparer = new ExternalSortComparison<T>( comparison );
        this.FilterAndSort();
        this.OnListChanged( ListChangedType.Reset, -1 );
    }

    /// <summary>
    /// Removes any sort currently applied to the view, restoring it to the order of the source list.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
#if NET5_0_OR_GREATER
    // sort not working under .NET 5 at this time. The IL Generator emits incorrect IL code.
    [Obsolete( "Stopped working after porting to NET 5.0; TODO: construct the Comparer from Comparison functions." )]
#else
#endif
    public void RemoveSort()
    {
        // An empty collection of sorts will achieve what we need.
        this.SortDescriptions = new ListSortDescriptionCollection();
    }

    /// <summary> Gets if the view is currently sorted. </summary>
    /// <value> The is sorted. </value>
    [Browsable( false )]
    public bool IsSorted =>
            // To be sorted there must be some sorts applied.
            this._comparer is object; // this.SortDescriptions.Count > 0;

    /// <summary>
    /// Gets or sets the string representation of the sort currently applied to the view.
    /// </summary>
    /// <value> The sort. </value>
#if NET5_0_OR_GREATER
    // sort not working under .NET 5 at this time. The IL Generator emits incorrect IL code.
    [Obsolete( "Stopped working after porting to NET 5.0; TODO: construct the Comparer from Comparison functions." )]
#else
#endif
    public string Sort
    {
        get
        {
            if ( this.IsSorted )
            {
                // Build a string of the properties being sorted by
                System.Text.StringBuilder sb = new();
                foreach ( ListSortDescription sortDescription in this.SortDescriptions )
                {
                    if ( sortDescription.PropertyDescriptor is not null )
                    {
                        _ = sb.Append( sortDescription.PropertyDescriptor.Name );
                        // Need to signal descending sorts
                        if ( sortDescription.SortDirection == ListSortDirection.Descending )
                        {
                            _ = sb.Append( ' ' ).Append( this.SortDescendingModifier );
                        }
                        // Separate by SortDelimiter
                        _ = sb.Append( this.SortDelimiter );
                    }
                }
                // Remove trailing SortDelimiter
                _ = sb.Remove( sb.Length - 1, 1 );
                // Return the string
                return sb.ToString();
            }

            return string.Empty;
        }

        set => this.ApplySort( value );
    }

    /// <summary> Determine if we should serialize sort. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
#if NET5_0_OR_GREATER
    // sort not working under .NET 5 at this time. The IL Generator emits incorrect IL code.
    [Obsolete( "Stopped working after porting to NET 5.0; TODO: construct the Comparer from Comparison functions." )]
#else
#endif
    private bool ShouldSerializeSort()
    {
        return !string.IsNullOrEmpty( this.Sort );
    }

    /// <summary>
    /// Gets the direction in which the view is sorted. If more than one sort is applied, the
    /// direction of the first is returned.
    /// </summary>
    /// <value> The sort direction. </value>
    [Browsable( false )]
#if NET5_0_OR_GREATER
    // sort not working under .NET 5 at this time. The IL Generator emits incorrect IL code.
    [Obsolete( "Stopped working after porting to NET 5.0; TODO: construct the Comparer from Comparison functions." )]
#else
#endif
    public ListSortDirection SortDirection
    {
        get
        {
            if ( this.IsSorted )
            {
                return this.SortDescriptions[0]!.SortDirection;
            }
            else
            {
                // We don't really want to throw exceptions.
                // Calling code should have checked IsSorted to know the true situation.
                return ListSortDirection.Ascending;
            }
        }
    }

    /// <summary>
    /// Gets the property the view is currently sorted by. If more than one sort is applied, the
    /// property of the first is returned.
    /// </summary>
    /// <value> The sort property. </value>
    [Browsable( false )]
#if NET5_0_OR_GREATER
    // sort not working under .NET 5 at this time. The IL Generator emits incorrect IL code.
    [Obsolete( "Stopped working after porting to NET 5.0; TODO: construct the Comparer from Comparison functions." )]
#else
#endif
    public PropertyDescriptor? SortProperty
    {
        get
        {
            if ( this.IsSorted )
            {
                return this.SortDescriptions[0]!.PropertyDescriptor;
            }
            else
            {
                // We don't really want to throw exceptions.
                // Calling code should have checked IsSorted to know the true situation.
                return null;
            }
        }
    }

    /// <summary> Gets or sets the sorts currently applied to the view. </summary>
    /// <value> The sort descriptions. </value>
    [Browsable( false )]
#if NET5_0_OR_GREATER
    // sort not working under .NET 5 at this time. The IL Generator emits incorrect IL code.
    [Obsolete( "Stopped working after porting to NET 5.0; TODO: construct the Comparer from Comparison functions." )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
#else
#endif
    public ListSortDescriptionCollection SortDescriptions
    {
        get => this._sorts;

        private set
        {
            this._sorts = value;
            this._comparer = new SortComparer( value );
            this.FilterAndSort();
            // Most of the list will have probably changed, so get bound objects to reset.
            this.OnListChanged( ListChangedType.Reset, -1 );
        }
    }

    /// <summary>
    /// Used to compare items in the view when sorting the _sourceIndices list. It supports multiple
    /// sorts by different properties and directions.
    /// </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-15 </para>
    /// </remarks>
    private class SortComparer : IComparer<KeyValuePair<ListItemPair<T>, int>>
    {
        /// <summary> The comparisons. </summary>
        /// <value> The comparisons. </value>
        private IDictionary<ListSortDescription, Comparison<T>> Comparisons { get; set; }

        /// <summary> Creates a new SortComparer that will use the given sorts. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="sorts"> The sorts to apply to the view. </param>
#if NET5_0_OR_GREATER
        // sort not working under .NET 5 at this time. The IL Generator emits incorrect IL code.
        [Obsolete( "Stopped working after porting to NET 5.0; TODO: construct the Comparer from Comparison functions." )]
#else
#endif
        public SortComparer( ListSortDescriptionCollection sorts )
        {
            this._sorts = sorts;

            // Build the delegates used to compare properties of objects
            this.Comparisons = new Dictionary<ListSortDescription, Comparison<T>>();
            foreach ( ListSortDescription sort in sorts )
            {
                this.Comparisons[sort] = BuildComparison( sort.PropertyDescriptor!.Name, sort.SortDirection )!;
            }
        }

        /// <summary>   Creates a new SortComparer that will use the given sorts. </summary>
        /// <remarks>   David, 2021-03-02. </remarks>
        /// <param name="comparisons">  The comparisons. </param>
        public SortComparer( IDictionary<ListSortDescription, Comparison<T>> comparisons )
        {
            this._sorts = new ListSortDescriptionCollection( [.. comparisons.Keys] );
            this.Comparisons = comparisons;
        }

        /// <summary> The sorts. </summary>
        private readonly ListSortDescriptionCollection _sorts;

        /// <summary> Compares two items according to the defined sorts. </summary>
        /// <remarks>
        /// Use of light-weight code generation comparison delegates gives ~10x speed up compared to the
        /// pure reflection based implementation.
        /// </remarks>
        /// <param name="x"> The first item to compare. </param>
        /// <param name="y"> The second item to compare. </param>
        /// <returns> -1 if x &lt; y, 0 if x = y and 1 if x &gt; y. </returns>
        public int Compare( KeyValuePair<ListItemPair<T>, int> x, KeyValuePair<ListItemPair<T>, int> y )
        {
            foreach ( ListSortDescription sort in this._sorts )
            {
                int result = this.Comparisons[sort]( x.Key.Item.EditableObject, y.Key.Item.EditableObject );
                if ( result != 0 )
                {
                    return result;
                }
            }

            return 0;
        }

        /// <summary> Builds a comparison. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="propertyName"> Name of the property. </param>
        /// <param name="direction">    The direction. </param>
        /// <returns> A Comparison{T} </returns>
#if NET5_0_OR_GREATER
        // sort not working under .NET 5 at this time. The IL Generator emits incorrect IL code.
        [Obsolete( "Stopped working after porting to NET 5.0; TODO: construct the Comparer from Comparison functions." )]
#else
#endif
        private static Comparison<T> BuildComparison( string propertyName, ListSortDirection direction )
        {
            PropertyInfo? pi = typeof( T ).GetProperty( propertyName );
            Debug.Assert( pi is not null, $"Property '{propertyName}' is not a member of type '{typeof( T ).FullName}'" );
            if ( pi == null )
            {
                return ( o1, o2 ) => o1.Equals( o2 ) ? 0 : string.Compare( o1.ToString(), o2.ToString(), StringComparison.Ordinal );
            }
            if ( typeof( IComparable ).IsAssignableFrom( pi.PropertyType ) )
            {
                if ( pi.PropertyType.IsValueType )
                {
                    return BuildValueTypeComparison( pi, direction );
                }
                else
                {
                    GetPropertyDelegate getProperty = BuildGetPropertyMethod( pi );
                    return ( x, y ) =>
                    {
                        int result;
                        object? value1 = getProperty( x );
                        object? value2 = getProperty( y );
                        result = value1 is not null && value2 is not null
                            ? (value1 is IComparable v1)
                                ? v1.CompareTo( value2 )
                                    : value1 is null && value2 is not null
                                    ? -1
                                : value1 is not null && value2 is null
                                    ? 1
                                    : 0
                                : -1;

                        if ( direction == ListSortDirection.Descending )
                        {
                            result *= -1;
                        }

                        return result;
                    };
                }
            }
            else if ( pi.PropertyType.IsGenericType && pi.PropertyType.GetGenericTypeDefinition().Equals( typeof( object ) ) )
            {
                return BuildNullableComparison( pi, direction );
            }
            else
            {
                // Return o1.ToString().CompareTo(o2.ToString())
                return ( o1, o2 ) => o1.Equals( o2 ) ? 0 : string.Compare( o1.ToString(), o2.ToString(), StringComparison.Ordinal );
            }
        }

        /// <summary> The delegate. </summary>
        private delegate object GetPropertyDelegate( T obj );

        /// <summary> Builds get property method. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="pi"> The pi. </param>
        /// <returns> A GetPropertyDelegate. </returns>
#if NET5_0_OR_GREATER
        // sort not working under .NET 5 at this time. The IL Generator emits incorrect IL code.
        [Obsolete( "Stopped working after porting to NET 5.0; TODO: construct the Comparer from Comparison functions." )]
#else
#endif
        private static GetPropertyDelegate BuildGetPropertyMethod( PropertyInfo pi )
        {
            MethodInfo? getMethod = pi.GetGetMethod();
            Debug.Assert( getMethod is not null );
            DynamicMethod dm = new( "__blw_get_" + pi.Name, typeof( object ), [typeof( T )], typeof( T ), true );
            ILGenerator il = dm.GetILGenerator();
            il.Emit( OpCodes.Ldarg_0 );
            il.EmitCall( OpCodes.Call, getMethod, null );

            // Return the result of the comparison.
            il.Emit( OpCodes.Ret );

            // Create the delegate pointing at the dynamic method.
            return ( GetPropertyDelegate ) dm.CreateDelegate( typeof( GetPropertyDelegate ) );
        }


        /// <summary> Builds reference type comparison. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="pi">        The pi. </param>
        /// <param name="direction"> The direction. </param>
        /// <returns> A Comparison{T} </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
#if NET5_0_OR_GREATER
        // sort not working under .NET 5 at this time. The IL Generator emits incorrect IL code.
        [Obsolete( "Stopped working after porting to NET 5.0; TODO: construct the Comparer from Comparison functions." )]
#else
#endif
        private static Comparison<T> BuildRefTypeComparison( PropertyInfo pi, ListSortDirection direction )
        {
            MethodInfo? getMethod = pi.GetGetMethod();
            Debug.Assert( getMethod is not null );
            DynamicMethod dm = new( "Get" + pi.Name, typeof( int ), [typeof( T ), typeof( T )], typeof( T ), true );
            ILGenerator il = dm.GetILGenerator();

            // Get the value of the first objectVu's property.
            il.Emit( OpCodes.Ldarg_0 );
            il.EmitCall( OpCodes.Call, getMethod, null );

            // Get the value of the second objectVu's property.
            il.Emit( OpCodes.Ldarg_1 );
            il.EmitCall( OpCodes.Call, getMethod, null );

            // Cast the first value to IComparable and call CompareTo,
            // passing the second value as the argument.
            il.Emit( OpCodes.Castclass, typeof( IComparable ) );
            il.EmitCall( OpCodes.Call, typeof( IComparable ).GetMethod( "CompareTo" )!, null );

            // If descending then multiply comparison result by -1
            // to reverse the ordering.
            if ( direction == ListSortDirection.Descending )
            {
                il.Emit( OpCodes.Ldc_I4_M1 );
                il.Emit( OpCodes.Mul );
            }

            // Return the result of the comparison.
            il.Emit( OpCodes.Ret );

            // Create the delegate pointing at the dynamic method.
            return ( Comparison<T> ) dm.CreateDelegate( typeof( Comparison<T> ) );
        }

        /// <summary> Builds value type comparison. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="pi">        The pi. </param>
        /// <param name="direction"> The direction. </param>
        /// <returns> A Comparison{T} </returns>
#if NET5_0_OR_GREATER
        // sort not working under .NET 5 at this time. The IL Generator emits incorrect IL code.
        [Obsolete( "Stopped working after porting to NET 5.0; TODO: construct the Comparer from Comparison functions." )]
#else
#endif
        private static Comparison<T> BuildValueTypeComparison( PropertyInfo pi, ListSortDirection direction )
        {
            MethodInfo? getMethod = pi.GetGetMethod();
            Debug.Assert( getMethod is not null );
            DynamicMethod dm = new( "Get" + pi.Name, typeof( int ), [typeof( T ), typeof( T )], typeof( T ), true );
            ILGenerator il = dm.GetILGenerator();

            // Get the value of the first objectVu's property.
            il.Emit( OpCodes.Ldarg_0 );
            il.EmitCall( OpCodes.Call, getMethod, null );
            // Box the value type
            il.Emit( OpCodes.Box, pi.PropertyType );

            // Get the value of the second objectVu's property.
            il.Emit( OpCodes.Ldarg_1 );
            il.EmitCall( OpCodes.Call, getMethod, null );
            // Box the value type
            il.Emit( OpCodes.Box, pi.PropertyType );

            // Cast the first value to IComparable and call CompareTo,
            // passing the second value as the argument.
            il.Emit( OpCodes.Castclass, typeof( IComparable ) );
            il.EmitCall( OpCodes.Call, typeof( IComparable ).GetMethod( "CompareTo" )!, null );

            // If descending then multiply comparison result by -1
            // to reverse the ordering.
            if ( direction == ListSortDirection.Descending )
            {
                il.Emit( OpCodes.Ldc_I4_M1 );
                il.Emit( OpCodes.Mul );
            }

            // Return the result of the comparison.
            il.Emit( OpCodes.Ret );

            // Create the delegate pointing at the dynamic method.
            return ( Comparison<T> ) dm.CreateDelegate( typeof( Comparison<T> ) );
        }

        /// <summary> Builds nullable comparison. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="pi">        The pi. </param>
        /// <param name="direction"> The direction. </param>
        /// <returns> A Comparison{T} </returns>
#if NET5_0_OR_GREATER
        // sort not working under .NET 5 at this time. The IL Generator emits incorrect IL code.
        [Obsolete( "Stopped working after porting to NET 5.0; TODO: construct the Comparer from Comparison functions." )]
#else
#endif
        private static Comparison<T> BuildNullableComparison( PropertyInfo pi, ListSortDirection direction )
        {
            MethodInfo? getMethod = pi.GetGetMethod();
            Debug.Assert( getMethod is not null );

            // Type nullableType = type of(Nullable<>).MakeGenericType(pi.PropertyType.GetGenericArguments()[0]);

            DynamicMethod dm = new( "Get" + pi.Name, typeof( int ), [typeof( T ), typeof( T )], typeof( T ), true );
            ILGenerator il = dm.GetILGenerator();

            // Get the value of the first objectVu's property.
            il.Emit( OpCodes.Ldarg_0 );
            il.EmitCall( OpCodes.Call, getMethod, null );

            // Get the value of the second objectVu's property.
            il.Emit( OpCodes.Ldarg_1 );
            il.EmitCall( OpCodes.Call, getMethod, null );

            // Call Nullable.Compare
            il.EmitCall( OpCodes.Call, typeof( Nullable ).GetMethod( "Compare", BindingFlags.Static | BindingFlags.Public )!.MakeGenericMethod( pi.PropertyType.GetGenericArguments()[0] ), null );

            // If descending then multiply comparison result by -1
            // to reverse the ordering.
            if ( direction == ListSortDirection.Descending )
            {
                il.Emit( OpCodes.Ldc_I4_M1 );
                il.Emit( OpCodes.Mul );
            }

            // Return the result of the comparison.
            il.Emit( OpCodes.Ret );

            // Create the delegate pointing at the dynamic method.
            return ( Comparison<T> ) dm.CreateDelegate( typeof( Comparison<T> ) );
        }
    }

    /// <summary>   An external sort comparer. </summary>
    /// <typeparam name="TCompared">    Type of the compared. </typeparam>
    /// <remarks> Constructor. </remarks>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="comparer"> The comparer. </param>
    private class ExternalSortComparer<TCompared>( IComparer<TCompared> comparer ) : IComparer<KeyValuePair<ListItemPair<TCompared>, int>> where TCompared : notnull
    {
        /// <summary> The comparer. </summary>
        private readonly IComparer<TCompared> _comparer = comparer;

        /// <summary>
        /// Compares two objects and returns a value indicating whether one is less than, equal to, or
        /// greater than the other.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="x"> The first objectVu to compare. </param>
        /// <param name="y"> The second objectVu to compare. </param>
        /// <returns>
        /// A signed integer that indicates the relative values of <paramref name="x" /> and
        /// <paramref name="y" />, as shown in the following table.Value Meaning Less than
        /// zero<paramref name="x" /> is less than <paramref name="y" />.Zero<paramref name="x" /> equals
        /// <paramref name="y" />.Greater than zero<paramref name="x" /> is greater than
        /// <paramref name="y" />.
        /// </returns>
        public int Compare( KeyValuePair<ListItemPair<TCompared>, int> x, KeyValuePair<ListItemPair<TCompared>, int> y )
        {
            return this._comparer.Compare( x.Key.Item.EditableObject, y.Key.Item.EditableObject );
        }
    }

    /// <summary>   An external sort comparison. </summary>
    /// <remarks>   David, 2021-08-09. </remarks>
    /// <typeparam name="TCompared">    Type of the compared. </typeparam>
    /// <remarks> Constructor. </remarks>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="comparison"> The comparison. </param>
    private class ExternalSortComparison<TCompared>( Comparison<TCompared> comparison ) : IComparer<KeyValuePair<ListItemPair<TCompared>, int>> where TCompared : notnull
    {
        /// <summary> The comparison. </summary>
        private readonly Comparison<TCompared> _comparison = comparison;

        /// <summary>
        /// Compares two objects and returns a value indicating whether one is less than, equal to, or
        /// greater than the other.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="x"> The first objectVu to compare. </param>
        /// <param name="y"> The second objectVu to compare. </param>
        /// <returns>
        /// A signed integer that indicates the relative values of <paramref name="x" /> and
        /// <paramref name="y" />, as shown in the following table.Value Meaning Less than
        /// zero<paramref name="x" /> is less than <paramref name="y" />.Zero<paramref name="x" /> equals
        /// <paramref name="y" />.Greater than zero<paramref name="x" /> is greater than
        /// <paramref name="y" />.
        /// </returns>
        public int Compare( KeyValuePair<ListItemPair<TCompared>, int> x, KeyValuePair<ListItemPair<TCompared>, int> y )
        {
            return this._comparison( x.Key.Item.EditableObject, y.Key.Item.EditableObject );
        }
    }

    #endregion

    #region "searching"

    /// <summary>
    /// Gets if this view supports searching using the Find method. Always returns true.
    /// </summary>
    /// <value> The i binding list supports searching. </value>
    bool IBindingList.SupportsSearching => true;

    /// <summary>
    /// Returns the index of the first item in the view who's property equals the given value.
    /// -1 is returned if no item is found.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="property"> The property of each item to check. </param>
    /// <param name="key">        The value being sought. </param>
    /// <returns> The index of the item, or -1 if not found. </returns>
    public int Find( PropertyDescriptor property, object key )
    {
        int result = -1;
        if ( property is not null )
        {
            for ( int i = 0, loopTo = this._sourceIndices.Count - 1; i <= loopTo; i++ )
            {
                if ( property.GetValue( this._sourceIndices[i].Key.Item.EditableObject )!.Equals( key ) )
                {
                    result = i;
                    break;
                }
            }
        }

        return result;
    }

    /// <summary>
    /// Returns the index of the first item in the view who's property equals the given value.
    /// -1 is returned if no item is found.
    /// </summary>
    /// <remarks>
    /// It is easier for users of this class to enter a property name and get the PropertyDescriptor
    /// ourselves.
    /// </remarks>
    /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    /// illegal values. </exception>
    /// <param name="propertyName"> The name of the property of each item to check. </param>
    /// <param name="key">          The value being sought. </param>
    /// <returns> The index of the item, or -1 if not found. </returns>
    public int Find( string propertyName, object key )
    {
        PropertyDescriptor? pd = GetPropertyDescriptor( propertyName );
        return pd is not null
            ? this.Find( pd, key )
            : throw new ArgumentException( string.Format( System.Globalization.CultureInfo.CurrentCulture,
                Properties.Resources.PropertyNotFound ?? string.Empty, propertyName, typeof( T ).FullName ), nameof( propertyName ) );
    }

    #endregion

    #region "ibindinglist members"

    /// <summary> Gets if this view raises the ListChanged event. Always returns true. </summary>
    /// <value> The i binding list supports change notification. </value>
    bool IBindingList.SupportsChangeNotification => true;

    /// <summary> Binding list add new. </summary>
    /// <remarks> Explicitly implemented so the type safe AddNew method is exposed instead. </remarks>
    /// <returns> An EditableObject. </returns>
    object IBindingList.AddNew()
    {
        return this.AddNew();
    }

    /// <summary> Gets if this view allows items to be edited. </summary>
    /// <remarks> Delegates to the source list. </remarks>
    /// <value> The i binding list allow edit. </value>
    bool IBindingList.AllowEdit
    {
        get
        {
            foreach ( object list in this.SourceLists )
            {
                if ( list is IBindingList bindingList )
                {
                    if ( !bindingList.AllowEdit )
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }

    /// <summary> Gets if this view allows new items to be added using AddNew(). </summary>
    /// <remarks> Delegates to the source list. </remarks>
    /// <value> The i binding list allow new. </value>
    bool IBindingList.AllowNew
    {
        get
        {
            if ( this._newItemsList is object )
            {
                if ( this._newItemsList is IBindingList newItemList )
                {
                    // Respect what the binding list says.
                    return newItemList.AllowNew;
                }
                // _newItemsList is a IList, so we can call Add()
                // it may fail at runtime - but that is the callee's problem
                return true;
            }

            return false;
        }
    }

    /// <summary> Gets if this view allows items to be removed. </summary>
    /// <remarks> Delegates to the source list. </remarks>
    /// <value> The i binding list allow remove. </value>
    bool IBindingList.AllowRemove
    {
        get
        {
            foreach ( object list in this.SourceLists )
            {
                if ( list is IBindingList l )
                {
                    if ( !l.AllowRemove )
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }

    /// <summary> Not implemented. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <exception cref="NotImplementedException"> Thrown when the requested operation is
    /// unimplemented. </exception>
    /// <param name="property"> The [property]. </param>
    void IBindingList.AddIndex( PropertyDescriptor property )
    {
        throw new NotImplementedException();
    }

    /// <summary> Not implemented. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <exception cref="NotImplementedException"> Thrown when the requested operation is
    /// unimplemented. </exception>
    /// <param name="property"> The [property]. </param>
    void IBindingList.RemoveIndex( PropertyDescriptor property )
    {
        throw new NotImplementedException();
    }

    #endregion

    #region "iraiseitemchangedevents members"

    /// <summary>
    /// Gets if this view raises the ListChanged event when an item changes. Always returns true.
    /// </summary>
    /// <value> The raises item changed events. </value>
    [Browsable( false )]
    public bool RaisesItemChangedEvents => true;

    #endregion

    #region " ilist members "

    /// <summary> List add. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="value"> Either an ObjectView{T} or T to remove. </param>
    /// <returns> An Integer. </returns>
    int IList.Add( object? value )
    {
        if ( value is not null )
        {
            _ = this.AddNew();
        }

        return this.Count - 1;
    }

    /// <summary> Cannot clear this view. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <exception cref="NotSupportedException"> Thrown when the requested operation is not supported. </exception>
    void IList.Clear()
    {
        throw new NotSupportedException( Properties.Resources.CannotClearView );
    }

    /// <summary>
    /// Checks if this view contains the given item. Note that items excluded by current filter are
    /// not searched.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="item"> The item to search for. </param>
    /// <returns> True if the item is in the view, else false. </returns>
    bool IList.Contains( object? item )
    {
        // See if the source indexes contain the item
        return item is ObjectView<T> value ? this._sourceIndices.ContainsKey( value ) : item is T t && this._sourceIndices.ContainsItem( t );
    }

    /// <summary> Gets the index in the view of an item. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="item"> The item to search for. </param>
    /// <returns> The index of the item, or -1 if not found. </returns>
    int IList.IndexOf( object? item )
    {
        if ( item is ObjectView<T> tItem )
        {
            return this._sourceIndices.IndexOfKey( tItem );
        }
        else if ( item is T t )
        {
            return this._sourceIndices.IndexOfItem( t );
        }

        return -1;
    }

    /// <summary> Cannot insert an external item into this collection. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <exception cref="NotSupportedException"> Thrown when the requested operation is not supported. </exception>
    /// <param name="index"> The index of the item to remove. </param>
    /// <param name="value"> Either an ObjectView{T} or T to remove. </param>
    void IList.Insert( int index, object? value )
    {
        throw new NotSupportedException( Properties.Resources.CannotInsertItem );
    }

    /// <summary> Gets a value indicating if this view is read-only. </summary>
    /// <remarks> Delegates to the source list. </remarks>
    /// <value> The i list is read only. </value>
    bool IList.IsReadOnly
    {
        get
        {
            foreach ( object list in this.SourceLists )
            {
                if ( list is IBindingList bindingList )
                {
                    if ( !bindingList.IsReadOnly )
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }

            return true;
        }
    }

    /// <summary>
    /// Always returns <code>false</code> because the view can change size when source lists are
    /// added.
    /// </summary>
    /// <value> The size of the list is fixed. </value>
    bool IList.IsFixedSize => false;

    /// <summary> Removes the given item from the view and underlying source list. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="value"> Either an ObjectView{T} or T to remove. </param>
    void IList.Remove( object? value )
    {
        int index = (this as IList).IndexOf( value );
        (this as IList).RemoveAt( index );
    }

    /// <summary> Removes the item from the view at the given index. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="index"> The index of the item to remove. </param>
    void IList.RemoveAt( int index )
    {
        // Get the index in the source list.
        int sourceIndex = this._sourceIndices[index].Value;
        IList sourceList = this._sourceIndices[index].Key.List;
        if ( sourceIndex > -1 )
        {
            sourceList.RemoveAt( sourceIndex );
            if ( sourceList is not IBindingList || !((sourceList as IBindingList)?.SupportsChangeNotification ?? false) )
            {
                this.FilterAndSort();
                this.OnListChanged( ListChangedType.Reset, -1 );
            }
        }
        else
        {
            // The item is not in the source list yet as it is new
            // So cancel the new operation instead.
            this.CancelNew( index );
        }
    }

    /// <summary>   Gets or sets the element at the specified index. </summary>
    /// <exception cref="NotSupportedException">    Thrown when the requested operation is not
    ///                                             supported. </exception>
    /// <param name="index">    The zero-based index of the element to get or set. </param>
    /// <returns>   The element at the specified index. </returns>
    object? IList.this[int index]
    {
        get => this[index];
        // The interface requires we supply a setter
        // But we don't want external code modifying the view
        // in this manner.
        set => throw new NotSupportedException( Properties.Resources.CannotSetItem );
    }

    #endregion

    #region "icollection members"

    /// <summary>
    /// Copies the <see cref="ObjectView{T}"/> objects of the view to an
    /// <see cref="Array"/>, starting at a particular System.Array index.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="array"> The one-dimensional <see cref="Array"/> that is the destination of the
    /// elements copied from view. The System.Array must have zero-based
    /// indexing. </param>
    /// <param name="index"> The zero-based index in array at which copying begins. </param>
    void ICollection.CopyTo( Array array, int index )
    {
        this._sourceIndices.Keys.CopyTo( array, index );
    }

    /// <summary>
    /// Gets a value indicating whether access to the <see cref="ICollection" />
    /// is synchronized (thread safe).
    /// </summary>
    /// <value> The i collection is synchronized. </value>
    bool ICollection.IsSynchronized => false;

    /// <summary> Not supported. </summary>
    /// <exception cref="NotSupportedException"> Thrown when the requested operation is not supported. </exception>
    /// <value> The i collection synchronization root. </value>
    object ICollection.SyncRoot => throw new NotSupportedException( Properties.Resources.SyncAccessNotSupported );

    /// <summary>
    /// Gets the number of items currently in the view. This does not include those items excluded by
    /// the current filter.
    /// </summary>
    /// <value> The count. </value>
    [Browsable( false )]
    public int Count => this._sourceIndices.Count;

    #endregion

    #region "ienumerable members"

    /// <summary>
    /// Returns an enumerator that iterates through all the <see cref="ObjectView{T}"/> items in
    /// the view. This does not include those items excluded by the current filter.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <returns> An IEnumerator to iterate with. </returns>
    IEnumerator IEnumerable.GetEnumerator()
    {
        return this._sourceIndices.GetKeyEnumerator();
    }

    #endregion

    #region "itypedlist members"

    /// <summary>
    /// Returns the <see cref="PropertyDescriptorCollection"/> that represents the properties on each
    /// item used to bind data.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="listAccessors"> Array of property descriptors to navigate objectVu hierarchy to
    /// actual item objectVu. It can be null. </param>
    /// <returns>
    /// The System.ComponentModel.PropertyDescriptorCollection that represents the properties on each
    /// item used to bind data.
    /// </returns>
#pragma warning disable CS8769 // Nullability of reference types in type of parameter doesn't match implemented member (possibly because of nullability attributes).
    PropertyDescriptorCollection ITypedList.GetItemProperties( PropertyDescriptor[] listAccessors )
#pragma warning restore CS8769 // Nullability of reference types in type of parameter doesn't match implemented member (possibly because of nullability attributes).
    {
        PropertyDescriptorCollection originalProps;
        IEnumerator<IList?> lists = this.EnumerateSourceLists().GetEnumerator();
        if ( lists.MoveNext() && lists.Current is ITypedList typedList )
        {
            // Ask the source list for the properties.
            originalProps = typedList.GetItemProperties( listAccessors );
        }
        else
        {
            // Get the properties ourself.
            // originalProps = PropertyDescriptorExtensions.PropertyDescriptorExtensionMethods.GetListItemProperties( listAccessors, typeof( T ) );
            originalProps = PropertyDescriptorExtensions.PropertyDescriptorExtensionMethods.GetListItemProperties<T>( listAccessors );
        }

        if ( listAccessors is not null && listAccessors.Length > 0 )
        {
            Type type = originalProps[0].ComponentType;
            if ( type.IsGenericType && ReferenceEquals( type.GetGenericTypeDefinition(), typeof( ObjectView<> ) ) )
            {
                originalProps = originalProps[0].GetChildProperties();
            }
        }

        // List<PropertyDescriptor> newProps = [.. originalProps, .. this.GetProvidedViews( originalProps )];

#pragma warning disable IDE0028 // Simplify collection initialization
        List<PropertyDescriptor> newProps = new();
        foreach ( PropertyDescriptor pd in originalProps )
            newProps.Add( pd );

        foreach ( PropertyDescriptor pd in this.GetProvidedViews( originalProps ) )
            newProps.Add( pd );
#pragma warning restore IDE0028 // Simplify collection initialization

        return new PropertyDescriptorCollection( [.. newProps] );
    }

    /// <summary> Determine if we should provide view. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="property"> The [property]. </param>
    /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    protected internal bool ShouldProvideView( PropertyDescriptor property )
    {
        return ProvidedViewPropertyDescriptor<T>.CanProvideViewOf( property );
    }

    /// <summary> Gets provided view name. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="sourceListProperty"> Source list property. </param>
    /// <returns> The provided view name. </returns>
    protected internal string GetProvidedViewName( MemberDescriptor sourceListProperty )
    {
        return $"{sourceListProperty?.Name}View";
    }

    /// <summary> Creates provided view. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="objectVu">           The EditableObject View. </param>
    /// <param name="sourceListProperty"> Source list property. </param>
    /// <returns> The new provided view. </returns>
    protected internal object? CreateProvidedView( ObjectView<T> objectVu, PropertyDescriptor sourceListProperty )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( sourceListProperty, nameof( sourceListProperty ) );
#else
        if ( sourceListProperty is null ) throw new ArgumentNullException( nameof( sourceListProperty ) );
#endif

        object? list = sourceListProperty.GetValue( objectVu );
        Type viewType = GetProvidedViewType( sourceListProperty );
        return Activator.CreateInstance( viewType, list );
    }

    /// <summary> Gets provided view type. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="sourceListProperty"> Source list property. </param>
    /// <returns> The provided view type. </returns>
    private static Type GetProvidedViewType( PropertyDescriptor sourceListProperty )
    {
        Type viewTypeDef = typeof( BindingListView<object> ).GetGenericTypeDefinition();
        Type typeParam = sourceListProperty.PropertyType.GetGenericArguments()[0];
        Type viewType = viewTypeDef.MakeGenericType( typeParam );
        return viewType;
    }

    /// <summary> Gets the provided views in this collection. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="properties"> The properties. </param>
    /// <returns>
    /// An enumerator that allows foreach to be used to process the provided views in this collection.
    /// </returns>
    private IEnumerable<PropertyDescriptor> GetProvidedViews( PropertyDescriptorCollection properties )
    {
        for ( int i = 0, loopTo = properties.Count - 1; i <= loopTo; i++ )
        {
            if ( this.ShouldProvideView( properties[i] ) )
            {
                string name = this.GetProvidedViewName( properties[i] );
                yield return new ProvidedViewPropertyDescriptor<T>( name, GetProvidedViewType( properties[i] ) );
            }
        }
    }

    /// <summary> Gets the name of the view. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="listAccessors"> Unused. Can be null. </param>
    /// <returns> The name of the view. </returns>
#pragma warning disable CS8769 // Nullability of reference types in type of parameter doesn't match implemented member (possibly because of nullability attributes).
    string ITypedList.GetListName( PropertyDescriptor[] listAccessors )
#pragma warning restore CS8769 // Nullability of reference types in type of parameter doesn't match implemented member (possibly because of nullability attributes).
    {
        return this.GetType().Name;
    }

    #endregion

    #region "helper methods"

    /// <summary>
    /// Creates a new <see cref="ListSortDescription"/> for given property name
    /// and sort direction.
    /// </summary>
    /// <remarks> Used by external code to simplify sorting the view. </remarks>
    /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    /// illegal values. </exception>
    /// <param name="propertyName"> The name of the property to sort by. </param>
    /// <param name="direction">    The direction in which to sort. </param>
    /// <returns> A ListSortDescription. </returns>
    public ListSortDescription CreateListSortDescription( string propertyName, ListSortDirection direction )
    {
        PropertyDescriptor? pd = GetPropertyDescriptor( propertyName );
        return pd is null
            ? throw new ArgumentException( string.Format( System.Globalization.CultureInfo.CurrentCulture,
                Properties.Resources.PropertyNotFound ?? string.Empty, propertyName, typeof( T ).FullName ), nameof( propertyName ) )
            : new ListSortDescription( pd, direction );
    }

    /// <summary> Gets the property descriptor for a given property name. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="propertyName"> The name of a property of <typeparamref name="T"/>. </param>
    /// <returns> The <see cref="PropertyDescriptor"/>. </returns>
    private static PropertyDescriptor? GetPropertyDescriptor( string propertyName )
    {
        return TypeDescriptor.GetProperties( typeof( T ) ).Find( propertyName, false );
    }

    /// <summary>
    /// Attaches event handlers to the given <see cref="ObjectView{T}"/>'s edit life cycle
    /// notification events.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="editableObject"> The <see cref="ObjectView{T}"/> to listen to. </param>
    private void HookEditableObjectEvents( ObjectView<T> editableObject )
    {
        editableObject.EditBegun += this.OnItemEditBegun;
        editableObject.EditCanceled += this.OnItemEditCanceled;
        editableObject.EditEnded += this.OnItemEditEnded;
    }

    /// <summary>
    /// Detaches event handlers from the given <see cref="ObjectView{T}"/>'s edit life cycle
    /// notification events.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="editableObject"> The <see cref="ObjectView{T}"/> to stop listening to. </param>
    private void UnHookEditableObjectEvents( ObjectView<T> editableObject )
    {
        editableObject.EditBegun -= this.OnItemEditBegun;
        editableObject.EditCanceled -= this.OnItemEditCanceled;
        editableObject.EditEnded -= this.OnItemEditEnded;
    }

    /// <summary>
    /// Attaches an event handler to the <see cref="ObjectView{T}"/>'s PropertyChanged event.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="editableObject"> The <see cref="ObjectView{T}"/> to listen to. </param>
    private void HookPropertyChangedEvent( ObjectView<T> editableObject )
    {
        editableObject.PropertyChanged += this.ItemPropertyChanged;
    }

    /// <summary>
    /// Detaches the event handler from the <see cref="ObjectView{T}"/>'s PropertyChanged event.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="editableObject"> The <see cref="ObjectView{T}"/> to stop listening to. </param>
    private void UnHookPropertyChangedEvent( ObjectView<T> editableObject )
    {
        editableObject.PropertyChanged -= this.ItemPropertyChanged;
    }

    /// <summary> Builds saved list. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    private void BuildSavedList()
    {
        this.SavedSourceLists.Clear();
        foreach ( object? list in this.EnumerateSourceLists() )
        {
            if ( list is IList l )
                this.SavedSourceLists.Add( l );
        }
    }

    /// <summary> Gets the source lists in this collection. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <returns>
    /// An enumerator that allows for each to be used to process the source lists in this collection.
    /// </returns>
    protected IEnumerable<IList?> EnumerateSourceLists()
    {
        foreach ( object obj in this._sourceLists )
        {
            if ( !string.IsNullOrEmpty( this.DataMember ) )
            {
                bool found = false;
                foreach ( PropertyDescriptor pd in TypeDescriptor.GetProperties( obj ) )
                {
                    if ( (pd.Name ?? "") == (this.DataMember ?? "") )
                    {
                        found = true;
                        yield return (pd.GetValue( obj ) as IList)!;
                        break;
                    }
                }

                if ( !found )
                {
                    yield return null;
                }
            }
            else
            {
                yield return obj is IListSource src
                    ? src.ContainsListCollection ? src.GetList() is IList list && list.Count > 0 && list[0] is IList l ? l : null : src.GetList()
                    : obj as IList;
            }
        }
    }

    #endregion
}
