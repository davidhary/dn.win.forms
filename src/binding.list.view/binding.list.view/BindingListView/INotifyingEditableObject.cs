using System;
using System.ComponentModel;

namespace cc.isr.WinForms;
/// <summary>
/// Extends <see cref="IEditableObject"/> by providing events to raise
/// during edit state changes.
/// </summary>
/// <remarks>
/// (c) 2006 Andrew Davey. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2018-12-15, 1.2.  </para><para>
/// https://blogs.warwick.ac.uk/andrewdavey and https://sourceforge.net/projects/blw/.
/// </para>
/// </remarks>
internal interface INotifyingEditableObject : IEditableObject
{
    /// <summary>
    /// An edit has started on the object.
    /// </summary>
    /// <remarks>
    /// This event should be raised from BeginEdit().
    /// </remarks>
    event EventHandler? EditBegun;

    /// <summary> Event queue for all listeners interested in EditCanceled events. </summary>
    /// <remarks>
    /// This event should be raised from CancelEdit().
    /// </remarks>
    event EventHandler? EditCanceled;

    /// <summary> Event queue for all listeners interested in EditEnded events. </summary>
    /// <remarks>
    /// This event should be raised from EndEdit().
    /// </remarks>
    event EventHandler? EditEnded;
}
