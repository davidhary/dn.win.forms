using System.Collections.Generic;

namespace cc.isr.WinForms;

/// <summary> Composite item filter. </summary>
/// <remarks>
/// (c) 2006 Andrew Davey. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2018-12-15, </para><para>
/// David, 2018-12-15, 1.2.* https://blogs.warwick.ac.uk/andrewdavey and
/// https://sourceforge.net/projects/blw/. </para>
/// </remarks>
public class CompositeItemFilter<T> : IItemFilter<T> where T : notnull
{
    /// <summary> The filters. </summary>
    /// <value> The filters. </value>
    private List<IItemFilter<T>> Filters { get; set; }

    /// <summary> Default constructor. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    public CompositeItemFilter() => this.Filters = [];

    /// <summary> Adds a filter. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="filter"> Specifies the filter. </param>
    public void AddFilter( IItemFilter<T> filter )
    {
        this.Filters.Add( filter );
    }

    /// <summary> Removes the filter described by filter. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="filter"> Specifies the filter. </param>
    public void RemoveFilter( IItemFilter<T> filter )
    {
        _ = this.Filters.Remove( filter );
    }

    /// <summary> Tests if the item should be included. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="item"> The item to test. </param>
    /// <returns> True if the item should be included, otherwise false. </returns>
    public bool Include( T item )
    {
        foreach ( IItemFilter<T> filter in this.Filters )
        {
            if ( !filter.Include( item ) )
            {
                return false;
            }
        }

        return true;
    }
}
