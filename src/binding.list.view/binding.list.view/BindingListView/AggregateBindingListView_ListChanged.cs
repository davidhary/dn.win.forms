using System.ComponentModel;

namespace cc.isr.WinForms;

public partial class AggregateBindingListView<T>
{
    /// <summary>   Event queue for all listeners interested in List Changed events. </summary>
    public event ListChangedEventHandler? ListChanged;

    /// <summary>
    /// Notifies List change synchronously. Not thread save.
    /// </summary>
    /// <param name="e"> List Changed event information. </param>
    protected virtual void NotifyListChanged( ListChangedEventArgs e )
    {
        this.ListChanged?.Invoke( this, e );
    }

    /// <summary>
    /// Helper method to build the ListChangedEventArgs needed for the ListChanged event.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="listChangedType"> The type of change that occurred. </param>
    /// <param name="newIndex">        The index of the changed item. </param>
    private void OnListChanged( ListChangedType listChangedType, int newIndex )
    {
        this.NotifyListChanged( new ListChangedEventArgs( listChangedType, newIndex ) );
    }

    /// <summary>
    /// Helper method to build the ListChangedEventArgs needed for the ListChanged event.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="listChangedType"> The type of change that  occurred. </param>
    /// <param name="newIndex">        The index of the item after the change. </param>
    /// <param name="oldIndex">        The index of the item before the change. </param>
    private void OnListChanged( ListChangedType listChangedType, int newIndex, int oldIndex )
    {
        this.NotifyListChanged( new ListChangedEventArgs( listChangedType, newIndex, oldIndex ) );
    }

    /// <summary> Raises the ListChanged event with the given event arguments. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="e"> The ListChangedEventArgs to raise the event with. </param>
    protected virtual void OnListChanged( ListChangedEventArgs e )
    {
        this.NotifyListChanged( e );
    }

}
