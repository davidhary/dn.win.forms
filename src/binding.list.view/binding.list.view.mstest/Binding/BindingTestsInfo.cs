using System;
using CommunityToolkit.Mvvm.ComponentModel;

namespace cc.isr.WinForms.Views.MSTest;

/// <summary> Binding Tests information. </summary>
/// <remarks>
/// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2019-02-18. </para>
/// </remarks>
internal sealed class BindingTestsInfo : ObservableObject
{
    #region " construction "

    /// <summary>   Default constructor. </summary>
    /// <remarks>   2023-05-22. </remarks>
    public BindingTestsInfo()
    {
    }

    #endregion

    #region " singleton "

    /// <summary>   Creates an instance of the <see cref="BindingTestsInfo"/> after restoring the 
    /// application context settings to both the user and all user files. </summary>
    /// <remarks>   2023-05-15. </remarks>
    /// <returns>   The new instance. </returns>
    private static BindingTestsInfo CreateInstance()
    {
        // get assembly files using the .Logging suffix.

        AssemblyFileInfo ai = new( typeof( BindingTestsInfo ).Assembly, null, ".Settings", ".json" );

        // must copy application context settings here to clear any bad settings files.

        AppSettingsScribe.CopySettings( ai.AppContextAssemblyFilePath!, ai.AllUsersAssemblyFilePath! );
        AppSettingsScribe.CopySettings( ai.AppContextAssemblyFilePath!, ai.ThisUserAssemblyFilePath! );

        BindingTestsInfo ti = new();
        AppSettingsScribe.ReadSettings( ai.AllUsersAssemblyFilePath!, nameof( BindingTestsInfo ), ti );

        return ti;
    }

    /// <summary>   Gets the instance. </summary>
    /// <value> The instance. </value>
    public static BindingTestsInfo Instance => _instance.Value;

    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0200:Remove unnecessary lambda expression", Justification = "<Pending>" )]
    private static readonly Lazy<BindingTestsInfo> _instance = new( () => CreateInstance(), true );

    #endregion

    #region " configuration information "

    private TraceLevel _traceLevel = TraceLevel.Verbose;

    /// <summary>   Gets or sets the trace level. </summary>
    /// <value> The trace level. </value>
    [System.ComponentModel.Description( "Sets the message level" )]
    public TraceLevel TraceLevel
    {
        get => this._traceLevel;
        set => _ = this.SetProperty( ref this._traceLevel, value );
    }

    private bool _enabled = true;

    /// <summary>   Gets or sets a value indicating whether this object is enabled. </summary>
    /// <value> True if enabled, false if not. </value>
    [System.ComponentModel.Description( "True if testing is enabled for this test class" )]
    public bool Enabled
    {
        get => this._enabled;
        set => this.SetProperty( ref this._enabled, value );
    }

    private bool _all = true;

    /// <summary> Gets or sets all. </summary>
    /// <value> all. </value>
    [System.ComponentModel.Description( "True if all testing is enabled for this test class" )]
    public bool All
    {
        get => this._all;
        set => this.SetProperty( ref this._all, value );
    }

    #endregion

    #region " i7 tests settings "

    /// <summary> Gets or sets the number of items. </summary>
    /// <value> The number of items. </value>
    public int ItemCount { get; set; }

    /// <summary> Gets or sets the 7k 2600 create timespan. </summary>
    /// <value> The i 7k 2600 create timespan. </value>
	public TimeSpan I7k2600CreateTimespan { get; set; }

    /// <summary> Gets or sets the 7k 2600 sort internal timespan. </summary>
    /// <value> The i 7k 2600 sort internal timespan. </value>
	public TimeSpan I7k2600SortInternalTimespan { get; set; }

    /// <summary> Gets or sets the 7k 2600 sort extent time span. </summary>
    /// <value> The i 7k 2600 sort extent time span. </value>
	public TimeSpan I7k2600SortExtTimeSpan { get; set; }

    /// <summary> Gets or sets the 7k 2600 data table sort timespan. </summary>
    /// <value> The i 7k 2600 data table sort timespan. </value>
	public TimeSpan I7k2600DataTableSortTimespan { get; set; }

    /// <summary> Gets or sets the 7k 2600. </summary>
    /// <value> The i 7k 2600. </value>
	public string? I7k2600 { get; set; }

    #endregion

    #region " aggregate feed tests settings "

    private string _newsOnlineFeedUrl = "http://feeds.bbci.co.uk/news/uk/rss.xml";

    /// <summary>   Gets or sets URL of the news online feed. </summary>
    /// <value> The news online feed URL. </value>
	public string NewsOnlineFeedUrl
    {
        get => this._newsOnlineFeedUrl;
        set => this.SetProperty( ref this._newsOnlineFeedUrl, value );
    }

    /// <summary> Gets or sets URL of the MSDN channel 9 feed. </summary>
    /// <value> The MSDN channel 9 feed URL. </value>
	public string? MSDNChannel9FeedUrl { get; set; }

    /// <summary> Gets or sets URL of the MSDN blogs feed. </summary>
    /// <value> The MSDN blogs feed URL. </value>
	public string? MSDNBlogsFeedUrl { get; set; }

    #endregion
}
