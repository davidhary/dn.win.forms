using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml;

namespace cc.isr.WinForms.Views.MSTest;

/// <summary>   (Unit Test Class) a binding tests. </summary>
/// <remarks>   David, 2020-09-18. </remarks>
[TestClass]
public partial class BindingTests
{
    #region " binding list view adding items plus timing"

    /// <summary> Compare action. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    /// <param name="leftHand">  The left hand. </param>
    /// <param name="rightHand"> The right hand. </param>
    /// <returns> An Integer. </returns>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
    private static int CompareAction( Foo leftHand, Foo rightHand )
    {
        return leftHand.Abscissa.CompareTo( rightHand.Abscissa );
    }

    /// <summary> A foo. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    private sealed class Foo
    {
        /// <summary> Initializes a new instance of the <see cref="object" /> class. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        public Foo()
        {
            Random r = new();
            this.Abscissa = r.Next( 0, 100 );
            this.Ordinate = r.Next( 100, 1000 ).ToString( System.Globalization.CultureInfo.CurrentCulture );
        }

        /// <summary> Gets or sets the abscissa. </summary>
        /// <value> The abscissa. </value>
        public int Abscissa { get; private set; }

        /// <summary> Gets or sets the ordinate. </summary>
        /// <value> The ordinate. </value>
        public string Ordinate { get; private set; }
    }

    /// <summary> (Unit Test Method) tests binding list view. </summary>
    /// <remarks> tested 2019-05-15. </remarks>
    [TestMethod()]
    public void BindingListViewTest()
    {
        List<Foo> list = [];
        for ( int i = 1, loopTo = BindingTestsInfo.Instance.ItemCount; i <= loopTo; i++ )
        {
            list.Add( new Foo() );
        }

        Assert.AreEqual( BindingTestsInfo.Instance.ItemCount, list.Count, "Expected list size" );
        Stopwatch sw = new();
        TimeSpan expectedTimeSpan = TimeSpan.Zero;
        TimeSpan actualTimeSpan = TimeSpan.Zero;
        sw.Start();
        using BindingListView<Foo> view = new( list );
        sw.Stop();
        expectedTimeSpan = BindingTestsInfo.Instance.I7k2600CreateTimespan;
        actualTimeSpan = sw.Elapsed;
        Assert.IsTrue( expectedTimeSpan > actualTimeSpan, $"BLV Create: expecting {BindingTestsInfo.Instance.I7k2600} time span {expectedTimeSpan.TotalMilliseconds} > {actualTimeSpan.TotalMilliseconds}" );
        sw.Reset();

#if NET5_0_OR_GREATER
        // sort not working under .NET 5 at this time. The IL Generator emits incorrect IL code.
#else
        sw.Start();
        view.Sort = nameof( Foo.Abscissa );
        sw.Stop();
        expectedTimeSpan = BindingTestsInfo.Instance.I7k2600SortInternalTimespan;
        actualTimeSpan = sw.Elapsed;
        Assert.IsTrue( expectedTimeSpan > actualTimeSpan, $"BLV.Sort: expecting {BindingTestsInfo.Instance.I7k2600} time span {expectedTimeSpan.TotalMilliseconds} > {actualTimeSpan.TotalMilliseconds}" );
        sw.Reset();
        sw.Start();
        view.ApplySort( BindingTests.CompareAction );
        sw.Stop();
        expectedTimeSpan = BindingTestsInfo.Instance.I7k2600SortExtTimeSpan;
        actualTimeSpan = sw.Elapsed;
        Assert.IsTrue( expectedTimeSpan > actualTimeSpan, $"BLV.ApplySort(delegate): expecting {BindingTestsInfo.Instance.I7k2600} {expectedTimeSpan.TotalMilliseconds} > {actualTimeSpan.TotalMilliseconds}" );
#endif

#if NET5_0_OR_GREATER
        // sort not working under .NET 5 at this time. The IL Generator emits incorrect IL code.
#else
        using System.Data.DataTable dataTable = new()
        {
            Locale = System.Globalization.CultureInfo.CurrentCulture
        };
        _ = dataTable.Columns.Add( nameof( Foo.Abscissa ), typeof( int ) );
        _ = dataTable.Columns.Add( nameof( Foo.Ordinate ), typeof( string ) );
        for ( int i = 1, loopTo1 = BindingTestsInfo.Instance.ItemCount; i <= loopTo1; i++ )
        {
            System.Data.DataRow row = dataTable.NewRow();
            row[0] = list[i - 1].Abscissa;
            row[1] = list[i - 1].Ordinate;
            dataTable.Rows.Add( row );
        }

        sw.Start();
        dataTable.DefaultView.Sort = nameof( Foo.Abscissa );
        sw.Stop();
        expectedTimeSpan = BindingTestsInfo.Instance.I7k2600DataTableSortTimespan;
        actualTimeSpan = sw.Elapsed;
        Assert.IsTrue( expectedTimeSpan > actualTimeSpan, $"{nameof( System.Data.DataTable )}.{nameof( System.Data.DataTable.DefaultView )}.Sort: expecting {BindingTestsInfo.Instance.I7k2600} {expectedTimeSpan.TotalMilliseconds} > {actualTimeSpan.TotalMilliseconds}" );
#endif
    }

    #endregion

    #region " adding new test "

    /// <summary> Handles the adding new. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Adding new event information. </param>
    private void HandleAddingNew( object? sender, AddingNewEventArgs e )
    {
        if ( sender is not BindingListView<string> )
        {
            return;
        }

        e.NewObject = nameof( e.NewObject );
    }

    /// <summary> (Unit Test Method) tests binding list adding new handling. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestMethod()]
    public void BindingListAddingNewHandlingTest()
    {
        List<string> l = [];
        using BindingListView<string> sender = new( l );
        Assert.IsNotNull( sender.NewItemsList );
        sender.AddingNew += this.HandleAddingNew;
        int expectedNewItemsCount = 0;
        Assert.AreEqual( expectedNewItemsCount, sender.NewItemsList.Count, "expects no new items" );
        l.Add( $"Item #{l.Count + 1}" );
        expectedNewItemsCount += 1;
        Assert.AreEqual( expectedNewItemsCount, sender.NewItemsList.Count, "expects new items after adding one item" );
    }

    #endregion

    #region " aggregate binding list view "

    /// <summary> A feed. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <remarks> Constructor. </remarks>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="url"> URL of the resource. </param>
    private sealed class Feed( string url )
    {
        /// <summary> Gets or sets URL of the document. </summary>
        /// <value> The URL. </value>
        private string Url { get; set; } = url;

        /// <summary> Updates this object. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        public void Update()
        {
            XmlDocument doc = new();
            doc!.Load( this.Url );
            this._title = doc!.SelectSingleNode( "/rss/channel/title" )!.InnerText;
            foreach ( XmlNode node in doc!.SelectNodes( "//item" )! )
            {
                FeedItem item = new()
                {
                    Title = node["title"]?.InnerText,
                    Description = node["description"]?.InnerText,
                    PubDate = DateTime.Parse( node["pubDate"]?.InnerText ?? string.Empty, System.Globalization.CultureInfo.CurrentCulture )
                };
                this.Items.Add( item );
            }
        }

        /// <summary> Gets or sets the items. </summary>
        /// <value> The items. </value>
        public BindingList<FeedItem> Items { get; private set; } = [];

        /// <summary> The title. </summary>
        private string? _title;

        /// <summary> Returns a string that represents the current object. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A <see cref="string" /> that represents the current object. </returns>
        public override string ToString()
        {
            return this._title ?? this.Url;
        }
    }

    /// <summary> A feed item. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    private sealed class FeedItem
    {
        /// <summary> Gets or sets the title. </summary>
        /// <value> The title. </value>
        public string? Title { get; set; }

        /// <summary> Gets or sets the description. </summary>
        /// <value> The description. </value>
        public string? Description { get; set; }

        /// <summary> Gets or sets the pub date. </summary>
        /// <value> The pub date. </value>
        public DateTime PubDate { get; set; }
    }

    /// <summary> (Unit Test Method) tests aggregate binding list view. </summary>
    /// <remarks> tested 2019-05-15. </remarks>
    [TestMethod]
    public void AggregateBindingListViewTest()
    {
        using AggregateBindingListView<FeedItem> itemsView = new();
        using System.Windows.Forms.Form panel = new();
        using System.Windows.Forms.DataGridView grid = new();
        // grid.CreateControl()
        panel.Controls.Add( grid );
        grid.DataSource = itemsView;
        int expectedCount = itemsView.Count;
        int actualCount = grid.Rows.Count;
        Assert.AreEqual( expectedCount, actualCount, "empty items view" );

        Feed feed = new( BindingTestsInfo.Instance.NewsOnlineFeedUrl );
        feed.Update();
        _ = itemsView.SourceLists.Add( feed.Items );
        expectedCount = feed.Items.Count;
        actualCount = itemsView.Count;
        Assert.AreEqual( expectedCount, actualCount, "expected items view count" );
        _ = cc.isr.Std.TimeSpanExtensions.TimeSpanExtensionMethods.AsyncWaitUntil( TimeSpan.FromMilliseconds( 100 ), TimeSpan.Zero, () => grid.Rows.Count >= expectedCount );
        actualCount = grid.Rows.Count;
        Assert.AreEqual( expectedCount, actualCount, "expected grid row count" );

        feed = new Feed( BindingTestsInfo.Instance.NewsOnlineFeedUrl );
        feed.Update();
        _ = itemsView.SourceLists.Add( feed.Items );
        expectedCount += feed.Items.Count;
        actualCount = itemsView.Count;
        Assert.AreEqual( expectedCount, actualCount, $"expected items view count after adding {feed.Items.Count} items" );
        _ = cc.isr.Std.TimeSpanExtensions.TimeSpanExtensionMethods.AsyncWaitUntil( TimeSpan.FromMilliseconds( 100 ), TimeSpan.Zero, () => grid.Rows.Count >= expectedCount );
        actualCount = grid.Rows.Count;
        Assert.AreEqual( expectedCount, actualCount, "expected grid row count" );

        itemsView.SourceLists.Remove( feed.Items );
        expectedCount -= feed.Items.Count;
        actualCount = itemsView.Count;
        Assert.AreEqual( expectedCount, actualCount, $"expected items view count after removing {feed.Items.Count} items" );
        _ = cc.isr.Std.TimeSpanExtensions.TimeSpanExtensionMethods.AsyncWaitUntil( TimeSpan.FromMilliseconds( 100 ), TimeSpan.Zero, () => grid.Rows.Count < expectedCount );
        actualCount = grid.Rows.Count;
        Assert.AreEqual( expectedCount, actualCount, "expected grid row count" );
    }

    #endregion
}
