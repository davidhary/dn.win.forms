using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using System.Windows.Forms;
using cc.isr.WinControls;

namespace cc.isr.WinForms.ModelViewForm;
/// <summary>
/// A Mode View Form to host <see cref="ModelViewBase"/> or <see cref="ModelViewLoggerBase"/>
/// controls.
/// </summary>
/// <remarks>
/// (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License. </para><para>
/// David, 2005-02-07, 2.0.2597 </para>
/// </remarks>
public partial class ModelViewForm
{
    #region " form event handlers "

    /// <summary>
    /// Raises the <see cref="Form.Closing" /> event. Releases all publishers.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="CancelEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnClosing( CancelEventArgs e )
    {
        try
        {
            this.Cursor = Cursors.WaitCursor;
            if ( e is not null && !e.Cancel )
            {
                // removes the private text box listener
                if ( this.TextBoxTextWriter is not null )
                    ModelViewForm.RemoveDisplayTextWriter( this.TextBoxTextWriter );

                if ( this.ModelView is not null )
                    this._layout.Controls.Remove( this.ModelView );

                if ( this._modelViewDisposeEnabled )
                    this.ModelView?.Dispose();

                if ( this._userControlDisposeEnabled && this.UserControl is not null )
                    this.UserControl?.Dispose();

                this.UserControl = null;
                if ( this.ModelViewLogger is not null )
                {
                    this._layout.Controls.Remove( this.ModelViewLogger );
                    if ( this._modelViewLoggerDisposeEnabled )
                        this.ModelViewLogger?.Dispose();
                }

                this.ModelViewLogger = null;
            }
        }
        finally
        {
            Application.DoEvents();
            base.OnClosing( e );
            this.Cursor = Cursors.Default;
        }
    }

    /// <summary>
    /// Called upon receiving the <see cref="Form.Load" /> event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnLoad( EventArgs e )
    {
        string activity = string.Empty;
        try
        {
            activity = $"{this.Name} loading";
            this.Cursor = Cursors.WaitCursor;
            Trace.CorrelationManager.StartLogicalOperation( System.Reflection.MethodBase.GetCurrentMethod()?.Name ?? string.Empty );

            // set the form caption
            activity = $"{this.Name} displaying title (text)";

            Assembly assembly = Assembly.GetCallingAssembly();
            this.Text = $"{assembly.GetName().Name} release {assembly.GetName().Version}";

            // default to center screen.
            activity = $"{this.Name} loading; centering to screen";
            this.CenterToScreen();
        }
        catch ( Exception ex )
        {
            _ = this.LogException( ex, activity );
            if ( DialogResult.Abort == System.Windows.Forms.MessageBox.Show( ex.ToString(), "Exception Occurred", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error ) )
            {
                Application.Exit();
            }
        }
        finally
        {
            base.OnLoad( e );
            Trace.CorrelationManager.StopLogicalOperation();
            this.Cursor = Cursors.Default;
        }
    }

    /// <summary>
    /// Called upon receiving the <see cref="Form.Shown" /> event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnShown( EventArgs e )
    {
        string activity = string.Empty;
        try
        {
            activity = $"showing {this.Name}";
            this.Cursor = Cursors.WaitCursor;
            Trace.CorrelationManager.StartLogicalOperation( System.Reflection.MethodBase.GetCurrentMethod()?.Name ?? string.Empty );

            // allow form rendering time to complete: process all messages currently in the queue.
            Application.DoEvents();
            if ( !this.DesignMode )
            {
            }

            activity = "Ready to open Visa Session";
            System.Diagnostics.Trace.WriteLine( $"{activity};. " );
        }
        catch ( Exception ex )
        {
            _ = this.LogException( ex, activity );
            if ( DialogResult.Abort == System.Windows.Forms.MessageBox.Show( ex.ToString(), "Exception Occurred", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error ) )
            {
                Application.Exit();
            }
        }
        finally
        {
            base.OnShown( e );
            Trace.CorrelationManager.StopLogicalOperation();
            this.Cursor = Cursors.Default;
        }
    }

    /// <summary> Resize client area. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="title">              The title. </param>
    /// <param name="clientControl">      The client control. </param>
    /// <param name="usingTraceMessages"> True to using trace messages. </param>
    public void ResizeClientArea( string title, Control clientControl, bool usingTraceMessages )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( clientControl, nameof( clientControl ) );
#else
        if ( clientControl is null ) throw new ArgumentNullException( nameof( clientControl ) );
#endif

        if ( usingTraceMessages )
        {
            this.ResizeClientArea( this._tabs, this._traceMessagesBox, clientControl );
            this._viewTabPage.Text = title;
            this._viewTabPage.ToolTipText = title;
            this._layout.Controls.Add( clientControl, 1, 1 );
        }
        else
        {
            this.ResizeClientArea( clientControl );
        }
    }

    /// <summary> Resize client area. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="clientControl"> The client control. </param>
    private void ResizeClientArea( Control clientControl )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( clientControl, nameof( clientControl ) );
#else
        if ( clientControl is null ) throw new ArgumentNullException( nameof( clientControl ) );
#endif

        this._tabs.Visible = false;
        this.Controls.Add( clientControl );
        System.Drawing.Size clientMargins = new( 0, 0 );
        System.Drawing.Size controlMargins = new( 0, 0 );
        System.Drawing.Size newClientSize;
        clientControl.BringToFront();
        newClientSize = new System.Drawing.Size( clientControl.Width + clientMargins.Width, this.Height );
        newClientSize = new System.Drawing.Size( newClientSize.Width, clientControl.Height + clientMargins.Height + this.StatusStrip.Height );
        bool resizeRequired = true;
        if ( resizeRequired )
        {
            this.ClientSize = new System.Drawing.Size( newClientSize.Width + controlMargins.Width, newClientSize.Height + controlMargins.Height );
            this.Refresh();
        }
    }

    /// <summary> Resize client area. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="tabControl">    The tab control. </param>
    /// <param name="messagesBox">   The messages box control. </param>
    /// <param name="clientControl"> The client control. </param>
    private void ResizeClientArea( TabControl tabControl, Control messagesBox, Control clientControl )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( messagesBox, nameof( messagesBox ) );
        ArgumentNullException.ThrowIfNull( tabControl, nameof( tabControl ) );
#else
        if ( messagesBox is null ) throw new ArgumentNullException( nameof( messagesBox ) );
        if ( tabControl is null ) throw new ArgumentNullException( nameof( tabControl ) );
#endif

        tabControl.BringToFront();
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( clientControl, nameof( clientControl ) );
#else
        if ( clientControl is null ) throw new ArgumentNullException( nameof( clientControl ) );
#endif

        System.Drawing.Size tabsMargins = new( tabControl.Width - messagesBox.Width, tabControl.Height - messagesBox.Height );
        System.Drawing.Size controlMargins = new( this.ClientSize.Width - this._layout.Width, this.ClientSize.Height - this._layout.Height );
        System.Drawing.Size newClientSize;
        newClientSize = new System.Drawing.Size( clientControl.Width + tabsMargins.Width, tabControl.Height );
        newClientSize = new System.Drawing.Size( newClientSize.Width, clientControl.Height + tabsMargins.Height );
        bool resizeRequired = true;
        if ( resizeRequired )
        {
            tabControl.Size = newClientSize;
            tabControl.Refresh();
            this.ClientSize = new System.Drawing.Size( newClientSize.Width + controlMargins.Width, newClientSize.Height + controlMargins.Height );
            this.Refresh();
        }
    }

    #endregion

    #region " user control "

    /// <summary> true to enable, false to disable the disposal of the User Control. </summary>
    private bool _userControlDisposeEnabled;

    /// <summary> The user control. </summary>
    private UserControl? UserControl { get; set; }

    /// <summary> Adds a User Control. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The value. </param>
    public void AddUserControl( UserControl value )
    {
        this.AddUserControl( "UI", value, true );
    }

    /// <summary> Adds a User Control. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="title"> The title. </param>
    /// <param name="value"> The value. </param>
    public void AddUserControl( string title, UserControl value )
    {
        this.AddUserControl( title, value, true );
    }

    /// <summary> Adds a User Control. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="title">             The title. </param>
    /// <param name="value">             The value. </param>
    /// <param name="disposeEnabled">    true to enable, false to disable the dispose. </param>
    /// <param name="showTraceMessages"> True to show, false to hide the trace messages. </param>
    public void AddUserControl( string title, UserControl value, bool disposeEnabled, bool showTraceMessages )
    {
        this.ResizeClientArea( title, value, showTraceMessages );
        this.UserControl = value;
        if ( showTraceMessages )
        {
            this.InitializeTraceListener();
        }

        this._userControlDisposeEnabled = disposeEnabled;
        this.UserControl.Dock = DockStyle.Fill;
        this.UserControl.TabIndex = 0;
        this.UserControl.BackColor = System.Drawing.Color.Transparent;
        this.UserControl.Font = new System.Drawing.Font( this.Font, System.Drawing.FontStyle.Regular );
        this.UserControl.Name = "_UserControl";
    }

    /// <summary> Adds a User Control. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="title">          The title. </param>
    /// <param name="value">          The value. </param>
    /// <param name="disposeEnabled"> true to enable, false to disable the dispose. </param>
    public void AddUserControl( string title, UserControl value, bool disposeEnabled )
    {
        this.AddUserControl( title, value, disposeEnabled, true );
    }

    #endregion

    #region " model view "

    /// <summary> true to enable, false to disable the disposal of the model view control. </summary>
    private bool _modelViewDisposeEnabled;

    /// <summary> The model view control. </summary>
    private ModelViewBase? _modelView;

    private ModelViewBase? ModelView
    {
        [System.Runtime.CompilerServices.MethodImpl( System.Runtime.CompilerServices.MethodImplOptions.Synchronized )]
        get => this._modelView;

        [System.Runtime.CompilerServices.MethodImpl( System.Runtime.CompilerServices.MethodImplOptions.Synchronized )]
        set
        {
            if ( this._modelView is not null )
            {
                this._modelView.PropertyChanged -= this.ModelViewPropertyChanged;
                this._modelView.ExceptionEventHandler -= this.ModelViewExceptionEvent;
            }

            this._modelView = value;
            if ( this._modelView is not null )
            {
                this._modelView.PropertyChanged += this.ModelViewPropertyChanged;
                this._modelView.ExceptionEventHandler += this.ModelViewExceptionEvent;
            }
        }
    }

    /// <summary> Adds a model view control. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The value. </param>
    public void AddModelView( ModelViewBase value )
    {
        this.AddModelView( "Instrument", value, true );
    }

    /// <summary> Adds a model view control. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="title"> The title. </param>
    /// <param name="value"> The value. </param>
    public void AddModelView( string title, ModelViewBase value )
    {
        this.AddModelView( title, value, true );
    }

    /// <summary> Adds a model view control. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="title">             The title. </param>
    /// <param name="value">             The value. </param>
    /// <param name="disposeEnabled">    true to enable, false to disable the dispose. </param>
    /// <param name="showTraceMessages"> True to show, false to hide the trace messages. </param>
    public void AddModelView( string title, ModelViewBase value, bool disposeEnabled, bool showTraceMessages )
    {
        this.ResizeClientArea( title, value, showTraceMessages );
        this.ModelView = value;
        if ( showTraceMessages )
            this.InitializeTraceListener();

        this._modelViewDisposeEnabled = disposeEnabled;
        this.ModelView.Dock = DockStyle.Fill;
        this.ModelView.TabIndex = 0;
        this.ModelView.BackColor = System.Drawing.Color.Transparent;
        this.ModelView.Font = new System.Drawing.Font( this.Font, System.Drawing.FontStyle.Regular );
        this.ModelView.Name = "_PropertyNotifyControl";
    }

    /// <summary> Adds a model view control. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="title">          The title. </param>
    /// <param name="value">          The value. </param>
    /// <param name="disposeEnabled"> true to enable, false to disable the dispose. </param>
    public void AddModelView( string title, ModelViewBase value, bool disposeEnabled )
    {
        this.AddModelView( title, value, disposeEnabled, true );
    }

    /// <summary> Executes the property change action. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender">       The sender. </param>
    /// <param name="propertyName"> Name of the property. </param>
    private void HandlePropertyChanged( ModelViewBase? sender, string? propertyName )
    {
        if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
        {
            return;
        }

        switch ( propertyName )
        {
            case nameof( ModelViewBase.StatusPrompt ):
                {
                    this.StatusPrompt = sender.StatusPrompt ?? string.Empty;
                    break;
                }

            default:
                {
                    break;
                }
        }
    }

    /// <summary> Model view property changed. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> The sender. </param>
    /// <param name="e">      Property Changed event information. </param>
    private void ModelViewPropertyChanged( object? sender, PropertyChangedEventArgs e )
    {
        if ( this.InitializingComponents || sender is null || e is null )
        {
            return;
        }

        string activity = $"handling {nameof( ModelViewBase )}.{e.PropertyName} change";
        try
        {
            if ( this.InvokeRequired )
            {
                _ = this.BeginInvoke( new Action<object, PropertyChangedEventArgs>( this.ModelViewPropertyChanged ), [sender, e] );
            }
            else
            {
                this.HandlePropertyChanged( sender as ModelViewBase, e.PropertyName );
            }
        }
        catch ( Exception ex )
        {
            _ = this.LogException( ex, activity );
        }
    }


    private void HandleExceptionEvent( ModelViewBase? sender, Exception ex )
    {
        if ( sender is not null && ex is not null )
        {
            _ = this.LogException( ex, $"Exception occurred in the {nameof( this.ModelView )}" );
        }
    }

    private void ModelViewExceptionEvent( object? sender, System.Threading.ThreadExceptionEventArgs e )
    {
        if ( this.InitializingComponents || sender is null || e is null )
        {
            return;
        }

        string activity = $"handling {nameof( ModelViewBase )} {nameof( ModelViewBase.ExceptionEventHandler )} event";
        try
        {
            if ( this.InvokeRequired )
            {
                _ = this.BeginInvoke( new Action<object, System.Threading.ThreadExceptionEventArgs>( this.ModelViewExceptionEvent ), [sender, e] );
            }
            else
            {
                this.HandleExceptionEvent( sender as ModelViewBase, e.Exception );
            }
        }
        catch ( Exception ex )
        {
            _ = this.LogException( ex, activity );
        }
    }

    #endregion

    #region " model view logger "

    /// <summary> true to enable, false to disable the disposal of a Model View Logger control. </summary>
    private bool _modelViewLoggerDisposeEnabled;

    private ModelViewLoggerBase? _modelViewLogger;

    private ModelViewLoggerBase? ModelViewLogger
    {
        [System.Runtime.CompilerServices.MethodImpl( System.Runtime.CompilerServices.MethodImplOptions.Synchronized )]
        get => this._modelViewLogger;

        [System.Runtime.CompilerServices.MethodImpl( System.Runtime.CompilerServices.MethodImplOptions.Synchronized )]
        set
        {
            if ( this._modelViewLogger is not null )
            {
                this._modelViewLogger.PropertyChanged -= this.ModelViewLoggerPropertyChanged;
                this._modelViewLogger.ExceptionEventHandler -= this.ModelViewLoggerExceptionEvent;
            }

            this._modelViewLogger = value;
            if ( this._modelViewLogger is not null )
            {
                this._modelViewLogger.PropertyChanged += this.ModelViewLoggerPropertyChanged;
                this._modelViewLogger.ExceptionEventHandler += this.ModelViewLoggerExceptionEvent;
            }
        }
    }

    /// <summary> Adds a Model View logger control panel. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The value. </param>
    public void AddModelViewLoggerControl( ModelViewLoggerBase value )
    {
        this.AddModelViewLoggerControl( "View", value, true );
    }

    /// <summary> Adds an Model View logger control. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="title"> The title. </param>
    /// <param name="value"> The value. </param>
    public void AddModelViewLoggerControl( string title, ModelViewLoggerBase value )
    {
        this.AddModelViewLoggerControl( title, value, true );
    }

    /// <summary> Adds an Model View logger control. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="title">             The title. </param>
    /// <param name="value">             The value. </param>
    /// <param name="disposeEnabled">    true to enable, false to disable the dispose. </param>
    /// <param name="showTraceMessages"> True to show, false to hide the trace messages. </param>
    public void AddModelViewLoggerControl( string title, ModelViewLoggerBase value, bool disposeEnabled, bool showTraceMessages )
    {
        this.ResizeClientArea( title, value, showTraceMessages );
        this.ModelViewLogger = value;
        if ( showTraceMessages )
            this.InitializeTraceListener();

        this._modelViewLoggerDisposeEnabled = disposeEnabled;
        this.ModelViewLogger.Dock = DockStyle.Fill;
        this.ModelViewLogger.TabIndex = 0;
        this.ModelViewLogger.BackColor = System.Drawing.Color.Transparent;
        this.ModelViewLogger.Font = new System.Drawing.Font( this.Font, System.Drawing.FontStyle.Regular );
        this.ModelViewLogger.Name = "_ModelViewLoggerControl";
    }

    /// <summary> Adds an Model View logger control. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="title">          The title. </param>
    /// <param name="value">          The value. </param>
    /// <param name="disposeEnabled"> true to enable, false to disable the dispose. </param>
    public void AddModelViewLoggerControl( string title, ModelViewLoggerBase value, bool disposeEnabled )
    {
        this.AddModelViewLoggerControl( title, value, disposeEnabled, true );
    }

    /// <summary> Executes the property change action. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender">       The sender. </param>
    /// <param name="propertyName"> Name of the property. </param>
    private void HandlePropertyChanged( ModelViewLoggerBase? sender, string? propertyName )
    {
        if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
        {
            return;
        }

        switch ( propertyName )
        {
            case nameof( ModelViewBase.StatusPrompt ):
                {
                    this.StatusPrompt = sender.StatusPrompt ?? string.Empty;
                    break;
                }

            default:
                {
                    break;
                }
        }

    }

    /// <summary> Model View logger control property changed. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> The sender. </param>
    /// <param name="e">      Property Changed event information. </param>
    private void ModelViewLoggerPropertyChanged( object? sender, PropertyChangedEventArgs e )
    {
        if ( this.InitializingComponents || sender is null || e is null )
        {
            return;
        }

        string activity = $"handling {nameof( ModelViewLoggerBase )}.{e.PropertyName} change";
        try
        {
            if ( this.InvokeRequired )
            {
                _ = this.BeginInvoke( new Action<object, PropertyChangedEventArgs>( this.ModelViewLoggerPropertyChanged ), [sender, e] );
            }
            else
            {
                this.HandlePropertyChanged( sender as ModelViewLoggerBase, e.PropertyName );
            }
        }
        catch ( Exception ex )
        {
            _ = this.LogException( ex, activity );
        }
    }


    private void HandleExceptionEvent( ModelViewLoggerBase? sender, Exception ex )
    {
        if ( sender is not null && ex is not null )
        {
            _ = this.LogException( ex, $"Exception occurred in the {nameof( this.ModelViewLogger )}" );
        }
    }

    private void ModelViewLoggerExceptionEvent( object? sender, System.Threading.ThreadExceptionEventArgs e )
    {
        if ( this.InitializingComponents || sender is null || e is null )
        {
            return;
        }

        string activity = $"handling {nameof( ModelViewLoggerBase )} {nameof( ModelViewBase.ExceptionEventHandler )} event";
        try
        {
            if ( this.InvokeRequired )
            {
                _ = this.BeginInvoke( new Action<object, System.Threading.ThreadExceptionEventArgs>( this.ModelViewLoggerExceptionEvent ), [sender, e] );
            }
            else
            {
                this.HandleExceptionEvent( sender as ModelViewLoggerBase, e.Exception );
            }
        }
        catch ( Exception ex )
        {
            _ = this.LogException( ex, activity );
        }
    }

    #endregion

    #region " exposed controls "

    /// <summary> Gets the status strip. </summary>
    /// <value> The status strip. </value>
    protected StatusStrip StatusStrip { get; private set; }

    /// <summary> Gets the status label. </summary>
    /// <value> The status label. </value>
    protected ToolStripStatusLabel StatusLabel { get; private set; }

    /// <summary> Gets or sets the status prompt. </summary>
    /// <value> The status prompt. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string StatusPrompt
    {
        get => this.StatusLabel?.ToolTipText ?? string.Empty;
        set
        {
            if ( !string.Equals( value, this.StatusPrompt, StringComparison.Ordinal ) )
            {
                this.StatusLabel.Text = WinForms.ModelViewForm.CompactExtensions.CompactExtensionMethods.Compact( value, this.StatusLabel );
                this.StatusLabel.ToolTipText = value;
            }
        }
    }

    #endregion
}
