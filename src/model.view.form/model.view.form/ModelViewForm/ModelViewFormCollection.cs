using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using cc.isr.WinControls;

namespace cc.isr.WinForms.ModelViewForm;

/// <summary> Collection of Mode View Forms. </summary>
/// <remarks>
/// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2016-01-04 </para>
/// </remarks>
public partial class ModelViewFormCollection : List<ModelViewForm>
{
    /// <summary> Adds and shows a new form,. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="form"> The form. </param>
    public void ShowNew( ModelViewForm form )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( form, nameof( form ) );
#else
        if ( form is null ) throw new ArgumentNullException( nameof( form ) );
#endif
        this.Add( form );
        form.FormClosed += this.OnClosed;
        form.Show();
    }

    /// <summary> Adds a form. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="form"> The form. </param>
    /// <returns> A ModeViewForm. </returns>
    public ModelViewForm AddForm( ModelViewForm form )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( form, nameof( form ) );
#else
        if ( form is null ) throw new ArgumentNullException( nameof( form ) );
#endif
        this.Add( form );
        form.FormClosed += this.OnClosed;
        return form;
    }

    /// <summary> Handles a member form closed event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information to send to registered event handlers. </param>
    private void OnClosed( object? sender, EventArgs e )
    {
        if ( sender is ModelViewForm f )
        {
            f.FormClosed -= this.OnClosed;
            _ = this.Remove( f );
            f.Dispose();
        }
    }

    /// <summary> Removes the dispose described by value. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The value. </param>
    public void RemoveDispose( ModelViewForm value )
    {
        ModelViewForm? f = value;
        if ( f is not null )
        {
            f.FormClosed -= this.OnClosed;
            _ = this.Remove( f );
            f.Dispose();
            f = null;
        }
    }

    /// <summary>
    /// Removes all items from the <see cref="System.Collections.ICollection" />.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public void ClearDispose()
    {
        while ( this.Any() )
        {
            this.RemoveDispose( this[0] );
            Application.DoEvents();
        }
    }

    #region " property notify control base "

    /// <summary> Adds and shows the form. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="title"> The title. </param>
    /// <param name="form">  The form. </param>
    /// <param name="panel"> The panel. </param>
    public void ShowNew( string title, ModelViewForm form, ModelViewBase panel )
    {
        this.ShowNew( title, form, panel, false );
    }

    /// <summary>   Adds and shows the form. </summary>
    /// <remarks>   David, 2020-09-21. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="title">            The title. </param>
    /// <param name="form">             The form. </param>
    /// <param name="panel">            The panel. </param>
    /// <param name="disposeEnabled">   True to enable, false to disable the dispose. </param>
    public void ShowNew( string title, ModelViewForm form, ModelViewBase panel, bool disposeEnabled )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( form, nameof( form ) );
#else
        if ( form is null ) throw new ArgumentNullException( nameof( form ) );
#endif
        form.AddModelView( title, panel, disposeEnabled );
        this.ShowNew( form );
    }

    /// <summary> Adds and shows the form. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="title"> The title. </param>
    /// <param name="panel"> The panel. </param>
    public void ShowNew( string title, ModelViewBase panel )
    {
        ModelViewForm? form = null;
        try
        {
            form = new ModelViewForm();
            this.ShowNew( title, form, panel );
        }
        catch
        {
            form?.Dispose();
            throw;
        }

    }

    /// <summary> Adds and shows the form. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="form">  The form. </param>
    /// <param name="panel"> The panel. </param>
    public void ShowNew( ModelViewForm form, ModelViewBase panel )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( form, nameof( form ) );
#else
        if ( form is null ) throw new ArgumentNullException( nameof( form ) );
#endif

        form.AddModelView( panel );
        this.ShowNew( form );
    }

    /// <summary> Adds and shows the form. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="panel"> The panel. </param>
    public void ShowNew( ModelViewBase panel )
    {
        ModelViewForm? form = null;
        try
        {
            form = new ModelViewForm();
            this.ShowNew( form, panel );
        }
        catch
        {
            form?.Dispose();
            throw;
        }
    }

    #endregion

    #region " model view control base"

    /// <summary> Adds and shows the form. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="form">  The form. </param>
    /// <param name="panel"> The panel. </param>
    public void ShowNew( ModelViewForm form, ModelViewLoggerBase panel )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( form, nameof( form ) );
#else
        if ( form is null ) throw new ArgumentNullException( nameof( form ) );
#endif
        form.AddModelViewLoggerControl( panel );
        this.ShowNew( form );
    }

    /// <summary> Adds and shows the form. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="panel"> The panel. </param>
    public void ShowNew( ModelViewLoggerBase panel )
    {
        ModelViewForm? form = null;
        try
        {
            form = new ModelViewForm();
            this.ShowNew( form, panel );
        }
        catch
        {
            form?.Dispose();
            throw;
        }
    }

    /// <summary> Adds and shows the form. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="title"> The title. </param>
    /// <param name="panel"> The panel. </param>
    public void ShowNew( string title, ModelViewLoggerBase panel )
    {
        ModelViewForm? form = null;
        try
        {
            form = new ModelViewForm();
            this.ShowNew( title, form, panel );
        }
        catch
        {
            form?.Dispose();
            throw;
        }
    }

    /// <summary> Adds and shows the form. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="title"> The title. </param>
    /// <param name="form">  The form. </param>
    /// <param name="panel"> The panel. </param>
    public void ShowNew( string title, ModelViewForm form, ModelViewLoggerBase panel )
    {
        this.ShowNew( title, form, panel, false );
    }

    /// <summary>   Adds and shows the form. </summary>
    /// <remarks>   David, 2020-09-21. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="title">            The title. </param>
    /// <param name="form">             The form. </param>
    /// <param name="panel">            The panel. </param>
    /// <param name="disposedEnabled">  True to enable, false to disable the disposed. </param>
    public void ShowNew( string title, ModelViewForm form, ModelViewLoggerBase panel, bool disposedEnabled )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( form, nameof( form ) );
#else
        if ( form is null ) throw new ArgumentNullException( nameof( form ) );
#endif
        form.AddModelViewLoggerControl( title, panel, disposedEnabled );
        this.ShowNew( form );
    }

    #endregion
}
