using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using Microsoft.Extensions.Logging;

namespace cc.isr.WinForms.ModelViewForm;

public partial class ModelViewForm
{
    #region " text  writer "

    /// <summary>   Adds a display trace event writer. </summary>
    /// <remarks>   David, 2021-02-23. </remarks>
    /// <param name="textWriter">   The trace Event tWriter. </param>
    public void AddDisplayTextWriter( Tracing.ITraceEventWriter textWriter )
    {
        textWriter.TraceLevel = this.DisplayTraceEventType;
        cc.isr.Tracing.TracingPlatform.Instance.AddTraceEventWriter( textWriter );
    }

    /// <summary>   Removes the display text writer described by <paramref name="textWriter"/>. </summary>
    /// <remarks>   David, 2021-02-23. </remarks>
    /// <param name="textWriter">   The trace Event tWriter. </param>
    public static void RemoveDisplayTextWriter( Tracing.ITraceEventWriter textWriter )
    {
        cc.isr.Tracing.TracingPlatform.Instance.RemoveTraceEventWriter( textWriter );
    }

    #endregion

    #region " display log level "
#if false
    // log level does not map to trace event types, which filter the display.

    private KeyValuePair<LogLevel, string> _displayLogLevelValueNamePair;

    /// <summary>
    /// Gets or sets the <see cref="Microsoft.Extensions.Logging.LogLevel"/> value name pair for display.
    /// </summary>
    /// <value> The log level value name pair. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public KeyValuePair<LogLevel, string> DisplayLogLevelValueNamePair
    {
        get => this._displayLogLevelValueNamePair;
        set
        {
             if ( !KeyValuePair<LogLevel, string>.Equals( value, this.DisplayLogLevelValueNamePair ) )
            {
                this._displayLogLevelValueNamePair = value;
                this.NotifyPropertyChanged();
            }
        }
    }

    private LogLevel _displayLogLevel;

    /// <summary>
    /// Gets or sets the <see cref="Microsoft.Extensions.Logging.LogLevel"/> for display.
    /// </summary>
    /// <value> The trace event writer log level. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public LogLevel DisplayLogLevel
    {
        get => this._displayLogLevel;
        set
        {
             if ( value != this.DisplayLogLevel )
            {
                this._displayLogLevel = value;
                this.NotifyPropertyChanged();
            }
        }
    }
#endif
    #endregion

    #region " display trace event type "

    private KeyValuePair<TraceEventType, string> _traceEventWriterTraceEventValueNamePair;

    /// <summary>
    /// Gets or sets the <see cref="TraceEventType"/> value name pair for the global trace event
    /// writer. This level determines the level of all the
    /// <see cref="Tracing.ITraceEventWriter"/>s Trace Listeners. Each trace listener can still listen at a
    /// lower level.
    /// </summary>
    /// <value> The trace event writer trace event value name pair. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public KeyValuePair<TraceEventType, string> TraceEventWriterTraceEventValueNamePair
    {
        get => this._traceEventWriterTraceEventValueNamePair;
        set => _ = this.SetProperty( ref this._traceEventWriterTraceEventValueNamePair, value );
    }

    private TraceEventType _traceEventWriterTraceEventType;

    /// <summary>
    /// Gets or sets the <see cref="TraceEventType"/> for the global trace event writer. This level
    /// determines the level of all the
    /// <see cref="Tracing.ITextWriter"/>s Trace Listeners. Each trace listener can still listen at a
    /// lower level.
    /// </summary>
    /// <value> The type of the trace event writer trace event. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public TraceEventType TraceEventWriterTraceEventType
    {
        get => this._traceEventWriterTraceEventType;
        set => _ = this.SetProperty( ref this._traceEventWriterTraceEventType, value );
    }

    /// <summary>
    /// Gets or sets the <see cref="TraceEventType"/> value name pair for display.
    /// </summary>
    /// <value> The log level value name pair. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public KeyValuePair<TraceEventType, string> DisplayTraceEventTypeValueNamePair { get; set; }

    /// <summary>
    /// Gets or sets the <see cref="TraceEventType"/> for display.
    /// </summary>
    /// <value> The trace event writer log level. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public TraceEventType DisplayTraceEventType { get; set; }

    #endregion

    #region " logging log level "

    private KeyValuePair<LogLevel, string> _loggingLevelValueNamePair;

    /// <summary>
    /// Gets or sets the <see cref="LogLevel"/> value name pair for logging. This level
    /// determines the level of the <see cref="ILogger"/>.
    /// </summary>
    /// <value> The log trace event value name pair. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public KeyValuePair<LogLevel, string> LoggingLevelValueNamePair
    {
        get => this._loggingLevelValueNamePair;
        set => _ = this.SetProperty( ref this._loggingLevelValueNamePair, value );
    }

    private LogLevel _loggingLogLevel;

    /// <summary>   Gets or sets <see cref="LogLevel"/> value for logging. </summary>
    /// <value> The trace event type for logging. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public LogLevel LoggingLogLevel
    {
        get => this._loggingLogLevel;
        set => _ = this.SetProperty( ref this._loggingLogLevel, value );
    }

    #endregion
}
