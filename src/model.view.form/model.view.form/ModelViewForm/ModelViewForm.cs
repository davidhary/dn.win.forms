using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Windows.Forms;

namespace cc.isr.WinForms.ModelViewForm;

/// <summary> A form for hosing model views and a text box listener. </summary>
/// <remarks>
/// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2015-12-26, 2.1.5836. </para>
/// </remarks>
public partial class ModelViewForm : Form, INotifyPropertyChanged
{
    #region " construction and cleanup "

    /// <summary> Gets the initializing components sentinel. </summary>
    /// <value> The initializing components sentinel. </value>
    protected bool InitializingComponents { get; set; }

    /// <summary> Specialized default constructor for use only by derived classes. </summary>
    public ModelViewForm() : base()
    {
        this.InitializingComponents = true;
        this.InitializeComponent();
        this.InitializingComponents = false;
    }

    /// <summary>
    /// Disposes of the resources (other than memory) used by the
    /// <see cref="Form" />.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="disposing"> true to release both managed and unmanaged resources; false to
    /// release only unmanaged resources. </param>
    [System.Diagnostics.DebuggerNonUserCode()]
    protected override void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                // this._traceMessagesBox.SuspendUpdatesReleaseIndicators();
                this.components?.Dispose();
                this.components = null;
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }

    #region " class style "

#pragma warning disable IDE1006
    /// <summary> The enable drop shadow version. </summary>
    public const int EnableDropShadowVersion = 5;
#pragma warning restore IDE1006

    /// <summary> Gets the class style. </summary>
    /// <value> The class style. </value>
    protected ClassStyleConstants ClassStyle { get; set; } = ClassStyleConstants.None;

    /// <summary>   Adds a drop shadow parameter. </summary>
    /// <remarks>
    /// From <see href="http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx">Code Project</see>.
    /// </remarks>
    /// <value> Options that control the create. </value>
    protected override CreateParams CreateParams
    {
        get
        {
            CreateParams cp = base.CreateParams;
            cp.ClassStyle |= ( int ) this.ClassStyle;
            return cp;
        }
    }

    #endregion

    #endregion

    #region " show "

    /// <summary>
    /// Shows the <see cref="RichTextBox">rich text box</see> form with these messages.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="mdiForm"> The MDI form. </param>
    /// <param name="owner">   The owner. </param>
    public void Show( Form mdiForm, IWin32Window owner )
    {
        if ( mdiForm is not null && mdiForm.IsMdiContainer )
        {
            this.MdiParent = mdiForm;
            mdiForm.Show();
        }

        this.Show( owner );
    }

    /// <summary>
    /// Shows the <see cref="RichTextBox">rich text box</see> form with these messages.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="mdiForm"> The MDI form. </param>
    public void ShowDialog( Form mdiForm )
    {
        if ( mdiForm is not null && mdiForm.IsMdiContainer )
        {
            this.MdiParent = mdiForm;
            mdiForm.Show();
        }

        _ = this.ShowDialog();
    }

    #endregion

    #region " notify property change implementation "

    /// <summary>   Occurs when a property value changes. </summary>
    public event PropertyChangedEventHandler? PropertyChanged;

    /// <summary>   Executes the 'property changed' action. </summary>
    /// <param name="propertyName"> Name of the property. </param>
    protected virtual void OnPropertyChanged( string? propertyName )
    {
        if ( !string.IsNullOrEmpty( propertyName ) )
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
    }

    /// <summary>   Executes the 'property changed' action. </summary>
    /// <typeparam name="TValue">    Generic type parameter. </typeparam>
    /// <param name="backingField"> [in,out] The backing field. </param>
    /// <param name="value">        The value. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected virtual bool OnPropertyChanged<TValue>( ref TValue backingField, TValue value, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = "" )
    {
        if ( EqualityComparer<TValue>.Default.Equals( backingField, value ) )
            return false;

        backingField = value;
        this.OnPropertyChanged( propertyName );
        return true;
    }

    /// <summary>   Sets a property. </summary>
    /// <typeparam name="TValue">    Generic type parameter. </typeparam>
    /// <param name="prop">         [in,out] The property. </param>
    /// <param name="value">        The value. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected bool SetProperty<TValue>( ref TValue prop, TValue value, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = null )
    {
        if ( EqualityComparer<TValue>.Default.Equals( prop, value ) ) return false;
        prop = value;
        this.OnPropertyChanged( propertyName );
        return true;
    }

    /// <summary>   Sets a property. </summary>
    /// <remarks>   2023-03-24. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <typeparam name="TValue">    Generic type parameter. </typeparam>
    /// <param name="oldValue">     The old value. </param>
    /// <param name="newValue">     The new value. </param>
    /// <param name="callback">     The callback. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected bool SetProperty<TValue>( TValue oldValue, TValue newValue, Action callback, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = null )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( callback, nameof( callback ) );
#else
        if ( callback is null ) throw new ArgumentNullException( nameof( callback ) );
#endif

        if ( EqualityComparer<TValue>.Default.Equals( oldValue, newValue ) )
        {
            return false;
        }

        callback();

        this.OnPropertyChanged( propertyName );

        return true;
    }

    /// <summary>   Notifies a property changed. </summary>
    /// <remarks>   2021-02-01. </remarks>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] string propertyName = "" )
    {
        this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
    }

    /// <summary>   Removes the property changed event handlers. </summary>
    /// <remarks>   2021-06-28. </remarks>
    protected void RemovePropertyChangedEventHandlers()
    {
        PropertyChangedEventHandler? handler = this.PropertyChanged;
        if ( handler is not null )
        {
            foreach ( Delegate? item in handler.GetInvocationList() )
            {
                handler -= ( PropertyChangedEventHandler ) item;
            }
        }
    }

    #endregion

    #region " trace listener "

    private Tracing.WinForms.TextBoxTraceEventWriter? TextBoxTextWriter { get; set; }

    /// <summary>   Initializes the trace listener. </summary>
    /// <remarks>   David, 2021-03-04. </remarks>
    [MemberNotNull( nameof( TextBoxTextWriter ) )]
    private void InitializeTraceListener()
    {
        this.TextBoxTextWriter = new( this._traceMessagesBox )
        {
            ContainerPanel = this._messagesTabPage,
            TabCaption = "Log",
            CaptionFormat = "{0} " + System.Text.Encoding.GetEncoding( 1200 ).GetString( [240] ),
            ResetCount = 1000,
            PresetCount = 500
        };

        this.AddDisplayTextWriter( this.TextBoxTextWriter );

        // Initialize the Alert Control 
#if false
        AllertToggle.AlertSoundEnabled = false;
        AllertToggle.AlertLevel = TraceEventType.Warning;
        AllertToggle.AlertsToggleControl = Me._alertsToolStripTextBox.TextBox
#endif
    }

    #endregion

    #region " log exception "

    /// <summary>   Adds an exception data. </summary>
    /// <remarks>   David, 2021-07-02. </remarks>
    /// <param name="ex">   The exception. </param>
    /// <returns>   True if exception data was added for this exception; otherwise, false. </returns>
    protected virtual bool AddExceptionData( Exception ex )
    {
        return false;
    }

    /// <summary>   Log exception. </summary>
    /// <remarks>
    /// Declared as must override so that the calling method could add exception data before
    /// recording this exception.
    /// </remarks>
    /// <param name="ex">               The ex. </param>
    /// <param name="activity">         The activity. </param>
    /// <param name="memberName">       (Optional) Name of the caller member. </param>
    /// <param name="sourceFilePath">   (Optional) Full pathname of the caller source file. </param>
    /// <param name="sourceLineNumber"> (Optional) Line number in the caller source file. </param>
    /// <returns>   A <see cref="string" />. </returns>
    protected virtual string LogException( Exception ex, string activity,
        [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
        [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
        [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0 )
    {
        _ = this.AddExceptionData( ex );
        string message = cc.isr.Logging.Extensions.ILoggerExtensions.BuildMultiLineMessage( ex, activity, memberName, sourceFilePath, sourceLineNumber );
        System.Diagnostics.Trace.WriteLine( message );
        return message;
    }

    /// <summary>   Logs a warning. </summary>
    /// <remarks>   2023-05-26. </remarks>
    /// <param name="activity">         The activity. </param>
    /// <param name="memberName">       (Optional) Name of the caller member. </param>
    /// <param name="sourceFilePath">   (Optional) Full pathname of the caller source file. </param>
    /// <param name="sourceLineNumber"> (Optional) Line number in the caller source file. </param>
    /// <returns>   A <see cref="string" />. </returns>
    protected virtual string LogWarning( string activity,
        [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
        [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
        [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0 )
    {
        string message = cc.isr.Logging.Extensions.ILoggerExtensions.BuildMultiLineMessage( activity, memberName, sourceFilePath, sourceLineNumber );
        System.Diagnostics.Trace.WriteLine( message );
        return message;
    }

    #endregion
}
/// <summary> Values that represent class style constants. </summary>
/// <remarks> David, 2020-09-24. </remarks>
[Flags]
public enum ClassStyleConstants
{
    /// <summary> . </summary>
    [System.ComponentModel.Description( "Not Specified" )]
    None = 0,

    /// <summary> . </summary>
    [System.ComponentModel.Description( "No close button" )]
    HideCloseButton = 0x200,

    /// <summary> . </summary>
    [System.ComponentModel.Description( "Drop Shadow" )]
    DropShadow = 0x20000 // 131072
}
