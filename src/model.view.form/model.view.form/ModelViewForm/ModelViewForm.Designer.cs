using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;

namespace cc.isr.WinForms.ModelViewForm;

public partial class ModelViewForm
{
    // Required by the Windows Form Designer
    private System.ComponentModel.IContainer components;
    // NOTE: The following procedure is required by the Windows Form Designer
    // It can be modified using the Windows Form Designer.
    // Do not modify it using the code editor.
    [DebuggerStepThrough()]
    [MemberNotNull( nameof (components ))]
    [MemberNotNull(nameof( StatusStrip ))]
    [MemberNotNull( nameof( StatusLabel ) )]
    private void InitializeComponent()
    {
        components = new System.ComponentModel.Container();
        _toolTip = new System.Windows.Forms.ToolTip(components);
        _tabs = new System.Windows.Forms.TabControl();
        _viewTabPage = new System.Windows.Forms.TabPage();
        _layout = new System.Windows.Forms.TableLayoutPanel();
        _messagesTabPage = new System.Windows.Forms.TabPage();
        _traceMessagesBox = new cc.isr.Logging.TraceLog.WinForms.MessagesBox();
        StatusStrip = new System.Windows.Forms.StatusStrip();
        StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
        _tabs.SuspendLayout();
        _viewTabPage.SuspendLayout();
        _messagesTabPage.SuspendLayout();
        StatusStrip.SuspendLayout();
        SuspendLayout();
        // 
        // _toolTip
        // 
        _toolTip.IsBalloon = true;
        // 
        // _tabs
        // 
        _tabs.Controls.Add(_viewTabPage);
        _tabs.Controls.Add(_messagesTabPage);
        _tabs.Dock = System.Windows.Forms.DockStyle.Fill;
        _tabs.ItemSize = new System.Drawing.Size(42, 22);
        _tabs.Location = new System.Drawing.Point(0, 0);
        _tabs.Name = "_Tabs";
        _tabs.SelectedIndex = 0;
        _tabs.Size = new System.Drawing.Size(466, 541);
        _tabs.TabIndex = 1;
        // 
        // _instrumentTabPage
        // 
        _viewTabPage.Controls.Add(_layout);
        _viewTabPage.Location = new System.Drawing.Point(4, 26);
        _viewTabPage.Name = "_InstrumentTabPage";
        _viewTabPage.Size = new System.Drawing.Size(458, 511);
        _viewTabPage.TabIndex = 5;
        _viewTabPage.Text = "Instrument";
        _viewTabPage.ToolTipText = "Instrument";
        _viewTabPage.UseVisualStyleBackColor = true;
        // 
        // _instrumentLayout
        // 
        _layout.ColumnCount = 3;
        _layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 6.0f));
        _layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
        _layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 6.0f));
        _layout.Dock = System.Windows.Forms.DockStyle.Fill;
        _layout.Location = new System.Drawing.Point(0, 0);
        _layout.Name = "_InstrumentLayout";
        _layout.RowCount = 3;
        _layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 6.0f));
        _layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
        _layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 6.0f));
        _layout.Size = new System.Drawing.Size(458, 511);
        _layout.TabIndex = 1;
        // 
        // _messagesTabPage
        // 
        _messagesTabPage.Controls.Add(_traceMessagesBox);
        _messagesTabPage.Location = new System.Drawing.Point(4, 26);
        _messagesTabPage.Name = "_MessagesTabPage";
        _messagesTabPage.Size = new System.Drawing.Size(458, 511);
        _messagesTabPage.TabIndex = 2;
        _messagesTabPage.Text = "Log";
        _messagesTabPage.UseVisualStyleBackColor = true;
        // 
        // _traceMessagesBox
        // 
        _traceMessagesBox.AcceptsReturn = true;
        _traceMessagesBox.BackColor = System.Drawing.SystemColors.Info;
        _traceMessagesBox.CausesValidation = false;
        _traceMessagesBox.Dock = System.Windows.Forms.DockStyle.Fill;
        _traceMessagesBox.Font = new System.Drawing.Font("Consolas", 8.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
        _traceMessagesBox.ForeColor = System.Drawing.SystemColors.WindowText;
        _traceMessagesBox.Location = new System.Drawing.Point(0, 0);
        _traceMessagesBox.MaxLength = 0;
        _traceMessagesBox.Multiline = true;
        _traceMessagesBox.Name = "_TraceMessagesBox";
        _traceMessagesBox.ReadOnly = true;
        _traceMessagesBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
        _traceMessagesBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
        _traceMessagesBox.Size = new System.Drawing.Size(458, 511);
        _traceMessagesBox.TabIndex = 15;
        // 
        // _statusStrip
        // 
        StatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { StatusLabel });
        StatusStrip.Location = new System.Drawing.Point(0, 541);
        StatusStrip.Name = "_StatusStrip";
        StatusStrip.Size = new System.Drawing.Size(466, 22);
        StatusStrip.TabIndex = 2;
        StatusStrip.Text = "Status Strip";
        // 
        // _statusLabel
        // 
        StatusLabel.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 0);
        StatusLabel.Name = "_StatusLabel";
        StatusLabel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
        StatusLabel.Size = new System.Drawing.Size(420, 17);
        StatusLabel.Spring = true;
        StatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        // 
        // ModeViewForm
        // 
        AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
        BackColor = System.Drawing.SystemColors.Control;
        ClientSize = new System.Drawing.Size(466, 563);
        Controls.Add(_tabs);
        Controls.Add(StatusStrip);
        Location = new System.Drawing.Point(297, 150);
        MaximizeBox = false;
        Name = "ModeViewForm";
        RightToLeft = System.Windows.Forms.RightToLeft.No;
        StartPosition = System.Windows.Forms.FormStartPosition.Manual;
        Text = "Console";
        _tabs.ResumeLayout(false);
        _viewTabPage.ResumeLayout(false);
        _messagesTabPage.ResumeLayout(false);
        _messagesTabPage.PerformLayout();
        StatusStrip.ResumeLayout(false);
        StatusStrip.PerformLayout();
        ResumeLayout(false);
        PerformLayout();
    }

    private cc.isr.Logging.TraceLog.WinForms.MessagesBox _traceMessagesBox;
    private System.Windows.Forms.TabPage _messagesTabPage;
    private System.Windows.Forms.TabControl _tabs;
    private System.Windows.Forms.ToolTip _toolTip;
    private System.Windows.Forms.TabPage _viewTabPage;
    private System.Windows.Forms.TableLayoutPanel _layout;
}
