using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace cc.isr.WinForms.ModelViewForm.ComboBoxExtensions;

/// <summary>   A combo box extensions methods. </summary>
/// <remarks>   David, 2021-03-04. </remarks>
public static class ComboBoxExtensionsMethods
{
    /// <summary> List trace event levels. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="control"> The control. </param>
    public static void ListTraceEventLevels( this System.Windows.Forms.ComboBox control )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( control, nameof( control ) );
#else
        if ( control is null )
        {
            throw new ArgumentNullException( nameof( control ) );
        }
#endif

        bool comboEnabled = control.Enabled;
        control.Enabled = false;
        control.DataSource = null;
        control.ValueMember = nameof( System.Collections.Generic.KeyValuePair<int, string>.Key );
        control.DisplayMember = nameof( System.Collections.Generic.KeyValuePair<int, string>.Value );
        control.Items.Clear();
        control.DataSource = cc.isr.Logging.TraceLog.TraceEventSelector.TraceEventValueNamePairs;
        // This does not seem to work. It lists the items in the combo box, but the control items are empty.
        // control.DataSource = cc.isr.Std.EnumExtensions.ValueDescriptionPairs(TraceEventType.Critical).ToList
        // control.SelectedIndex = -1
        control.Enabled = comboEnabled;
        control.Invalidate();
    }

    /// <summary> Select item. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="control"> The control. </param>
    /// <param name="value">   The value. </param>
    public static void SelectItem( this System.Windows.Forms.ToolStripComboBox control, TraceEventType value )
    {
        if ( control is not null )
        {
            // control.SelectedItem = New KeyValuePair(Of TraceEventType, String)(value, value.ToString)
            control.SelectedItem = Logging.TraceLog.TraceEventSelector.ToTraceEventValueNamePair( value );
        }
    }

    /// <summary> Selected value. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="control">      The control. </param>
    /// <param name="defaultValue"> The default value. </param>
    /// <returns> A TraceEventType. </returns>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    public static TraceEventType SelectedValue( this System.Windows.Forms.ToolStripComboBox control, TraceEventType defaultValue )
    {
        if ( control is not null && control.SelectedItem is not null )
        {
            KeyValuePair<TraceEventType, string> kvp = ( KeyValuePair<TraceEventType, string> ) control.SelectedItem;
#pragma warning disable CA2263
            if ( Enum.IsDefined( typeof( TraceEventType ), kvp.Key ) )
#pragma warning restore CA2263
            {
                defaultValue = kvp.Key;
            }
        }

        return defaultValue;
    }

}
