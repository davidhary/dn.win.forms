using System;
using System.Windows.Forms;

namespace cc.isr.WinForms.ModelViewForm.BindingExtensions;
/// <summary>
/// Contains extension methods for <see cref="BindingsCollection">data binding</see>.
/// </summary>
internal static class BindingExtensionMethods
{
    #region " parse and display "

    /// <summary>
    /// Toggle image; converts the event arguments value from boolean to true or false image.
    /// </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="convertEventArgs"> Convert event information. </param>
    /// <param name="trueImage">        The true image. </param>
    /// <param name="falseImage">       The false image. </param>
    public static void ToggleImage( this ConvertEventArgs convertEventArgs, System.Drawing.Image trueImage, System.Drawing.Image falseImage )
    {
        if ( convertEventArgs?.Value is not null && ReferenceEquals( convertEventArgs.DesiredType, typeof( System.Drawing.Image ) ) )
        {
            convertEventArgs.Value = ( bool ) convertEventArgs.Value ? trueImage : falseImage;
        }
    }

    /// <summary>
    /// Toggles caption; converts the event arguments value from boolean to true or false text value.
    /// </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="convertEventArgs"> Convert event information. </param>
    /// <param name="trueValue">        The true value. </param>
    /// <param name="falseValue">       The false value. </param>
    public static void ToggleCaption( this ConvertEventArgs convertEventArgs, string trueValue, string falseValue )
    {
        if ( convertEventArgs?.Value is not null && ReferenceEquals( convertEventArgs.DesiredType, typeof( string ) ) )
        {
            convertEventArgs.Value = ( bool ) convertEventArgs.Value ? trueValue : falseValue;
        }
    }

    /// <summary> Displays <see cref="DateTimeOffset"/> as local date. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="convertEventArgs"> Convert event information. </param>
    public static void DisplayDateTimeOffsetDate( this ConvertEventArgs convertEventArgs )
    {
        if ( convertEventArgs?.Value is not null && ReferenceEquals( convertEventArgs.DesiredType, typeof( string ) ) )
        {
            convertEventArgs.Value = (( DateTimeOffset ) convertEventArgs.Value).ToLocalTime().ToString( "d", System.Globalization.CultureInfo.CurrentCulture );
        }
    }

    /// <summary> Displays a date time offset time described by convertEventArgs. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="convertEventArgs"> Convert event information. </param>
    public static void DisplayDateTimeOffsetTime( this ConvertEventArgs convertEventArgs )
    {
        if ( convertEventArgs?.Value is not null && ReferenceEquals( convertEventArgs.DesiredType, typeof( string ) ) )
        {
            convertEventArgs.Value = (( DateTimeOffset ) convertEventArgs.Value).ToLocalTime().ToString( "t", System.Globalization.CultureInfo.CurrentCulture );
        }
    }

    /// <summary> Displays a date time offset described by convertEventArgs. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="convertEventArgs"> Convert event information. </param>
    public static void DisplayDateTimeOffset( this ConvertEventArgs convertEventArgs )
    {
        if ( convertEventArgs?.Value is not null && ReferenceEquals( convertEventArgs.DesiredType, typeof( string ) ) )
        {
            convertEventArgs.Value = (( DateTimeOffset ) convertEventArgs.Value).ToLocalTime().ToString( "0", System.Globalization.CultureInfo.CurrentCulture );
        }
    }

    /// <summary> Parse a local time string to date time offset. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="convertEventArgs"> Convert event information. </param>
    public static void ParseDateTimeOffset( this ConvertEventArgs convertEventArgs )
    {
        if ( convertEventArgs?.Value is not null && ReferenceEquals( convertEventArgs.DesiredType, typeof( DateTime ) ) )
        {
            if ( DateTimeOffset.TryParse( convertEventArgs.Value.ToString(), out DateTimeOffset value ) )
            {
            }

            convertEventArgs.Value = value;
        }
    }

    /// <summary> Displays universal time value as local date. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="convertEventArgs"> Convert event information. </param>
    public static void DisplayLocalDate( this ConvertEventArgs convertEventArgs )
    {
        if ( convertEventArgs?.Value is not null && ReferenceEquals( convertEventArgs.DesiredType, typeof( string ) ) )
        {
            convertEventArgs.Value = (( DateTime ) convertEventArgs.Value).ToLocalTime().ToShortDateString();
        }
    }

    /// <summary> Displays universal time value as local time. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="convertEventArgs"> Convert event information. </param>
    public static void DisplayLocalTime( this ConvertEventArgs convertEventArgs )
    {
        if ( convertEventArgs?.Value is not null && ReferenceEquals( convertEventArgs.DesiredType, typeof( string ) ) )
        {
            convertEventArgs.Value = (( DateTimeOffset ) convertEventArgs.Value).ToLocalTime().ToString( "T", System.Globalization.CultureInfo.CurrentCulture );
        }
    }

    /// <summary> Parse a local time string to universal time. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="convertEventArgs"> Convert event information. </param>
    public static void ParseLocalTime( this ConvertEventArgs convertEventArgs )
    {
        if ( convertEventArgs?.Value is not null && ReferenceEquals( convertEventArgs.DesiredType, typeof( DateTime ) ) )
        {
            convertEventArgs.Value = DateTimeOffset.TryParse( convertEventArgs.Value.ToString(), out DateTimeOffset value ) ? value.ToUniversalTime() : value.ToUniversalTime();
        }
    }

    /// <summary> Displays a timespan described by convertEventArgs. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="convertEventArgs"> Convert event information. </param>
    public static void DisplayTimespan( this ConvertEventArgs convertEventArgs )
    {
        if ( convertEventArgs?.Value is not null && ReferenceEquals( convertEventArgs.DesiredType, typeof( string ) ) )
        {
            convertEventArgs.Value = (( TimeSpan ) convertEventArgs.Value).ToString();
        }
    }

    /// <summary> Displays a timespan described by convertEventArgs. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="convertEventArgs"> Convert event information. </param>
    /// <param name="format">           Describes the format to use. </param>
    public static void DisplayTimespan( this ConvertEventArgs convertEventArgs, string format )
    {
        if ( convertEventArgs?.Value is not null && ReferenceEquals( convertEventArgs.DesiredType, typeof( string ) ) )
        {
            convertEventArgs.Value = (( TimeSpan ) convertEventArgs.Value).ToString( format, System.Globalization.CultureInfo.CurrentCulture );
        }
    }

    /// <summary> Parse timespan. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="convertEventArgs"> Convert event information. </param>
    public static void ParseTimespan( this ConvertEventArgs convertEventArgs )
    {
        if ( convertEventArgs?.Value is not null && ReferenceEquals( convertEventArgs.DesiredType, typeof( TimeSpan ) ) )
        {
            convertEventArgs.Value = TimeSpan.TryParse( convertEventArgs.Value.ToString(), out TimeSpan value ) ? value : value;
        }
    }

    /// <summary> Displays An enum value (numeric value) described by convertEventArgs. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="convertEventArgs"> Convert event information. </param>
    /// <param name="format">           Describes the format to use. </param>
    public static void DisplayEnumValue( this ConvertEventArgs convertEventArgs, string format )
    {
        if ( convertEventArgs?.Value is not null && ReferenceEquals( convertEventArgs.DesiredType, typeof( string ) ) )
        {
            convertEventArgs.Value = string.Format( System.Globalization.CultureInfo.CurrentCulture, format, ( int ) convertEventArgs.Value );
        }
    }

    /// <summary> Parse enum value. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="convertEventArgs"> Convert event information. </param>
    public static void ParseEnumValue<T>( this ConvertEventArgs convertEventArgs )
    {
        if ( convertEventArgs?.Value is not null && ReferenceEquals( convertEventArgs.DesiredType, typeof( T ) ) )
        {
            convertEventArgs.Value = ( T ) convertEventArgs.Value;
        }
    }

    /// <summary> Writes an inverted described by convertEventArgs. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="convertEventArgs"> Convert event information. </param>
    public static void WriteInverted( this ConvertEventArgs convertEventArgs )
    {
        if ( convertEventArgs?.Value is not null && ReferenceEquals( convertEventArgs.DesiredType, typeof( bool ) ) )
        {
            convertEventArgs.Value = !( bool ) convertEventArgs.Value;
        }
    }

    /// <summary> Parse inverted. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="convertEventArgs"> Convert event information. </param>
    public static void ParseInverted( this ConvertEventArgs convertEventArgs )
    {
        if ( convertEventArgs?.Value is not null && ReferenceEquals( convertEventArgs.DesiredType, typeof( bool ) ) )
        {
            if ( bool.TryParse( convertEventArgs.Value.ToString(), out bool value ) )
            {
                value = !value;
            }

            convertEventArgs.Value = value;
        }
    }

    #endregion

    #region " binding collections: remove "

    /// <summary> Clears all bindings. Verifies that binding indeed were cleared. </summary>
    /// <remarks>
    /// The base <see cref="ControlBindingsCollection.Clear">clear</see> may not clear fast enough
    /// for the binding to be cleared when a new binding is added.
    /// </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="value"> The value. </param>
    public static void ClearAll( this ControlBindingsCollection value )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( value, nameof( value ) );
#else
        if ( value is null ) throw new ArgumentNullException( nameof( value ) );
#endif

        while ( value.Count > 0 )
        {
            try
            {
                value.RemoveAt( 0 );
                Application.DoEvents();
            }
            catch ( ArgumentOutOfRangeException )
            {
            }
        }
    }

    /// <summary> Removes the specified binding based on the binding property name. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="value">        The value. </param>
    /// <param name="propertyName"> Name of the property. </param>
    /// <returns> A Binding. </returns>
    public static Binding? RemoveAndVerify( this ControlBindingsCollection value, string propertyName )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( value, nameof( value ) );
#else
        if ( value is null ) throw new ArgumentNullException( nameof( value ) );
#endif

        if ( string.IsNullOrWhiteSpace( propertyName ) )
        {
            throw new ArgumentNullException( nameof( propertyName ) );
        }

        Binding? result = null;
        while ( value.Exists( propertyName ) )
        {
            foreach ( Binding b in value )
            {
                if ( string.Equals( b.PropertyName, propertyName, StringComparison.Ordinal ) )
                {
                    try
                    {
                        result = b;
                        value.Remove( b );
                        Application.DoEvents();
                    }
                    catch ( ArgumentOutOfRangeException )
                    {
                        // remove could cause an exception due to the way the binding is checked and released.
                        // as it happens, the check for exists passes but the binding is no longer there when trying to
                        // remove it.
                    }

                    break;
                }
            }
        }

        return result;
    }

    /// <summary> Removes the specified binding based on the binding property name. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="value">   The value. </param>
    /// <param name="binding"> The binding. </param>
    /// <returns> A Binding. </returns>
    public static Binding? RemoveAndVerify( this ControlBindingsCollection value, Binding binding )
    {
        return value is null
            ? throw new ArgumentNullException( nameof( value ) )
            : binding is null ? throw new ArgumentNullException( nameof( binding ) ) : value.RemoveAndVerify( binding.PropertyName );
    }

    #endregion

    #region " binding collections: find "

    /// <summary> Checks if the binding already exists. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="value">        The value. </param>
    /// <param name="propertyName"> Name of the property. </param>
    /// <returns> <c>true</c> if binding exists, <c>false</c> otherwise. </returns>
    public static bool Exists( this ControlBindingsCollection value, string propertyName )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( value, nameof( value ) );
#else
        if ( value is null ) throw new ArgumentNullException( nameof( value ) );
#endif

        if ( string.IsNullOrWhiteSpace( propertyName ) )
        {
            throw new ArgumentNullException( nameof( propertyName ) );
        }

        foreach ( Binding b in value )
        {
            if ( string.Equals( b.PropertyName, propertyName, StringComparison.Ordinal ) )
            {
                return true;
            }
        }

        return false;
    }

    /// <summary> Checks if the binding already exists. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="value">   The value. </param>
    /// <param name="binding"> The binding. </param>
    /// <returns> <c>true</c> if binding exists, <c>false</c> otherwise. </returns>
    public static bool Exists( this ControlBindingsCollection value, Binding binding )
    {
        return value is null
            ? throw new ArgumentNullException( nameof( value ) )
            : binding is null ? throw new ArgumentNullException( nameof( binding ) ) : value.Exists( binding.PropertyName );
    }

    #endregion

    #region " binding collections: replace "

    /// <summary> Replaces the binding for the bound property of the control. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="value">   The value. </param>
    /// <param name="binding"> The binding. </param>
    /// <returns> Binding. </returns>
    public static Binding Replace( this ControlBindingsCollection value, Binding binding )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( value, nameof( value ) );
        ArgumentNullException.ThrowIfNull( binding, nameof( binding ) );
#else
        if ( value is null ) throw new ArgumentNullException( nameof( value ) );
        if ( binding is null ) throw new ArgumentNullException( nameof( binding ) );
#endif
        _ = value.RemoveAndVerify( binding );
        value.Add( binding );
        return binding;
    }

    #endregion

    #region " binding collections: select "

    /// <summary> Selects an existing binding if there. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="bindings"> The binding collection. </param>
    /// <param name="binding">  The binding. </param>
    /// <returns> Binding or nothing if not found. </returns>
    public static Binding? Find( this ControlBindingsCollection bindings, Binding binding )
    {
        return binding is null ? throw new ArgumentNullException( nameof( binding ) ) : bindings.Find( binding.PropertyName );
    }

    /// <summary> Selects an existing binding if there. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="bindings">     The binding collection. </param>
    /// <param name="propertyName"> Name of the property. </param>
    /// <returns> A Binding. </returns>
    public static Binding? Find( this BindingsCollection bindings, string propertyName )
    {
        Binding? result = null;
        if ( bindings is not null )
        {
            foreach ( Binding b in bindings )
            {
                if ( string.Equals( b.PropertyName, propertyName, StringComparison.Ordinal ) )
                {
                    result = b;
                    break;
                }
            }
        }

        return result;
    }

    /// <summary> Select binding by property name. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="control">      The control. </param>
    /// <param name="propertyName"> Name of the property. </param>
    /// <returns> A Binding. </returns>
    public static Binding? SelectBinding( this Control control, string propertyName )
    {
        return control is null ? throw new ArgumentNullException( nameof( control ) ) : control.DataBindings.Find( propertyName );
    }

    #endregion

    #region " binding collections: add "

    /// <summary>
    /// Adds a binding to the control. Disables the control while adding the binding to allow the
    /// disabling of control events while the binding is added. Note that the control event of value
    /// change occurs before the bound count increments so the bound count cannot be used to
    /// determine the control bindable status.
    /// </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="value">   The value. </param>
    /// <param name="binding"> . </param>
    /// <returns> The binding. </returns>
    public static Binding SilentAdd( this ControlBindingsCollection value, Binding binding )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( value, nameof( value ) );
        ArgumentNullException.ThrowIfNull( binding, nameof( binding ) );
#else
        if ( value is null ) throw new ArgumentNullException( nameof( value ) );
        if ( binding is null ) throw new ArgumentNullException( nameof( binding ) );
#endif
        if ( value.Control is not null )
        {
            bool isEnabled = value.Control.Enabled;
            value.Control.Enabled = false;
            value.Add( binding );
            value.Control.Enabled = isEnabled;
        }
        return binding;
    }

    /// <summary> Adds binding to a <see cref="IBindableComponent">bindable component</see> </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="component">       The bindable component. </param>
    /// <param name="propertyName">    Name of the property. </param>
    /// <param name="dataSource">      The data source. </param>
    /// <param name="dataMember">      The data member. </param>
    /// <param name="bindingComplete"> The binding complete. </param>
    /// <returns> A Binding. </returns>
    public static Binding AddBinding( this IBindableComponent component, string propertyName, object dataSource, string dataMember, BindingCompleteEventHandler bindingComplete )
    {
        return component.AddRemoveBinding( true, new Binding( propertyName, dataSource, dataMember )
        {
            ControlUpdateMode = ControlUpdateMode.OnPropertyChanged,
            DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged
        }, bindingComplete );
    }

    /// <summary>
    /// Removes binding from a <see cref="IBindableComponent">bindable component</see>
    /// </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="component">       The bindable component. </param>
    /// <param name="propertyName">    Name of the property. </param>
    /// <param name="dataSource">      The data source. </param>
    /// <param name="dataMember">      The data member. </param>
    /// <param name="bindingComplete"> The binding complete. </param>
    /// <returns> A Binding. </returns>
    public static Binding RemoveBinding( this IBindableComponent component, string propertyName, object dataSource, string dataMember, BindingCompleteEventHandler bindingComplete )
    {
        return component.AddRemoveBinding( false, new Binding( propertyName, dataSource, dataMember ), bindingComplete );
    }

    /// <summary>
    /// Adds or removes binding from a <see cref="IBindableComponent">bindable component</see>
    /// </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="component">       The bindable component. </param>
    /// <param name="add">             True to add; otherwise, remove. </param>
    /// <param name="binding">         The binding. </param>
    /// <param name="bindingComplete"> The binding complete handler. </param>
    /// <returns> A Binding. </returns>
    public static Binding AddRemoveBinding( this IBindableComponent component, bool add, Binding binding, BindingCompleteEventHandler bindingComplete )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( component, nameof( component ) );
        ArgumentNullException.ThrowIfNull( binding, nameof( binding ) );
#else
        if ( component is null ) throw new ArgumentNullException( nameof( component ) );
        if ( binding is null ) throw new ArgumentNullException( nameof( binding ) );
#endif

        if ( add )
        {
            // required to prevent cross thread exceptions. Invoke is required on the property change event.
            // binding.ControlUpdateMode = ControlUpdateMode.OnPropertyChanged
            // binding.DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged
            component.DataBindings.Add( binding );
            binding.BindingComplete += bindingComplete;
        }
        else
        {
            // .DataBindings.Remove(binding)
            Binding? b = component.DataBindings.Find( binding.PropertyName );
            if ( b is not null )
            {
                component.DataBindings.Remove( b );
            }

            binding.BindingComplete -= bindingComplete;
        }

        return binding;
    }

    #endregion

    #region " control write bound value "

    /// <summary> Writes a bound value to the data source. </summary>
    /// <remarks>
    /// The combo box does not implement a property change for the selected item property. When
    /// called from the Selected Value Changed event of the control, this method effects a change of
    /// selected item when the selected value changes by writing the selected item to the data source
    /// identified in the binding of the selected item data source property, if such item exists.
    /// </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="control">      The control. </param>
    /// <param name="propertyName"> Name of the property. </param>
    public static void WriteBoundValue( this Control control, string propertyName )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( control, nameof( control ) );
#else
        if ( control is null )
        {
            throw new ArgumentNullException( nameof( control ) );
        }
#endif

        control.SelectBinding( propertyName )?.WriteValue();
    }

    /// <summary> Writes a bound selected item value to the data source. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="control"> The control. </param>
    public static void WriteBoundSelectedItem( this ComboBox control )
    {
        BindingExtensions.BindingExtensionMethods.WriteBoundValue( control, nameof( ComboBox.SelectedItem ) );
    }

    #endregion
}
