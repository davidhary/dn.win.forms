
namespace TraceMessagesDemo.UI
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region " windows form designer generated code "

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.TopToolStrip = new System.Windows.Forms.ToolStrip();
            this.WarningAlertToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.ErrorAlertToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.InformationAlertToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.TraceLevelComboBoxLabel = new System.Windows.Forms.ToolStripLabel();
            this.ShowLevelComboBox = new cc.isr.WinControls.ToolStripComboBox();
            this.LogLevelComboBoxLabel = new System.Windows.Forms.ToolStripLabel();
            this.LogLevelComboBox = new cc.isr.WinControls.ToolStripComboBox();
            this.SendLogMessageButton = new System.Windows.Forms.ToolStripButton();
            this.EventLevelComboBox = new cc.isr.WinControls.ToolStripComboBox();
            this.TreePanel = new cc.isr.WinControls.TreePanel();
            this.TopToolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TreePanel)).BeginInit();
            this.TreePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // TopToolStrip
            // 
            this.TopToolStrip.GripMargin = new System.Windows.Forms.Padding(0);
            this.TopToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.TopToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.WarningAlertToolStripButton,
            this.ErrorAlertToolStripButton,
            this.InformationAlertToolStripButton,
            this.TraceLevelComboBoxLabel,
            this.ShowLevelComboBox,
            this.LogLevelComboBoxLabel,
            this.LogLevelComboBox,
            this.SendLogMessageButton,
            this.EventLevelComboBox});
            this.TopToolStrip.Location = new System.Drawing.Point(0, 0);
            this.TopToolStrip.Name = "TopToolStrip";
            this.TopToolStrip.Size = new System.Drawing.Size(681, 25);
            this.TopToolStrip.TabIndex = 0;
            this.TopToolStrip.Text = "toolStrip1";
            // 
            // WarningAlertToolStripButton
            // 
            this.WarningAlertToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.WarningAlertToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.WarningAlertToolStripButton.Name = "WarningAlertToolStripButton";
            this.WarningAlertToolStripButton.Size = new System.Drawing.Size(56, 22);
            this.WarningAlertToolStripButton.Text = "Warning";
            // 
            // ErrorAlertToolStripButton
            // 
            this.ErrorAlertToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ErrorAlertToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ErrorAlertToolStripButton.Name = "ErrorAlertToolStripButton";
            this.ErrorAlertToolStripButton.Size = new System.Drawing.Size(36, 22);
            this.ErrorAlertToolStripButton.Text = "Error";
            // 
            // InformationAlertToolStripButton
            // 
            this.InformationAlertToolStripButton.BackColor = System.Drawing.Color.Transparent;
            this.InformationAlertToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.InformationAlertToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.InformationAlertToolStripButton.Name = "InformationAlertToolStripButton";
            this.InformationAlertToolStripButton.Size = new System.Drawing.Size(74, 22);
            this.InformationAlertToolStripButton.Text = "Information";
            // 
            // TraceLevelComboBoxLabel
            // 
            this.TraceLevelComboBoxLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.TraceLevelComboBoxLabel.Name = "TraceLevelComboBoxLabel";
            this.TraceLevelComboBoxLabel.Size = new System.Drawing.Size(53, 22);
            this.TraceLevelComboBoxLabel.Text = "Show @:";
            // 
            // ShowLevelComboBox
            // 
            this.ShowLevelComboBox.Name = "ShowLevelComboBox";
            this.ShowLevelComboBox.Size = new System.Drawing.Size(100, 25);
            this.ShowLevelComboBox.ToolTipText = "Show (Trace Writer) level for filtering the trace messages";
            this.ShowLevelComboBox.Validated += new System.EventHandler(this.TraceLevelComboBox_Validated);
            this.ShowLevelComboBox.Click += new System.EventHandler(this.TraceLevelComboBox_Validated);
            // 
            // LogLevelComboBoxLabel
            // 
            this.LogLevelComboBoxLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.LogLevelComboBoxLabel.Name = "LogLevelComboBoxLabel";
            this.LogLevelComboBoxLabel.Size = new System.Drawing.Size(44, 22);
            this.LogLevelComboBoxLabel.Text = "Log @:";
            // 
            // LogLevelComboBox
            // 
            this.LogLevelComboBox.Name = "LogLevelComboBox";
            this.LogLevelComboBox.Size = new System.Drawing.Size(100, 25);
            this.LogLevelComboBox.ToolTipText = "Log Level";
            this.LogLevelComboBox.Validated += new System.EventHandler(this.LogLevelComboBox_Validated);
            this.LogLevelComboBox.Click += new System.EventHandler(this.LogLevelComboBox_Validated);
            // 
            // SendLogMessageButton
            // 
            this.SendLogMessageButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.SendLogMessageButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SendLogMessageButton.Name = "SendLogMessageButton";
            this.SendLogMessageButton.Size = new System.Drawing.Size(72, 22);
            this.SendLogMessageButton.Text = "Send Event:";
            this.SendLogMessageButton.ToolTipText = "Sends a log message";
            this.SendLogMessageButton.Click += new System.EventHandler(this.SendLogMessageButton_Click);
            // 
            // EventLevelComboBox
            // 
            this.EventLevelComboBox.Name = "EventLevelComboBox";
            this.EventLevelComboBox.Size = new System.Drawing.Size(100, 25);
            this.EventLevelComboBox.ToolTipText = "Trace event level for sending the message";
            this.EventLevelComboBox.Validated += new System.EventHandler(this.EventLevelComboBox_Validated);
            this.EventLevelComboBox.Click += new System.EventHandler(this.EventLevelComboBox_Validated);
            // 
            // TreePanel
            // 
            this.TreePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TreePanel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TreePanel.Location = new System.Drawing.Point(0, 25);
            this.TreePanel.MinTreeSize = 25;
            this.TreePanel.Name = "TreePanel";
            this.TreePanel.Size = new System.Drawing.Size(681, 424);
            this.TreePanel.SplitterDistance = 226;
            this.TreePanel.TabIndex = 1;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(681, 449);
            this.Controls.Add(this.TreePanel);
            this.Controls.Add(this.TopToolStrip);
            this.Name = "MainForm";
            this.Text = "Trace Messages Demo";
            this.TopToolStrip.ResumeLayout(false);
            this.TopToolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TreePanel)).EndInit();
            this.TreePanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip TopToolStrip;
        private System.Windows.Forms.ToolStripButton WarningAlertToolStripButton;
        private System.Windows.Forms.ToolStripButton ErrorAlertToolStripButton;
        private System.Windows.Forms.ToolStripButton InformationAlertToolStripButton;
        private cc.isr.WinControls.TreePanel TreePanel;
        private System.Windows.Forms.ToolStripLabel TraceLevelComboBoxLabel;
        private System.Windows.Forms.ToolStripButton SendLogMessageButton;
        private System.Windows.Forms.ToolStripLabel LogLevelComboBoxLabel;
        private cc.isr.WinControls.ToolStripComboBox LogLevelComboBox;
        private cc.isr.WinControls.ToolStripComboBox ShowLevelComboBox;
        private cc.isr.WinControls.ToolStripComboBox EventLevelComboBox;
    }
}

