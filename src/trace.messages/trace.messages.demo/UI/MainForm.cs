using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.Versioning;
using System.Windows.Forms;
using cc.isr.Enums.WinControls.ComboBoxEnumExtensions;
using cc.isr.Logging.TraceLog;
using cc.isr.Logging.TraceLog.WinForms;

namespace TraceMessagesDemo.UI;

public partial class MainForm : Form
{
    #region " construction and cleanup "

    /// <summary>   Gets or sets the trace logger. </summary>
    /// <value> The trace logger. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public static TraceLogger<MainForm> TraceLogger { get; set; } = new();


    public MainForm()
    {
        this.InitializeComponent();

        // Microsoft Log Levels
        // Trace:
        //     Logs that contain the most detailed messages. These messages may contain sensitive
        //     application data. These messages are disabled by default and should never be
        //     enabled in a production environment.
        // 
        //
        // Debug:
        //     Logs that are used for interactive investigation during development. These logs
        //     should primarily contain information useful for debugging and have no long-term
        //     value.
        //            Debug,
        //
        // Information:
        //     Logs that track the general flow of the application. These logs should have long-term
        //     value.
        //            Information,
        //
        // Warning:
        //     Logs that highlight an abnormal or unexpected event in the application flow,
        //     but do not otherwise cause the application execution to stop.
        //            Warning,
        //
        // Error:
        //     Logs that highlight when the current flow of execution is stopped due to a failure.
        //     These should indicate a failure in the current activity, not an application-wide
        //     failure.
        //            Error,
        //
        // Critical:
        //     Logs that describe an unrecoverable application or system crash, or a catastrophic
        //     failure that requires immediate attention.
        //            Critical,
        //
        // None:
        //     Not used for writing log messages. Specifies that a logging category should not
        //     write any messages.
        //            None
        // Serilog log levels:
        //     Verbose: Anything and everything you might want to know about a running block of code.
        //       Debug:  Internal system events that aren't necessarily observable from the outside.
        // Information: The lifeblood of operational intelligence - things happen.
        //     Warning: Service is degraded or endangered.
        //       Error: Functionality is unavailable, invariants are broken or data is lost.
        //       Fatal: If you have a pager, it goes off when one of these occurs.
        // 
        _ = this.ShowLevelComboBox.ComboBox.ListEnumNames<TraceEventType>();
        _ = this.EventLevelComboBox.ComboBox.ListEnumNames<Microsoft.Extensions.Logging.LogLevel>();
        _ = this.LogLevelComboBox.ComboBox.ListEnumNames<Microsoft.Extensions.Logging.LogLevel>();
        this.ShowLevelComboBox.ComboBox.SelectedValue = TraceEventType.Information;
        this.EventLevelComboBox.ComboBox.SelectedValue = Microsoft.Extensions.Logging.LogLevel.Error;
        this.LogLevelComboBox.ComboBox.SelectedValue = Microsoft.Extensions.Logging.LogLevel.Information;

        TargetFrameworkAttribute? framework = Assembly.GetEntryAssembly()?.GetCustomAttribute<TargetFrameworkAttribute>();
        this.Text = $"Trace Messages Demo; {framework?.FrameworkName}.";

        // add a node so we can hide the messages box.
        TextBox hideTextBox = new()
        {
            Multiline = true,
            WordWrap = true,
            ReadOnly = true,
            BackColor = System.Drawing.Color.AliceBlue,
            Text = @"This panel is used to hide the Message Box in order to test the change of node caption when the messages box changes visibility"
        };
        _ = this.TreePanel.AddNode( "Messages Not Visible", "Invisible", hideTextBox );

        this._messagesBox = new();
        TreeNode messagesNode = this.TreePanel.AddNode( "Message Box", "Log", this._messagesBox );

        this.TextBoxTextWriter = new( this._messagesBox )
        {
            ContainerTreeNode = messagesNode,
            TabCaption = "Log",
            CaptionFormat = "{0} " + Convert.ToChar( 0x1C2 ),
            ResetCount = 1000,
            PresetCount = 500,
            TraceLevel = this.ShowLevelComboBox.SelectedEnumValue( TraceEventType.Error )
        };
        cc.isr.Tracing.TracingPlatform.Instance.AddTraceEventWriter( this.TextBoxTextWriter );

        this.ErrorButtonTraceAlertContainer = new( this.ErrorAlertToolStripButton, this._messagesBox )
        {
            AlertAnnunciatorText = "Error",
            AlertLevel = TraceEventType.Error,
            AlertSoundEnabled = true
        };
        cc.isr.Tracing.TracingPlatform.Instance.AddTraceEventWriter( this.ErrorButtonTraceAlertContainer );

        this.WarningButtonTraceAlertContainer = new( this.WarningAlertToolStripButton, this._messagesBox )
        {
            AlertAnnunciatorText = "Warning",
            AlertLevel = TraceEventType.Warning,
            AlertSoundEnabled = true
        };
        cc.isr.Tracing.TracingPlatform.Instance.AddTraceEventWriter( this.WarningButtonTraceAlertContainer );

        this.InformationButtonTraceAlertContainer = new( this.InformationAlertToolStripButton, this._messagesBox )
        {
            AlertAnnunciatorText = "Information",
            AlertLevel = TraceEventType.Information,
            AlertSoundEnabled = true
        };
        cc.isr.Tracing.TracingPlatform.Instance.AddTraceEventWriter( this.InformationButtonTraceAlertContainer );
    }

    /// <summary>
    ///  Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose( bool disposing )
    {
        if ( disposing )
        {
            this.components?.Dispose();
        }
        base.Dispose( disposing );
    }

    #endregion

    #region " messages box "

    private readonly MessagesBox _messagesBox;

    #endregion

    #region " text box trace event writer  "

    private cc.isr.Tracing.WinForms.TextBoxTraceEventWriter TextBoxTextWriter { get; set; }

    #endregion

    #region " trace alert containers "

    private cc.isr.Tracing.WinForms.TraceAlertContainer ErrorButtonTraceAlertContainer { get; set; }

    private cc.isr.Tracing.WinForms.TraceAlertContainer WarningButtonTraceAlertContainer { get; set; }

    private cc.isr.Tracing.WinForms.TraceAlertContainer InformationButtonTraceAlertContainer { get; set; }

    #endregion

    #region " control event handlers "

    private void TraceLevelComboBox_Validated( object? sender, EventArgs e )
    {
        this.TextBoxTextWriter.TraceLevel = this.ShowLevelComboBox.SelectedEnumValue( TraceEventType.Error );
    }
    private void EventLevelComboBox_Validated( object? sender, EventArgs e )
    {
    }

    private void LogLevelComboBox_Validated( object? sender, EventArgs e )
    {
        TraceLogger.MinimumLogLevel = this.LogLevelComboBox.SelectedEnumValue( Microsoft.Extensions.Logging.LogLevel.Error );
    }

    private int _messageNumber;
    private void SendLogMessageButton_Click( object? sender, EventArgs e )
    {
        this._messageNumber += 1;
        Microsoft.Extensions.Logging.LogLevel logLevel = this.EventLevelComboBox.SelectedEnumValue( Microsoft.Extensions.Logging.LogLevel.Trace );
        string message = $"{logLevel} Message #{this._messageNumber} @ {DateTime.Now}";
        _ = TraceLogger.LogCallerMessage( logLevel, message );
    }

    #endregion
}
