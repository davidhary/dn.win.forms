# About

[Trace Messages Demo] demonstrates tracing messages using the [Win Forms Repository] and [Win Controls Repository] forms and controls.

# How to Use

# Key Features

# Key Issue

## Support for .Net 4.72 and 4.8 Framework

Presently (Visual Studio 17.10.4), this project targets .NET 8.0 Core only. Unfortunately, logging fails when targeting legacy .NET frameworks such as 4.72 or 4.8 with the following exception:

```
System.IO.FileLoadException: Could not load file or assembly 'Serilog, Version=2.0.0.0, Culture=neutral, PublicKeyToken=24c2f752a8e58a10' or one of its dependencies. The located assembly's manifest definition does not match the assembly reference. (Exception from HRESULT: 0x80131040)
```

This failure occurred upon attempting to configure the Serilog logging platform form our .NETSTANDARD 2.0 logging library. We are not sure at this time how this could be remedied. Apparently, a similar issue was reported in 2017 ([2017 issue]) that was reported as fixed in Visual Studio 15.5.

# Feedback

[Trace Messages Demo] is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Win Forms Repository].

[Win Forms Repository]: https://bitbucket.org/davidhary/dn.win.forms
[Win Controls Repository]: https://bitbucket.org/davidhary/dn.win.controls
[Trace Messages Demo]: https://bitbucket.org/davidhary/dn.win.forms/src/trace.messages

[2017 issue]: https://developercommunity.visualstudio.com/t/could-not-load-file-or-assembly-error-when-using-s/35539
