namespace cc.isr.WinForms.Metro.Demo;

public partial class Dashboard : Form
{
    public Dashboard() => this.InitializeComponent();

    /// <summary>
    ///  Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose( bool disposing )
    {
        this.components?.Dispose(); base.Dispose( disposing );
    }


    #region " show me listener "

    protected override void WndProc( ref Message m )
    {
        if ( m.Msg == Win32.NativeMethods.WM_SHOWME )
        {
            this.ShowMe();
        }
        base.WndProc( ref m );
    }

    private void ShowMe()
    {
        if ( this.WindowState == FormWindowState.Minimized )
        {
            this.WindowState = FormWindowState.Normal;
        }


        // get our current "TopMost" value (ours will always be false though)

        bool top = this.TopMost;

        // make our form jump to the top of everything

        this.TopMost = true;

        // set it back to whatever it was

        this.TopMost = top;
    }

    #endregion #region
}
