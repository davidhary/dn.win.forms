using Microsoft.Extensions.Logging;
using cc.isr.Logging.Extensions;

namespace cc.isr.WinForms.Metro.Demo;

internal partial class AppContext : ApplicationContext
{
    private readonly ILogger<AppContext>? _logger;

    internal AppContext( ILogger<AppContext> logger ) : this()
    {
        this._logger = logger;

        TimeSpan timeout = TimeSpan.FromMilliseconds( 100 );
        bool fileOpened = cc.isr.Logging.Orlog.HeaderWriter.AwaitFullFileName( timeout, TimeSpan.FromMilliseconds( 20 ), TimeSpan.FromMilliseconds( 20 ) );
        string message = fileOpened
            ? $"Logging into {cc.isr.Logging.Orlog.HeaderWriter.FullFileName}."
            : $"File not open after {timeout.TotalMilliseconds}ms.";
        this._logger.LogInformationMessage( message );

    }

}
