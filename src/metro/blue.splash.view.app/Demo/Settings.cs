using System.ComponentModel;
using System.Diagnostics;
using CommunityToolkit.Mvvm.ComponentModel;
using cc.isr.Json.AppSettings.Models;

namespace cc.isr.WinForms.Metro.Demo;

/// <summary>   A settings. </summary>
/// <remarks>   David, 2021-02-01. </remarks>
internal sealed class Settings : ObservableObject
{
    #region " construction "

    /// <summary>   Default constructor. </summary>
    /// <remarks>   2023-05-09. </remarks>
    public Settings()
    { }

    #endregion

    #region " singleton "

    /// <summary>   Creates an instance of the <see cref="Settings"/> after restoring the 
    /// application context settings to both the user and all user files. </summary>
    /// <remarks>   2023-05-15. </remarks>
    /// <returns>   The new instance. </returns>
    private static Settings CreateInstance()
    {
        // get assembly files using the .Logging suffix.

        AssemblyFileInfo ai = new( typeof( Settings ).Assembly, null, ".Settings", ".json" );

        if ( !File.Exists( ai.AllUsersAssemblyFilePath! ) )
            AppSettingsScribe.CopySettings( ai.AppContextAssemblyFilePath!, ai.AllUsersAssemblyFilePath! );

        if ( !File.Exists( ai.ThisUserAssemblyFilePath! ) )
            AppSettingsScribe.CopySettings( ai.AppContextAssemblyFilePath!, ai.ThisUserAssemblyFilePath! );
        Settings.SettingsPath = ai.AllUsersAssemblyFilePath!;
        Settings.SettingsSectionName = nameof( Settings );
        Settings ti = new();

        ti.ReadSettings();

        return ti;
    }

    /// <summary>   Gets the instance. </summary>
    /// <value> The instance. </value>
    public static Settings Instance => _instance.Value;

    private static readonly Lazy<Settings> _instance = new( CreateInstance, true );

    #endregion

    #region " i/o "

    internal static string SettingsPath = string.Empty;
    internal static string SettingsSectionName = string.Empty;

    /// <summary>   Reads the settings. </summary>
    /// <remarks>   2023-05-23. </remarks>
    public void ReadSettings()
    {
        AppSettingsScribe.ReadSettings( SettingsPath, SettingsSectionName, this );
    }

    /// <summary>   Saves the settings. </summary>
    /// <remarks>   2023-05-23. </remarks>
    public void SaveSettings()
    {
        AppSettingsScribe.ReadSettings( SettingsPath, SettingsSectionName, this );
    }

    #endregion

    #region " common configuration information "


    /// <summary>   Gets or sets the trace level. </summary>
    /// <value> The trace level. </value>
    [Description( "Sets the message level" )]
    public TraceLevel TraceLevel
    {
        get;
        set => _ = this.SetProperty( ref field, value );
    } = TraceLevel.Verbose;

    /// <summary>   Gets or sets a value indicating whether this object is enabled. </summary>
    /// <value> True if enabled, false if not. </value>
    [Description( "True if testing is enabled for this test class" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CommunityToolkit.Mvvm.SourceGenerators.ObservablePropertyGenerator", "MVVMTK0056:Prefer using [ObservableProperty] over semi-auto properties", Justification = "<Pending>" )]
    public bool Enabled
    {
        get;
        set => this.SetProperty( ref field, value );
    } = true;

    /// <summary> Gets or sets all. </summary>
    /// <value> all. </value>
    [Description( "True if all testing is enabled for this test class" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CommunityToolkit.Mvvm.SourceGenerators.ObservablePropertyGenerator", "MVVMTK0056:Prefer using [ObservableProperty] over semi-auto properties", Justification = "<Pending>" )]
    public bool All
    {
        get;
        set => this.SetProperty( ref field, value );
    } = true;

    #endregion

    #region " custom configuration information "


    /// <summary>   Gets or sets the duration of the splash. </summary>
    /// <value> The splash duration. </value>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CommunityToolkit.Mvvm.SourceGenerators.ObservablePropertyGenerator", "MVVMTK0056:Prefer using [ObservableProperty] over semi-auto properties", Justification = "<Pending>" )]
    public TimeSpan SplashDuration
    {
        get;
        set => this.SetProperty( ref field, value );
    }

    /// <summary>   Gets or sets the application log level. </summary>
    /// <remarks>
    /// The minimal level for logging events at the application level. The logger logs the message if
    /// the message level is the same or higher than this level.
    /// </remarks>
    /// <value> The application log level. </value>
    [Description( "The minimal level for logging events at the application level. The logger logs the message if the message level is the same or higher than this level" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CommunityToolkit.Mvvm.SourceGenerators.ObservablePropertyGenerator", "MVVMTK0056:Prefer using [ObservableProperty] over semi-auto properties", Justification = "<Pending>" )]
    public Microsoft.Extensions.Logging.LogLevel ApplicationLogLevel
    {
        get;
        set => this.SetProperty( ref field, value );
    } = Microsoft.Extensions.Logging.LogLevel.Trace;

    /// <summary>   Gets or sets the assembly log level. </summary>
    /// <remarks>
    /// The minimum log level for sending the message to the logger by this assembly. This
    /// filters the message before it is sent to the Logging program.
    /// </remarks>
    /// <value> The assembly log level. </value>
    [Description( "The minimum log level for sending the message to the logger by this assembly. This filters the message before it is sent to the Logging program." )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CommunityToolkit.Mvvm.SourceGenerators.ObservablePropertyGenerator", "MVVMTK0056:Prefer using [ObservableProperty] over semi-auto properties", Justification = "<Pending>" )]
    public Microsoft.Extensions.Logging.LogLevel AssemblyLogLevel
    {
        get;
        set => this.SetProperty( ref field, value );
    } = Microsoft.Extensions.Logging.LogLevel.Trace;

    /// <summary>   Gets or sets the display level for log and trace messages. </summary>
    /// <remarks>
    /// The maximum trace event type for displaying logged and trace events. Only messages with a
    /// message <see cref="Diagnostics.TraceEventType"/> level that is same or higher than
    /// this level are displayed.
    /// </remarks>
    /// <value> The message display level. </value>
    [Description( "The maximum trace event type for displaying logged and trace events. Only messages with a message a level that is same or higher than this level are displayed." )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CommunityToolkit.Mvvm.SourceGenerators.ObservablePropertyGenerator", "MVVMTK0056:Prefer using [ObservableProperty] over semi-auto properties", Justification = "<Pending>" )]
    public TraceEventType MessageDisplayLevel
    {
        get;
        set => this.SetProperty( ref field, value );
    } = TraceEventType.Verbose;

    #endregion
}
