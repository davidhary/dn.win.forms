using cc.isr.Tracing;
using System.ComponentModel;

namespace BlueSplashCommandApp;

internal sealed class TracingParser
{
    /// <summary>   Configure trace logging. </summary>
    /// <remarks>   2023-05-24. </remarks>
    /// <param name="traceListenerMode">    (Optional) The queue listener mode, defaults to 'Async'
    ///                                     which uses the <see cref="ITraceEventWriter"/> otherwise
    ///                                     a <see cref="TraceEventQueueTraceListener"/> is used.. </param>
    public static void ConfigureTraceLogging( TraceWriterListenerMode traceListenerMode = TraceWriterListenerMode.EventWriter )
    {
        if ( traceListenerMode == TraceWriterListenerMode.EventWriter )
        {
            Console.WriteLine();
            Console.WriteLine( $"Queue option: {traceListenerMode}" );
            Console.WriteLine();
            TracingPlatform.Instance.AddTraceEventWriter( TracingPlatform.Instance.CriticalTraceEventTraceListener );
            // comment out to see all messages.
            // TracingPlatform.Instance.CriticalTraceEventSourceLevel = TracingPlatform.Instance.TraceEventSourceLevel;
        }
        else
        {
            // Add Concurrent Queue trace listener
            TracingPlatform.Instance.AddTraceListener( TracingPlatform.Instance.CriticalTraceEventTraceListener );
        }
    }
}
/// <summary>   Values that represent trace writer listener modes. </summary>
/// <remarks>   2023-05-26. </remarks>
internal enum TraceWriterListenerMode
{
    [Description( "An asynchronous text writer trace listener.This class cannot be inherited." )]
    TextWriter,
    [Description( "An asynchronous text event writer trace listener.This class cannot be inherited." )]
    EventWriter,
    [Description( "A concurrent queue text writer trace listener." )]
    TextQueue,
    [Description( "A concurrent queue trace event trace listener." )]
    EventQueue
}
