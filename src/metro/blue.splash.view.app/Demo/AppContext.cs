using System.Diagnostics;
using cc.isr.Logging.TraceLog.ExceptionExtensions;
using System.Net.NetworkInformation;
using System.Diagnostics.CodeAnalysis;
using cc.isr.WinForms.Metro.Demo.ExceptionExtensions;
using System.Reflection;
using cc.isr.Logging.TraceLog;
using System.ComponentModel;

namespace cc.isr.WinForms.Metro.Demo;

/// <summary>   The class that handles the creation of the application windows. </summary>
/// <remarks>
/// David, 2021-03-15. <para>
/// You can use the ApplicationContext class to redefine the circumstances that cause a message
/// loop to exit. By default, the ApplicationContext listens to the Closed event on the
/// application's main Form, then exits the thread's message loop.
/// </para><para>
/// https://docs.Microsoft.com/en-us/dotnet/api/system.windows.forms.applicationcontext?view=net-5.0
/// </para>
/// </remarks>
internal sealed partial class AppContext : ApplicationContext
{
    #region " construction and cleanup "

    /// <summary>   Gets or sets the trace logger. </summary>
    /// <value> The trace logger. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public static TraceLogger<AppContext> TraceLogger { get; set; } = new();

    /// <summary>   Default constructor. </summary>
    /// <remarks>   David, 2021-03-15. </remarks>
    internal AppContext() : base()
    {
        System.AppDomain.CurrentDomain.UnhandledException += this.OnUnhandledException;

        TaskScheduler.UnobservedTaskException += this.OnTaskSchedulerUnobservedException;

        // Handle the ApplicationExit event to know when the application is exiting.
        Application.ApplicationExit += new EventHandler( this.OnApplicationExit );
        Application.EnterThreadModal += new EventHandler( this.OnEnterThreadModal );
        Application.ThreadException += new ThreadExceptionEventHandler( this.OnThreadException );
        Application.Idle += new EventHandler( this.OnApplicationIdle );
        Application.LeaveThreadModal += new EventHandler( this.OnLeaveThreadModal );
        Application.ThreadExit += new EventHandler( this.OnThreadExit );
        NetworkChange.NetworkAvailabilityChanged += new NetworkAvailabilityChangedEventHandler( this.OnNetworkAddressChanged );

        AppContext.HighDpiModeWasSet = Application.SetHighDpiMode( HighDpiMode.SystemAware );
        Application.EnableVisualStyles();
        Application.SetCompatibleTextRenderingDefault( false );
        TraceLogger.MinimumLogLevel = Settings.Instance.ApplicationLogLevel;
        WinForms.Metro.Demo.Settings set = Settings.Instance;

        // Read the settings.
        this.ReadSettings();

        // show the splash screen
        this.ShowSplashForm();

        // show the main form.
        this.ShowDashboard( Settings.SplashDuration );
    }

    /// <summary>
    /// Releases the unmanaged resources used by the <see cref="ApplicationContext" />
    /// and optionally releases the managed resources.
    /// </summary>
    /// <remarks>   2023-05-27. </remarks>
    /// <param name="disposing">    <see langword="true" /> to release both managed and unmanaged
    ///                             resources; <see langword="false" /> to release only unmanaged
    ///                             resources. </param>
    protected override void Dispose( bool disposing )
    {
        try
        {
            if ( disposing )
            {
                this.SplashForm?.Dispose();
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }

    #endregion

    #region " splash form "

    private Blue.BlueSplashView SplashForm { get; set; }

    private ViewModels.Splash.SplashViewModel SplashViewModel { get; set; }

    /// <summary>   Shows the splash form. </summary>
    /// <remarks>   David, 2021-03-13. </remarks>
    [MemberNotNull( nameof( SplashForm ) )]
    [MemberNotNull( nameof( SplashViewModel ) )]
    private void ShowSplashForm()
    {
        this.SplashForm = new();
        this.SplashViewModel = this.SplashForm.SplashViewModel!;

        try
        {
            // Show the splash form.
            this.SplashViewModel.TopMost = !Debugger.IsAttached;
            AssemblyName assemblyName = typeof( AppContext ).Assembly.GetName();
            this.SplashViewModel.LargeApplicationCaption = assemblyName.Name;
            this.SplashViewModel.SmallApplicationCaption = $"{assemblyName.Name} {assemblyName.Version}";
            this.SplashForm.Show();
        }
        catch ( Exception ex )
        {
            // Inform the user that an error occurred.
            _ = ex.AddExceptionData();
            string message = "An error occurred while attempting to show the splash form";
            _ = TraceLogger.LogException( ex, message );
            _ = MessageBox.Show( $"{message}. The error is: {ex.BuildMessage()}", "Splash Form error occurred" );

            // Exit the current thread instead of showing the windows.
            this.ExitThread();
        }
    }

    #endregion

    #region " main form (dashboard) "

    /// <summary>   Shows the dashboard. </summary>
    /// <remarks>   David, 2021-03-15. </remarks>
    /// <param name="splashDuration">   Duration of the splash. </param>
    private void ShowDashboard( TimeSpan splashDuration )
    {
        string activity = string.Empty;
        try
        {
            activity = $"attempting to construct the {nameof( Dashboard )} form";
            _ = (this.SplashViewModel?.DisplayMessage( activity ));
            Dashboard dashboard = new();

            activity = $"awaiting the splash display duration";
            _ = (this.SplashViewModel?.DisplayMessage( activity ));
            DateTime endTime = DateTime.UtcNow.Add( splashDuration );
            do
            {
                // this is required; otherwise the splash form freezes until it is removed. 
                Application.DoEvents();
            } while ( endTime > DateTime.UtcNow );

            activity = $"attempting to register the {nameof( Dashboard )} form Load and closed events";
            _ = (this.SplashViewModel?.DisplayMessage( activity ));

            // this event handler disposes of the splash form.
            dashboard.Load += new EventHandler( this.Dashboard_Load );

            // this is necessary because the context instantiates two forms.
            dashboard.Closed += new EventHandler( this.Dashboard_Closed );

            // Show the dashboard
            activity = $"attempting to show the {nameof( Dashboard )} form";
            _ = (this.SplashViewModel?.DisplayMessage( activity ));
            dashboard.Show();
        }
        catch ( Exception ex )
        {
            // Inform the user that an error occurred.
            _ = ex.AddExceptionData();
            string message = $"An error occurred while {activity}";
            _ = (this.SplashViewModel?.DisplayMessage( message ));
            _ = TraceLogger.LogException( ex, message );
            _ = MessageBox.Show( $"{message}. The error is: {ex.BuildMessage()}", $"{nameof( Dashboard )} Form error occurred" );

            // Exit the current thread instead of showing the windows.
            this.ExitThread();
        }

    }

    /// <summary>   Event handler. Called by Dashboard for closed events. </summary>
    /// <remarks>   David, 2021-03-15. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void Dashboard_Closed( object? sender, EventArgs e )
    {
        // this makes sure the application exits when the main form closes.
        // it is necessary because the context instantiates two forms.
        this.ExitThread();
        this.Dispose();
    }

    /// <summary>   Event handler. Called by Dashboard for load events. </summary>
    /// <remarks>   David, 2021-03-13. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void Dashboard_Load( object? sender, EventArgs e )
    {
        //close splash
        if ( this.SplashForm == null )
        {
            return;
        }

        // the operator could close the program from the splash screen
        bool closeRequested = this.SplashForm.IsCloseRequested;

        // _ = this.SplashForm.Invoke( new Action ( this.SplashForm.Close ) );
        // this.SplashForm.Dispose();
        // this.SplashForm = null;
        _ = this.SplashForm.BeginInvoke( new System.Windows.Forms.MethodInvoker( this.SplashForm.Close ) );
        // _ = binding.Control.BeginInvoke( new MethodInvoker( binding.ReadValue ) );

        if ( closeRequested )
        {
            if ( DialogResult.Yes == MessageBox.Show( "User clicked the Splash form close button. Select Yes to terminate the program or No to ignore the close request",
                                                     "User close requested. Terminate the program?", MessageBoxButtons.YesNo,
                                                     MessageBoxIcon.Information, MessageBoxDefaultButton.Button2 ) )
            {
                // log a warning and exit; no error
                _ = TraceLogger.LogWarning( "User close requested." );
                if ( sender is Dashboard dashboard )
                {
                    _ = dashboard.BeginInvoke( new System.Windows.Forms.MethodInvoker( dashboard.Close ) );
                }

            }
        }

    }

    #endregion

    #region " unhandled exception handling "

    private void OnUnhandledException( object? sender, UnhandledExceptionEventArgs e )
    {
        string activity;
        if ( e is null || e.ExceptionObject is null )
        {
            activity = "Unhandled exception event occurred with event arguments set to nothing.";
            _ = TraceLogger.LogWarning( activity );

            Debug.Assert( !Debugger.IsAttached, "Unhandled exception event occurred with event arguments set to nothing." );
            return;
        }

        try
        {
            if ( e.ExceptionObject is Exception ex )
            {
                _ = ex.AddExceptionData();
                ex.Data.Add( "@isr", $"unhandled exception Exception Occurred." );

                _ = (this.SplashViewModel?.DisplayMessage( "unhandled exception Exception Occurred." ));
                _ = TraceLogger.LogException( ex, "Unhanded Exception" );

                if ( DialogResult.Abort == MessageBox.Show( $"An error occurred while running the application. The error is: {ex.BuildMessage()}",
                                                         "Unhandled exception occurred", MessageBoxButtons.AbortRetryIgnore,
                                                         MessageBoxIcon.Error, MessageBoxDefaultButton.Button3 ) )
                {
                    // exit with an error code
                    Environment.Exit( -1 );
                    this.ExitThread();
                }
            }
        }
        catch
        {
            if ( System.Windows.Forms.MessageBox.Show( e.ExceptionObject.ToString(), "Unhandled Exception occurred.",
                                                       MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error,
                                                       MessageBoxDefaultButton.Button3, MessageBoxOptions.DefaultDesktopOnly ) == DialogResult.Abort )
            {
            }
        }
        finally
        {
        }
    }

    private void OnTaskSchedulerUnobservedException( object? sender, UnobservedTaskExceptionEventArgs e )
    {
        string activity;
        if ( e is null )
        {
            activity = "Unhandled exception event occurred with event arguments set to nothing.";
            _ = TraceLogger.LogWarning( activity );

            Debug.Assert( !Debugger.IsAttached, "Unhandled exception event occurred with event arguments set to nothing." );
            return;
        }

        try
        {
            _ = e.Exception.AddExceptionData();
            e.Exception.Data.Add( "@isr", $"{(e.Observed ? "" : "un")}observed exception Exception Occurred." );

            _ = (this.SplashViewModel?.DisplayMessage( $"{(e.Observed ? "" : "un")}observed exception Exception Occurred." ));
            _ = TraceLogger.LogException( e.Exception, $"Unhanded Exception" );

            if ( DialogResult.Abort == MessageBox.Show( $"An error occurred while running the application. The error is: {e.Exception.BuildMessage()}",
                                                     "Unhandled exception occurred", MessageBoxButtons.AbortRetryIgnore,
                                                     MessageBoxIcon.Error, MessageBoxDefaultButton.Button3 ) )
            {
                // exit with an error code
                Environment.Exit( -1 );
                this.ExitThread();
            }
        }
        catch
        {
            if ( System.Windows.Forms.MessageBox.Show( e.Exception.ToString(), "Unhandled Exception occurred.",
                                                       MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error,
                                                       MessageBoxDefaultButton.Button3, MessageBoxOptions.DefaultDesktopOnly ) == DialogResult.Abort )
            {
            }
        }
        finally
        {
        }
    }

    #endregion

    #region " application event handlers "

    /// <summary>   Event handler. Called by Application for thread exception events. </summary>
    /// <remarks>
    /// David, 2021-03-15. <para>
    /// Because <see cref="Application.ThreadException"/> is a static event, you must detach any
    /// event handlers attached to this event in the ApplicationExit event handler itself. If you do
    /// not detach these handlers, they will remain attached to the event and continue to consume
    /// memory.
    /// </para>
    /// </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Thread exception event information. </param>
    private void OnThreadException( object? sender, ThreadExceptionEventArgs e )
    {
        string activity;
        if ( e is null )
        {
            activity = "Unhandled exception event occurred with event arguments set to nothing.";
            _ = TraceLogger.LogWarning( activity );

            Debug.Assert( !Debugger.IsAttached, "Unhandled exception event occurred with event arguments set to nothing." );
            return;
        }

        try
        {
            _ = e.Exception.AddExceptionData();
            e.Exception.Data.Add( "@isr", "Unhandled Exception Occurred." );

            _ = (this.SplashViewModel?.DisplayMessage( "Unhandled Exception Occurred." ));
            _ = TraceLogger.LogException( e.Exception, "Unhanded Exception" );

            if ( DialogResult.Abort == MessageBox.Show( $"An error occurred while running the application. The error is: {e.Exception.BuildMessage()}",
                                                     "Unhandled exception occurred", MessageBoxButtons.AbortRetryIgnore,
                                                     MessageBoxIcon.Error, MessageBoxDefaultButton.Button3 ) )
            {
                // exit with an error code
                Environment.Exit( -1 );
                this.ExitThread();
            }
        }
        catch
        {
            if ( System.Windows.Forms.MessageBox.Show( e.Exception.ToString(), "Unhandled Exception occurred.",
                                                       MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error,
                                                       MessageBoxDefaultButton.Button3, MessageBoxOptions.DefaultDesktopOnly ) == DialogResult.Abort )
            {
            }
        }
        finally
        {
        }
    }

    /// <summary>   Event handler. Called by Application for application exit events. </summary>
    /// <remarks>
    /// David, 2021-03-15. <para>You must attach the event handlers to the ApplicationExit event to
    /// perform unhandled, required tasks before the application stops running. You can close files
    /// opened by this application, or dispose of objects that garbage collection did not reclaim.
    /// </para><para>
    /// Because  <see cref="Application.ApplicationExit"/> is a static event, you must detach any event handlers attached to this event in
    /// the ApplicationExit event handler itself. If you do not detach these handlers, they will
    /// remain attached to the event and continue to consume memory.
    /// </para>
    /// </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void OnApplicationExit( object? sender, EventArgs e )
    {
        // Because these are static events, you must detach your event handlers when your application
        // is disposed, or memory leaks will result.
        Application.ThreadException -= this.OnThreadException;
        Application.ApplicationExit -= this.OnApplicationExit;
        Application.EnterThreadModal -= this.OnEnterThreadModal;
        Application.Idle -= this.OnApplicationIdle;
        Application.LeaveThreadModal -= this.OnLeaveThreadModal;
        Application.ThreadExit -= this.OnThreadExit;
        NetworkChange.NetworkAvailabilityChanged -= this.OnNetworkAddressChanged;
    }

    /// <summary>   Event handler. Called by Application for enter thread modal events. </summary>
    /// <remarks>
    /// David, 2021-03-15. <para>
    /// Because <see cref="Application.EnterThreadModal"/> is a static event, you must detach any
    /// event handlers attached to this event in the ApplicationExit event handler itself. If you do
    /// not detach these handlers, they will remain attached to the event and continue to consume
    /// memory.
    /// </para>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void OnEnterThreadModal( object? sender, EventArgs e )
    {
    }

    /// <summary>   Event handler. Called by Application for leave thread modal events. </summary>
    /// <remarks>   David, 2021-03-15. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information to send to registered event handlers. </param>
    private void OnLeaveThreadModal( object? sender, EventArgs e )
    {
    }

    /// <summary>   Event handler. Called by Application for entering idle state. </summary>
    /// <remarks>
    /// David, 2021-03-15. <para>
    /// Occurs when the application finishes processing and is about to enter the idle state</para>
    /// </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information to send to registered event handlers. </param>
    private void OnApplicationIdle( object? sender, EventArgs e )
    {
    }

    /// <summary>   Event handler. Called by Application for exiting the thread. </summary>
    /// <remarks>
    /// David, 2021-03-15. <para>
    /// Occurs when a thread is about to shut down. When the main thread for an application is about
    /// to be shut down, this event is raised first, followed by an ApplicationExit</para>
    /// </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information to send to registered event handlers. </param>
    private void OnThreadExit( object? sender, EventArgs e )
    {
        try
        {
            _ = TraceLogger.LogInformation( "Saving assembly settings" );
            Settings.Instance.SaveSettings();
        }
        catch
        {
        }
        finally
        {
        }
        Logging.Orlog.Orlogger.CloseAndFlush();
    }

    /// <summary>   Event handler. Called by Network Change for change of network availability. </summary>
    /// <remarks>   David, 2021-03-15. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information to send to registered event handlers. </param>
    private void OnNetworkAddressChanged( object? sender, NetworkAvailabilityEventArgs e )
    {
    }

    #endregion

    #region " settings "

    /// <summary>   Reads the settings. </summary>
    /// <remarks>   David, 2021-03-15. </remarks>
    private void ReadSettings()
    {
        try
        {
            // Read the settings.
            Settings.Instance.ReadSettings();
        }
        catch ( Exception ex )
        {
            // Inform the user that an error occurred.
            _ = ex.AddExceptionData();
            string message = "An error occurred while attempting to read the program settings";
            _ = TraceLogger.LogException( ex, message );
            _ = MessageBox.Show( $"{message}. The error is: {ex.BuildMessage()}", "Settings error occurred" );

            // Exit the current thread instead of showing the windows.
            this.ExitThread();
        }
    }

    /// <summary>   Gets the <see cref="My.MyProject.Settings"/> configuration information instance. </summary>
    /// <value> The <see cref="Settings"/> configuration information instance. </value>
    [System.ComponentModel.Design.HelpKeyword( "MyApplicationContext.Settings" )]
    internal static Settings Settings => Settings.Instance;

    #endregion

    #region " properties "

    /// <summary>   Gets a value indicating whether the high DPI mode was set. </summary>
    /// <value> True if high DPI was set, false if not. </value>
    public static bool HighDpiModeWasSet { get; private set; }

    #endregion
}

