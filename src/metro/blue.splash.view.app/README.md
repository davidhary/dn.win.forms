# Windows Forms Blue Splash View Command Line Application Example

This Windows Forms example application uses .NET 7.0 in preview mode with JSon settings and Serilog Logging using .NET 7.0 Application Command line design.

## Logging and Tracing
Logging is defined by the application setting JSon file. Trace listening, which can be used to display logged commands, is enabled.

## Settings
Settings with saving capabilities is supported using JSon.

## Single Instance
Upon starting a second instance, the application notifies the existing instance and exists.

## Splash screen
The splash screen is implemented with MVVM design where the program sets properties of the splash screen model view that is bound to the splash screen view.

## Command Line
The program demonstrates the following implementations:

# Command Line Options
The program defines a few command line options as displayed using -h or --help. 

# Command Line Arguments
The command line could consist of an ordered list of arguments where the argument values determine the functionality.

# Command Line Commands
The program demonstrates using a simple or complex command.

# Program Execution Plan
Actual implementation is defined in the execution function of the configuration application.

# Program Execution
Finally, upon execution, the configuration application runs per the defined execution plan.


