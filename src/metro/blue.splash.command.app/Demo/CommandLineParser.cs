using System.CommandLine.Builder;
using System.CommandLine;
using Microsoft.Extensions.Logging;
using BlueSplashCommandApp;
using Microsoft.Extensions.DependencyInjection;
using System.CommandLine.Parsing;
using Win32;

using cc.isr.Logging.Extensions;

namespace cc.isr.WinForms.Metro.Demo;

/// <summary>   A class for constructing a custom command line parser. </summary>
/// <remarks>   2023-04-26. </remarks>
internal sealed class CommandLineParser : Command
{
    /// <summary>   Query if <paramref name="args"> is console out. </summary>
    /// <remarks>   2023-04-26. </remarks>
    /// <param name="args"> The arguments. </param>
    /// <returns>   True if console out, false if not. </returns>
    public static bool IsConsoleOut( string[] args )
    {
        return args is not null && args.Length > 0 && ContainsAny( args, ["-?", "-h", "--help", "--version"] );
    }

    /// <summary>   Query if <paramref name="args"> contains any one of the specified values. </summary>
    /// <remarks>   2023-04-26. </remarks>
    /// <param name="args">     The arguments. </param>
    /// <param name="values">   The values. </param>
    /// <returns>   True if contains, false if not. </returns>
    public static bool ContainsAny( string[] args, string[] values )
    {
        foreach ( string item in values )
        {
            if ( args.Contains( item ) )
                return true;
        }
        return false;
    }

    /// <summary>   Starts an application. </summary>
    /// <remarks>   2023-05-27. </remarks>
    /// <param name="args"> The arguments. </param>
    /// <returns>   An int. </returns>
    public static int StartApplication( string[] args )
    {
        int exitCodeValue;
        if ( CommandLineParser.IsConsoleOut( args ) )
        {
            _ = NativeMethods.AllocateConsole();
            exitCodeValue = new CommandLineParser().Parser.Invoke( args );
            _ = Console.ReadKey();
        }
        else
            exitCodeValue = new CommandLineParser().Parser.Invoke( args );

        return exitCodeValue;
    }

    /// <summary>   Starts a single instance application. </summary>
    /// <remarks>
    /// <see href="https://StackOverflow.com/questions/19147/what-is-the-correct-way-to-create-a-single-instance-wpf-application"/>
    /// 
    /// Having a named mutex allows us to stack synchronization across multiple threads and processes
    /// which is just the magic I'm looking for.
    /// 
    /// Mutex.WaitOne has an overload that specifies an amount of time for us to wait. Since we're
    /// not actually wanting to synchronizing our code (more just check if it is currently in use) we
    /// use the overload with two parameters: Mutex.WaitOne(Timespan timeout, bool exitContext). Wait
    /// one returns true if it is able to enter, and false if it wasn't. In this case, we don't want
    /// to wait at all; If our mutex is being used, skip it, and move on, so we pass in TimeSpan.Zero
    /// (wait 0 milliseconds), and set the exitContext to true so we can exit the synchronization
    /// context before we try to acquire a lock on it.
    /// 
    /// So, if our app is running, WaitOne will return false, otherwise, I opted to utilize a little
    /// Win32 to notify my running instance that someone forgot that it was already running( by
    /// bringing itself to the top of all the other windows). To achieve this I used PostMessage to
    /// broadcast a custom message to every window( the custom message was registered with
    /// RegisterWindowMessage by my running application, which means only my application knows what
    /// it is) then my second instance exits.The running application instance would receive that
    /// notification and process it.In order to do that, I overrode WndProc in my main form and
    /// listened for my custom notification.When I received that notification I set the form's
    /// TopMost property to true to bring it up on top.
    /// </remarks>
    /// <param name="args"> The arguments. </param>
    /// <returns>   The exit code. 0 for success. </returns>
    public static int StartSingleInstanceApplication( string[] args )
    {
        int exitCodeValue = 0;

        if ( _mutex.WaitOne( TimeSpan.Zero, true ) )
        {
            exitCodeValue = CommandLineParser.StartApplication( args );

            _mutex.ReleaseMutex();
        }
        else
        {
            // send our Win32 message to make the currently running instance
            // jump on top of all the other windows
            _ = Win32.NativeMethods.ShowCurrentAppInstance();
        }
        return exitCodeValue;
    }

    /// <summary>   The mutex. </summary>
    private static readonly Mutex _mutex = new( true, "{A49D47E2-2637-40E2-A94C-45ACD7FEEE7F}" );

    /// <summary>   Gets the parser. </summary>
    /// <value> The parser. </value>
    public Parser Parser { get; }

    public CommandLineParser() : base( "BlueSplashCommandApp", "Demonstrate starting a windows form application with a splash screen" )
    {
        Option<DirectoryInfo?> dirOption = new(
            name: "--dir",
            description: "The relative folder for the log file, e.g., ..\\_log." );
        dirOption.AddAlias( "-d" );

        Option<LogLevel> logLevelOption = new(
            name: "--verbosity",
            description: $"Logging level verbosity: {LogLevel.Information}, {LogLevel.Warning}, {LogLevel.Error}.",
            getDefaultValue: () => LogLevel.Information );
        logLevelOption.AddAlias( "-v" );

        Option<TraceWriterListenerMode> traceListenerOption = new(
            name: "--tracing",
            description: $"Trace listener mode: {TraceWriterListenerMode.EventWriter},{TraceWriterListenerMode.EventQueue}.",
            getDefaultValue: () => TraceWriterListenerMode.EventQueue );
        traceListenerOption.AddAlias( "-t" );

        Option<BlueSplashCommandApp.LoggerProvider> logOption = new(
            name: "--logger",
            description: $"Logging provider: {BlueSplashCommandApp.LoggerProvider.Default}, {BlueSplashCommandApp.LoggerProvider.Serilog}",
            getDefaultValue: () => BlueSplashCommandApp.LoggerProvider.Serilog );
        logOption.AddAlias( "-l" );

        this.AddOption( dirOption );
        this.AddOption( logLevelOption );
        this.AddOption( traceListenerOption );
        this.AddOption( logOption );

        this.SetHandler( async ( file, level, traceListenerOption, logOption ) =>
        {
            // Create service collection
            ServiceCollection serviceCollection = new();

            // configure the logger
            ILogger<Program> logger = LoggingParser.ConfigureLoggingServices( serviceCollection, logOption, level );

            logger.LogInformationMessage( "Starting application..." );

            TracingParser.ConfigureTraceLogging( traceListenerOption );

            // Add the application
            _ = serviceCollection.AddTransient<SingleInstanceApplication>();

            // entry to inject the logger and run the application
            serviceCollection.BuildServiceProvider().GetService<SingleInstanceApplication>()!.Run();

            await Task.Delay( 10 );

        },
            dirOption, logLevelOption, traceListenerOption, logOption );

        Parser parser = new CommandLineBuilder( this )
            .UseDefaults()
            .UseHelp( ctx =>
            {
                ctx.HelpBuilder.CustomizeSymbol( logOption,
                    firstColumnText: ( ctx ) => $"--logger <{BlueSplashCommandApp.LoggerProvider.Default}, or {BlueSplashCommandApp.LoggerProvider.Serilog}>",
                    secondColumnText: ( ctx ) => "Specifies the logging provider. " +
                        $"The {BlueSplashCommandApp.LoggerProvider.Serilog} adds file logging " +
                        "as specified in the 'Serilog' Json settings file." );
                ctx.HelpBuilder.CustomizeSymbol( traceListenerOption,
                    firstColumnText: ( ctx ) => $"--tracing <{TraceWriterListenerMode.EventWriter} or {TraceWriterListenerMode.EventQueue}>",
                    secondColumnText: ( ctx ) => "Specifies if trace messages use an asynchronous event writer " +
                        "or a concurrent queue for writing the trace event to the trace event writer.." );
            } ).Build();
        this.Parser = parser;
    }
}

