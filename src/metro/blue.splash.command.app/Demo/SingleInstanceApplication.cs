using Microsoft.Extensions.Logging;

namespace cc.isr.WinForms.Metro.Demo;

/// <summary>   A logger injected application run class. </summary>
/// <remarks>
/// David, 2021-02-04. This class is part of the design that maintains a separation of concern
/// between the business based logic and the logic used to configure and run the actual console
/// application. The <see cref="CommandLineParser"/>
/// bootstraps everything need to support the application. The parser, which is invoked by <see cref="Program"/>,
/// fires off all the logic that is needed to support the "business" needs by way of executing
/// the Run() method of this class, where the actual <see cref="ApplicationContext"/> is invoked
/// via our custom <see cref="AppContext"/>
/// class.
/// </remarks>
internal sealed class SingleInstanceApplication( ILogger<AppContext> logger )
{
    private readonly ILogger<AppContext> _logger = logger;

    /// <summary>
    /// Creates an instance of the <see cref="ApplicationContext"/> and runs the <see cref="Application"/>.
    /// </summary>
    /// <remarks>   2023-05-27. </remarks>
    public void Run()
    {
        // Create the Application Context that derives from ApplicationContext,
        // which manages when the application should exit.
        AppContext context = new( this._logger );

        // Run the application with the specific context. It will exit when
        // all forms are closed.
        Application.Run( context );
    }
}

