using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.ComponentModel;

namespace BlueSplashCommandApp;

internal sealed class LoggingParser
{
    /// <summary>   Configure logging services. </summary>
    /// <remarks>   David, 2021-02-09. </remarks>
    /// <param name="serviceCollection">    Collection of services. </param>
    /// <param name="logLevel">             The log level. </param>
    public static void ConfigureLoggingServices( IServiceCollection serviceCollection, LogLevel logLevel )
    {
        cc.isr.Logging.Orlog.Orlogger.Instance.RuntimeSerilogLevel = logLevel;
        cc.isr.Logging.Orlog.Orlogger.Instance.LoggingServicesProvider = cc.isr.Logging.Orlog.Orlogger.ConfigureLoggingServices(
            serviceCollection,
            cc.isr.Logging.Orlog.Orlogger.BuildEntryAssemblySettingsFileName(),
            cc.isr.Logging.Orlog.Orlogger.Instance.SerilogLevelSwitch,
            cc.isr.Logging.Orlog.Orlogger.Instance.TraceSourceSwitch, cc.isr.Logging.Orlog.LoggingProviderKinds.Serilog ).BuildServiceProvider();
    }

    /// <summary>   Configure logging services. </summary>
    /// <remarks>   2023-05-26. </remarks>
    /// <param name="serviceCollection">    Collection of services. </param>
    /// <param name="provider">             The provider. </param>
    /// <param name="logLevel">             The log level. </param>
    /// <returns>   An <see cref="ILogger{Program}" /> </returns>
    public static ILogger<Program> ConfigureLoggingServices( IServiceCollection serviceCollection, LoggerProvider provider, LogLevel logLevel )
    {
        if ( provider == LoggerProvider.Serilog )
            LoggingParser.ConfigureLoggingServices( serviceCollection, logLevel );

        // a logger can be instantiated at this point. 
        return cc.isr.Logging.Orlog.Orlogger.Instance.CreateLogger<Program>()!;
    }

}
/// <summary>   Values that represent logger providers. </summary>
/// <remarks>   2023-05-26. </remarks>
internal enum LoggerProvider
{
    [Description( "Uses the default Windows logger for console, tracing, event source and event logs" )]
    Default,
    [Description( "Uses the Serilog logger for file, console, tracing, event source and event logs." )]
    Serilog
}
