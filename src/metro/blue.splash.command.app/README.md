# Windows Forms Blue Splash Command Application Example

This Windows Forms example application uses .NET 7.0 with preview. It uses Json settings and Serilog Logging using .NET 7.0 Application Command design.

## Logging and Tracing
Logging is defined by the application setting JSon file. Trace listening, which can be used to display logged commands, is enabled.

## Settings
Settings with saving capabilities is supported using JSon.

## Single Instance
Upon starting a second instance, the application notifies the existing instance and exists.

## Command Line
The program demonstrates the following implementations:

# Command Line Options
The program define a few command line options that can be displayed using the -h and --help keys. These invoke a console screen to display the command line options.

# Command Line Arguments
The command line could consist of an ordered list of arguments where the argument values determine the functionality.

# Command Line Commands
The program demonstrates using a simple or complex command.

# Program Execution Plan
Actual implementation is defined in the execution function of the configuration application.

# Program Execution
Finally, upon execution, the configuration application runs per the defined execution plan.


