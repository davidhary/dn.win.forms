using System.Runtime.InteropServices;

namespace Win32;

/// <summary>   A native methods. </summary>
/// <remarks>   2023-04-26. </remarks>
internal sealed partial class NativeMethods
{
    public static int STD_OUTPUT_HANDLE = -11;

    /// Return Type: HANDLE->void*
    /// nStdHandle: DWORD->unsigned int
    [LibraryImport( "kernel32.dll", EntryPoint = "GetStdHandle", SetLastError = true )]
#pragma warning disable CS3016 // Arrays as attribute arguments is not CLS-compliant
    [UnmanagedCallConv( CallConvs = new Type[] { typeof( System.Runtime.CompilerServices.CallConvStdcall ) } )]
#pragma warning restore CS3016 // Arrays as attribute arguments is not CLS-compliant
    public static partial IntPtr GetStdHandle( int nStdHandle );

    [LibraryImport( "kernel32.dll", EntryPoint = "AllocConsole", SetLastError = true )]
#pragma warning disable CS3016 // Arrays as attribute arguments is not CLS-compliant
    [UnmanagedCallConv( CallConvs = new Type[] { typeof( System.Runtime.CompilerServices.CallConvStdcall ) } )]
#pragma warning restore CS3016 // Arrays as attribute arguments is not CLS-compliant
    [return: MarshalAs( UnmanagedType.Bool )]
    public static partial bool AllocateConsole();
}

