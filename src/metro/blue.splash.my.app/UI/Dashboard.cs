namespace cc.isr.WinForms.Metro.Demo;

/// <summary> A dashboard. </summary>
/// <remarks> David, 2020-09-30. </remarks>
public partial class Dashboard
{
    /// <summary>   Default constructor. </summary>
    /// <remarks>   David, 2020-09-30. </remarks>
    public Dashboard() => this.InitializeComponent();
}
