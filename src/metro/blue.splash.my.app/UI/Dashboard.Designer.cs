using System.Diagnostics;
using System.Windows.Forms;

namespace cc.isr.WinForms.Metro.Demo;

public partial class Dashboard : Form
{
    /// <summary>   Form overrides dispose to clean up the component list. </summary>
    /// <remarks>   David, 2020-09-30. </remarks>
    /// <param name="disposing">    <see langword="true" /> to release both managed and unmanaged
    ///                             resources; <see langword="false" /> to release only unmanaged
    ///                             resources. </param>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    protected override void Dispose(bool disposing)
    {
        try
        {
            if ( disposing )
            {
                this.components?.Dispose();
                this.components = null;
            }
        }
        finally
        {
            base.Dispose(disposing);
        }
    }

    // Required by the Windows Form Designer
    private System.ComponentModel.IContainer components;

    // NOTE: The following procedure is required by the Windows Form Designer
    // It can be modified using the Windows Form Designer.  
    // Do not modify it using the code editor.
    [DebuggerStepThrough()]
    private void InitializeComponent()
    {
        components = new System.ComponentModel.Container();
        AutoScaleMode = AutoScaleMode.Font;
        Text = "Dashboard";
    }
}
