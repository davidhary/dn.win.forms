using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Microsoft.Extensions.Logging;
using Microsoft.VisualBasic.ApplicationServices;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;
using cc.isr.WinForms.Metro.Demo.ExceptionExtensions;
using cc.isr.Logging.Extensions;

namespace cc.isr.WinForms.Metro.Demo.My;

internal partial class MyApplication
{
    #region " network events "

    /// <summary>   Occurs when the network connection is connected or disconnected. </summary>
    /// <remarks>   David, 2020-09-30. </remarks>
    private void HandleNetworkAvailabilityChanged()
    {
        MyProject.Computer.Network.NetworkAvailabilityChanged += HandleNetworkAvailabilityChanged;
    }

    #endregion

    #region " application override events "

    /// <summary>
    /// Sets the visual styles, text display styles, and current principal for the main application
    /// thread (if the application uses Windows authentication), and initializes the splash screen,
    /// if defined. Replaces the default trace listener with the modified listener. Updates the
    /// minimum splash screen display time.
    /// </summary>
    /// <remarks>   David, 2020-09-30. </remarks>
    /// <param name="commandLineArgs">  A <see cref="Collections.ObjectModel.ReadOnlyCollection" />
    ///                                 of String, containing the command-line arguments as strings
    ///                                 for the current application. </param>
    /// <returns>
    /// A <see cref="Boolean" /> indicating if application startup should continue.
    /// </returns>
    protected override bool OnInitialize( System.Collections.ObjectModel.ReadOnlyCollection<string> commandLineArgs )
    {
        return base.OnInitialize( commandLineArgs );
    }

    /// <summary>
    /// When overridden in a derived class, allows a designer to emit code that initializes the
    /// splash screen.
    /// </summary>
    /// <remarks> David, 2020-09-30. </remarks>
    [DebuggerStepThrough()]
    protected override void OnCreateSplashScreen()
    {
        this.CreateSplashScreen() ;
    }

    /// <summary>
    /// Handles the Shutdown event of the MyApplication control. Saves user settings for all related
    /// libraries.
    /// </summary>
    /// <remarks>
    /// This event is not raised if the application terminates abnormally. Application log is set at
    /// verbose level to log shut down operations.
    /// </remarks>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
    protected override void OnShutdown()
    {
        MyProject.Application.SaveMySettingsOnExit = true;
        // Save library settings here
        this.ProcessShutDown();
        try
        {
            Trace.CorrelationManager.StartLogicalOperation( System.Reflection.MethodBase.GetCurrentMethod().Name );
            if ( MyProject.Application.SaveMySettingsOnExit )
            {
                TraceLogger.LogDebug( "Saving assembly settings" );
                Settings.Instance.SaveSettings();
            }

            cc.isr.Logging.Orlog.Orlogger.CloseAndFlush();
        }
        catch
        {
        }
        finally
        {
        }

        try
        {
            this.ReleaseSplashScreen();
        }
        catch
        {
        }
        finally
        {
            Trace.CorrelationManager.StopLogicalOperation();
            base.OnShutdown();
        }
    }

    /// <summary> Occurs when the application starts, before the startup form is created. </summary>
    /// <remarks> David, 2020-09-30. </remarks>
    /// <param name="eventArgs"> Startup event information. </param>
    /// <returns>
    /// A <see cref="Boolean" /> that indicates if the application should continue starting
    /// up.
    /// </returns>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
    protected override bool OnStartup( StartupEventArgs eventArgs )
    {
        if ( eventArgs is null )
        {
            eventArgs = new StartupEventArgs( new System.Collections.ObjectModel.ReadOnlyCollection<string>( Array.Empty<string>() ) );
        }

        string activity = string.Empty;

        // Turn on the screen hourglass
        Cursor.Current = Cursors.AppStarting;
        Application.DoEvents();
        try
        {
            Cursor.Current = Cursors.AppStarting;
            Trace.CorrelationManager.StartLogicalOperation( System.Reflection.MethodBase.GetCurrentMethod().Name );

            activity = $"processing {System.Reflection.Assembly.GetExecutingAssembly().FullName} startup";
            string message = TraceLogger.LogDebug( activity );
            (( MySplashScreen ) this.SplashScreen)?.DisplayMessage( message );
            (bool Success, string Details) result = MyApplication.ProcessStartup( eventArgs );

            if ( eventArgs.Cancel )
            {
                // Show the exception message box with three custom buttons.
                Cursor.Current = Cursors.Default;
                if ( DialogResult.Cancel == MessageBox.Show( $"Failed starting up. Details: {result.Details}.",
                                                         "Failed Starting Program", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2 ) )
                {
                    message = TraceLogger.LogWarning( $"Application aborted by the user. Details: {result.Details}" );
                    (( MySplashScreen ) this.SplashScreen)?.DisplayMessage( message );
                    eventArgs.Cancel = true;
                }
                else
                {
                    eventArgs.Cancel = false;
                }

                Cursor.Current = Cursors.AppStarting;
            }

            if ( !eventArgs.Cancel )
            {
                eventArgs.Cancel = !this.TryInitializeKnownState();
                if ( eventArgs.Cancel )
                {
                    _ = MessageBox.Show( $"Failed initializing application state. Check the program log at '{Logging.Orlog.TraceLogBrowser.FullLogFileName}' for additional information.",
                                         "Failed Starting Program", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button2 );
                }
            }

            if ( eventArgs.Cancel )
            {
                message = TraceLogger.LogWarning( "Application failed to start up." );
                (( MySplashScreen ) this.SplashScreen)?.DisplayMessage( message );
                Logging.Orlog.Orlogger.CloseAndFlush();

                // exit with an error code
                Environment.Exit( -1 );
                Application.Exit();
            }
            else if ( this.UserCloseRequested() )
             {
                message = TraceLogger.LogWarning( "User close requested." );
                (( MySplashScreen ) this.SplashScreen)?.DisplayMessage( message );

                Logging.Orlog.Orlogger.CloseAndFlush();

                // exit with an error code
                Environment.Exit( -1 );
                Application.Exit();
            }
            else
            {
                message = TraceLogger.LogVerbose( "Loading application window..." );
                (( MySplashScreen ) this.SplashScreen)?.DisplayMessage( message );
            }
        }
        catch ( Exception ex )
        {
            ExceptionDataMethods.AddExceptionData( ex );
            ex.Data.Add( "@isr", "Exception occurred starting this application" );
            string message = TraceLogger.LogError( $"Exception {activity}" );
            (( MySplashScreen ) this.SplashScreen)?.DisplayMessage( message );
            TraceLogger.LogException( ex, $"Exception {activity}" );

            Cursor.Current = Cursors.Default;
            if ( DialogResult.Abort == MessageBox.Show( $"Exception {activity}. Details: {ex.BuildMessage()}.",
                                                     "Failed Starting Program", MessageBoxButtons.AbortRetryIgnore,
                                                     MessageBoxIcon.Error, MessageBoxDefaultButton.Button1 ) )
            {
                // exit with an error code
                Environment.Exit( -1 );
                Application.Exit();
            }
        }
        finally
        {
            Cursor.Current = Cursors.Default;
            Trace.CorrelationManager.StopLogicalOperation();
        }

        return base.OnStartup( eventArgs );
    }

    /// <summary>
    /// Occurs when launching a single-instance application and the application is already active.
    /// </summary>
    /// <remarks> David, 2020-09-30. </remarks>
    /// <param name="eventArgs"> Startup next instance event information. </param>
    protected override void OnStartupNextInstance( StartupNextInstanceEventArgs eventArgs )
    {
        string message = TraceLogger.LogInformation( "Application next instant starting." );
        (( MySplashScreen ) this.SplashScreen)?.DisplayMessage( message );
        TraceLogger.LogInformation( message );
        (( MySplashScreen ) this.SplashScreen)?.DisplayMessage( message );
        base.OnStartupNextInstance( eventArgs );
    }

    /// <summary>
    /// When overridden in a derived class, allows for code to run when an unhandled exception occurs
    /// in the application.
    /// </summary>
    /// <remarks> David, 2020-09-30. </remarks>
    /// <param name="e"> <see cref="Microsoft.VisualBasic.ApplicationServices.UnhandledExceptionEventArgs" />. </param>
    /// <returns>
    /// A <see cref="Boolean" /> that indicates whether the
    /// <see cref="E:Microsoft.VisualBasic.ApplicationServices.WindowsFormsApplicationBase.UnhandledException" />
    /// event was raised.
    /// </returns>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
    protected override bool OnUnhandledException( Microsoft.VisualBasic.ApplicationServices.UnhandledExceptionEventArgs e )
    {
        string activity = string.Empty;
        bool returnedValue = true;
        if ( e is null )
        {
            Debug.Assert( !Debugger.IsAttached, "Unhandled exception event occurred with event arguments set to nothing." );
            return base.OnUnhandledException( e );
        }

        try
        {
            e.Exception.Data.Add( "@isr", "Unhandled Exception Occurred." );
            TraceLogger.LogError( $"Exception {activity}" );
            TraceLogger.LogException( e.Exception, "Unhanded Exception" );

            if ( DialogResult.Abort == MessageBox.Show( $"Unhandled exception. Details: {e.Exception.BuildMessage()}.",
                                                     "Unhandled exception occurred", MessageBoxButtons.AbortRetryIgnore,
                                                     MessageBoxIcon.Error, MessageBoxDefaultButton.Button3 ) )
            {
                // exit with an error code
                Environment.Exit( -1 );
                Application.Exit();
            }
        }
        catch
        {
            if ( System.Windows.Forms.MessageBox.Show( e.Exception.ToString(), "Unhandled Exception occurred.",
                                                       MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error,
                                                       MessageBoxDefaultButton.Button3, MessageBoxOptions.DefaultDesktopOnly ) == DialogResult.Abort )
            {
            }
        }
        finally
        {
        }

        return returnedValue;
    }

    #endregion
}
