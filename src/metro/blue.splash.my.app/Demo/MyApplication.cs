using System.ComponentModel;
using System.Diagnostics;
using cc.isr.Logging.TraceLog;
using cc.isr.WinForms.Metro.Demo.ExceptionExtensions;

namespace cc.isr.WinForms.Metro.Demo.My;

internal partial class MyApplication
{
    #region " event handling methods "

    /// <summary>   Gets or sets the trace logger. </summary>
    /// <value> The trace logger. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public static TraceLogger<MyApplication> TraceLogger { get; set; } = new();

    /// <summary>   Releases the splash. </summary>
    /// <remarks>   David, 2020-09-30. </remarks>
    internal void ReleaseSplashScreen()
    {
        MyProject.Forms.MySplashScreen.Close();
        MyProject.Forms.MySplashScreen.Dispose();
        this.SplashScreen = null;
    }

    /// <summary>   Creates splash screen. </summary>
    /// <remarks>   David, 2020-09-30. </remarks>
    internal void CreateSplashScreen()
    {
        string message = TraceLogger.LogVerbose( "Creating splash screen" );
        (( MySplashScreen ) this.SplashScreen)?.DisplayMessage( message );
        this.MinimumSplashScreenDisplayTime = ( int ) Settings.Instance.SplashFormDisplayDuration.TotalMilliseconds;
        this.SplashScreen = MyProject.Forms.MySplashScreen;
        Demo.MySplashScreen.CreateInstance( MyProject.Application.SplashScreen );
    }

    /// <summary>   Determines if user requested a close. </summary>
    /// <remarks>   David, 2020-09-30. </remarks>
    /// <returns>   True if close requested; false otherwise. </returns>
    internal bool UserCloseRequested()
    {
        return ((( MySplashScreen ) this.SplashScreen)?.IsCloseRequested).GetValueOrDefault( false );
    }

    /// <summary> Instantiates the application to its known state. </summary>
    /// <remarks> David, 2020-09-30. </remarks>
    /// <returns> <c>true</c> if success or <c>false</c> if failed. </returns>
    private bool TryInitializeKnownState()
    {
        string activity = string.Empty;
        try
        {
            Cursor.Current = Cursors.AppStarting;

            // show status
            activity = Debugger.IsAttached ? "Application is initializing in design mode." : "Application is initializing in runtime mode.";
            _ = TraceLogger.LogVerbose( activity );
            activity = "handle network availability change";
            this.HandleNetworkAvailabilityChanged();

            return true;
        }
        catch ( Exception ex )
        {
            // Turn off the hourglass
            Cursor.Current = Cursors.Default;
            _ = ExceptionDataMethods.AddExceptionData( ex );
            _ = TraceLogger.LogException( ex, $"Exception {activity}" );
            try
            {
                this.ReleaseSplashScreen();
            }
            finally
            {
            }

            return false;
        }
        finally
        {
            Cursor.Current = Cursors.Default;
        }
    }

    /// <summary> Processes the shut down. </summary>
    /// <remarks> David, 2020-09-30. </remarks>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    private void ProcessShutDown()
    {
        MyProject.Application.SaveMySettingsOnExit = true;
        if ( MyProject.Application.SaveMySettingsOnExit )
        {
            // Save library settings here
        }
    }

    /// <summary>
    /// Processes the startup. Sets the event arguments
    /// <see cref="Microsoft.VisualBasic.ApplicationServices.StartupEventArgs"/> cancel
    /// value if failed.
    /// </summary>
    /// <remarks> David, 2020-09-30. </remarks>
    /// <param name="e"> The <see cref="Microsoft.VisualBasic.ApplicationServices.StartupEventArgs" />
    /// instance containing the event data. </param>
    private static (bool Success, string Details) ProcessStartup( Microsoft.VisualBasic.ApplicationServices.StartupEventArgs e )
    {
        (bool Success, string Details) result = (true, string.Empty);
        if ( !e.Cancel )
        {
            string activity = string.Empty;
            try
            {
                TraceLogger.MinimumLogLevel = Settings.Instance.ApplicationLogLevel;

                activity = "creating splash screen instance";
                Demo.MySplashScreen.CreateInstance( MyProject.Application.SplashScreen );

                activity = "using splash screen";
                _ = TraceLogger.LogVerbose( activity );

            }
            catch ( Exception ex )
            {
                _ = TraceLogger.LogError( $"Exception {activity}" );
                result = (false, $"Exception {activity};. {ex.BuildMessage()}");
                _ = TraceLogger.LogException( ex, $"Exception {activity}" );
            }
        }
        e.Cancel = !result.Success;
        return result;
    }

    #endregion

    #region " network events "

    /// <summary> Occurs when the network connection is connected or disconnected. </summary>
    /// <remarks> David, 2020-09-30. </remarks>
    /// <param name="sender"> The source of the event. </param>
    /// <param name="e">      Network available event information. </param>
    private void HandleNetworkAvailabilityChanged( object? sender, Microsoft.VisualBasic.Devices.NetworkAvailableEventArgs e )
    {
    }

    #endregion
}
