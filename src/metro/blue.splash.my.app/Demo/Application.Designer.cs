using System.Diagnostics;

namespace cc.isr.WinForms.Metro.Demo.My;

/// <summary>   my application. </summary>
/// <remarks>   David, 2021-03-13. </remarks>
internal partial class MyApplication
{
    [DebuggerStepThrough()]
    public MyApplication() : base(Microsoft.VisualBasic.ApplicationServices.AuthenticationMode.Windows)
    {
        IsSingleInstance = false;
        EnableVisualStyles = true;
        SaveMySettingsOnExit = true;
        ShutdownStyle = Microsoft.VisualBasic.ApplicationServices.ShutdownMode.AfterMainFormCloses;
    }

    [DebuggerStepThrough()]
    protected override void OnCreateMainForm()
    {
        MainForm = MyProject.Forms.Dashboard;
    }
}
