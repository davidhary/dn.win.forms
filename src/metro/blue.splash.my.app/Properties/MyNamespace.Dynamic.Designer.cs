using System;
using System.ComponentModel;
using System.Diagnostics;

namespace cc.isr.WinForms.Metro.Demo.My;

internal static partial class MyProject
{
    internal partial class MyForms
    {
        /// <summary>   The dashboard. </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public Dashboard _dashboard;

        /// <summary>   Gets or sets the dashboard. </summary>
        /// <exception cref="ArgumentException">    Thrown when one or more arguments have unsupported or
        ///                                         illegal values. </exception>
        /// <value> The dashboard. </value>
        public Dashboard Dashboard
        {
            [DebuggerHidden]
            get
            {
                _dashboard = Create__Instance__(_dashboard);
                return _dashboard;
            }

            [DebuggerHidden]
            set
            {
                if (ReferenceEquals(value, _dashboard))
                    return;
                if (value is object)
                    throw new ArgumentException("Property can only be set to Nothing");
                Dispose__Instance__(ref _dashboard);
            }
        }

        /// <summary>   my splash screen. </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public MySplashScreen _mySplashScreen;

        /// <summary>   Gets or sets my splash screen. </summary>
        /// <exception cref="ArgumentException">    Thrown when one or more arguments have unsupported or
        ///                                         illegal values. </exception>
        /// <value> my splash screen. </value>
        public MySplashScreen MySplashScreen
        {
            [DebuggerHidden]
            get
            {
                _mySplashScreen = Create__Instance__(_mySplashScreen);
                return _mySplashScreen;
            }

            [DebuggerHidden]
            set
            {
                if (ReferenceEquals(value, _mySplashScreen))
                    return;
                if (value is object)
                    throw new ArgumentException("Property can only be set to Nothing");
                Dispose__Instance__(ref _mySplashScreen);
            }
        }
    }
}
