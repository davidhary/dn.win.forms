# Windows Forms Blue Splash My Application Example

This Windows Forms example application uses .NET 5.0 with JSon settings and 
Serilog Logging using the Visual Basic My Platform ported to .NET 5.0.

## Logging and Tracing
Logging is defined by the application setting JSon file. Trace listening,
which can be used to display logged commands, is enabled.

## Settings
Settings with saving capabilities is supported using JSon.


