namespace cc.isr.WinForms.Metro.Blue;
public partial class BlueSplashView
{
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    #region "windows form designer generated code"

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BlueSplashView));
            this._smallApplicationCaptionLabel = new System.Windows.Forms.Label();
            this._largeApplicationCaptionLabel = new System.Windows.Forms.Label();
            this._currentTaskLabel = new System.Windows.Forms.Label();
            this._closeLabel = new System.Windows.Forms.Label();
            this._minimizeLabel = new System.Windows.Forms.Label();
            this._folderLabel = new System.Windows.Forms.Label();
            this._tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._authorLabel = new System.Windows.Forms.Label();
            this._topPanel = new System.Windows.Forms.Panel();
            this._progressLayout = new System.Windows.Forms.TableLayoutPanel();
            this._metroProgressBarPictureBox = new System.Windows.Forms.PictureBox();
            this.splashViewModelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this._tableLayoutPanel.SuspendLayout();
            this._topPanel.SuspendLayout();
            this._progressLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._metroProgressBarPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splashViewModelBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // _smallApplicationCaptionLabel
            // 
            this._smallApplicationCaptionLabel.AutoSize = true;
            this._smallApplicationCaptionLabel.BackColor = System.Drawing.Color.Transparent;
            this._smallApplicationCaptionLabel.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold);
            this._smallApplicationCaptionLabel.ForeColor = System.Drawing.Color.White;
            this._smallApplicationCaptionLabel.Location = new System.Drawing.Point(32, 0);
            this._smallApplicationCaptionLabel.Name = "_smallApplicationCaptionLabel";
            this._smallApplicationCaptionLabel.Size = new System.Drawing.Size(124, 20);
            this._smallApplicationCaptionLabel.TabIndex = 0;
            this._smallApplicationCaptionLabel.Text = "Application Title";
            // 
            // _largeApplicationCaptionLabel
            // 
            this._largeApplicationCaptionLabel.AutoSize = true;
            this._largeApplicationCaptionLabel.BackColor = System.Drawing.Color.Transparent;
            this._largeApplicationCaptionLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this._largeApplicationCaptionLabel.Font = new System.Drawing.Font("Segoe UI Semibold", 36F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this._largeApplicationCaptionLabel.ForeColor = System.Drawing.Color.White;
            this._largeApplicationCaptionLabel.Location = new System.Drawing.Point(3, 112);
            this._largeApplicationCaptionLabel.Name = "_largeApplicationCaptionLabel";
            this._largeApplicationCaptionLabel.Size = new System.Drawing.Size(506, 65);
            this._largeApplicationCaptionLabel.TabIndex = 1;
            this._largeApplicationCaptionLabel.Text = "Application";
            this._largeApplicationCaptionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _currentTaskLabel
            // 
            this._currentTaskLabel.AutoEllipsis = true;
            this._currentTaskLabel.AutoSize = true;
            this._currentTaskLabel.BackColor = System.Drawing.Color.Transparent;
            this._currentTaskLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this._currentTaskLabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic);
            this._currentTaskLabel.ForeColor = System.Drawing.Color.White;
            this._currentTaskLabel.Location = new System.Drawing.Point(3, 280);
            this._currentTaskLabel.Name = "_currentTaskLabel";
            this._currentTaskLabel.Size = new System.Drawing.Size(506, 21);
            this._currentTaskLabel.TabIndex = 2;
            this._currentTaskLabel.Text = "current task";
            // 
            // _closeLabel
            // 
            this._closeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._closeLabel.AutoSize = true;
            this._closeLabel.BackColor = System.Drawing.Color.Transparent;
            this._closeLabel.Font = new System.Drawing.Font("Webdings", 18F);
            this._closeLabel.ForeColor = System.Drawing.Color.White;
            this._closeLabel.Location = new System.Drawing.Point(467, 0);
            this._closeLabel.Name = "_closeLabel";
            this._closeLabel.Size = new System.Drawing.Size(36, 26);
            this._closeLabel.TabIndex = 3;
            this._closeLabel.Text = "r";
            this._closeLabel.Click += new System.EventHandler(this.Close_Click);
            this._closeLabel.MouseLeave += new System.EventHandler(this.Close_MouseLeave);
            this._closeLabel.MouseHover += new System.EventHandler(this.Close_MouseHover);
            // 
            // _minimizeLabel
            // 
            this._minimizeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._minimizeLabel.AutoSize = true;
            this._minimizeLabel.BackColor = System.Drawing.Color.Transparent;
            this._minimizeLabel.Font = new System.Drawing.Font("Webdings", 18F);
            this._minimizeLabel.ForeColor = System.Drawing.Color.White;
            this._minimizeLabel.Location = new System.Drawing.Point(425, 0);
            this._minimizeLabel.Name = "_minimizeLabel";
            this._minimizeLabel.Size = new System.Drawing.Size(36, 26);
            this._minimizeLabel.TabIndex = 4;
            this._minimizeLabel.Text = "0";
            this._minimizeLabel.Click += new System.EventHandler(this.Minimize_Click);
            this._minimizeLabel.MouseLeave += new System.EventHandler(this.Minimize_MouseLeave);
            this._minimizeLabel.MouseHover += new System.EventHandler(this.Minimize_MouseHover);
            // 
            // _folderLabel
            // 
            this._folderLabel.AutoSize = true;
            this._folderLabel.BackColor = System.Drawing.Color.Transparent;
            this._folderLabel.Font = new System.Drawing.Font("Wingdings", 11.25F);
            this._folderLabel.ForeColor = System.Drawing.Color.White;
            this._folderLabel.Location = new System.Drawing.Point(3, 3);
            this._folderLabel.Name = "_folderLabel";
            this._folderLabel.Size = new System.Drawing.Size(27, 16);
            this._folderLabel.TabIndex = 6;
            this._folderLabel.Text = "1";
            // 
            // _tableLayoutPanel
            // 
            this._tableLayoutPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(86)))), ((int)(((byte)(154)))));
            this._tableLayoutPanel.ColumnCount = 1;
            this._tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayoutPanel.Controls.Add(this._authorLabel, 0, 9);
            this._tableLayoutPanel.Controls.Add(this._topPanel, 0, 1);
            this._tableLayoutPanel.Controls.Add(this._currentTaskLabel, 0, 7);
            this._tableLayoutPanel.Controls.Add(this._largeApplicationCaptionLabel, 0, 3);
            this._tableLayoutPanel.Controls.Add(this._progressLayout, 0, 5);
            this._tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this._tableLayoutPanel.Name = "_tableLayoutPanel";
            this._tableLayoutPanel.RowCount = 10;
            this._tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3F));
            this._tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this._tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3F));
            this._tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._tableLayoutPanel.Size = new System.Drawing.Size(512, 324);
            this._tableLayoutPanel.TabIndex = 7;
            // 
            // _authorLabel
            // 
            this._authorLabel.AutoSize = true;
            this._authorLabel.BackColor = System.Drawing.Color.Transparent;
            this._authorLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this._authorLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this._authorLabel.ForeColor = System.Drawing.Color.WhiteSmoke;
            this._authorLabel.Location = new System.Drawing.Point(3, 304);
            this._authorLabel.Name = "_authorLabel";
            this._authorLabel.Size = new System.Drawing.Size(506, 17);
            this._authorLabel.TabIndex = 9;
            this._authorLabel.Text = "A product of Integrated Scientific Resources";
            this._authorLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _topPanel
            // 
            this._topPanel.BackColor = System.Drawing.Color.Transparent;
            this._topPanel.Controls.Add(this._closeLabel);
            this._topPanel.Controls.Add(this._folderLabel);
            this._topPanel.Controls.Add(this._minimizeLabel);
            this._topPanel.Controls.Add(this._smallApplicationCaptionLabel);
            this._topPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this._topPanel.Location = new System.Drawing.Point(3, 6);
            this._topPanel.Name = "_topPanel";
            this._topPanel.Size = new System.Drawing.Size(506, 26);
            this._topPanel.TabIndex = 0;
            // 
            // _progressLayout
            // 
            this._progressLayout.ColumnCount = 3;
            this._progressLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._progressLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._progressLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._progressLayout.Controls.Add(this._metroProgressBarPictureBox, 1, 0);
            this._progressLayout.Dock = System.Windows.Forms.DockStyle.Top;
            this._progressLayout.Location = new System.Drawing.Point(3, 190);
            this._progressLayout.Name = "_progressLayout";
            this._progressLayout.RowCount = 1;
            this._progressLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._progressLayout.Size = new System.Drawing.Size(506, 10);
            this._progressLayout.TabIndex = 8;
            // 
            // _metroProgressBarPictureBox
            // 
            this._metroProgressBarPictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._metroProgressBarPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("_metroProgressBarPictureBox.Image")));
            this._metroProgressBarPictureBox.Location = new System.Drawing.Point(23, 3);
            this._metroProgressBarPictureBox.Name = "_metroProgressBarPictureBox";
            this._metroProgressBarPictureBox.Size = new System.Drawing.Size(460, 4);
            this._metroProgressBarPictureBox.TabIndex = 0;
            this._metroProgressBarPictureBox.TabStop = false;
            // 
            // splashViewModelBindingSource
            // 
            this.splashViewModelBindingSource.DataSource = typeof(cc.isr.ViewModels.Splash.SplashViewModel);
            // 
            // BlueSplashView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::cc.isr.WinForms.Properties.Resources.BlueSplash;
            this.ClientSize = new System.Drawing.Size(512, 324);
            this.Controls.Add(this._tableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximumSize = new System.Drawing.Size(512, 324);
            this.MinimumSize = new System.Drawing.Size(512, 324);
            this.Name = "BlueSplashView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Splash";
            this._tableLayoutPanel.ResumeLayout(false);
            this._tableLayoutPanel.PerformLayout();
            this._topPanel.ResumeLayout(false);
            this._topPanel.PerformLayout();
            this._progressLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._metroProgressBarPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splashViewModelBindingSource)).EndInit();
            this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label _smallApplicationCaptionLabel;
    private System.Windows.Forms.Label _largeApplicationCaptionLabel;
    private System.Windows.Forms.Label _currentTaskLabel;
    private System.Windows.Forms.Label _closeLabel;
    private System.Windows.Forms.Label _minimizeLabel;
    private System.Windows.Forms.Label _folderLabel;
    private System.Windows.Forms.TableLayoutPanel _tableLayoutPanel;
    private System.Windows.Forms.Panel _topPanel;
    private System.Windows.Forms.TableLayoutPanel _progressLayout;
    private System.Windows.Forms.Label _authorLabel;
    private System.Windows.Forms.PictureBox _metroProgressBarPictureBox;
    private System.Windows.Forms.BindingSource splashViewModelBindingSource;
}
