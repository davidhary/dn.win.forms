using System;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using System.ComponentModel;

namespace cc.isr.WinForms.Metro.Blue;

/// <summary> Blue splash. </summary>
/// <remarks>
/// (c) 2015 Magyar ANDRÁS. All rights reserved.<para>
/// Licensed under The MIT License. </para><para>
/// David, 2012-05-08, <see href="http://www.codeproject.com/Articles/804316/Office-Style-Splash-Screen."/> </para>
/// </remarks>
public partial class BlueSplash : Forms.FormBase
{
    #region " construction "

    /// <summary>   Default constructor. </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <param name="assembly"> The assembly. This is introduced as a breaking change to force setting the assembly information. </param>
    public BlueSplash( Assembly assembly )
    {
        this.Shown += this.Form_Shown;
        this.InitializeComponent();

        try
        {
            // this fails under .net 4.72 and 4.8
            System.ComponentModel.ComponentResourceManager resources = new( typeof( BlueSplash ) );
            this._metroProgressBarPictureBox.Image = resources.GetObject( "_metroProgressBarPictureBox.Image", System.Globalization.CultureInfo.CurrentCulture ) as System.Drawing.Image;
            this.Icon = resources.GetObject( "$this.Icon", System.Globalization.CultureInfo.CurrentCulture ) as System.Drawing.Icon;
        }
        catch ( Exception )
        {
        }

        this.UpdateInfo( assembly );
    }

    /// <summary>
    /// Disposes of the resources (other than memory) used by the <see cref="Form" />.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="disposing"> true to release both managed and unmanaged resources; false to
    /// release only unmanaged resources. </param>
    protected override void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this.components?.Dispose();
                this.components = null;
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }

    #endregion

    #region " event handlers "

    /// <summary>
    /// Does all the post processing after all the form controls are rendered as the user expects
    /// them.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void Form_Shown( object? sender, EventArgs e )
    {
        System.Windows.Forms.Application.DoEvents();
        try
        {
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CurrentTask = "Starting...";
        }
        catch ( Exception ex )
        {
            _ = System.Windows.Forms.MessageBox.Show( ex.ToString(), "Exception Occurred", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Exclamation, System.Windows.Forms.MessageBoxDefaultButton.Button1, System.Windows.Forms.MessageBoxOptions.DefaultDesktopOnly );
        }
        finally
        {
            this.Cursor = System.Windows.Forms.Cursors.Default;
        }
    }

    /// <summary> Close Application. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void Close_Click( object? sender, EventArgs e )
    {
        this.IsCloseRequested = true;
    }

    /// <summary> Minimize label effects. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void Minimize_Click( object? sender, EventArgs e )
    {
        this.IsMinimizeRequested = true;
    }

    /// <summary> Closes label Mouse hover and leave effects </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void Close_MouseHover( object? sender, EventArgs e )
    {
        this._closeLabel.ForeColor = Color.Silver;
    }

    /// <summary> Closes mouse leave. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void Close_MouseLeave( object? sender, EventArgs e )
    {
        this._closeLabel.ForeColor = Color.White;
    }

    /// <summary> Minimize mouse hover. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void Minimize_MouseHover( object? sender, EventArgs e )
    {
        this._minimizeLabel.ForeColor = Color.Silver;
    }

    /// <summary> Minimize mouse leave. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void Minimize_MouseLeave( object? sender, EventArgs e )
    {
        this._minimizeLabel.ForeColor = Color.White;
    }
    #endregion

    #region " properties and methods "

    /// <summary> Gets a value indicating whether this object is minimize requested. </summary>
    /// <value> <c>true</c> if this object is minimize requested; otherwise <c>false</c> </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public bool IsMinimizeRequested { get; private set; }

    /// <summary> Gets a value indicating whether this object is close requested. </summary>
    /// <value> <c>true</c> if this object is close requested; otherwise <c>false</c> </value>
    /// <summary> Gets a value indicating whether this object is close requested. </summary>
    /// <value> <c>true</c> if this object is close requested; otherwise <c>false</c> </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public bool IsCloseRequested { get; private set; }

    /// <summary> Gets or sets the current task. </summary>
    /// <value> The current task. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string CurrentTask
    {
        get => this._currentTaskLabel.Text;
        set
        {
            if ( !string.Equals( value, this.CurrentTask, StringComparison.Ordinal ) )
            {
                _ = SafeTextSetter( this._currentTaskLabel, value );
            }
        }
    }

    /// <summary> true to topmost. </summary>
    private bool _topmost;

    /// <summary> Sets the top most status in a thread safe way. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> true to value. </param>
    public void TopmostSetter( bool value )
    {
        this._topmost = value;
        SafeTopMostSetter( this, value );
    }

    /// <summary> Displays a message on the splash screen. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The message. </param>
    public void DisplayMessage( string value )
    {
        this.CurrentTask = value;
    }

    /// <summary> Gets or sets the small application caption. </summary>
    /// <value> The small application caption. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string SmallApplicationCaption
    {
        get => this._smallApplicationCaptionLabel.Text;
        set
        {
            if ( !string.Equals( value, this.SmallApplicationCaption, StringComparison.Ordinal ) )
            {
                _ = SafeTextSetter( this._smallApplicationCaptionLabel, value );
            }
        }
    }

    /// <summary> Gets or sets the large application caption. </summary>
    /// <value> The large application caption. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string LargeApplicationCaption
    {
        get => this._largeApplicationCaptionLabel.Text;
        set
        {
            if ( !string.Equals( value, this.LargeApplicationCaption, StringComparison.Ordinal ) )
            {
                _ = SafeTextSetter( this._largeApplicationCaptionLabel, value );
            }
        }
    }

    /// <summary>   Measure text. </summary>
    /// <remarks>   2023-05-25. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="font"> The font. </param>
    /// <param name="text"> The text. </param>
    /// <returns>   A SizeF. </returns>
    private static SizeF MeasureText( Font font, string text )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( font, nameof( font ) );
#else
        if ( font is null )
        {
            throw new ArgumentNullException( nameof( font ) );
        }
#endif

        using Control ctrl = new();
        using Graphics graphics = ctrl.CreateGraphics();
        return graphics.MeasureString( text, font );
    }

    /// <summary>   Select font. </summary>
    /// <remarks>   David, 2021-03-02. </remarks>
    /// <param name="largestSize">  Size of the largest. </param>
    /// <param name="width">        The width. </param>
    /// <param name="font">         The font. </param>
    /// <param name="text">         The text. </param>
    /// <returns>   A Font. </returns>
    private static Font SelectFont( Font font, float largestSize, float width, string text )
    {
        float increment = 1;
        float size = largestSize + increment;
        do
        {
            size -= increment;
            font = new Font( font.FontFamily, size, font.Style );
        }
        while ( MeasureText( font, text ).Width >= width );

        // this is required to fit the font inside.
        font = new Font( font.FontFamily, ( float ) (size - 0.5), font.Style );

        return font;
    }

    /// <summary>   Update the information on screen. </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="assembly"> The assembly. This is introduced as a breaking change to force
    ///                         setting the assembly information. </param>
    private void UpdateInfo( Assembly assembly )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( assembly, nameof( assembly ) );
#else
        if ( assembly is null ) throw new ArgumentNullException( nameof( assembly ) );
#endif
        (string name, string version) = assembly.GetName() is AssemblyName assemblyName
            ? (assemblyName.Name ?? "unknown", assemblyName.Version is Version assemblyVersion
                                                    ? assemblyVersion.ToString() : "?.?.?.?")
            : ("unknown", "?.?.?.?");
        this.UpdateInfo( name, version );
    }

    private void UpdateInfo( string assemblyName, string assemblyVersion )
    {
        if ( this.InvokeRequired )
        {
            _ = this.BeginInvoke( new Action<string, string>( this.UpdateInfo ), [assemblyName, assemblyVersion] );
        }
        else
        {
            Font font = SelectFont( this._largeApplicationCaptionLabel.Font, 36, this.Width, assemblyName );
            this._largeApplicationCaptionLabel.Font = font;
            this._largeApplicationCaptionLabel.Text = assemblyName;
            this._smallApplicationCaptionLabel.Text = $"{assemblyName} {assemblyVersion}";
            this.TopMost = this._topmost;
        }
    }

    #endregion

    #region " thread safe methods "

    /// <summary> Safe height setter. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="control"> The control. </param>
    /// <param name="value">   true to value. </param>
    private static void SafeHeightSetter( Control control, int value )
    {
        if ( control is not null )
        {
            if ( control.InvokeRequired )
            {
                _ = control.BeginInvoke( new Action<Control, int>( SafeHeightSetter ), [control, value] );
            }
            else
            {
                control.Height = value;
                control.Invalidate();
                System.Windows.Forms.Application.DoEvents();
            }
        }
    }

    /// <summary>   Reads control text. </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <param name="control">   The variable control. </param>
    /// <returns>   The control text. </returns>
    public static string SafeTextGetter( Control control )
    {
        if ( control.InvokeRequired )
        {
#pragma warning disable IDE0004 // Remove Unnecessary Cast
            return ( string ) control.Invoke( new Func<string>( () => SafeTextGetter( control ) ) );
#pragma warning restore IDE0004 // Remove Unnecessary Cast
        }
        else
        {
            string varText = control.Text;
            return varText;
        }
    }

    /// <summary>   Writes a control text. </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <param name="control">   The variable control. </param>
    /// <param name="value">        The message. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public static string SafeTextSetter( Control control, string value )
    {
        if ( control is not null )
        {
            if ( control.InvokeRequired )
            {
#pragma warning disable IDE0004 // Remove Unnecessary Cast
                return ( string ) control.Invoke( new Func<string>( () => SafeTextSetter( control, value ) ) );
#pragma warning restore IDE0004 // Remove Unnecessary Cast
            }
            else
            {
                control.Text = value;
                control.Invalidate();
                System.Windows.Forms.Application.DoEvents();
                return control.Text;
            }
        }
        return value;
    }

    /// <summary>
    /// Sets the <see cref="Control">control</see> text to the
    /// <paramref name="value">value</paramref>.
    /// This setter is thread safe.
    /// </summary>
    /// <remarks> The value is set to empty if null or empty. </remarks>
    /// <param name="control"> The control. </param>
    /// <param name="value">   The value. </param>
    /// <returns> value. </returns>
    private static void SafeTextWriter( Control control, string value )
    {
        if ( control is not null )
        {
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                value = string.Empty;
            }

            if ( control.InvokeRequired )
            {
                _ = control.BeginInvoke( new Action<Control, string>( SafeTextWriter ), [control, value] );
            }
            else
            {
                control.Text = value;
                control.Invalidate();
                System.Windows.Forms.Application.DoEvents();
            }
        }
    }

    /// <summary> Safe top most setter. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="form">  The form. </param>
    /// <param name="value"> true to value. </param>
    private static void SafeTopMostSetter( Form form, bool value )
    {
        if ( form is not null )
        {
            if ( form.InvokeRequired )
            {
                _ = form.BeginInvoke( new Action<Form, bool>( SafeTopMostSetter ), [form, value] );
            }
            else
            {
                form.TopMost = value;
                System.Windows.Forms.Application.DoEvents();
            }
        }
    }

    #endregion
}
