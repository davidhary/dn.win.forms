using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using cc.isr.ViewModels.Splash;

namespace cc.isr.WinForms.Metro.Blue;

/// <summary>   Blue splash View. </summary>
/// <remarks>   2023-05-25. </remarks>
public partial class BlueSplashView : Forms.FormBase
{
    #region " construction "

    /// <summary>   Default constructor. </summary>
    /// <remarks>   2023-05-25. </remarks>
    public BlueSplashView()
    {
        this.Shown += this.Form_Shown;
        this.InitializeComponent();
#if NET5_0_OR_GREATER
        this.DataContextChanged += this.HandleDataContextChanged;
#endif
        this.SplashViewModel = new SplashViewModel( this );
        this.DebugBinding();
    }

    private void DebugBinding()
    {
        Binding binding = new( nameof( System.Windows.Forms.Label.Text ), this.splashViewModelBindingSource,
            nameof( cc.isr.ViewModels.Splash.SplashViewModel.SmallApplicationCaption ), true,
            System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged );

        this._smallApplicationCaptionLabel.DataBindings.Add( binding );

        binding = new( nameof( System.Windows.Forms.Label.Text ), this.splashViewModelBindingSource,
            nameof( cc.isr.ViewModels.Splash.SplashViewModel.LargeApplicationCaption ), true,
            System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged );

        this._largeApplicationCaptionLabel.DataBindings.Add( binding );
#if true
        binding = new( nameof( System.Windows.Forms.Label.Text ), this.splashViewModelBindingSource,
            nameof( cc.isr.ViewModels.Splash.SplashViewModel.Message ), true,
            System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged );
        binding.BindingComplete += this.HandleBindingComplete;
        this._currentTaskLabel.DataBindings.Add( binding );
#endif

        binding = new( nameof( System.Windows.Forms.Form.TopMost ), this.splashViewModelBindingSource,
            nameof( cc.isr.ViewModels.Splash.SplashViewModel.TopMost ), true,
            System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged );

        this.DataBindings.Add( binding );
    }

    private void HandleBindingComplete( object? sender, BindingCompleteEventArgs e )
    {
        if ( e.Exception is Exception ex )
            _ = MessageBox.Show( ex.ToString() );
    }

    private void HandleDataContextChanged( object? sender, EventArgs e )
    {
#if NET5_0_OR_GREATER
        this.splashViewModelBindingSource.DataSource = this.DataContext;
#endif
    }

    /// <summary>   Default constructor. </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="splashViewModel">  The application settings editor view model. </param>
    [CLSCompliant( false )]
    public BlueSplashView( SplashViewModel splashViewModel ) : this() => this.SplashViewModel = splashViewModel ?? throw new ArgumentNullException( nameof( splashViewModel ) );

    /// <summary>
    /// Disposes of the resources (other than memory) used by the
    /// <see cref="Form" />.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="disposing"> true to release both managed and unmanaged resources; false to
    /// release only unmanaged resources. </param>
    protected override void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                if ( this.SplashViewModel is not null )
                    this.SplashViewModel.SplashDisplay = null;
                this.components?.Dispose();
                this.components = null;
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }

    #endregion

    #region " view model "

    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0032:Use auto property", Justification = "<Pending>" )]
    private SplashViewModel? _splashViewModel;

    /// <summary>   Gets or sets the application settings editor view model. </summary>
    /// <value> The application settings editor view model. </value>
    [CLSCompliant( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public SplashViewModel? SplashViewModel
    {
        get => this._splashViewModel;
        set
        {
            if ( value is null && this._splashViewModel is not null )
            {
                this._splashViewModel.PropertyChanged -= this.HandlePropertyChanged;
                this._splashViewModel.SplashDisplay = null;
            }
            this._splashViewModel = value;
#if NET5_0_OR_GREATER
            this.DataContext = value;
#endif
            if ( this._splashViewModel is not null )
            {
                this._splashViewModel.PropertyChanged += this.HandlePropertyChanged;
                this._splashViewModel.SplashDisplay = this;
                this._splashViewModel.Width = this.Width;
                this._splashViewModel.Height = this.Height;
                this._splashViewModel.TopMost = this.TopMost;
            }
        }
    }

    /// <summary>   Handles the property changed. </summary>
    /// <remarks>   2023-05-02. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Property changed event information. </param>
    private void HandlePropertyChanged( object? sender, PropertyChangedEventArgs e )
    {
        if ( sender is SplashViewModel splashViewModel )
        {
            if ( this.InvokeRequired )
                _ = this.BeginInvoke( new Action<object, PropertyChangedEventArgs>( this.HandlePropertyChanged ), [sender, e] );
            else
            {
                if ( string.IsNullOrEmpty( splashViewModel.LargeApplicationCaption )
                      && (string.Equals( e?.PropertyName, nameof( this.SplashViewModel.Width ), StringComparison.Ordinal )
                          || string.Equals( e?.PropertyName, nameof( this.SplashViewModel.LargeApplicationCaption ), StringComparison.Ordinal )) )
                {
                    Font font = SelectFont( this._largeApplicationCaptionLabel.Font, 36, this.Width, splashViewModel.LargeApplicationCaption! );
                    this._largeApplicationCaptionLabel.Font = font;
                    this._largeApplicationCaptionLabel.Refresh();
                }
            }
        }
    }

    #endregion

    #region " form event handlers "
    private async void Form_Shown( object? sender, EventArgs e )
    {
        System.Windows.Forms.Application.DoEvents();
        try
        {
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            if ( this.SplashViewModel?.DisplayMessageCommand?.CanExecute( null ) ?? false )
                await this.SplashViewModel.DisplayMessageCommand.ExecuteAsync( "Starting..." );
        }
        catch ( Exception ex )
        {
            _ = System.Windows.Forms.MessageBox.Show( ex.ToString(), "Exception Occurred", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Exclamation, System.Windows.Forms.MessageBoxDefaultButton.Button1, System.Windows.Forms.MessageBoxOptions.DefaultDesktopOnly );
        }
        finally
        {
            this.Cursor = System.Windows.Forms.Cursors.Default;
        }
    }

    #endregion

    #region " form controls event handlers "

    /// <summary> Close Application. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void Close_Click( object? sender, EventArgs e )
    {
        this.IsCloseRequested = true;
    }

    /// <summary> Minimize label effects. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void Minimize_Click( object? sender, EventArgs e )
    {
        this.IsMinimizeRequested = true;
    }

    /// <summary> Closes label Mouse hover and leave effects </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void Close_MouseHover( object? sender, EventArgs e )
    {
        this._closeLabel.ForeColor = Color.Silver;
    }

    /// <summary> Closes mouse leave. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void Close_MouseLeave( object? sender, EventArgs e )
    {
        this._closeLabel.ForeColor = Color.White;
    }

    /// <summary> Minimize mouse hover. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void Minimize_MouseHover( object? sender, EventArgs e )
    {
        this._minimizeLabel.ForeColor = Color.Silver;
    }

    /// <summary> Minimize mouse leave. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void Minimize_MouseLeave( object? sender, EventArgs e )
    {
        this._minimizeLabel.ForeColor = Color.White;
    }

    #endregion

    #region " properties and methods "

    /// <summary> Gets a value indicating whether this object is minimize requested. </summary>
    /// <value> <c>true</c> if this object is minimize requested; otherwise <c>false</c> </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public bool IsMinimizeRequested { get; private set; }

    /// <summary> Gets a value indicating whether this object is close requested. </summary>
    /// <value> <c>true</c> if this object is close requested; otherwise <c>false</c> </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public bool IsCloseRequested { get; private set; }

    #endregion

    #region " test size measurement "
    private static SizeF MeasureText( Font font, string text )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( font, nameof( font ) );
#else
        if ( font is null )
        {
            throw new ArgumentNullException( nameof( font ) );
        }
#endif

        using Control ctrl = new();
        using Graphics graphics = ctrl.CreateGraphics();
        return graphics.MeasureString( text, font );
    }

    /// <summary>   Select font. </summary>
    /// <remarks>   David, 2021-03-02. </remarks>
    /// <param name="largestSize">  Size of the largest. </param>
    /// <param name="width">        The width. </param>
    /// <param name="font">         The font. </param>
    /// <param name="text">         The text. </param>
    /// <returns>   A Font. </returns>
    private static Font SelectFont( Font font, float largestSize, float width, string text )
    {
        float increment = 1;
        float size = largestSize + increment;
        do
        {
            size -= increment;
            font = new Font( font.FontFamily, size, font.Style );
        }
        while ( MeasureText( font, text ).Width >= width );

        // this is required to fit the font inside.
        font = new Font( font.FontFamily, ( float ) (size - 0.5), font.Style );

        return font;
    }

    #endregion
}
