using System;
using System.Windows.Forms;

namespace cc.isr.WinForms.Metro.Forms;

/// <summary> Form with drop shadow. </summary>
/// <remarks>
/// (c) 2007 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2007-08-13 1.0.2781 from Nicholas Seward  </para><para>
/// http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx.   </para><para>
/// David, 2007-08-13, 1.0.2781 Convert from C#. </para>
/// </remarks>
public partial class FormBase : Form
{
    /// <summary> Gets the initializing components sentinel. </summary>
    /// <value> The initializing components sentinel. </value>
    protected bool InitializingComponents { get; set; }

    /// <summary> Specialized default constructor for use only by derived classes. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    protected FormBase() : base()
    {
        this.InitializingComponents = true;
        this.InitializeComponent();
        this.InitializingComponents = false;
    }

    /// <summary> Initializes the component. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private void InitializeComponent()
    {
        this.SuspendLayout();
        // 
        // FormBase
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF( 7F, 17F );
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.BackColor = System.Drawing.SystemColors.Control;
        this.ClientSize = new System.Drawing.Size( 331, 341 );
        this.Cursor = System.Windows.Forms.Cursors.Default;
        this.Font = new System.Drawing.Font( "Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point );
        this.Icon = Properties.Resources.favicon;
        this.Name = "FormBase";
        this.ResumeLayout( false );

    }

    #region " class style "

#pragma warning disable IDE1006
    /// <summary> The enable drop shadow version. </summary>
    public const int EnableDropShadowVersion = 5;
#pragma warning restore IDE1006

    /// <summary> Gets the class style. </summary>
    /// <value> The class style. </value>
    protected ClassStyleConstants ClassStyle { get; set; } = ClassStyleConstants.None;

    /// <summary> Adds a drop shadow parameter. </summary>
    /// <remarks>
    /// From <see href="http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx">Code Project</see>.
    /// </remarks>
    /// <value> Options that control the create. </value>
    protected override CreateParams CreateParams
    {
        get
        {
            CreateParams cp = base.CreateParams;
            cp.ClassStyle |= ( int ) this.ClassStyle;
            return cp;
        }
    }

    #endregion
}
/// <summary> Values that represent class style constants. </summary>
/// <remarks> David, 2020-09-24. </remarks>
[Flags]
public enum ClassStyleConstants
{
    /// <summary> . </summary>
    [System.ComponentModel.Description( "Not Specified" )]
    None = 0,

    /// <summary> . </summary>
    [System.ComponentModel.Description( "No close button" )]
    HideCloseButton = 0x200,

    /// <summary> . </summary>
    [System.ComponentModel.Description( "Drop Shadow" )]
    DropShadow = 0x20000 // 131072
}
