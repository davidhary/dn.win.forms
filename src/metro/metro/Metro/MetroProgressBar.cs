using System.Windows.Forms;

namespace cc.isr.WinForms.Metro.Metro;

/// <summary> Metro progress bar. </summary>
/// <remarks>
/// (c) 2014 Magyar Andras, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2012-05-08 from Magyar ANDRÁS   </para><para>
/// http://www.codeproject.com/Articles/804316/Office-Style-Splash-Screen. </para>
/// </remarks>
public partial class MetroProgressBar : UserControl
{
    /// <summary> Default constructor. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public MetroProgressBar() => this.InitializeComponent();

    /// <summary>
    /// Releases the unmanaged resources used by the cc.isr.WinForms.MetroProgressBar and
    /// optionally releases the managed resources.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="disposing"> true to release both managed and unmanaged resources; false to
    /// release only unmanaged resources. </param>
    protected override void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this.components?.Dispose();
                this.components = null;
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }
}
