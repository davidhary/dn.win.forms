namespace cc.isr.WinForms.Metro.Metro;

public partial class MetroProgressBar
{
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    #region "component designer generated code"

    /// <summary>
    /// Required method for Designer support - do not modify the contents of this method with the
    /// code editor.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private void InitializeComponent()
    {
        var resources = new System.ComponentModel.ComponentResourceManager(typeof(MetroProgressBar));
        _pictureBox = new System.Windows.Forms.PictureBox();
        ((System.ComponentModel.ISupportInitialize)_pictureBox).BeginInit();
        SuspendLayout();
        // 
        // _pictureBox
        // 
        _pictureBox.BackColor = System.Drawing.Color.Transparent;
        _pictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
        _pictureBox.Image = (System.Drawing.Image)resources.GetObject("_pictureBox.Image");
        _pictureBox.Location = new System.Drawing.Point(0, 0);
        _pictureBox.Name = "_PictureBox";
        _pictureBox.Size = new System.Drawing.Size(308, 20);
        _pictureBox.TabIndex = 0;
        _pictureBox.TabStop = false;
        // 
        // MetroProgressBar
        // 
        AutoScaleDimensions = new System.Drawing.SizeF(6.0f, 13.0f);
        AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        BackColor = System.Drawing.Color.Transparent;
        Controls.Add(_pictureBox);
        Name = "MetroProgressBar";
        Size = new System.Drawing.Size(308, 20);
        ((System.ComponentModel.ISupportInitialize)_pictureBox).EndInit();
        ResumeLayout(false);
    }

    #endregion

    /// <summary> The picture box control. </summary>
    private System.Windows.Forms.PictureBox _pictureBox;
}
