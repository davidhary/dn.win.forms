namespace cc.isr.WinForms.Metro.Demo;

internal static class Program
{
    /// <summary>   The main entry point for the application. </summary>
    /// <remarks>
    /// https://StackOverflow.com/questions/19147/what-is-the-correct-way-to-create-a-single-instance-wpf-application
    /// 
    /// Having a named mutex allows us to stack synchronization across multiple threads and processes
    /// which is just the magic I'm looking for.
    /// 
    /// Mutex.WaitOne has an overload that specifies an amount of time for us to wait. Since we're
    /// not actually wanting to synchronizing our code (more just check if it is currently in use) we
    /// use the overload with two parameters: Mutex.WaitOne(Timespan timeout, bool exitContext). Wait
    /// one returns true if it is able to enter, and false if it wasn't. In this case, we don't want
    /// to wait at all; If our mutex is being used, skip it, and move on, so we pass in TimeSpan.Zero
    /// (wait 0 milliseconds), and set the exitContext to true so we can exit the synchronization
    /// context before we try to acquire a lock on it.
    /// 
    /// So, if our app is running, WaitOne will return false, otherwise, I opted to utilize a little
    /// Win32 to notify my running instance that someone forgot that it was already running( by
    /// bringing itself to the top of all the other windows). To achieve this I used PostMessage to
    /// broadcast a custom message to every window( the custom message was registered with
    /// RegisterWindowMessage by my running application, which means only my application knows what
    /// it is) then my second instance exits.The running application instance would receive that
    /// notification and process it.In order to do that, I overrode WndProc in my main form and
    /// listened for my custom notification.When I received that notification I set the form's
    /// TopMost property to true to bring it up on top.
    /// </remarks>
    [STAThread]
    private static void Main()
    {
        if ( _mutex.WaitOne( TimeSpan.Zero, true ) )
        {
            // Create the MyApplicationContext, that derives from ApplicationContext,
            // that manages when the application should exit.
            MyApplicationContext context = new();

            // Run the application with the specific context. It will exit when
            // all forms are closed.
            Application.Run( context );

            _mutex.ReleaseMutex();
        }
        else
        {
            // send our Win32 message to make the currently running instance
            // jump on top of all the other windows
            _ = NativeMethods.ShowCurrentAppInstance();
        }
    }

    /// <summary>   The mutex. </summary>
    private static readonly Mutex _mutex = new( true, "{A49D47E2-2637-40E2-A94C-45ACD7FEEE7F}" );
}
