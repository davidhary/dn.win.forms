namespace cc.isr.WinForms.Metro.Demo.ExceptionExtensions;
/// <summary>
/// Exception methods for adding exception data and building a detailed exception message.
/// </summary>
public static partial class ExceptionDataMethods
{
    /// <summary>
    /// Adds the <paramref name="exception"/> data to <paramref name="value"/> exception.
    /// </summary>
    /// <param name="value">        The value. </param>
    /// <param name="exception">    The exception. </param>
    /// <returns>
    /// <c>true</c> if it <see cref="Exception"/> is not nothing; otherwise <c>false</c>
    /// </returns>
    private static bool AddExceptionData( Exception value, AbandonedMutexException? exception )
    {
        if ( value is not null && exception is not null )
        {
            value.Data.Add( $"{value.Data.Count}-MutexIndex", exception.MutexIndex );
        }
        return exception is not null;
    }

    /// <summary> Adds exception data from the specified exception. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="exception"> The exception. </param>
    /// <returns> <c>true</c> if exception was added; otherwise <c>false</c> </returns>
    public static bool AddExceptionData( this Exception exception )
    {
        return cc.isr.WinForms.Metro.ExceptionExtensions.ExceptionDataMethods.AddExceptionData( exception ) ||
               AddExceptionData( exception, exception as AbandonedMutexException );
    }

    /// <summary>   An Exception extension method that builds a message. </summary>
    /// <remarks>   David, 2021-08-12. </remarks>
    /// <param name="exception">    The exception. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public static string BuildMessage( this Exception exception )
    {
        return cc.isr.WinForms.Metro.ExceptionExtensions.ExceptionDataMethods.BuildMessage( exception );
    }
}
