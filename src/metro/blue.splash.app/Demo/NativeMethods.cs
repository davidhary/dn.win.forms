using System.Runtime.InteropServices;
namespace cc.isr.WinForms.Metro.Demo;
/// <summary>
/// This class just wraps some Win32 stuff that is used for implementing a single instance
/// application.
/// </summary>
/// <remarks>   2023-05-24. </remarks>
internal sealed partial class NativeMethods
{
    /// <summary>   (Immutable) the broadcast. </summary>
    public const int HWND_BROADCAST = 0xffff;

    /// <summary>   (Immutable) the windows message show-me. </summary>
    public static readonly uint WM_SHOWME = RegisterWindowMessageA( "WM_SHOWME" );

    /// <summary>   Posts a message. </summary>
    /// <remarks>   2023-05-24. </remarks>
    /// <param name="handle">   The handle. </param>
    /// <param name="msg">      The message. </param>
    /// <param name="addInfo1"> The first add information. </param>
    /// <param name="addInfo2"> The second add information. </param>
    /// <returns>   True if it succeeds; otherwise, false. </returns>
#if NET5_0_OR_GREATER
    [LibraryImport( "user32" )]
    [return: MarshalAs( UnmanagedType.Bool )]
    public static partial bool PostMessage( IntPtr handle, uint msg, IntPtr addInfo1, IntPtr addInfo2 );
#else
    [return: MarshalAs( UnmanagedType.Bool )]
    [DllImport( "user32.dll", SetLastError = true, CharSet = CharSet.Auto )]
    public static extern bool PostMessage( IntPtr handle, uint msg, IntPtr addInfo1, IntPtr addInfo2 );
#endif

    /// <summary>   Registers the window message described by message. </summary>
    /// <remarks>   2023-05-24. </remarks>
    /// <param name="message">  The message. </param>
    /// <returns>   An int. </returns>
#if NET5_0_OR_GREATER
    [LibraryImport( "user32.dll", SetLastError = true, StringMarshalling = StringMarshalling.Utf16 )]
    public static partial uint RegisterWindowMessageA( string message );
#else
    [DllImport( "user32.dll", SetLastError = true, CharSet = CharSet.Auto )]
    public static extern uint RegisterWindowMessageA( string message );
#endif

    /// <summary>   Static constructor. </summary>
    /// <remarks>   2023-05-23. </remarks>
    /// <returns>   True if it succeeds; otherwise, false. </returns>
    public static bool ShowCurrentAppInstance()
    {
        // send our Win32 message to make the currently running instance
        // jump on top of all the other windows
        return NativeMethods.PostMessage( new IntPtr( NativeMethods.HWND_BROADCAST ), NativeMethods.WM_SHOWME, IntPtr.Zero, IntPtr.Zero );
    }
}

