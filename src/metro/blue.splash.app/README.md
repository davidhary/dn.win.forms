# Windows Forms Blue Splash Application Example

This Windows Forms example application uses .NET 5.0 with JSon settings and 
Serilog Logging using .NET 5.0 Application Context design.

## Logging and Tracing
Logging is defined by the application setting JSon file. Trace listening,
which can be used to display logged commands, is enabled.

## Settings
Settings with saving capabilities is supported using JSon.

## Single Instance
Upon starting a second instance, the application notifies the existing instance and exists.

