namespace cc.isr.WinForms.Demo;

/// <summary>   A dashboard. </summary>
/// <remarks>   David, 2021-03-16. 
/// Created/modified in 2011 by Simon Baer
/// Licensed under the Code Project Open License (CPOL).
/// </remarks>
public partial class Dashboard : Form
{
    /// <summary>
    /// Initializes a new instance of the <see cref="Form" /> class.
    /// </summary>
    /// <remarks> David, 2021-03-12. </remarks>
    public Dashboard() => this.InitializeComponent();

    /// <summary> Shows the button click. </summary>
    /// <remarks> David, 2021-03-12. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void ShowButton_Click( object? sender, EventArgs e )
    {
        this._popupNotifier.TitleText = this._notificationTitleTextBox.Text;
        this._popupNotifier.ContentText = this._notificationTextTextBox.Text;
        this._popupNotifier.ShowCloseButton = this._showNotificationCloseButtonCheckBox.Checked;
        this._popupNotifier.ShowOptionsButton = this._showNotificationOptionMenuCheckBox.Checked;
        this._popupNotifier.ShowGrip = this._showNotificationGripCheckBox.Checked;
        this._popupNotifier.Delay = int.Parse( this._notificationDelayTextBox.Text, System.Globalization.CultureInfo.CurrentCulture );
        this._popupNotifier.AnimationInterval = int.Parse( this._notificationAnimationIntervalTextBox.Text, System.Globalization.CultureInfo.CurrentCulture );
        this._popupNotifier.AnimationDuration = int.Parse( this._animationDurationTextBox.Text, System.Globalization.CultureInfo.CurrentCulture );
        this._popupNotifier.TitlePadding = new Padding( int.Parse( this._notificationTitlePaddingTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) );
        this._popupNotifier.ContentPadding = new Padding( int.Parse( this._notificationContentPaddingTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) );
        this._popupNotifier.ImagePadding = new Padding( int.Parse( this._notificationIconPaddingTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) );
        this._popupNotifier.Scroll = this._notificationScrollCheckBox.Checked;
        this._popupNotifier.Image = this._showNotificationIconCheckBox.Checked ? Properties.Resources._157_GetPermission_48x48_72 : null;
        this._popupNotifier.Popup();
    }

    /// <summary> Exit button click. </summary>
    /// <remarks> David, 2021-03-12. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void ExitButton_Click( object? sender, EventArgs e )
    {
        this.Close();
        Application.Exit();
    }
}
