using System;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace cc.isr.WinForms.Demo;

[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses")]
public partial class Dashboard
{
    /// <summary>
/// Required designer variable.
/// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
/// Clean up any resources being used.
/// </summary>
/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if ( disposing )
        {
            components?.Dispose();
        }

        base.Dispose(disposing);
    }

    /// <summary>
    /// Required method for Designer support - do not modify the contents of this method with the
    /// code editor.
    /// </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    private void InitializeComponent()
    {
        components = new System.ComponentModel.Container();
        _exitButton = new Button();
        _exitButton.Click += new EventHandler(ExitButton_Click);
        _showButton = new Button();
        _showButton.Click += new EventHandler(ShowButton_Click);
        _notificationTitleTextBoxLabel = new Label();
        _notificationTextTextBoxLabel = new Label();
        _notificationTitleTextBox = new TextBox();
        _notificationTextTextBox = new TextBox();
        _showNotificationIconCheckBox = new CheckBox();
        _showNotificationCloseButtonCheckBox = new CheckBox();
        _showNotificationOptionMenuCheckBox = new CheckBox();
        _notificationDelayTextBoxLabel = new Label();
        _notificationAnimationIntervalTextBoxLabel = new Label();
        _notificationDelayTextBox = new TextBox();
        _notificationAnimationIntervalTextBox = new TextBox();
        _showNotificationGripCheckBox = new CheckBox();
        _notificationTitlePaddingTextBoxLabel = new Label();
        _notificationIconPaddingTextBox = new TextBox();
        _notificationScrollCheckBox = new CheckBox();
        _notificationContentPaddingTextBox = new TextBox();
        _notificationIconPaddingTextBoxLabel = new Label();
        _notificationTitlePaddingTextBox = new TextBox();
        _notificationContentPaddingTextBoxLabel = new Label();
        _contextMenuStrip = new ContextMenuStrip(components);
        _aboutToolStripMenuItem = new ToolStripMenuItem();
        _settingsToolStripMenuItem = new ToolStripMenuItem();
        _exitToolStripMenuItem = new ToolStripMenuItem();
        _animationDurationTextBoxLabel = new Label();
        _animationDurationTextBox = new TextBox();
        _popupNotifier = new cc.isr.WinForms.PopupNotifier();
        _contextMenuStrip.SuspendLayout();
        SuspendLayout();
        // 
        // _exitButton
        // 
        _exitButton.Location = new Point(425, 12);
        _exitButton.Name = "_ExitButton";
        _exitButton.Size = new Size(75, 23);
        _exitButton.TabIndex = 0;
        _exitButton.Text = "Exit";
        _exitButton.UseVisualStyleBackColor = true;
        // Me._exitButton.Click += New System.EventHandler(Me.ExitButton_Click)
        // 
        // _showButton
        // 
        _showButton.Location = new Point(12, 202);
        _showButton.Name = "_ShowButton";
        _showButton.Size = new Size(75, 23);
        _showButton.TabIndex = 1;
        _showButton.Text = "Show!";
        _showButton.UseVisualStyleBackColor = true;
        // Me._showButton.Click += New System.EventHandler(Me._showButton_Click)
        // 
        // _notificationTitleTextBoxLabel
        // 
        _notificationTitleTextBoxLabel.AutoSize = true;
        _notificationTitleTextBoxLabel.Location = new Point(12, 17);
        _notificationTitleTextBoxLabel.Name = "_NotificationTitleTextBoxLabel";
        _notificationTitleTextBoxLabel.Size = new Size(30, 13);
        _notificationTitleTextBoxLabel.TabIndex = 2;
        _notificationTitleTextBoxLabel.Text = "Title:";
        // 
        // _notificationTextTextBoxLabel
        // 
        _notificationTextTextBoxLabel.AutoSize = true;
        _notificationTextTextBoxLabel.Location = new Point(12, 43);
        _notificationTextTextBoxLabel.Name = "_NotificationTextTextBoxLabel";
        _notificationTextTextBoxLabel.Size = new Size(31, 13);
        _notificationTextTextBoxLabel.TabIndex = 3;
        _notificationTextTextBoxLabel.Text = "Text:";
        // 
        // _notificationTitleTextBox
        // 
        _notificationTitleTextBox.Location = new Point(48, 14);
        _notificationTitleTextBox.Name = "_NotificationTitleTextBox";
        _notificationTitleTextBox.Size = new Size(372, 20);
        _notificationTitleTextBox.TabIndex = 4;
        _notificationTitleTextBox.Text = "Notification Title";
        // 
        // _notificationTextTextBox
        // 
        _notificationTextTextBox.Location = new Point(49, 40);
        _notificationTextTextBox.Name = "_NotificationTextTextBox";
        _notificationTextTextBox.Size = new Size(371, 20);
        _notificationTextTextBox.TabIndex = 5;
        _notificationTextTextBox.Text = "This is the notification text!";
        // 
        // _showNotificationIconCheckBox
        // 
        _showNotificationIconCheckBox.AutoSize = true;
        _showNotificationIconCheckBox.Checked = true;
        _showNotificationIconCheckBox.CheckState = CheckState.Checked;
        _showNotificationIconCheckBox.Location = new Point(15, 75);
        _showNotificationIconCheckBox.Name = "_ShowNotificationIconCheckBox";
        _showNotificationIconCheckBox.Size = new Size(76, 17);
        _showNotificationIconCheckBox.TabIndex = 6;
        _showNotificationIconCheckBox.Text = "Show icon";
        _showNotificationIconCheckBox.UseVisualStyleBackColor = true;
        // 
        // _showNotificationCloseButtonCheckBox
        // 
        _showNotificationCloseButtonCheckBox.AutoSize = true;
        _showNotificationCloseButtonCheckBox.Checked = true;
        _showNotificationCloseButtonCheckBox.CheckState = CheckState.Checked;
        _showNotificationCloseButtonCheckBox.Location = new Point(15, 98);
        _showNotificationCloseButtonCheckBox.Name = "_ShowNotificationCloseButtonCheckBox";
        _showNotificationCloseButtonCheckBox.Size = new Size(114, 17);
        _showNotificationCloseButtonCheckBox.TabIndex = 7;
        _showNotificationCloseButtonCheckBox.Text = "Show close button";
        _showNotificationCloseButtonCheckBox.UseVisualStyleBackColor = true;
        // 
        // _showNotificationOptionMenuCheckBox
        // 
        _showNotificationOptionMenuCheckBox.AutoSize = true;
        _showNotificationOptionMenuCheckBox.Checked = true;
        _showNotificationOptionMenuCheckBox.CheckState = CheckState.Checked;
        _showNotificationOptionMenuCheckBox.Location = new Point(15, 121);
        _showNotificationOptionMenuCheckBox.Name = "_ShowNotificationOptionMenuCheckBox";
        _showNotificationOptionMenuCheckBox.Size = new Size(114, 17);
        _showNotificationOptionMenuCheckBox.TabIndex = 8;
        _showNotificationOptionMenuCheckBox.Text = "Show option menu";
        _showNotificationOptionMenuCheckBox.UseVisualStyleBackColor = true;
        // 
        // _notificationDelayTextBoxLabel
        // 
        _notificationDelayTextBoxLabel.AutoSize = true;
        _notificationDelayTextBoxLabel.Location = new Point(199, 76);
        _notificationDelayTextBoxLabel.Name = "_NotificationDelayTextBoxLabel";
        _notificationDelayTextBoxLabel.Size = new Size(59, 13);
        _notificationDelayTextBoxLabel.TabIndex = 9;
        _notificationDelayTextBoxLabel.Text = "Delay [ms]:";
        // 
        // _notificationAnimationIntervalTextBoxLabel
        // 
        _notificationAnimationIntervalTextBoxLabel.AutoSize = true;
        _notificationAnimationIntervalTextBoxLabel.Location = new Point(199, 99);
        _notificationAnimationIntervalTextBoxLabel.Name = "_NotificationAnimationIntervalTextBoxLabel";
        _notificationAnimationIntervalTextBoxLabel.Size = new Size(115, 13);
        _notificationAnimationIntervalTextBoxLabel.TabIndex = 10;
        _notificationAnimationIntervalTextBoxLabel.Text = "Animation interval [ms]:";
        // 
        // _notificationDelayTextBox
        // 
        _notificationDelayTextBox.Location = new Point(320, 70);
        _notificationDelayTextBox.Name = "_NotificationDelayTextBox";
        _notificationDelayTextBox.Size = new Size(100, 20);
        _notificationDelayTextBox.TabIndex = 11;
        _notificationDelayTextBox.Text = "3000";
        // 
        // _notificationAnimationIntervalTextBox
        // 
        _notificationAnimationIntervalTextBox.Location = new Point(320, 96);
        _notificationAnimationIntervalTextBox.Name = "_NotificationAnimationIntervalTextBox";
        _notificationAnimationIntervalTextBox.Size = new Size(100, 20);
        _notificationAnimationIntervalTextBox.TabIndex = 12;
        _notificationAnimationIntervalTextBox.Text = "10";
        // 
        // _showNotificationGripCheckBox
        // 
        _showNotificationGripCheckBox.AutoSize = true;
        _showNotificationGripCheckBox.Checked = true;
        _showNotificationGripCheckBox.CheckState = CheckState.Checked;
        _showNotificationGripCheckBox.Location = new Point(15, 144);
        _showNotificationGripCheckBox.Name = "_ShowNotificationGripCheckBox";
        _showNotificationGripCheckBox.Size = new Size(73, 17);
        _showNotificationGripCheckBox.TabIndex = 13;
        _showNotificationGripCheckBox.Text = "Show grip";
        _showNotificationGripCheckBox.UseVisualStyleBackColor = true;
        // 
        // _notificationTitlePaddingTextBoxLabel
        // 
        _notificationTitlePaddingTextBoxLabel.AutoSize = true;
        _notificationTitlePaddingTextBoxLabel.Location = new Point(199, 151);
        _notificationTitlePaddingTextBoxLabel.Name = "_NotificationTitlePaddingTextBoxLabel";
        _notificationTitlePaddingTextBoxLabel.Size = new Size(91, 13);
        _notificationTitlePaddingTextBoxLabel.TabIndex = 14;
        _notificationTitlePaddingTextBoxLabel.Text = "Title padding [pixels]:";
        // 
        // _notificationIconPaddingTextBox
        // 
        _notificationIconPaddingTextBox.Location = new Point(320, 200);
        _notificationIconPaddingTextBox.Name = "_NotificationIconPaddingTextBox";
        _notificationIconPaddingTextBox.Size = new Size(100, 20);
        _notificationIconPaddingTextBox.TabIndex = 15;
        _notificationIconPaddingTextBox.Text = "0";
        // 
        // _notificationScrollCheckBox
        // 
        _notificationScrollCheckBox.AutoSize = true;
        _notificationScrollCheckBox.Checked = true;
        _notificationScrollCheckBox.CheckState = CheckState.Checked;
        _notificationScrollCheckBox.Location = new Point(15, 167);
        _notificationScrollCheckBox.Name = "_NotificationScrollCheckBox";
        _notificationScrollCheckBox.Size = new Size(83, 17);
        _notificationScrollCheckBox.TabIndex = 16;
        _notificationScrollCheckBox.Text = "Scroll in/out";
        _notificationScrollCheckBox.UseVisualStyleBackColor = true;
        // 
        // _notificationContentPaddingTextBox
        // 
        _notificationContentPaddingTextBox.Location = new Point(320, 174);
        _notificationContentPaddingTextBox.Name = "_NotificationContentPaddingTextBox";
        _notificationContentPaddingTextBox.Size = new Size(100, 20);
        _notificationContentPaddingTextBox.TabIndex = 17;
        _notificationContentPaddingTextBox.Text = "0";
        // 
        // _notificationIconPaddingTextBoxLabel
        // 
        _notificationIconPaddingTextBoxLabel.AutoSize = true;
        _notificationIconPaddingTextBoxLabel.Location = new Point(199, 203);
        _notificationIconPaddingTextBoxLabel.Name = "_NotificationIconPaddingTextBoxLabel";
        _notificationIconPaddingTextBoxLabel.Size = new Size(92, 13);
        _notificationIconPaddingTextBoxLabel.TabIndex = 18;
        _notificationIconPaddingTextBoxLabel.Text = "Icon padding [pixels]:";
        // 
        // _notificationTitlePaddingTextBox
        // 
        _notificationTitlePaddingTextBox.Location = new Point(320, 148);
        _notificationTitlePaddingTextBox.Name = "_NotificationTitlePaddingTextBox";
        _notificationTitlePaddingTextBox.Size = new Size(100, 20);
        _notificationTitlePaddingTextBox.TabIndex = 19;
        _notificationTitlePaddingTextBox.Text = "0";
        // 
        // _notificationContentPaddingTextBoxLabel
        // 
        _notificationContentPaddingTextBoxLabel.AutoSize = true;
        _notificationContentPaddingTextBoxLabel.Location = new Point(199, 177);
        _notificationContentPaddingTextBoxLabel.Name = "_NotificationContentPaddingTextBoxLabel";
        _notificationContentPaddingTextBoxLabel.Size = new Size(108, 13);
        _notificationContentPaddingTextBoxLabel.TabIndex = 20;
        _notificationContentPaddingTextBoxLabel.Text = "Content padding [pixels]:";
        // 
        // _contextMenuStrip
        // 
        _contextMenuStrip.Items.AddRange(new ToolStripItem[] { _aboutToolStripMenuItem, _settingsToolStripMenuItem, _exitToolStripMenuItem });
        _contextMenuStrip.Name = "_ContextMenuStrip";
        _contextMenuStrip.Size = new Size(126, 70);
        // 
        // _aboutToolStripMenuItem
        // 
        _aboutToolStripMenuItem.Name = "_aboutToolStripMenuItem";
        _aboutToolStripMenuItem.Size = new Size(125, 22);
        _aboutToolStripMenuItem.Text = "About...";
        // 
        // _settingsToolStripMenuItem
        // 
        _settingsToolStripMenuItem.Name = "_SettingsToolStripMenuItem";
        _settingsToolStripMenuItem.Size = new Size(125, 22);
        _settingsToolStripMenuItem.Text = "Settings...";
        // 
        // _exitToolStripMenuItem
        // 
        _exitToolStripMenuItem.Name = "_ExitToolStripMenuItem";
        _exitToolStripMenuItem.Size = new Size(125, 22);
        _exitToolStripMenuItem.Text = "Exit";
        // 
        // _animationDurationTextBoxLabel
        // 
        _animationDurationTextBoxLabel.AutoSize = true;
        _animationDurationTextBoxLabel.Location = new Point(199, 125);
        _animationDurationTextBoxLabel.Name = "_animationDurationTextBoxLabel";
        _animationDurationTextBoxLabel.Size = new Size(118, 13);
        _animationDurationTextBoxLabel.TabIndex = 21;
        _animationDurationTextBoxLabel.Text = "Animation Duration [ms]:";
        // 
        // _animationDurationTextBox
        // 
        _animationDurationTextBox.Location = new Point(320, 122);
        _animationDurationTextBox.Name = "_animationDurationTextBox";
        _animationDurationTextBox.Size = new Size(100, 20);
        _animationDurationTextBox.TabIndex = 22;
        _animationDurationTextBox.Text = "1000";
        // 
        // _popupNotifier
        // 
        _popupNotifier.BodyColor = Color.FromArgb(128, 128, 255);
        _popupNotifier.ContentFont = new Font("Tahoma", 8.0f);
        _popupNotifier.ContentText = null;
        _popupNotifier.GradientPower = 300;
        _popupNotifier.HeaderHeight = 20;
        _popupNotifier.Image = null;
        _popupNotifier.OptionsMenu = _contextMenuStrip;
        _popupNotifier.Size = new Size(400, 100);
        _popupNotifier.TitleFont = new Font("Segoe UI", 9.0f);
        _popupNotifier.TitleText = null;
        // 
        // Form1
        // 
        AutoScaleDimensions = new SizeF(6.0f, 13.0f);
        AutoScaleMode = AutoScaleMode.Font;
        ClientSize = new Size(512, 237);
        Controls.Add(_notificationTitlePaddingTextBox);
        Controls.Add(_notificationContentPaddingTextBoxLabel);
        Controls.Add(_animationDurationTextBox);
        Controls.Add(_animationDurationTextBoxLabel);
        Controls.Add(_notificationScrollCheckBox);
        Controls.Add(_notificationContentPaddingTextBox);
        Controls.Add(_notificationIconPaddingTextBox);
        Controls.Add(_notificationIconPaddingTextBoxLabel);
        Controls.Add(_showNotificationGripCheckBox);
        Controls.Add(_notificationTitlePaddingTextBoxLabel);
        Controls.Add(_notificationDelayTextBox);
        Controls.Add(_notificationAnimationIntervalTextBoxLabel);
        Controls.Add(_notificationAnimationIntervalTextBox);
        Controls.Add(_notificationDelayTextBoxLabel);
        Controls.Add(_showNotificationOptionMenuCheckBox);
        Controls.Add(_showNotificationCloseButtonCheckBox);
        Controls.Add(_showNotificationIconCheckBox);
        Controls.Add(_notificationTextTextBox);
        Controls.Add(_notificationTitleTextBox);
        Controls.Add(_notificationTextTextBoxLabel);
        Controls.Add(_notificationTitleTextBoxLabel);
        Controls.Add(_showButton);
        Controls.Add(_exitButton);
        FormBorderStyle = FormBorderStyle.FixedDialog;
        MaximizeBox = false;
        MinimizeBox = false;
        Name = "Form1";
        Text = "Notification Window Demo";
        _contextMenuStrip.ResumeLayout(false);
        ResumeLayout(false);
        PerformLayout();
    }

    private Button _exitButton;
    private Button _showButton;
    private Label _notificationTitleTextBoxLabel;
    private Label _notificationTextTextBoxLabel;
    private TextBox _notificationTitleTextBox;
    private TextBox _notificationTextTextBox;
    private CheckBox _showNotificationIconCheckBox;
    private CheckBox _showNotificationCloseButtonCheckBox;
    private CheckBox _showNotificationOptionMenuCheckBox;
    private Label _notificationDelayTextBoxLabel;
    private Label _notificationAnimationIntervalTextBoxLabel;
    private TextBox _notificationDelayTextBox;
    private TextBox _notificationAnimationIntervalTextBox;
    private CheckBox _showNotificationGripCheckBox;
    private Label _notificationTitlePaddingTextBoxLabel;
    private TextBox _notificationIconPaddingTextBox;
    private CheckBox _notificationScrollCheckBox;
    private TextBox _notificationContentPaddingTextBox;
    private Label _notificationIconPaddingTextBoxLabel;
    private TextBox _notificationTitlePaddingTextBox;
    private Label _notificationContentPaddingTextBoxLabel;
    private ContextMenuStrip _contextMenuStrip;
    private ToolStripMenuItem _aboutToolStripMenuItem;
    private ToolStripMenuItem _settingsToolStripMenuItem;
    private ToolStripMenuItem _exitToolStripMenuItem;
    private Label _animationDurationTextBoxLabel;
    private TextBox _animationDurationTextBox;
    private cc.isr.WinForms.PopupNotifier _popupNotifier;
}
