# About

cc.isr.WinForms.PopupNotifier is a .Net library supporting a custom windows forms popup notifier.

# How to Use

```
TBD
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

cc.isr.WinForms.PopupNotifier is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Core Framework Repository].

[Core Framework Repository]: https://bitbucket.org/davidhary/dn.core

