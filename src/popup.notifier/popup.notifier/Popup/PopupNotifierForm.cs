using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.ComponentModel;

namespace cc.isr.WinForms;

/// <summary> This is the form of the actual notification window. </summary>
/// <remarks>
/// (c) 2011 Simon Baer.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2014-07-11 Created/modified in 2011 by Simon Baer. </para><para>
/// http://www.codeproject.com/KB/dialog/notificationwindow.aspx Based on the Code Project
/// article by Nicolas WÄLTI: http://www.codeproject.com/KB/cpp/PopupNotifier.aspx David,
/// 2014-07-11 Updated. </para>
/// </remarks>
internal class PopupNotifierForm : Form
{
    #region " construction and cleanup "

    /// <summary> Gets the initializing components sentinel. </summary>
    /// <value> The initializing components sentinel. </value>
    protected bool InitializingComponents { get; set; }

    /// <summary> Create a new instance. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="parent"> Popup Notifier. </param>
    public PopupNotifierForm( PopupNotifier parent ) : base()
    {
        this.InitializingComponents = true;
        this.InitializeComponent();
        this.InitializingComponents = false;

        this.Parent = parent;
        this.SetStyle( ControlStyles.OptimizedDoubleBuffer, true );
        this.SetStyle( ControlStyles.ResizeRedraw, true );
        this.SetStyle( ControlStyles.AllPaintingInWmPaint, true );
        this.ShowInTaskbar = false;
        VisibleChanged += this.PopupNotifierForm_VisibleChanged;
        MouseMove += this.PopupNotifierForm_MouseMove;
        MouseUp += this.PopupNotifierForm_MouseUp;
        Paint += this.PopupNotifierForm_Paint;
    }

    /// <summary> Used in design mode. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private void InitializeComponent()
    {
        this.SuspendLayout();
        this.ClientSize = new Size( 392, 66 );
        this.FormBorderStyle = FormBorderStyle.None;
        this.Name = "PopupNotifierForm";
        this.StartPosition = FormStartPosition.Manual;
        this.TopMost = true;
        this.ResumeLayout( false );
    }

    #endregion

    /// <summary> Event that is raised when the text is clicked. </summary>
    public event EventHandler<EventArgs>? LinkClick;

    /// <summary> Removes event handler. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The handler. </param>
    private void RemoveLinkClickEventHandler( EventHandler<EventArgs>? value )
    {
        foreach ( Delegate d in value is null ? ([]) : value.GetInvocationList() )
        {
            try
            {
                this.LinkClick -= ( EventHandler<EventArgs> ) d;
            }
            catch ( Exception ex )
            {
                Trace.TraceError( ex.ToString() );
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
        }
    }

    /// <summary> Event that is raised when the notification window is manually closed. </summary>
    public event EventHandler<EventArgs>? CloseClick;

    /// <summary> Removes event handler. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The handler. </param>
    private void RemoveCloseClickEventHandler( EventHandler<EventArgs>? value )
    {
        foreach ( Delegate d in value is null ? ([]) : value.GetInvocationList() )
        {
            try
            {
                CloseClick -= ( EventHandler<EventArgs> ) d;
            }
            catch ( Exception ex )
            {
                Trace.TraceError( ex.ToString() );
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
        }
    }

    /// <summary> Event queue for all listeners interested in ContextMenuOpened events. </summary>
    internal event EventHandler<EventArgs>? ContextMenuOpened;

    /// <summary> Event queue for all listeners interested in ContextMenuClosed events. </summary>
    internal event EventHandler<EventArgs>? ContextMenuClosed;

    /// <summary> true to mouse on close. </summary>
    private bool _mouseOnClose;

    /// <summary> true to mouse on link. </summary>
    private bool _mouseOnLink;

    /// <summary> true to mouse on options. </summary>
    private bool _mouseOnOptions;

    /// <summary> Height of the title. </summary>
    private int _heightOfTitle;

    #region " gdi objects "

    /// <summary> <c>true</c> if GDI initialized. </summary>
    private bool _gdiInitialized;

    /// <summary> The rectangle body. </summary>
    private Rectangle _rcBody;

    /// <summary> The rectangle header. </summary>
    private Rectangle _rcHeader;

    /// <summary> The rectangle form. </summary>
    private Rectangle _rcForm;

    /// <summary> The brush body. </summary>
    private LinearGradientBrush? _brushBody;

    /// <summary> The brush header. </summary>
    private LinearGradientBrush? _brushHeader;

    /// <summary> The brush button hover. </summary>
    private Brush? _brushButtonHover;

    /// <summary> The pen button border. </summary>
    private Pen? _penButtonBorder;

    /// <summary> The pen content. </summary>
    private Pen? _penContent;

    /// <summary> The pen border. </summary>
    private Pen? _penBorder;

    /// <summary> The brush foreground color. </summary>
    private Brush? _brushForeColor;

    /// <summary> The brush link hover. </summary>
    private Brush? _brushLinkHover;

    /// <summary> The brush content. </summary>
    private Brush? _brushContent;

    /// <summary> The brush title. </summary>
    private Brush? _brushTitle;

    #endregion

    /// <summary> The form is shown/hidden. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> . </param>
    /// <param name="e">      . </param>
    private void PopupNotifierForm_VisibleChanged( object? sender, EventArgs e )
    {
        if ( this.Visible )
        {
            this._mouseOnClose = false;
            this._mouseOnLink = false;
            this._mouseOnOptions = false;
        }
    }

    /// <summary> Gets the parent control. </summary>
    /// <value> The parent. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public new PopupNotifier Parent { get; set; }

    /// <summary> Add two values but do not return a value greater than 255. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="input"> first value. </param>
    /// <param name="add">   value to add. </param>
    /// <returns> sum of both values. </returns>
    private static int AddValueMax255( int input, int add )
    {
        return input + add < 256 ? input + add : 255;
    }

    /// <summary> Subtract two values but do not returns a value below 0. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="input"> first value. </param>
    /// <param name="ded">   value to subtract. </param>
    /// <returns> first value minus second value. </returns>
    private static int DedValueMin0( int input, int ded )
    {
        return input - ded > 0 ? input - ded : 0;
    }

    /// <summary> Returns a color which is darker than the given color. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="color"> Color. </param>
    /// <returns> darker color. </returns>
    private Color GetDarkerColor( Color color )
    {
        return Color.FromArgb( 255, DedValueMin0( color.R, this.Parent.GradientPower ), DedValueMin0( color.G, this.Parent.GradientPower ), DedValueMin0( color.B, this.Parent.GradientPower ) );
    }

    /// <summary> Returns a color which is lighter than the given color. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="color"> Color. </param>
    /// <returns> lighter color. </returns>
    private Color GetLighterColor( Color color )
    {
        return Color.FromArgb( 255, AddValueMax255( color.R, this.Parent.GradientPower ), AddValueMax255( color.G, this.Parent.GradientPower ), AddValueMax255( color.B, this.Parent.GradientPower ) );
    }

    /// <summary> Gets the rectangle of the content text. </summary>
    /// <value> The rectangle content text. </value>
    private RectangleF RectContentText => this.Parent.Image is not null ? new RectangleF( this.Parent.ImagePadding.Left + this.Parent.ImageSize.Width + this.Parent.ImagePadding.Right + this.Parent.ContentPadding.Left, this.Parent.HeaderHeight + this.Parent.TitlePadding.Top + this._heightOfTitle + this.Parent.TitlePadding.Bottom + this.Parent.ContentPadding.Top, this.Width - this.Parent.ImagePadding.Left - this.Parent.ImageSize.Width - this.Parent.ImagePadding.Right - this.Parent.ContentPadding.Left - this.Parent.ContentPadding.Right - 16 - 5, this.Height - this.Parent.HeaderHeight - this.Parent.TitlePadding.Top - this._heightOfTitle - this.Parent.TitlePadding.Bottom - this.Parent.ContentPadding.Top - this.Parent.ContentPadding.Bottom - 1 ) : new RectangleF( this.Parent.ContentPadding.Left, this.Parent.HeaderHeight + this.Parent.TitlePadding.Top + this._heightOfTitle + this.Parent.TitlePadding.Bottom + this.Parent.ContentPadding.Top, this.Width - this.Parent.ContentPadding.Left - this.Parent.ContentPadding.Right - 16 - 5, this.Height - this.Parent.HeaderHeight - this.Parent.TitlePadding.Top - this._heightOfTitle - this.Parent.TitlePadding.Bottom - this.Parent.ContentPadding.Top - this.Parent.ContentPadding.Bottom - 1 );

    /// <summary> gets the rectangle of the close button. </summary>
    /// <value> The rectangle close. </value>
    private Rectangle RectClose => new( this.Width - 5 - 16, this.Parent.HeaderHeight + 3, 16, 16 );

    /// <summary> Gets the rectangle of the options button. </summary>
    /// <value> Options that control the rectangle. </value>
    private Rectangle RectOptions => new( this.Width - 5 - 16, this.Parent.HeaderHeight + 3 + 16 + 5, 16, 16 );

    /// <summary>
    /// Update form to display hover styles when the mouse moves over the notification form.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> . </param>
    /// <param name="e">      . </param>
    private void PopupNotifierForm_MouseMove( object? sender, MouseEventArgs e )
    {
        if ( this.Parent.ShowCloseButton )
        {
            this._mouseOnClose = this.RectClose.Contains( e.X, e.Y );
        }

        if ( this.Parent.ShowOptionsButton )
        {
            this._mouseOnOptions = this.RectOptions.Contains( e.X, e.Y );
        }

        this._mouseOnLink = this.RectContentText.Contains( e.X, e.Y );
        this.Invalidate();
    }

    /// <summary> A mouse button has been released, check if something has been clicked. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> . </param>
    /// <param name="e">      . </param>
    private void PopupNotifierForm_MouseUp( object? sender, MouseEventArgs e )
    {
        if ( e.Button == MouseButtons.Left )
        {
            if ( this.RectClose.Contains( e.X, e.Y ) )
            {
                this.CloseClick?.Invoke( this, EventArgs.Empty );
            }

            if ( this.RectContentText.Contains( e.X, e.Y ) )
            {
                this.LinkClick?.Invoke( this, EventArgs.Empty );
            }

            if ( this.RectOptions.Contains( e.X, e.Y ) && this.Parent.OptionsMenu is not null )
            {
                ContextMenuOpened?.Invoke( this, EventArgs.Empty );
                this.Parent.OptionsMenu.Show( new Point( this.RectOptions.Right - this.Parent.OptionsMenu.Width, this.RectOptions.Bottom ) );
                this.Parent.OptionsMenu.Closed += this.OptionsMenu_Closed;
            }
        }
    }

    /// <summary> The options popup menu has been closed. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> . </param>
    /// <param name="e">      Tool strip drop down closed event information. </param>
    private void OptionsMenu_Closed( object? sender, ToolStripDropDownClosedEventArgs e )
    {
        this.Parent!.OptionsMenu!.Closed -= this.OptionsMenu_Closed;
        this.ContextMenuClosed?.Invoke( this, EventArgs.Empty );
    }

    /// <summary> Create all GDI objects used to paint the form. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private void AllocateGDIObjects()
    {
        this._rcBody = new Rectangle( 0, 0, this.Width, this.Height );
        this._rcHeader = new Rectangle( 0, 0, this.Width, this.Parent.HeaderHeight );
        this._rcForm = new Rectangle( 0, 0, this.Width - 1, this.Height - 1 );
        this._brushBody = new LinearGradientBrush( this._rcBody, this.Parent.BodyColor, this.GetLighterColor( this.Parent.BodyColor ), LinearGradientMode.Vertical );
        this._brushHeader = new LinearGradientBrush( this._rcHeader, this.Parent.HeaderColor, this.GetDarkerColor( this.Parent.HeaderColor ), LinearGradientMode.Vertical );
        this._brushButtonHover = new SolidBrush( this.Parent.ButtonHoverColor );
        this._penButtonBorder = new Pen( this.Parent.ButtonBorderColor );
        this._penContent = new Pen( this.Parent.ContentColor, 2f );
        this._penBorder = new Pen( this.Parent.BorderColor );
        this._brushForeColor = new SolidBrush( this.ForeColor );
        this._brushLinkHover = new SolidBrush( this.Parent.ContentHoverColor );
        this._brushContent = new SolidBrush( this.Parent.ContentColor );
        this._brushTitle = new SolidBrush( this.Parent.TitleColor );
        this._gdiInitialized = true;
    }

    /// <summary> Free all GDI objects. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private void DisposeGDIObjects()
    {
        if ( this._gdiInitialized )
        {
            this._brushBody?.Dispose();
            this._brushHeader?.Dispose();
            this._brushButtonHover?.Dispose();
            this._penButtonBorder?.Dispose();
            this._penContent?.Dispose();
            this._penBorder?.Dispose();
            this._brushForeColor?.Dispose();
            this._brushLinkHover?.Dispose();
            this._brushContent?.Dispose();
            this._brushTitle?.Dispose();
        }
    }

    /// <summary>   Fixes. </summary>
    /// <remarks>   David, 2021-03-08. </remarks>
    /// <param name="number">   Number of. </param>
    /// <returns>   An int. </returns>
    private static int Fix( double number )
    {
        return Math.Sign( number ) * ( int ) Math.Abs( number );
    }

    /// <summary> Draw the notification form. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> . </param>
    /// <param name="e">      . </param>
    private void PopupNotifierForm_Paint( object? sender, PaintEventArgs e )
    {
        if ( !this._gdiInitialized )
            this.AllocateGDIObjects();

        // draw window
        e.Graphics.FillRectangle( this._brushBody!, this._rcBody );
        e.Graphics.FillRectangle( this._brushHeader!, this._rcHeader );
        e.Graphics.DrawRectangle( this._penBorder!, this._rcForm );
        if ( this.Parent.ShowGrip )
        {
            e.Graphics.DrawImage( Properties.Resources.Grip, (this.Width - Properties.Resources.Grip.Width) / 2, PopupNotifierForm.Fix( (this.Parent.HeaderHeight - 3) / 2d ) );
        }

        if ( this.Parent.ShowCloseButton )
        {
            if ( this._mouseOnClose )
            {
                e.Graphics.FillRectangle( this._brushButtonHover!, this.RectClose );
                e.Graphics.DrawRectangle( this._penButtonBorder!, this.RectClose );
            }

            e.Graphics.DrawLine( this._penContent!, this.RectClose.Left + 4, this.RectClose.Top + 4, this.RectClose.Right - 4, this.RectClose.Bottom - 4 );
            e.Graphics.DrawLine( this._penContent!, this.RectClose.Left + 4, this.RectClose.Bottom - 4, this.RectClose.Right - 4, this.RectClose.Top + 4 );
        }

        if ( this.Parent.ShowOptionsButton )
        {
            if ( this._mouseOnOptions )
            {
                e.Graphics.FillRectangle( this._brushButtonHover!, this.RectOptions );
                e.Graphics.DrawRectangle( this._penButtonBorder!, this.RectOptions );
            }

            e.Graphics.FillPolygon( this._brushForeColor!, [ new( this.RectOptions.Left + 4, this.RectOptions.Top + 6 ),
                new( this.RectOptions.Left + 12, this.RectOptions.Top + 6 ),
                new( this.RectOptions.Left + 8, this.RectOptions.Top + 4 + 6 ) ] );
        }

        // draw icon
        if ( this.Parent.Image is not null )
        {
            e.Graphics.DrawImage( this.Parent.Image, this.Parent.ImagePadding.Left, this.Parent.HeaderHeight + this.Parent.ImagePadding.Top, this.Parent.ImageSize.Width, this.Parent.ImageSize.Height );
        }

        // calculate height of title
        this._heightOfTitle = ( int ) e.Graphics.MeasureString( "A", this.Parent.TitleFont ).Height;
        int titleX = this.Parent.TitlePadding.Left;
        if ( this.Parent.Image is not null )
        {
            titleX += this.Parent.ImagePadding.Left + this.Parent.ImageSize.Width + this.Parent.ImagePadding.Right;
        }

        // draw title
        e.Graphics.DrawString( this.Parent.TitleText, this.Parent.TitleFont, this._brushTitle!, titleX, this.Parent.HeaderHeight + this.Parent.TitlePadding.Top );

        // draw content text, optionally with a bold part
        this.Cursor = this._mouseOnLink ? Cursors.Hand : Cursors.Default;
        Brush brushText = this._mouseOnLink ? this._brushLinkHover! : this._brushContent!;
        e.Graphics.DrawString( this.Parent.ContentText, this.Parent.ContentFont, brushText, this.RectContentText );
    }

    /// <summary> Dispose GDI objects. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="disposing"> . </param>
    protected override void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this.DisposeGDIObjects();
                this.RemoveCloseClickEventHandler( this.CloseClick );
                this.RemoveLinkClickEventHandler( this.LinkClick );
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }
}

