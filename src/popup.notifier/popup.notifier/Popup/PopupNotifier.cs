using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace cc.isr.WinForms;
/// <summary>
/// Non-visual component to show a notification window in the right lower corner of the screen.
/// </summary>
/// <remarks>
/// (c) 2011 Simon Baer.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2014-07-11 Created/modified in 2011 by Simon Baer.  </para><para>
/// <see href="http://www.codeproject.com/KB/dialog/notificationwindow.aspx"/> Based on the Code Project
/// article by Nicolas WÄLTI: <see href="http://www.codeproject.com/KB/cpp/PopupNotifier.aspx"/>  </para><para>
/// David, 2014-07-11 Updated. </para>
/// </remarks>
[ToolboxBitmap( typeof( PopupNotifier ), "Icon.ico" )]
[DefaultEvent( "Click" )]
public class PopupNotifier : Component
{
    /// <summary> Event that is raised when the text in the notification window is clicked. </summary>
    public event EventHandler<EventArgs>? Click;

    /// <summary> Removes event handler. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The handler. </param>
    private void RemoveClickEventHandler( EventHandler<EventArgs>? value )
    {
        foreach ( Delegate d in value is null ? ([]) : value.GetInvocationList() )
        {
            try
            {
                Click -= ( EventHandler<EventArgs> ) d;
            }
            catch ( Exception ex )
            {
                Trace.TraceError( ex.ToString() );
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
        }
    }

    /// <summary> Event that is raised when the notification window is manually closed. </summary>
    public event EventHandler<EventArgs>? Close;

    /// <summary> Removes event handler. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The handler. </param>
    private void RemoveCloseEventHandler( EventHandler<EventArgs>? value )
    {
        foreach ( Delegate d in value is null ? ([]) : value.GetInvocationList() )
        {
            try
            {
                Close -= ( EventHandler<EventArgs> ) d;
            }
            catch ( Exception ex )
            {
                Trace.TraceError( ex.ToString() );
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
        }
    }

    /// <summary> <c>true</c> if this object is disposed. </summary>
    private bool _isDisposed;

    /// <summary> The popup form. </summary>
    private readonly PopupNotifierForm _popupForm;

    /// <summary> The animation timer. </summary>
    private readonly Timer _animationTimer;

    /// <summary> The wait timer. </summary>
    private readonly Timer _waitTimer;

    /// <summary> <c>true</c> if this object is appearing. </summary>
    private bool _isAppearing = true;

    /// <summary> <c>true</c> if mouse is on. </summary>
    private bool _mouseIsOn;

    /// <summary> The maximum position. </summary>
    private int _maxPosition;

    /// <summary> The maximum opacity. </summary>
    private double _maxOpacity;

    /// <summary> The position start. </summary>
    private int _posStart;

    /// <summary> The position stop. </summary>
    private int _posStop;

    /// <summary> The opacity start. </summary>
    private double _opacityStart;

    /// <summary> The opacity stop. </summary>
    private double _opacityStop;

    /// <summary> The software. </summary>
    private Stopwatch? _stopWatch;

    #region " properties "

    /// <summary> Color of the window header. </summary>
    /// <value> The color of the header. </value>
    [Category( "Header" )]
    [DefaultValue( typeof( Color ), "ControlDark" )]
    [Description( "Color of the window header." )]
    public Color HeaderColor { get; set; }

    /// <summary> Color of the window background. </summary>
    /// <value> The color of the body. </value>
    [Category( "Appearance" )]
    [DefaultValue( typeof( Color ), "Control" )]
    [Description( "Color of the window background." )]
    public Color BodyColor { get; set; }

    /// <summary> Color of the title text. </summary>
    /// <value> The color of the title. </value>
    [Category( "Title" )]
    [DefaultValue( typeof( Color ), "Gray" )]
    [Description( "Color of the title text." )]
    public Color TitleColor { get; set; }

    /// <summary> Color of the content text. </summary>
    /// <value> The color of the content. </value>
    [Category( "Content" )]
    [DefaultValue( typeof( Color ), "ControlText" )]
    [Description( "Color of the content text." )]
    public Color ContentColor { get; set; }

    /// <summary> Color of the window border. </summary>
    /// <value> The color of the border. </value>
    [Category( "Appearance" )]
    [DefaultValue( typeof( Color ), "WindowFrame" )]
    [Description( "Color of the window border." )]
    public Color BorderColor { get; set; }

    /// <summary> Border color of the close and options buttons when the mouse is over them. </summary>
    /// <value> The color of the button border. </value>
    [Category( "Buttons" )]
    [DefaultValue( typeof( Color ), "WindowFrame" )]
    [Description( "Border color of the close and options buttons when the mouse is over them." )]
    public Color ButtonBorderColor { get; set; }

    /// <summary>
    /// Background color of the close and options buttons when the mouse is over them.
    /// </summary>
    /// <value> The color of the button hover. </value>
    [Category( "Buttons" )]
    [DefaultValue( typeof( Color ), "Highlight" )]
    [Description( "Background color of the close and options buttons when the mouse is over them." )]
    public Color ButtonHoverColor { get; set; }

    /// <summary> Color of the content text when the mouse is hovering over it. </summary>
    /// <value> The color of the content hover. </value>
    [Category( "Content" )]
    [DefaultValue( typeof( Color ), "HotTrack" )]
    [Description( "Color of the content text when the mouse is hovering over it." )]
    public Color ContentHoverColor { get; set; }

    /// <summary> Gradient of window background color. </summary>
    /// <value> The gradient power. </value>
    [Category( "Appearance" )]
    [DefaultValue( 50 )]
    [Description( "Gradient of window background color." )]
    public int GradientPower { get; set; }

    /// <summary> Font of the content text. </summary>
    /// <value> The content font. </value>
    [Category( "Content" )]
    [Description( "Font of the content text." )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Font ContentFont { get; set; }

    /// <summary> Font of the title. </summary>
    /// <value> The title font. </value>
    [Category( "Title" )]
    [Description( "Font of the title." )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Font TitleFont { get; set; }

    /// <summary> Size of the image. </summary>
    private Size _imageSize = new( 0, 0 );

    /// <summary> Gets or sets the size of the image. </summary>
    /// <value> The size of the image. </value>
    [Category( "Image" )]
    [Description( "Size of the icon image." )]
    public Size ImageSize
    {
        get => this._imageSize.Width == 0 ? this.Image is not null ? this.Image.Size : new Size( 0, 0 ) : this._imageSize;
        set => this._imageSize = value;
    }

    /// <summary> Resets the image size. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public void ResetImageSize()
    {
        this._imageSize = Size.Empty;
    }

    /// <summary> Determine if we should serialize image size. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    private bool ShouldSerializeImageSize()
    {
        return !this._imageSize.Equals( Size.Empty );
    }

    /// <summary> Icon image to display. </summary>
    /// <value> The image. </value>
    [Category( "Image" )]
    [Description( "Icon image to display." )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Image? Image { get; set; }

    /// <summary> Whether to show a 'grip' image within the window header. </summary>
    /// <value> The show grip. </value>
    [Category( "Header" )]
    [DefaultValue( true )]
    [Description( "Whether to show a 'grip' image within the window header." )]
    public bool ShowGrip { get; set; }

    /// <summary> Whether to scroll the window or only fade it. </summary>
    /// <value> The scroll. </value>
    [Category( "Behavior" )]
    [DefaultValue( true )]
    [Description( "Whether to scroll the window or only fade it." )]
    public bool Scroll { get; set; }

    /// <summary> Content text to display. </summary>
    /// <value> The content text. </value>
    [Category( "Content" )]
    [Description( "Content text to display." )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string? ContentText { get; set; }

    /// <summary> Title text to display. </summary>
    /// <value> The title text. </value>
    [Category( "Title" )]
    [Description( "Title text to display." )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string? TitleText { get; set; }

    /// <summary> Padding of title text. </summary>
    /// <value> The title padding. </value>
    [Category( "Title" )]
    [Description( "Padding of title text." )]
    public Padding TitlePadding { get; set; }

    /// <summary> Resets the title padding. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public void ResetTitlePadding()
    {
        this.TitlePadding = Padding.Empty;
    }

    /// <summary> Determine if we should serialize title padding. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    private bool ShouldSerializeTitlePadding()
    {
        return !this.TitlePadding.Equals( Padding.Empty );
    }

    /// <summary> Padding of content text. </summary>
    /// <value> The content padding. </value>
    [Category( "Content" )]
    [Description( "Padding of content text." )]
    public Padding ContentPadding { get; set; }

    /// <summary> Resets the content padding. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public void ResetContentPadding()
    {
        this.ContentPadding = Padding.Empty;
    }

    /// <summary> Determine if we should serialize content padding. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    private bool ShouldSerializeContentPadding()
    {
        return !this.ContentPadding.Equals( Padding.Empty );
    }

    /// <summary> Padding of icon image. </summary>
    /// <value> The image padding. </value>
    [Category( "Image" )]
    [Description( "Padding of icon image." )]
    public Padding ImagePadding { get; set; }

    /// <summary> Resets the image padding. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public void ResetImagePadding()
    {
        this.ImagePadding = Padding.Empty;
    }

    /// <summary> Determine if we should serialize image padding. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    private bool ShouldSerializeImagePadding()
    {
        return !this.ImagePadding.Equals( Padding.Empty );
    }

    /// <summary> Height of window header. </summary>
    /// <value> The height of the header. </value>
    [Category( "Header" )]
    [DefaultValue( 9 )]
    [Description( "Height of window header." )]
    public int HeaderHeight { get; set; }

    /// <summary> Whether to show the close button. </summary>
    /// <value> The show close button. </value>
    [Category( "Buttons" )]
    [DefaultValue( true )]
    [Description( "Whether to show the close button." )]
    public bool ShowCloseButton { get; set; }

    /// <summary> Whether to show the options button. </summary>
    /// <value> The show options button. </value>
    [Category( "Buttons" )]
    [DefaultValue( false )]
    [Description( "Whether to show the options button." )]
    public bool ShowOptionsButton { get; set; }

    /// <summary> Context menu to open when clicking on the options button. </summary>
    /// <value> The options menu. </value>
    [Category( "Behavior" )]
    [Description( "Context menu to open when clicking on the options button." )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public ContextMenuStrip? OptionsMenu { get; set; }

    /// <summary> Time in milliseconds the window is displayed. </summary>
    /// <value> The delay. </value>
    [Category( "Behavior" )]
    [DefaultValue( 3000 )]
    [Description( "Time in milliseconds the window is displayed." )]
    public int Delay { get; set; }

    /// <summary> Time in milliseconds needed to make the window appear or disappear. </summary>
    /// <value> The animation duration. </value>
    [Category( "Behavior" )]
    [DefaultValue( 1000 )]
    [Description( "Time in milliseconds needed to make the window appear or disappear." )]
    public int AnimationDuration { get; set; }

    /// <summary> Interval in milliseconds used to draw the animation. </summary>
    /// <value> The animation interval. </value>
    [Category( "Behavior" )]
    [DefaultValue( 10 )]
    [Description( "Interval in milliseconds used to draw the animation." )]
    public int AnimationInterval { get; set; }

    /// <summary> Size of the window. </summary>
    /// <value> The size. </value>
    [Category( "Appearance" )]
    [Description( "Size of the window." )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Size Size { get; set; }

    #endregion

    /// <summary> Create a new instance of the popup component. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public PopupNotifier()
    {
        // set default values
        this.HeaderColor = SystemColors.ControlDark;
        this.BodyColor = SystemColors.Control;
        this.TitleColor = Color.Gray;
        this.ContentColor = SystemColors.ControlText;
        this.BorderColor = SystemColors.WindowFrame;
        this.ButtonBorderColor = SystemColors.WindowFrame;
        this.ButtonHoverColor = SystemColors.Highlight;
        this.ContentHoverColor = SystemColors.HotTrack;
        this.GradientPower = 50;
        this.ContentFont = SystemFonts.DialogFont!;
        this.TitleFont = SystemFonts.CaptionFont!;
        this.ShowGrip = true;
        this.Scroll = true;
        this.TitlePadding = new Padding( 0 );
        this.ContentPadding = new Padding( 0 );
        this.ImagePadding = new Padding( 0 );
        this.HeaderHeight = 9;
        this.ShowCloseButton = true;
        this.ShowOptionsButton = false;
        this.Delay = 3000;
        this.AnimationInterval = 10;
        this.AnimationDuration = 1000;
        this.Size = new Size( 400, 100 );
        this._popupForm = new PopupNotifierForm( this )
        {
            TopMost = true,
            FormBorderStyle = FormBorderStyle.None,
            StartPosition = FormStartPosition.Manual
        };
        this._popupForm.MouseEnter += this.PopupForm_MouseEnter;
        this._popupForm.MouseLeave += this.PopupForm_MouseLeave;
        this._popupForm.CloseClick += this.PopupForm_CloseClick;
        this._popupForm.LinkClick += this.PopupForm_LinkClick;
        this._popupForm.ContextMenuOpened += this.PopupForm_ContextMenuOpened;
        this._popupForm.ContextMenuClosed += this.PopupForm_ContextMenuClosed;
        this._animationTimer = new Timer();
        this._animationTimer.Tick += this.AnimationTimer_Tick;
        this._waitTimer = new Timer();
        this._waitTimer.Tick += this.WaitTimer_Tick;
    }

    /// <summary>
    /// Show the notification window if it is not already visible. If the window is currently
    /// disappearing, it is shown again.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public void Popup()
    {
        if ( !this._isDisposed )
        {
            if ( !this._popupForm.Visible && Screen.PrimaryScreen is not null )
            {
                this._popupForm.Size = this.Size;
                if ( this.Scroll )
                {
                    this._posStart = Screen.PrimaryScreen.WorkingArea.Bottom;
                    this._posStop = Screen.PrimaryScreen.WorkingArea.Bottom - this._popupForm.Height;
                }
                else
                {
                    this._posStart = Screen.PrimaryScreen.WorkingArea.Bottom - this._popupForm.Height;
                    this._posStop = Screen.PrimaryScreen.WorkingArea.Bottom - this._popupForm.Height;
                }

                this._opacityStart = 0d;
                this._opacityStop = 1d;
                this._popupForm.Opacity = this._opacityStart;
                this._popupForm.Location = new Point( Screen.PrimaryScreen.WorkingArea.Right - this._popupForm.Size.Width - 1, this._posStart );
                this._popupForm.Show();
                this._isAppearing = true;
                this._waitTimer.Interval = this.Delay;
                this._animationTimer.Interval = this.AnimationInterval;
                this._animationTimer.Start();
                this._stopWatch = Stopwatch.StartNew();
                Debug.WriteLine( "Animation started." );
            }
            else
            {
                if ( !this._isAppearing )
                {
                    this._popupForm.Top = this._maxPosition;
                    this._popupForm.Opacity = this._maxOpacity;
                    this._animationTimer.Stop();
                    Debug.WriteLine( "Animation stopped." );
                    this._waitTimer.Stop();
                    this._waitTimer.Start();
                    Debug.WriteLine( "Wait timer started." );
                }

                this._popupForm.Invalidate();
            }
        }
    }

    /// <summary> Hide the notification window. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public void Hide()
    {
        Debug.WriteLine( "Animation stopped." );
        Debug.WriteLine( "Wait timer stopped." );
        this._animationTimer.Stop();
        this._waitTimer.Stop();
        this._popupForm.Hide();
    }

    /// <summary>
    /// The custom options menu has been closed. Restart the timer for closing the notification
    /// window.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> . </param>
    /// <param name="e">      . </param>
    private void PopupForm_ContextMenuClosed( object? sender, EventArgs e )
    {
        Debug.WriteLine( "Menu closed." );
        if ( !this._mouseIsOn )
        {
            this._waitTimer.Interval = this.Delay;
            this._waitTimer.Start();
            Debug.WriteLine( "Wait timer started." );
        }
    }

    /// <summary>
    /// The custom options menu has been opened. The window must not be closed as long as the menu is
    /// open.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> . </param>
    /// <param name="e">      . </param>
    private void PopupForm_ContextMenuOpened( object? sender, EventArgs e )
    {
        Debug.WriteLine( "Menu opened." );
        this._waitTimer.Stop();
        Debug.WriteLine( "Wait timer stopped." );
    }

    /// <summary> The text has been clicked. Raise the 'Click' event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> . </param>
    /// <param name="e">      . </param>
    private void PopupForm_LinkClick( object? sender, EventArgs e )
    {
        this.Click?.Invoke( this, EventArgs.Empty );
    }

    /// <summary>
    /// The close button has been clicked. Hide the notification window and raise the 'Close' event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> . </param>
    /// <param name="e">      . </param>
    private void PopupForm_CloseClick( object? sender, EventArgs e )
    {
        this.Hide();
        this.Close?.Invoke( this, EventArgs.Empty );
    }

    /// <summary> Update form position and opacity to show/hide the window. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> . </param>
    /// <param name="e">      . </param>
    private void AnimationTimer_Tick( object? sender, EventArgs e )
    {
        long elapsed = this._stopWatch!.ElapsedMilliseconds;
        int posCurrent = ( int ) (this._posStart + ((this._posStop - this._posStart) * elapsed / this.AnimationDuration));
        bool neg = this._posStop - this._posStart < 0;
        if ( (neg && posCurrent < this._posStop) || (!neg && posCurrent > this._posStop) )
        {
            posCurrent = this._posStop;
        }

        double opacityCurrent = this._opacityStart + (( int ) ((this._opacityStop - this._opacityStart) * elapsed) / this.AnimationDuration);
        neg = this._opacityStop - this._opacityStart < 0d;
        if ( (neg && opacityCurrent < this._opacityStop) || (!neg && opacityCurrent > this._opacityStop) )
        {
            opacityCurrent = this._opacityStop;
        }

        this._popupForm.Top = posCurrent;
        this._popupForm.Opacity = opacityCurrent;

        // animation has ended
        if ( elapsed > this.AnimationDuration )
        {
            (this._posStop, this._posStart) = (this._posStart, this._posStop);
            (this._opacityStart, this._opacityStop) = (this._opacityStop, this._opacityStart);
            this._stopWatch.Reset();
            this._animationTimer.Stop();
            Debug.WriteLine( "Animation stopped." );
            if ( this._isAppearing )
            {
                this._isAppearing = false;
                this._maxPosition = this._popupForm.Top;
                this._maxOpacity = this._popupForm.Opacity;
                if ( !this._mouseIsOn )
                {
                    this._waitTimer.Stop();
                    this._waitTimer.Start();
                    Debug.WriteLine( "Wait timer started." );
                }
            }
            else
            {
                this._popupForm.Hide();
            }
        }
    }

    /// <summary> The wait timer has elapsed, start the animation to hide the window. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> . </param>
    /// <param name="e">      . </param>
    private void WaitTimer_Tick( object? sender, EventArgs e )
    {
        Debug.WriteLine( "Wait timer elapsed." );
        this._waitTimer.Stop();
        this._animationTimer.Interval = this.AnimationInterval;
        this._animationTimer.Start();
        this._stopWatch!.Restart();
        Debug.WriteLine( "Animation started." );
    }

    /// <summary> Start wait timer if the mouse leaves the form. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> . </param>
    /// <param name="e">      . </param>
    private void PopupForm_MouseLeave( object? sender, EventArgs e )
    {
        Debug.WriteLine( "MouseLeave" );
        if ( this._popupForm.Visible && (this.OptionsMenu is null || !this.OptionsMenu.Visible) )
        {
            this._waitTimer.Interval = this.Delay;
            this._waitTimer.Start();
            Debug.WriteLine( "Wait timer started." );
        }

        this._mouseIsOn = false;
    }

    /// <summary> Stop wait timer if the mouse enters the form. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> . </param>
    /// <param name="e">      . </param>
    private void PopupForm_MouseEnter( object? sender, EventArgs e )
    {
        Debug.WriteLine( "MouseEnter" );
        if ( !this._isAppearing )
        {
            this._popupForm.Top = this._maxPosition;
            this._popupForm.Opacity = this._maxOpacity;
            this._animationTimer.Stop();
            Debug.WriteLine( "Animation stopped." );
        }

        this._waitTimer.Stop();
        Debug.WriteLine( "Wait timer stopped." );
        this._mouseIsOn = true;
    }

    /// <summary> Dispose the notification form. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="disposing"> . </param>
    protected override void Dispose( bool disposing )
    {
        try
        {
            if ( !this._isDisposed && disposing )
            {
                this.RemoveClickEventHandler( this.Click );
                this.RemoveCloseEventHandler( this.Close );
                this._popupForm?.Dispose();
            }
        }
        finally
        {
            this._isDisposed = true;
            base.Dispose( disposing );
        }
    }
}

