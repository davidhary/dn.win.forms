using System;
using System.Drawing;
using System.Windows.Forms;

namespace cc.isr.WinForms.Dialogs;

/// <summary> Form with drop shadow. </summary>
/// <remarks>
/// (c) 2007 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2007-08-13 1.0.2781 from Nicholas Seward  </para><para>
/// http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx.   </para><para>
/// David, 2007-08-13, 1.0.2781 Convert from C#. </para>
/// </remarks>
public partial class FormBase : Form
{
    #region " construction and cleanup "

    /// <summary> Gets the initializing components sentinel. </summary>
    /// <value> The initializing components sentinel. </value>
    protected bool InitializingComponents { get; set; }

    /// <summary> Specialized default constructor for use only by derived classes. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    protected FormBase() : base()
    {
        this.InitializingComponents = true;
        this.InitializeComponent();
        this.InitializingComponents = false;
    }

    /// <summary> Initializes the component. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private void InitializeComponent()
    {
        this.SuspendLayout();
        this.AutoScaleDimensions = new SizeF( 7.0f, 17.0f );
        this.AutoScaleMode = AutoScaleMode.Font;
        this.BackColor = SystemColors.Control;
        this.ClientSize = new Size( 331, 341 );
        this.Cursor = Cursors.Default;
        this.Font = new Font( SystemFonts.DefaultFont.FontFamily, 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0 );
        this.Icon = Properties.Resources.favicon;
        this.Name = nameof( FormBase );
        this.ResumeLayout( false );
    }

    #region " class style "

    /// <summary> Gets the class style. </summary>
    /// <value> The class style. </value>
    protected ClassStyleConstants ClassStyle { get; set; } = ClassStyleConstants.None;

    /// <summary> Adds a drop shadow parameter. </summary>
    /// <remarks>
    /// From <see href="http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx">Code Project</see>.
    /// </remarks>
    /// <value> Options that control the create. </value>
    protected override CreateParams CreateParams
    {
        get
        {
            CreateParams cp = base.CreateParams;
            cp.ClassStyle |= ( int ) this.ClassStyle;
            return cp;
        }
    }

    #endregion

    #endregion
}
/// <summary> Values that represent class style constants. </summary>
/// <remarks> David, 2020-09-24. </remarks>
[Flags]
public enum ClassStyleConstants
{
    /// <summary> . </summary>
    [System.ComponentModel.Description( "Not Specified" )]
    None = 0,

    /// <summary> . </summary>
    [System.ComponentModel.Description( "No close button" )]
    HideCloseButton = 0x200,

    /// <summary> . </summary>
    [System.ComponentModel.Description( "Drop Shadow" )]
    DropShadow = 0x20000 // 131072
}

