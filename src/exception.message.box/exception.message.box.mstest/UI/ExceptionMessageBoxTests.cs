using cc.isr.WinForms.Dialogs;

namespace cc.isr.WinForms.ExceptionMessageBox.MSTest;

/// <summary> SQL Exception Message Box tests. </summary>
/// <remarks>
/// <para>
/// David, 2018-04-13 </para>
/// </remarks>
[TestClass]
public class ExceptionMessageBoxTests
{
    /// <summary> Shows the dialog abort ignore. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    /// <exception cref="DivideByZeroException"> Thrown when an attempt is made to divide a number by
    /// zero. </exception>
    public static void ShowDialogException()
    {
        try
        {
            throw new DivideByZeroException();
        }
        catch ( Exception ex )
        {
            MyDialogResult expected = MyDialogResult.Ok;
            MyDialogResult actual;
            ex.Data.Add( "@isr", "Testing exception message" );
            actual = MyMessageBox.ShowDialog( ex );
            Assert.AreEqual( expected, actual );
        }
    }

    /// <summary>
    /// Send the given keys to the active application and then wait for the message to be processed.
    /// </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="keys"> The keys. </param>
    public static void SendWait( string keys )
    {
        System.Windows.Forms.SendKeys.SendWait( keys );
    }

    /// <summary>   (Unit Test Method) exception message should show. </summary>
    /// <remarks>   David, 2020-09-18. </remarks>
    [TestMethod()]
    public void ExceptionMessageShouldShow()
    {
        Thread oThread = new( new ThreadStart( ShowDialogException ) );
        oThread.Start();
        // a long delay was required...
        System.Threading.Tasks.Task.Delay( 2000 ).Wait();
        SendWait( "{ENTER}" );
        // This tabbed into another application.  cc.isr.WinForms.SendWait("%{TAB}{ENTER}")
        oThread.Join();
    }

    /// <summary> Shows the dialog abort ignore. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    /// <exception cref="DivideByZeroException"> Thrown when an attempt is made to divide a number by
    /// zero. </exception>
    public static void ShowDialogAbortIgnore()
    {
        try
        {
            throw new DivideByZeroException();
        }
        catch ( DivideByZeroException ex )
        {
            ex.Data.Add( "@isr", "Testing exception message" );
            MyDialogResult result = MyMessageBox.ShowDialogAbortIgnore( ex, MyMessageBoxIcon.Error );
            Assert.IsTrue( result is MyDialogResult.Abort or MyDialogResult.Ignore, $"{result} expected {MyDialogResult.Abort} or {MyDialogResult.Ignore}" );
        }
    }

    /// <summary>   (Unit Test Method) exception message should abort or ignore. </summary>
    /// <remarks>   David, 2020-09-18. </remarks>
    [TestMethod()]
    public void ExceptionMessageShouldAbortOrIgnore()
    {
        Thread oThread = new( new ThreadStart( ShowDialogAbortIgnore ) );
        oThread.Start();
        // a long delay was required...
        System.Threading.Tasks.Task.Delay( 2000 ).Wait();
        SendWait( "{ENTER}" );
        // This tabbed into another application.  cc.isr.WinForms.SendWait("%{TAB}{ENTER}")
        oThread.Join();
    }
}
