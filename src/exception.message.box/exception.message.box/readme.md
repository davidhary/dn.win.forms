# About

cc.isr.WinForms.ExceptionMessageBox is a .Net library exposing the SQL exception message box.

# How to Use

```
TBD
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

cc.isr.WinForms.ExceptionMessageBox is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Core Framework Repository].

[Core Framework Repository]: https://bitbucket.org/davidhary/dn.core

