using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace cc.isr.WinForms;

public partial class CoolPrintPreviewDialog
{
    // Required by the Windows Form Designer
    private System.ComponentModel.IContainer components;

    // NOTE: The following procedure is required by the Windows Form Designer
    // It can be modified using the Windows Form Designer.  
    // Do not modify it using the code editor.
    [DebuggerStepThrough()]
    private void InitializeComponent()
    {
        var resources = new System.ComponentModel.ComponentResourceManager(typeof(CoolPrintPreviewDialog));
        _toolStrip = new ToolStrip();
        _printButton = new System.Windows.Forms.ToolStripButton();
        _printButton.Click += new EventHandler(PrintButton_Click);
        _pageSetupButton = new System.Windows.Forms.ToolStripButton();
        _pageSetupButton.Click += new EventHandler(PageSetupButton_Click);
        _separator2 = new ToolStripSeparator();
        _zoomButton = new System.Windows.Forms.ToolStripSplitButton();
        _zoomButton.ButtonClick += new EventHandler(ZoomButton_ButtonClick);
        _zoomButton.DropDownItemClicked += new ToolStripItemClickedEventHandler(ZoomButton_DropDownItemClicked);
        _actualSizeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _fullPageMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _pageWidthMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _twoPagesMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _zoomSeparatorMenuItem = new ToolStripSeparator();
        _zoom500MenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _zoom200MenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _zoom150MenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _zoom100MenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _zoom75MenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _zoom50MenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _zoom25MenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _zoom10MenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _firstButton = new System.Windows.Forms.ToolStripButton();
        _firstButton.Click += new EventHandler(FirstButton_Click);
        _previousButton = new System.Windows.Forms.ToolStripButton();
        _previousButton.Click += new EventHandler(PreviousButton_Click);
        _startPageTextBox = new System.Windows.Forms.ToolStripTextBox();
        _startPageTextBox.Enter += new EventHandler(StartPageTextBox_Enter);
        _startPageTextBox.KeyPress += new KeyPressEventHandler(StartPageTextBox_KeyPress);
        _startPageTextBox.Validating += new System.ComponentModel.CancelEventHandler(StartPageTextBox_Validating);
        _pageCountLabel = new System.Windows.Forms.ToolStripLabel();
        _nextButton = new System.Windows.Forms.ToolStripButton();
        _nextButton.Click += new EventHandler(NextButton_Click);
        _lastButton = new System.Windows.Forms.ToolStripButton();
        _lastButton.Click += new EventHandler(LastButton_Click);
        _separator1 = new ToolStripSeparator();
        _cancelButton = new System.Windows.Forms.ToolStripButton();
        _cancelButton.Click += new EventHandler(CancelButton_Click);
        _preview = new CoolPrintPreviewControl();
        _preview.PageCountChanged += new EventHandler<EventArgs>(Preview_PageCountChanged);
        _preview.StartPageChanged += new EventHandler<EventArgs>(Preview_StartPageChanged);
        _toolStrip.SuspendLayout();
        SuspendLayout();
        // 
        // _toolStrip
        // 
        _toolStrip.GripStyle = ToolStripGripStyle.Hidden;
        _toolStrip.Items.AddRange(new ToolStripItem[] { _printButton, _pageSetupButton, _separator2, _zoomButton, _firstButton, _previousButton, _startPageTextBox, _pageCountLabel, _nextButton, _lastButton, _separator1, _cancelButton });
        _toolStrip.Location = new Point(0, 0);
        _toolStrip.Name = "_toolStrip";
        _toolStrip.Size = new Size(532, 25);
        _toolStrip.TabIndex = 1;
        _toolStrip.Text = "toolStrip1";
        // 
        // _printButton
        // 
        _printButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
        _printButton.Image = (Image)resources.GetObject("PrinterImage");
        _printButton.ImageTransparentColor = Color.Magenta;
        _printButton.Name = "_PrintButton";
        _printButton.Size = new Size(23, 22);
        _printButton.Text = "Print Document";
        // 
        // _pageSetupButton
        // 
        _pageSetupButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
        _pageSetupButton.Image = (Image)resources.GetObject("PageSetupImage");
        _pageSetupButton.ImageTransparentColor = Color.Magenta;
        _pageSetupButton.Name = "_PageSetupButton";
        _pageSetupButton.Size = new Size(23, 22);
        _pageSetupButton.Text = "Page Setup";
        // 
        // _separator2
        // 
        _separator2.Name = "_Separator2";
        _separator2.Size = new Size(6, 25);
        // 
        // _zoomButton
        // 
        _zoomButton.AutoToolTip = false;
        _zoomButton.DropDownItems.AddRange(new ToolStripItem[] { _actualSizeMenuItem, _fullPageMenuItem, _pageWidthMenuItem, _twoPagesMenuItem, _zoomSeparatorMenuItem, _zoom500MenuItem, _zoom200MenuItem, _zoom150MenuItem, _zoom100MenuItem, _zoom75MenuItem, _zoom50MenuItem, _zoom25MenuItem, _zoom10MenuItem });
        _zoomButton.Image = (Image)resources.GetObject("ZoomImage");
        _zoomButton.ImageTransparentColor = Color.Magenta;
        _zoomButton.Name = "_ZoomButton";
        _zoomButton.Size = new Size(77, 22);
        _zoomButton.Text = "&Zoom";
        // 
        // _actualSizeMenuItem
        // 
        _actualSizeMenuItem.Image = (Image)resources.GetObject("ActualSizeImage");
        _actualSizeMenuItem.Name = "_actualSizeMenuItem";
        _actualSizeMenuItem.Size = new Size(150, 22);
        _actualSizeMenuItem.Text = "Actual Size";
        // 
        // _fullPageMenuItem
        // 
        _fullPageMenuItem.Image = (Image)resources.GetObject("FullPageImage");
        _fullPageMenuItem.Name = "_FullPageMenuItem";
        _fullPageMenuItem.Size = new Size(150, 22);
        _fullPageMenuItem.Text = "Full Page";
        // 
        // _pageWidthMenuItem
        // 
        _pageWidthMenuItem.Image = (Image)resources.GetObject("PageWidthImage");
        _pageWidthMenuItem.Name = "_PageWidthMenuItem";
        _pageWidthMenuItem.Size = new Size(150, 22);
        _pageWidthMenuItem.Text = "Page Width";
        // 
        // _twoPagesMenuItem
        // 
        _twoPagesMenuItem.Image = (Image)resources.GetObject("TwoPagesImage");
        _twoPagesMenuItem.Name = "_TwoPagesMenuItem";
        _twoPagesMenuItem.Size = new Size(150, 22);
        _twoPagesMenuItem.Text = "Two Pages";
        // 
        // _zoomSeparatorMenuItem
        // 
        _zoomSeparatorMenuItem.Name = "_ZoomSeparatorMenuItem";
        _zoomSeparatorMenuItem.Size = new Size(147, 6);
        // 
        // _zoom500MenuItem
        // 
        _zoom500MenuItem.Name = "_Zoom500MenuItem";
        _zoom500MenuItem.Size = new Size(150, 22);
        _zoom500MenuItem.Text = "500%";
        // 
        // _zoom200MenuItem
        // 
        _zoom200MenuItem.Name = "_Zoom200MenuItem";
        _zoom200MenuItem.Size = new Size(150, 22);
        _zoom200MenuItem.Text = "200%";
        // 
        // _zoom150MenuItem
        // 
        _zoom150MenuItem.Name = "_Zoom150MenuItem";
        _zoom150MenuItem.Size = new Size(150, 22);
        _zoom150MenuItem.Text = "150%";
        // 
        // _zoom100MenuItem
        // 
        _zoom100MenuItem.Name = "_Zoom100MenuItem";
        _zoom100MenuItem.Size = new Size(150, 22);
        _zoom100MenuItem.Text = "100%";
        // 
        // _zoom75MenuItem
        // 
        _zoom75MenuItem.Name = "_Zoom75MenuItem";
        _zoom75MenuItem.Size = new Size(150, 22);
        _zoom75MenuItem.Text = "75%";
        // 
        // _zoom50MenuItem
        // 
        _zoom50MenuItem.Name = "_Zoom50MenuItem";
        _zoom50MenuItem.Size = new Size(150, 22);
        _zoom50MenuItem.Text = "50%";
        // 
        // _zoom25MenuItem
        // 
        _zoom25MenuItem.Name = "_Zoom25MenuItem";
        _zoom25MenuItem.Size = new Size(150, 22);
        _zoom25MenuItem.Text = "25%";
        // 
        // _zoom10MenuItem
        // 
        _zoom10MenuItem.Name = "_Zoom10MenuItem";
        _zoom10MenuItem.Size = new Size(150, 22);
        _zoom10MenuItem.Text = "10%";
        // 
        // _firstButton
        // 
        _firstButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
        _firstButton.Image = (Image)resources.GetObject("FirstImage");
        _firstButton.ImageTransparentColor = Color.Red;
        _firstButton.Name = "_FirstButton";
        _firstButton.Size = new Size(23, 22);
        _firstButton.Text = "First Page";
        // 
        // _previousButton
        // 
        _previousButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
        _previousButton.Image = (Image)resources.GetObject("PreviousImage");
        _previousButton.ImageTransparentColor = Color.Red;
        _previousButton.Name = "_PreviousButton";
        _previousButton.Size = new Size(23, 22);
        _previousButton.Text = "Previous Page";
        // 
        // _startPageTextBox
        // 
        _startPageTextBox.AutoSize = false;
        _startPageTextBox.Name = "_StartPageTextBox";
        _startPageTextBox.Size = new Size(39, 24);
        _startPageTextBox.TextBoxTextAlign = HorizontalAlignment.Center;
        // 
        // _pageCountLabel
        // 
        _pageCountLabel.Name = "_PageCountLabel";
        _pageCountLabel.Size = new Size(13, 22);
        _pageCountLabel.Text = " ";
        // 
        // _nextButton
        // 
        _nextButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
        _nextButton.Image = (Image)resources.GetObject("NextImage");
        _nextButton.ImageTransparentColor = Color.Red;
        _nextButton.Name = "_NextButton";
        _nextButton.Size = new Size(23, 22);
        _nextButton.Text = "Next Page";
        // 
        // _lastButton
        // 
        _lastButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
        _lastButton.Image = (Image)resources.GetObject("LastImage");
        _lastButton.ImageTransparentColor = Color.Red;
        _lastButton.Name = "_LastButton";
        _lastButton.Size = new Size(23, 22);
        _lastButton.Text = "Last Page";
        // 
        // _separator1
        // 
        _separator1.Name = "_Separator1";
        _separator1.Size = new Size(6, 25);
        _separator1.Visible = false;
        // 
        // _cancelButton
        // 
        _cancelButton.AutoToolTip = false;
        _cancelButton.Image = (Image)resources.GetObject("CancelImage");
        _cancelButton.ImageTransparentColor = Color.Magenta;
        _cancelButton.Name = "_CancelButton";
        _cancelButton.Size = new Size(70, 22);
        _cancelButton.Text = "Cancel";
        // 
        // _preview
        // 
        _preview.Dock = DockStyle.Fill;
        _preview.Document = null;
        _preview.Location = new Point(0, 25);
        _preview.Name = "_Preview";
        _preview.Size = new Size(532, 410);
        _preview.TabIndex = 2;
        _preview.ZoomMode = ZoomMode.FullPage;
        // 
        // CoolPrintPreviewDialog
        // 
        AutoScaleDimensions = new SizeF(120.0f, 120.0f);
        AutoScaleMode = AutoScaleMode.Dpi;
        ClientSize = new Size(532, 435);
        Controls.Add(_preview);
        Controls.Add(_toolStrip);
        Name = "CoolPrintPreviewDialog";
        ShowIcon = false;
        ShowInTaskbar = false;
        StartPosition = FormStartPosition.CenterParent;
        Text = "Print Preview";
        _toolStrip.ResumeLayout(false);
        _toolStrip.PerformLayout();
        ResumeLayout(false);
        PerformLayout();
    }

    private ToolStrip _toolStrip;
    private System.Windows.Forms.ToolStripButton _printButton;
    private System.Windows.Forms.ToolStripButton _pageSetupButton;
    private ToolStripSeparator _separator2;
    private System.Windows.Forms.ToolStripSplitButton _zoomButton;
    private System.Windows.Forms.ToolStripMenuItem _actualSizeMenuItem;
    private System.Windows.Forms.ToolStripMenuItem _fullPageMenuItem;
    private System.Windows.Forms.ToolStripMenuItem _pageWidthMenuItem;
    private System.Windows.Forms.ToolStripMenuItem _twoPagesMenuItem;
    private ToolStripSeparator _zoomSeparatorMenuItem;
    private System.Windows.Forms.ToolStripMenuItem _zoom500MenuItem;
    private System.Windows.Forms.ToolStripMenuItem _zoom200MenuItem;
    private System.Windows.Forms.ToolStripMenuItem _zoom150MenuItem;
    private System.Windows.Forms.ToolStripMenuItem _zoom100MenuItem;
    private System.Windows.Forms.ToolStripMenuItem _zoom75MenuItem;
    private System.Windows.Forms.ToolStripMenuItem _zoom50MenuItem;
    private System.Windows.Forms.ToolStripMenuItem _zoom25MenuItem;
    private System.Windows.Forms.ToolStripMenuItem _zoom10MenuItem;
    private System.Windows.Forms.ToolStripButton _firstButton;
    private System.Windows.Forms.ToolStripButton _previousButton;
    private System.Windows.Forms.ToolStripTextBox _startPageTextBox;
    private System.Windows.Forms.ToolStripLabel _pageCountLabel;
    private System.Windows.Forms.ToolStripButton _nextButton;
    private System.Windows.Forms.ToolStripButton _lastButton;
    private ToolStripSeparator _separator1;
    private System.Windows.Forms.ToolStripButton _cancelButton;
    private CoolPrintPreviewControl _preview;
    private PrintDocument _doc;

}
