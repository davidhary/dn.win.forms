using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Windows.Forms;

namespace cc.isr.WinForms;

/// <summary> Cool print preview control. </summary>
/// <remarks>
/// (c) 2009 Bernardo Castillo. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2009-08-05 from Bernardo Castilho </para><para>
/// http://www.codeproject.com/Articles/38758/An-Enhanced-PrintPreviewDialog. </para>
/// </remarks>
[Description( "Print Preview Control" )]
public class CoolPrintPreviewControl : UserControl
{
    #region " construction and cleanup "

    /// <summary> Default constructor. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public CoolPrintPreviewControl() : base()
    {
        this._backBrush = new SolidBrush( SystemColors.AppWorkspace );
        this._imagePixelSize = new PointF( -1.0f, -1.0f );
        this._pageImages = [];
        this.BackColor = SystemColors.AppWorkspace;
        this.ZoomMode = ZoomMode.FullPage;
        this.StartPage = 0;
        this.SetStyle( ControlStyles.OptimizedDoubleBuffer, true );
    }

    /// <summary>
    /// Releases the unmanaged resources used by the CoolPrintPreviewVB.CoolPrintPreviewControl and
    /// optionally releases the managed resources.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="disposing"> true to release both managed and unmanaged resources; false to
    /// release only unmanaged resources. </param>
    protected override void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this._backBrush?.Dispose();
                this.RemovePageCountChangedEventHandler( PageCountChanged );
                this.RemoveStartPageChangedEventHandler( StartPageChanged );
                this.RemoveZoomModeChangedEventHandler( ZoomModeChanged );
                this._pageImages?.Clear();
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }

    #endregion

    #region " event handlers "

    /// <summary> Print document end print. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Print event information. </param>
    private void PrintDocumentEndPrint( object? sender, PrintEventArgs e )
    {
        this.SyncPageImages( true );
    }

    /// <summary> Print document print page. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Print page event information. </param>
    private void PrintDocumentPrintPage( object? sender, PrintPageEventArgs e )
    {
        if ( e is null ) return;
        this.SyncPageImages( false );
        if ( this._cancel )
        {
            e.Cancel = true;
        }
    }

    #endregion

    #region " functions "

    /// <summary> True to cancel. </summary>
    private bool _cancel;

    /// <summary> Cancels printing. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public void Cancel()
    {
        this._cancel = true;
    }

    /// <summary> Gets an image. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="page"> The page. </param>
    /// <returns> The image. </returns>
    private Image? GetImage( int page )
    {
        return page > -1 && page < this.PageCount ? this._pageImages[page] : null;
    }

    /// <summary> Gets image rectangle. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="img"> The image. </param>
    /// <returns> The image rectangle. </returns>
    private Rectangle GetImageRectangle( Image img )
    {
        Size sz = this.GetImageSizeInPixels( img );
        Rectangle rc = new( 0, 0, sz.Width, sz.Height );
        Rectangle rcCli = this.ClientRectangle;
        switch ( this._zoomMode )
        {
            case ZoomMode.ActualSize:
                {
                    this._zoom = 1d;
                    break;
                }

            case ZoomMode.FullPage:
                {
                    this._zoom = Math.Min( rc.Width > 0 ? rcCli.Width / ( double ) rc.Width : 0,
                                           rc.Height > 0 ? rcCli.Height / ( double ) rc.Height : 0 );
                    break;
                }

            case ZoomMode.PageWidth:
                {
                    this._zoom = Convert.ToDouble( rc.Width > 0 ? rcCli.Width / ( double ) rc.Width : 0 );
                    break;
                }

            case ZoomMode.TwoPages:
                {
                    rc.Width *= 2;
                    this._zoom = Math.Min( rc.Width > 0 ? rcCli.Width / ( double ) rc.Width : 0,
                                           rc.Height > 0 ? rcCli.Height / ( double ) rc.Height : 0 );
                    break;
                }

            default:
                {
                    break;
                }
        }
        // not used Dim zoomX As Double = CDbl(IIf((rc.Width > 0), (CDbl(rcCli.Width) / CDbl(rc.Width)), 0))
        // not used Dim zoomY As Double = CDbl(IIf((rc.Height > 0), (CDbl(rcCli.Height) / CDbl(rc.Height)), 0))
        rc.Width = ( int ) (rc.Width * this._zoom);
        rc.Height = ( int ) (rc.Height * this._zoom);
        int dx = ( int ) ((rcCli.Width - rc.Width) / 2d);
        if ( dx > 0 )
        {
            rc.X += dx;
        }

        int dy = ( int ) ((rcCli.Height - rc.Height) / 2d);
        if ( dy > 0 )
        {
            rc.Y += dy;
        }

        rc.Inflate( -4, -4 );
        if ( this._zoomMode == ZoomMode.TwoPages )
        {
            rc.Inflate( -2, 0 );
        }

        return rc;
    }

    /// <summary>   Size of the image in pixels. </summary>
    private PointF _imagePixelSize;

    /// <summary> Gets image size in pixels. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="img"> The image. </param>
    /// <returns> The image size in pixels. </returns>
    private Size GetImageSizeInPixels( Image img )
    {
        SizeF szf = img.PhysicalDimension;
        if ( img is Metafile )
        {
            if ( this._imagePixelSize.X < 0.0f )
            {
                using Graphics g = this.CreateGraphics();
                this._imagePixelSize.X = g.DpiX / 2540.0f;
                this._imagePixelSize.Y = g.DpiY / 2540.0f;
            }

            szf.Width *= this._imagePixelSize.X;
            szf.Height *= this._imagePixelSize.Y;
        }

        return Size.Truncate( szf );
    }

    /// <summary>
    /// Determines whether the specified key is a regular input key or a special key that requires
    /// preprocessing.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="keyData"> One of the <see cref="Keys" /> values. </param>
    /// <returns> true if the specified key is a regular input key; otherwise, false. </returns>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0010:Add missing cases", Justification = "<Pending>" )]
    protected override bool IsInputKey( Keys keyData )
    {
        switch ( keyData )
        {
            case Keys.Prior:
            case Keys.Next:
            case Keys.End:
            case Keys.Home:
            case Keys.Left:
            case Keys.Up:
            case Keys.Right:
            case Keys.Down:
                {
                    return true;
                }

            default:
                break;
        }

        return base.IsInputKey( keyData );
    }

    #endregion

    #region " overrides "

    /// <summary> Raises the <see cref="Control.KeyDown" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="KeyEventArgs" /> that contains the event
    /// data. </param>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0010:Add missing cases", Justification = "<Pending>" )]
    protected override void OnKeyDown( KeyEventArgs e )
    {
        if ( e is null ) return;

        base.OnKeyDown( e );
        if ( e.Handled )
        {
            return;
        }

        switch ( e.KeyCode )
        {
            // arrow keys scroll or browse, depending on ZoomMode
            case Keys.Left:
            case Keys.Up:
            case Keys.Right:
            case Keys.Down:
                {
                    // browse
                    if ( this.ZoomMode is ZoomMode.FullPage or ZoomMode.TwoPages )
                    {
                        switch ( e.KeyCode )
                        {
                            case Keys.Left:
                            case Keys.Up:
                                {
                                    this.StartPage -= 1;
                                    break;
                                }

                            case Keys.Right:
                            case Keys.Down:
                                {
                                    this.StartPage += 1;
                                    break;
                                }
                        }
                    }

                    // scroll
                    Point pt = this.AutoScrollPosition;
                    switch ( e.KeyCode )
                    {
                        case Keys.Left:
                            {
                                pt.X += 20;
                                break;
                            }

                        case Keys.Right:
                            {
                                pt.X -= 20;
                                break;
                            }

                        case Keys.Up:
                            {
                                pt.Y += 20;
                                break;
                            }

                        case Keys.Down:
                            {
                                pt.Y -= 20;
                                break;
                            }
                    }

                    this.AutoScrollPosition = new Point( -pt.X, -pt.Y );
                    break;
                }

            // page up/down browse pages
            case Keys.PageUp:
                {
                    this.StartPage -= 1;
                    break;
                }

            case Keys.PageDown:
                {
                    this.StartPage += 1;
                    break;
                }

            // home/end 
            case Keys.Home:
                {
                    this.AutoScrollPosition = Point.Empty;
                    this.StartPage = 0;
                    break;
                }

            case Keys.End:
                {
                    this.AutoScrollPosition = Point.Empty;
                    this.StartPage = this.PageCount - 1;
                    break;
                }

            default:
                {
                    return;
                }
        }

        // if we got here, the event was handled
        e.Handled = true;
    }

    /// <summary> The point last. </summary>
    private Point _lastPoint;

    /// <summary> Raises the mouse event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="MouseEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnMouseDown( MouseEventArgs e )
    {
        if ( e is null ) return;
        base.OnMouseDown( e );
        if ( e.Button == MouseButtons.Left && this.AutoScrollMinSize != Size.Empty )
        {
            this.Cursor = Cursors.NoMove2D;
            this._lastPoint = new Point( e.X, e.Y );
        }
    }

    /// <summary> Raises the <see cref="Control.MouseMove" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="MouseEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnMouseMove( MouseEventArgs e )
    {
        if ( e is null ) return;
        base.OnMouseMove( e );
        if ( ReferenceEquals( this.Cursor, Cursors.NoMove2D ) )
        {
            int dx = e.X - this._lastPoint.X;
            int dy = e.Y - this._lastPoint.Y;
            if ( dx != 0 || dy != 0 )
            {
                Point pt = this.AutoScrollPosition;
                this.AutoScrollPosition = new Point( -(pt.X + dx), -(pt.Y + dy) );
                this._lastPoint = new Point( e.X, e.Y );
            }
        }
    }

    /// <summary> Raises the <see cref="Control.MouseUp" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="MouseEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnMouseUp( MouseEventArgs e )
    {
        if ( e is null ) return;
        base.OnMouseUp( e );
        if ( e.Button == MouseButtons.Left && ReferenceEquals( this.Cursor, Cursors.NoMove2D ) )
        {
            this.Cursor = Cursors.Default;
        }
    }

    /// <summary> Event queue for all listeners interested in PageCountChanged events. </summary>
    public event EventHandler<EventArgs>? PageCountChanged;

    /// <summary> Removes event handler. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The handler. </param>
    private void RemovePageCountChangedEventHandler( EventHandler<EventArgs>? value )
    {
        foreach ( Delegate d in value is null ? ([]) : value.GetInvocationList() )
        {
            try
            {
                PageCountChanged -= ( EventHandler<EventArgs> ) d;
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
        }
    }

    /// <summary> Raises the page count changed event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> Event information to send to registered event handlers. </param>
    protected void OnPageCountChanged( EventArgs e )
    {
        PageCountChanged?.Invoke( this, e );
    }

    /// <summary> Raises the <see cref="Control.Paint" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="PaintEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnPaint( PaintEventArgs e )
    {
        if ( e is null ) return;
        Image? img = this.GetImage( this.StartPage );
        if ( img is not null )
        {
            Rectangle rc = this.GetImageRectangle( img );
            if ( rc.Width > 2 && rc.Height > 2 )
            {
                rc.Offset( this.AutoScrollPosition );
                if ( this._zoomMode != ZoomMode.TwoPages )
                {
                    RenderPage( e.Graphics, img, rc );
                }
                else
                {
                    rc.Width = ( int ) ((rc.Width - 4) / 2d);
                    RenderPage( e.Graphics, img, rc );
                    img = this.GetImage( this.StartPage + 1 );
                    if ( img is not null )
                    {
                        rc = this.GetImageRectangle( img );
                        rc.Width = ( int ) ((rc.Width - 4) / 2d);
                        rc.Offset( rc.Width + 4, 0 );
                        RenderPage( e.Graphics, img, rc );
                    }
                }
            }
        }

        e.Graphics.FillRectangle( this._backBrush, this.ClientRectangle );
    }

    /// <summary>
    /// Raises the <see cref="Control.SizeChanged" /> event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnSizeChanged( EventArgs e )
    {
        this.UpdateScrollBars();
        base.OnSizeChanged( e );
    }

    /// <summary> Event queue for all listeners interested in StartPageChanged events. </summary>
    public event EventHandler<EventArgs>? StartPageChanged;

    /// <summary> Removes event handler. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The handler. </param>
    private void RemoveStartPageChangedEventHandler( EventHandler<EventArgs>? value )
    {
        foreach ( Delegate d in value is null ? ([]) : value.GetInvocationList() )
        {
            try
            {
                StartPageChanged -= ( EventHandler<EventArgs> ) d;
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
        }
    }

    /// <summary> Raises the start page changed event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> Event information to send to registered event handlers. </param>
    protected void OnStartPageChanged( EventArgs e )
    {
        StartPageChanged?.Invoke( this, e );
    }

    /// <summary> Event queue for all listeners interested in ZoomModeChanged events. </summary>
    public event EventHandler<EventArgs>? ZoomModeChanged;

    /// <summary> Removes event handler. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The handler. </param>
    private void RemoveZoomModeChangedEventHandler( EventHandler<EventArgs>? value )
    {
        foreach ( Delegate d in value is null ? ([]) : value.GetInvocationList() )
        {
            try
            {
                ZoomModeChanged -= ( EventHandler<EventArgs> ) d;
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
        }
    }

    /// <summary> Raises the zoom mode changed event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> Event information to send to registered event handlers. </param>
    protected void OnZoomModeChanged( EventArgs e )
    {
        ZoomModeChanged?.Invoke( this, e );
    }

    #endregion

    #region " print functions "

    /// <summary> Prints this object. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public void Print()
    {
        if ( this._printDocument is null || this.Document is null )
            throw new InvalidOperationException( "Document must be set for printing." );

        PrinterSettings ps = this._printDocument.PrinterSettings;
        int first = ps.MinimumPage - 1;
        int last = ps.MaximumPage - 1;
        switch ( ps.PrintRange )
        {
            case PrintRange.AllPages:
                {
                    this.Document.Print();
                    return;
                }

            case PrintRange.Selection:
                {
                    first = this.StartPage;
                    last = this.StartPage;
                    if ( this.ZoomMode == ZoomMode.TwoPages )
                    {
                        last = Math.Min( first + 1, this.PageCount - 1 );
                    }

                    break;
                }

            case PrintRange.SomePages:
                {
                    first = ps.FromPage - 1;
                    last = ps.ToPage - 1;
                    break;
                }

            case PrintRange.CurrentPage:
                {
                    first = this.StartPage;
                    last = this.StartPage;
                    break;
                }

            default:
                break;
        }

        using DocumentPrinter dp = new( this, first, last );
        dp.Print();
    }

    /// <summary> Refresh preview. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public void RefreshPreview()
    {
        if ( this._printDocument is not null )
        {
            this._pageImages.Clear();
            PrintController savePC = this._printDocument.PrintController;
            try
            {
                this._cancel = false;
                this.IsRendering = true;
                this._printDocument.PrintController = new PreviewPrintController();
                this._printDocument.PrintPage += new PrintPageEventHandler( this.PrintDocumentPrintPage );
                this._printDocument.EndPrint += new PrintEventHandler( this.PrintDocumentEndPrint );
                this._printDocument.Print();
            }
            finally
            {
                this._cancel = false;
                this.IsRendering = false;
                this._printDocument.PrintPage -= new PrintPageEventHandler( this.PrintDocumentPrintPage );
                this._printDocument.EndPrint -= new PrintEventHandler( this.PrintDocumentEndPrint );
                this._printDocument.PrintController = savePC;
            }
        }

        this.OnPageCountChanged( EventArgs.Empty );
        this.UpdatePreview();
        this.UpdateScrollBars();
    }

    /// <summary> Renders the page. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="g">   The Graphics to process. </param>
    /// <param name="img"> The image. </param>
    /// <param name="rc">  The rectangle. </param>
    private static void RenderPage( Graphics g, Image img, Rectangle rc )
    {
        if ( g is null )
        {
            return;
        }

        rc.Offset( 1, 1 );
        g.DrawRectangle( Pens.Black, rc );
        rc.Offset( -1, -1 );
        g.FillRectangle( Brushes.White, rc );
        g.DrawImage( img, rc );
        g.DrawRectangle( Pens.Black, rc );
        rc.Width += 1;
        rc.Height += 1;
        g.ExcludeClip( rc );
        rc.Offset( 1, 1 );
        g.ExcludeClip( rc );
    }

    /// <summary> Synchronization page images. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="lastPageReady"> true to last page ready. </param>
    private void SyncPageImages( bool lastPageReady )
    {
        PreviewPrintController pv = ( PreviewPrintController ) this._printDocument!.PrintController;
        if ( pv is not null )
        {
            PreviewPageInfo[] pageInfo = pv.GetPreviewPageInfo();
            int count = lastPageReady ? pageInfo.Length : pageInfo.Length - 1;
            int i;
            int loopTo = count - 1;
            for ( i = this._pageImages.Count; i <= loopTo; i++ )
            {
                Image img = pageInfo[i].Image;
                this._pageImages.Add( img );
                this.OnPageCountChanged( EventArgs.Empty );
                if ( this.StartPage < 0 )
                {
                    this.StartPage = 0;
                }

                if ( i == this.StartPage || i == this.StartPage + 1 )
                {
                    this.Refresh();
                }

                Application.DoEvents();
            }
        }
    }

    /// <summary> Updates the preview. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private void UpdatePreview()
    {
        if ( this._startPage < 0 )
        {
            this._startPage = 0;
        }

        if ( this._startPage > this.PageCount - 1 )
        {
            this._startPage = this.PageCount - 1;
        }

        this.Invalidate();
    }

    /// <summary> Updates the scroll bars. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0010:Add missing cases", Justification = "<Pending>" )]
    private void UpdateScrollBars()
    {
        Rectangle rc = Rectangle.Empty;
        Image? img = this.GetImage( this.StartPage );
        if ( img is not null )
        {
            rc = this.GetImageRectangle( img );
        }

        Size scrollSize = new( 0, 0 );
        switch ( this._zoomMode )
        {
            case ZoomMode.ActualSize:
            case ZoomMode.Custom:
                {
                    scrollSize = new Size( rc.Width + 8, rc.Height + 8 );
                    break;
                }

            case ZoomMode.PageWidth:
                {
                    scrollSize = new Size( 0, rc.Height + 8 );
                    break;
                }

            default:
                break;
        }

        if ( scrollSize != this.AutoScrollMinSize )
        {
            this.AutoScrollMinSize = scrollSize;
        }

        this.UpdatePreview();
    }

    #endregion

    #region " properties "

    /// <summary> The background color brush. </summary>
    private Brush _backBrush;

    /// <summary> Gets or sets the background color for the control. </summary>
    /// <value>
    /// A <see cref="Color" /> that represents the background color of the control.
    /// The default is the value of the
    /// <see cref="Control.DefaultBackColor" /> property.
    /// </value>
    [DefaultValue( typeof( SystemColors ), "AppWorkspace" )]
    public override Color BackColor
    {
        get => base.BackColor;
        set
        {
            this._backBrush = new SolidBrush( SystemColors.AppWorkspace );
            base.BackColor = value;
            this._backBrush = new SolidBrush( value );
        }
    }

    /// <summary> The print document. </summary>
    private PrintDocument? _printDocument;

    /// <summary> Gets or sets the document. </summary>
    /// <value> The document. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public PrintDocument? Document
    {
        get => this._printDocument;
        set
        {
            if ( !ReferenceEquals( value, this._printDocument ) )
            {
                this._printDocument = value;
                this.RefreshPreview();
            }
        }
    }

    /// <summary> Gets a value indicating whether this object is rendering. </summary>
    /// <value> <c>true</c> if this object is rendering; otherwise <c>false</c> </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    [Browsable( false )]
    public bool IsRendering { get; private set; }

    /// <summary> Gets the number of pages. </summary>
    /// <value> The number of pages. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public int PageCount => this._pageImages.Count;

    /// <summary> The page images. </summary>
    private readonly List<Image> _pageImages;

    /// <summary> Gets the page images. </summary>
    /// <value> The page images. </value>
    [Browsable( false )]
    public PageImageCollection PageImages => new( [.. this._pageImages] );

    /// <summary> The start page. </summary>
    private int _startPage;

    /// <summary> Gets or sets the start page number. </summary>
    /// <value> The Zero-Based start page number. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public int StartPage
    {
        get => this._startPage;
        set
        {
            if ( value > this.PageCount - 1 )
            {
                value = this.PageCount - 1;
            }

            if ( value < 0 )
            {
                value = 0;
            }

            if ( value != this._startPage )
            {
                this._startPage = value;
                this.UpdateScrollBars();
                this.OnStartPageChanged( EventArgs.Empty );
            }
        }
    }

    /// <summary> The zoom. </summary>
    private double _zoom;

    /// <summary> Gets or sets the zoom. </summary>
    /// <value> The zoom. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public double Zoom
    {
        get => this._zoom;
        set
        {
            if ( value != this._zoom || this.ZoomMode != ZoomMode.Custom )
            {
                this.ZoomMode = ZoomMode.Custom;
                this._zoom = value;
                this.UpdateScrollBars();
                this.OnZoomModeChanged( EventArgs.Empty );
            }
        }
    }

    /// <summary> The zoom mode. </summary>
    private ZoomMode _zoomMode;

    /// <summary> Gets or sets the zoom mode. </summary>
    /// <value> The zoom mode. </value>
    [DefaultValue( 1 )]
    public ZoomMode ZoomMode
    {
        get => this._zoomMode;
        set
        {
            if ( value != this._zoomMode )
            {
                this._zoomMode = value;
                this.UpdateScrollBars();
                this.OnZoomModeChanged( EventArgs.Empty );
            }
        }
    }

    #endregion

    #region " document printer class "

    /// <summary> Document printer. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-05-02 </para>
    /// </remarks>
    internal sealed class DocumentPrinter : PrintDocument
    {
        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="preview"> The preview. </param>
        /// <param name="first">   The first. </param>
        /// <param name="last">    The last. </param>
        public DocumentPrinter( CoolPrintPreviewControl preview, int first, int last )
        {
#if NET8_0_OR_GREATER
            ArgumentNullException.ThrowIfNull( preview, nameof( preview ) );
#else
            if ( preview is null ) throw new ArgumentNullException( nameof( preview ) );
#endif

            if ( preview.Document is null ) throw new InvalidOperationException( "A preview document must be defined." );

            this.First = first;
            this.Last = last;
            this.PageImages = preview.PageImages;
            this.DefaultPageSettings = preview.Document.DefaultPageSettings;
            this.PrinterSettings = preview.Document.PrinterSettings;
        }

        /// <summary> Gets or sets the first. </summary>
        /// <value> The first. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public int First { get; private set; }

        /// <summary> Gets or sets the zero-based index of this object. </summary>
        /// <value> The index. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public int Index { get; private set; }

        /// <summary>
        /// Raises the <see cref="PrintDocument.BeginPrint" /> event. It is
        /// called after the <see cref="PrintDocument.Print" /> method is
        /// called and before the first page of the document prints.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="PrintEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnBeginPrint( PrintEventArgs e )
        {
            this.Index = this.First;
        }

        /// <summary> Gets or sets the page images. </summary>
        /// <value> The page images. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public PageImageCollection PageImages { get; private set; }

        /// <summary> Gets or sets the last. </summary>
        /// <value> The last. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public int Last { get; private set; }

        /// <summary>
        /// Raises the <see cref="PrintDocument.PrintPage" /> event. It is
        /// called before a page prints.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="PrintPageEventArgs" /> that contains
        /// the event data. </param>
        protected override void OnPrintPage( PrintPageEventArgs e )
        {
            if ( e.Graphics is null ) return;
            e.Graphics.PageUnit = GraphicsUnit.Display;
            e.Graphics.DrawImage( this.PageImages[this.Index], e.PageBounds );
            this.Index += 1;
            e.HasMorePages = this.Index <= this.Last;
        }
    }
    #endregion
}
/// <summary> Collection of page images. </summary>
/// <remarks>
/// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2015-05-02 </para>
/// </remarks>
/// <remarks> Constructor. </remarks>
/// <remarks> David, 2020-09-24. </remarks>
/// <param name="images"> The images. </param>
public class PageImageCollection( Image[] images ) : System.Collections.ObjectModel.ReadOnlyCollection<Image>( images )
{
}
/// <summary> Values that represent zoom modes. </summary>
/// <remarks> David, 2020-09-24. </remarks>
public enum ZoomMode
{
    /// <summary> An enum constant representing the actual size option. </summary>
    ActualSize = 0,

    /// <summary> An enum constant representing the full page option. </summary>
    FullPage = 1,

    /// <summary> An enum constant representing the page width option. </summary>
    PageWidth = 2,

    /// <summary> An enum constant representing the two pages option. </summary>
    TwoPages = 3,

    /// <summary> An enum constant representing the custom option. </summary>
    Custom = 4
}
