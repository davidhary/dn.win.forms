using System;
using System.ComponentModel;
using System.Drawing.Printing;
using System.Windows.Forms;

namespace cc.isr.WinForms;

/// <summary> Dialog for setting the cool print preview. </summary>
/// <remarks>
/// (c) 2009 Bernardo Castillo. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2009-08-05 from Bernardo Castilho </para><para>
/// http://www.codeproject.com/Articles/38758/An-Enhanced-PrintPreviewDialog. </para>
/// </remarks>
public partial class CoolPrintPreviewDialog : Form
{
    /// <summary> Constructor. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="parentForm"> The parent form. </param>
    public CoolPrintPreviewDialog( Control? parentForm ) : base()
    {
        this.components = null;
        this.InitializeComponent();
        if ( parentForm is not null )
            this.Size = parentForm.Size;

        this._printButton.Name = "_PrintButton";
        this._pageSetupButton.Name = "_PageSetupButton";
        this._zoomButton.Name = "_ZoomButton";
        this._firstButton.Name = "_FirstButton";
        this._previousButton.Name = "_PreviousButton";
        this._startPageTextBox.Name = "_StartPageTextBox";
        this._nextButton.Name = "_NextButton";
        this._lastButton.Name = "_LastButton";
        this._cancelButton.Name = "_CancelButton";
        this._preview.Name = "_Preview";
    }

    #region " construction "

    /// <summary> Default constructor. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public CoolPrintPreviewDialog() : this( null )
    {
    }

    /// <summary>
    /// Disposes of the resources (other than memory) used by the
    /// <see cref="Form" />.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="disposing"> true to release both managed and unmanaged resources; false to
    /// release only unmanaged resources. </param>
    protected override void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this.components?.Dispose();
                this.components = null;
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }

    #endregion

    #region " form overloads "

    /// <summary> Raises the <see cref="Form.FormClosing" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="FormClosingEventArgs" /> that contains
    /// the event data. </param>
    protected override void OnFormClosing( FormClosingEventArgs e )
    {
        if ( e is null ) return;
        base.OnFormClosing( e );
        if ( !(!this._preview.IsRendering || e.Cancel) )
        {
            this._preview.Cancel();
        }
    }

    /// <summary> Raises the <see cref="Form.Shown" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnShown( EventArgs e )
    {
        if ( e is null ) return;
        base.OnShown( e );
        this._preview.Document = this.Document;
    }

    #endregion

    #region " properties "

    /// <summary> Gets or sets the document. </summary>
    /// <value> The document. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public PrintDocument Document
    {
        get => this._doc;
        set
        {
            if ( this._doc is not null )
            {
                this._doc.BeginPrint -= new PrintEventHandler( this.Doc_BeginPrint );
                this._doc.EndPrint -= new PrintEventHandler( this.Doc_EndPrint );
            }

            this._doc = value;
            if ( this._doc is not null )
            {
                this._doc.BeginPrint += new PrintEventHandler( this.Doc_BeginPrint );
                this._doc.EndPrint += new PrintEventHandler( this.Doc_EndPrint );
            }

            if ( this.Visible )
            {
                this._preview.Document = this.Document;
            }
        }
    }

    #endregion

    #region " event handlers "

    /// <summary> Cancel button click. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void CancelButton_Click( object? sender, EventArgs e )
    {
        if ( this._preview.IsRendering )
        {
            this._preview.Cancel();
        }
        else
        {
            this.Close();
        }
    }

    /// <summary> First button click. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void FirstButton_Click( object? sender, EventArgs e )
    {
        this._preview.StartPage = 0;
    }

    /// <summary> Last button click. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void LastButton_Click( object? sender, EventArgs e )
    {
        this._preview.StartPage = this._preview.PageCount - 1;
    }

    /// <summary> Next button click. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void NextButton_Click( object? sender, EventArgs e )
    {
        this._preview.StartPage += 1;
    }

    /// <summary> Previous button click. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void PreviousButton_Click( object? sender, EventArgs e )
    {
        this._preview.StartPage -= 1;
    }

    /// <summary> Page setup button click. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void PageSetupButton_Click( object? sender, EventArgs e )
    {
        using PageSetupDialog dlg = new()
        {
            Document = this.Document
        };
        if ( dlg.ShowDialog( this ) == DialogResult.OK )
        {
            this._preview.RefreshPreview();
        }
    }

    /// <summary> Print button click. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void PrintButton_Click( object? sender, EventArgs e )
    {
        using PrintDialog dlg = new()
        {
            AllowSomePages = true,
            AllowSelection = true,
            UseEXDialog = true,
            Document = this.Document
        };
        PrinterSettings ps = dlg.PrinterSettings;
        ps.MinimumPage = 1;
        ps.MaximumPage = this._preview.PageCount;
        ps.FromPage = 1;
        ps.ToPage = this._preview.PageCount;
        if ( dlg.ShowDialog( this ) == DialogResult.OK )
        {
            this._preview.Print();
        }
    }

    /// <summary> Zoom button click. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void ZoomButton_ButtonClick( object? sender, EventArgs e )
    {
        this._preview.ZoomMode = this._preview.ZoomMode == ZoomMode.ActualSize ? ZoomMode.FullPage : ZoomMode.ActualSize;
    }

    /// <summary> Zoom button drop down item clicked. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Tool strip item clicked event information. </param>
    private void ZoomButton_DropDownItemClicked( object? sender, ToolStripItemClickedEventArgs e )
    {
        if ( e is null ) return;
        if ( ReferenceEquals( e.ClickedItem, this._actualSizeMenuItem ) )
        {
            this._preview.ZoomMode = ZoomMode.ActualSize;
        }
        else if ( ReferenceEquals( e.ClickedItem, this._fullPageMenuItem ) )
        {
            this._preview.ZoomMode = ZoomMode.FullPage;
        }
        else if ( ReferenceEquals( e.ClickedItem, this._pageWidthMenuItem ) )
        {
            this._preview.ZoomMode = ZoomMode.PageWidth;
        }
        else if ( ReferenceEquals( e.ClickedItem, this._twoPagesMenuItem ) )
        {
            this._preview.ZoomMode = ZoomMode.TwoPages;
        }

        if ( ReferenceEquals( e.ClickedItem, this._zoom10MenuItem ) )
        {
            this._preview.Zoom = 0.1d;
        }
        else if ( ReferenceEquals( e.ClickedItem, this._zoom100MenuItem ) )
        {
            this._preview.Zoom = 1d;
        }
        else if ( ReferenceEquals( e.ClickedItem, this._zoom150MenuItem ) )
        {
            this._preview.Zoom = 1.5d;
        }
        else if ( ReferenceEquals( e.ClickedItem, this._zoom200MenuItem ) )
        {
            this._preview.Zoom = 2d;
        }
        else if ( ReferenceEquals( e.ClickedItem, this._zoom25MenuItem ) )
        {
            this._preview.Zoom = 0.25d;
        }
        else if ( ReferenceEquals( e.ClickedItem, this._zoom50MenuItem ) )
        {
            this._preview.Zoom = 0.5d;
        }
        else if ( ReferenceEquals( e.ClickedItem, this._zoom500MenuItem ) )
        {
            this._preview.Zoom = 5d;
        }
        else if ( ReferenceEquals( e.ClickedItem, this._zoom75MenuItem ) )
        {
            this._preview.Zoom = 0.75d;
        }
    }

    /// <summary> Event handler. Called by Doc for begin print events. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Print event information. </param>
    private void Doc_BeginPrint( object? sender, PrintEventArgs e )
    {
        this._cancelButton.Text = "&Cancel";
        this._printButton.Enabled = !this._pageSetupButton.Enabled;
    }

    /// <summary> Event handler. Called by Doc for end print events. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Print event information. </param>
    private void Doc_EndPrint( object? sender, PrintEventArgs e )
    {
        this._cancelButton.Text = "&Close";
        this._printButton.Enabled = this._pageSetupButton.Enabled;
    }

    /// <summary> Preview page count changed. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void Preview_PageCountChanged( object? sender, EventArgs e )
    {
        this.Update();
        Application.DoEvents();
        this._pageCountLabel.Text = $"of {this._preview.PageCount}";
    }

    /// <summary> Preview start page changed. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void Preview_StartPageChanged( object? sender, EventArgs e )
    {
        this._startPageTextBox.Text = (this._preview.StartPage + 1).ToString( System.Globalization.CultureInfo.CurrentCulture );
    }

    /// <summary> Starts page text box enter. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void StartPageTextBox_Enter( object? sender, EventArgs e )
    {
        this._startPageTextBox.SelectAll();
    }

    /// <summary> Starts page text box key press. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Key press event information. </param>
    private void StartPageTextBox_KeyPress( object? sender, KeyPressEventArgs e )
    {
        if ( e is null ) return;
        char c = e.KeyChar;
        if ( c == '\r' )
        {
            this.CommitPageNumber();
            e.Handled = true;
        }
        else if ( !(c <= ' ' || char.IsDigit( c )) )
        {
            e.Handled = true;
        }
    }

    /// <summary> Starts page text box validating. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Cancel event information. </param>
    private void StartPageTextBox_Validating( object? sender, CancelEventArgs e )
    {
        this.CommitPageNumber();
    }

    /// <summary> Commits page number. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private void CommitPageNumber()
    {
        if ( int.TryParse( this._startPageTextBox.Text, out int page ) )
        {
            this._preview.StartPage = page - 1;
        }
    }

    #endregion
}
