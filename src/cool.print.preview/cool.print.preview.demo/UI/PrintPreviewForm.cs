namespace cc.isr.WinForms.Demo;

/// <summary>   Form for viewing the print preview. </summary>
/// <remarks>
/// (c) 2009 Bernardo Castilho. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2009-08-05" by="Bernardo Castilho" revision=""
/// http://www.codeproject.com/Articles/38758/An-Enhanced-PrintPreviewDialog. </para>
/// </remarks>
public partial class PrintPreviewForm
{
    /// <summary>
    /// Initializes a new instance of the <see cref="Form" /> class.
    /// </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    public PrintPreviewForm()
    {
        // This call is required by the designer.
        this.InitializeComponent();

        // Add any initialization after the InitializeComponent() call.
        this._coolCodeLabel.Text = @"Using dialog As CoolPrintPreviewDialog = New CoolPrintPreviewDialog
    dialog.Document = Me._printDocument
    dialog.ShowDialog(Me)
End Using";
        this._standardCodeLabel.Text = @"Using dialog As PrintPreviewDialog = New PrintPreviewDialog
    dialog.Document = Me._printDocument
    dialog.ShowDialog(Me)
End Using";
    }

    /// <summary>   The font. </summary>
    private readonly Font _font = new( "Segoe UI", 14.0f );

    /// <summary>   The page. </summary>
    private int _page;

    /// <summary>   The start. </summary>
    private DateTimeOffset _start;

    /// <summary>
    /// Event handler. Called by _openStandardPreviewDialogButton for click events. Show standard
    /// print preview.
    /// </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void OpenStandardPreviewDialogButtonClick( object? sender, EventArgs e )
    {
        using PrintPreviewDialog dialog = new();
        dialog.Document = this._printDocument;
        _ = dialog.ShowDialog( this );
    }

    /// <summary>
    /// Event handler. Called by _openCoolPreviewDialogButton for click events. Show cool print
    /// preview.
    /// </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void OpenCoolPreviewDialogButtonClick( object? sender, EventArgs e )
    {
        using CoolPrintPreviewDialog dialog = new();
        dialog.Document = this._printDocument;
        _ = dialog.ShowDialog( this );
    }

    /// <summary>
    /// Event handler. Called by _printDocument for begin print events. Render document.
    /// </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Print event information. </param>
    private void PrintDocumentBeginPrint( object? sender, System.Drawing.Printing.PrintEventArgs e )
    {
        this._start = DateTimeOffset.Now;
        this._page = 0;
    }

    /// <summary>   Event handler. Called by _printDocument for end print events. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Print event information. </param>
    private void PrintDocumentEndPrint( object? sender, System.Drawing.Printing.PrintEventArgs e )
    {
        Console.WriteLine( $"Document rendered in {DateTimeOffset.Now.Subtract( this._start ).TotalMilliseconds} ms" );
    }

    /// <summary>   Event handler. Called by _printDocument for print page events. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Print page event information. </param>
    private void PrintDocumentPrintPage( object? sender, System.Drawing.Printing.PrintPageEventArgs e )
    {
        if ( e.Graphics is null ) return;
        Rectangle rc = e.MarginBounds;
        rc.Height = this._font.Height + 10;
        int i = 0;
        while ( true )
        {
            string text = $"line {i + 1} on page {this._page + 1}";
            e.Graphics.DrawString( text, this._font, Brushes.Black, rc );
            rc.Y += rc.Height;
            if ( rc.Bottom > e.MarginBounds.Bottom )
            {
                this._page += 1;
                e.HasMorePages = this._longDocumentCheckBox.Checked ? this._page < 3000 : this._page < 30;
                return;
            }
            i += 1;
        }
    }
}
