using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace cc.isr.WinForms.Demo;

public partial class PrintPreviewForm : Form
{
    /// <summary>   Form overrides dispose to clean up the component list. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="disposing">    <see langword="true" /> to release both managed and unmanaged
    ///                             resources; <see langword="false" /> to release only unmanaged
    ///                             resources. </param>
    [DebuggerNonUserCode()]
    protected override void Dispose(bool disposing)
    {
        try
        {
            if (disposing)
            {
                components?.Dispose();
                if (_font is object)
                    _font.Dispose();
            }
        }
        finally
        {
            base.Dispose(disposing);
        }
    }


    private System.ComponentModel.IContainer components;

    // NOTE: The following procedure is required by the Windows Form Designer
    // It can be modified using the Windows Form Designer.  
    // Do not modify it using the code editor.
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "PrintPreviewDialog")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "CoolPrintPreviewDialog")]
    [DebuggerStepThrough()]
    private void InitializeComponent()
    {
        this.components = new Container();
        _longDocumentCheckBox = new CheckBox();
        _coolCodeLabel = new Label();
        _standardCodeLabel = new Label();
        _openCoolPreviewDialogButton = new Button();
        _openCoolPreviewDialogButton.Click += new EventHandler(OpenCoolPreviewDialogButtonClick);
        _openStandardPreviewDialogButton = new Button();
        _openStandardPreviewDialogButton.Click += new EventHandler(OpenStandardPreviewDialogButtonClick);
        _printDocument = new System.Drawing.Printing.PrintDocument();
        _printDocument.BeginPrint += new System.Drawing.Printing.PrintEventHandler(PrintDocumentBeginPrint);
        _printDocument.EndPrint += new System.Drawing.Printing.PrintEventHandler(PrintDocumentEndPrint);
        _printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(PrintDocumentPrintPage);
        SuspendLayout();
        // 
        // _longDocumentCheckBox
        // 
        _longDocumentCheckBox.AutoSize = true;
        _longDocumentCheckBox.Location = new Point(232, 238);
        _longDocumentCheckBox.Name = "_LongDocumentCheckBox";
        _longDocumentCheckBox.Size = new Size(162, 21);
        _longDocumentCheckBox.TabIndex = 15;
        _longDocumentCheckBox.Text = "Create Long Document";
        _longDocumentCheckBox.UseVisualStyleBackColor = true;
        // 
        // _coolCodeLabel
        // 
        _coolCodeLabel.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
        _coolCodeLabel.Font = new Font("Courier New", 7.8f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _coolCodeLabel.Location = new Point(229, 124);
        _coolCodeLabel.Name = "_CoolCodeLabel";
        _coolCodeLabel.Size = new Size(388, 102);
        _coolCodeLabel.TabIndex = 13;
        // 
        // _standardCodeLabel
        // 
        _standardCodeLabel.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
        _standardCodeLabel.Font = new Font("Courier New", 7.8f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _standardCodeLabel.Location = new Point(229, 13);
        _standardCodeLabel.Name = "_StandardCodeLabel";
        _standardCodeLabel.Size = new Size(388, 102);
        _standardCodeLabel.TabIndex = 14;
        // 
        // _openCoolPreviewDialogButton
        // 
        _openCoolPreviewDialogButton.Location = new Point(13, 124);
        _openCoolPreviewDialogButton.Margin = new Padding(4);
        _openCoolPreviewDialogButton.Name = "_OpenCoolPreviewDialogButton";
        _openCoolPreviewDialogButton.Size = new Size(209, 102);
        _openCoolPreviewDialogButton.TabIndex = 12;
        _openCoolPreviewDialogButton.Text = "Enhanced" + '\r' + '\n' + "CoolPrintPreviewDialog";
        _openCoolPreviewDialogButton.UseVisualStyleBackColor = true;
        // 
        // _openStandardPreviewDialogButton
        // 
        _openStandardPreviewDialogButton.Location = new Point(13, 13);
        _openStandardPreviewDialogButton.Margin = new Padding(4);
        _openStandardPreviewDialogButton.Name = "_OpenStandardPreviewDialogButton";
        _openStandardPreviewDialogButton.Size = new Size(209, 102);
        _openStandardPreviewDialogButton.TabIndex = 11;
        _openStandardPreviewDialogButton.Text = "Standard" + '\r' + '\n' + "PrintPreviewDialog";
        _openStandardPreviewDialogButton.UseVisualStyleBackColor = true;
        // 
        // PrintDocument1
        // 
        // 
        // Form1
        // 
        AutoScaleDimensions = new SizeF(120.0f, 120.0f);
        AutoScaleMode = AutoScaleMode.Dpi;
        ClientSize = new Size(650, 275);
        Controls.Add(_longDocumentCheckBox);
        Controls.Add(_coolCodeLabel);
        Controls.Add(_standardCodeLabel);
        Controls.Add(_openCoolPreviewDialogButton);
        Controls.Add(_openStandardPreviewDialogButton);
        Name = "Form1";
        StartPosition = FormStartPosition.CenterScreen;
        Text = "Compare Print Preview Dialogs";
        ResumeLayout(false);
        PerformLayout();
    }

    private CheckBox _longDocumentCheckBox;
    private Label _coolCodeLabel;
    private Label _standardCodeLabel;
    private Button _openCoolPreviewDialogButton;
    private Button _openStandardPreviewDialogButton;
    private System.Drawing.Printing.PrintDocument _printDocument;
}
