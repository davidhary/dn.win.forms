# .NET Windows Forms Libraries

Support libraries for Windows Forms.
This is a fork of the [dn.core] repository.

* Project README files:
  * [cc.isr.WinForms.BindingListView](/src/libs/binding.list.view/readme.md) 
  * [cc.isr.WinForms.CoolPrintPreview](/src/libs/cool.print.preview/readme.md) 
  * [cc.isr.WinForms.Dialogs](/src/libs/dialogs/readme.md) 
  * [cc.isr.WinForms.ExceptionMessageBox](/src/libs/exception.message.box/readme.md) 
  * [cc.isr.WinForms.Forms](/src/libs/forms/readme.md) 
  * [cc.isr.WinForms.Metro](/src/libs/metro/readme.md) 
* [Attributions](Attributions.md)
* [Change Log](./CHANGELOG.md)
* [Cloning](Cloning.md)
* [Code License](LICENSE-CODE)
* [Code of Conduct](code_of_conduct.md)
* [Contributing](contributing.md)
* [Legal Notices](#legal-notices)
* [License](LICENSE)
* [Open Source](Open-Source.md)
* [Repository Owner](#Repository-Owner)
* [Security](security.md)

<a name="Repository-Owner"></a>
## Repository Owner
[ATE Coder]

<a name="Authors"></a>
## Authors
* [ATE Coder]  

<a name="legal-notices"></a>
## Legal Notices

Integrated Scientific Resources, Inc., and any contributors grant you a license to the documentation and other content in this repository under the [Creative Commons Attribution 4.0 International Public License] and grant you a license to any code in the repository under the [MIT License].

Integrated Scientific Resources, Inc., and/or other Integrated Scientific Resources, Inc., products and services referenced in the documentation may be either trademarks or registered trademarks of Integrated Scientific Resources, Inc., in the United States and/or other countries. The licenses for this project do not grant you rights to use any Integrated Scientific Resources, Inc., names, logos, or trademarks.

Integrated Scientific Resources, Inc., and any contributors reserve all other rights, whether under their respective copyrights, patents, or trademarks, whether by implication, estoppel or otherwise.

[Creative Commons Attribution 4.0 International Public License]: https://github.com/ATECoder/dn.vi.ivi/blob/main/license
[MIT License]: https://github.com/ATECoder/dn.vi.ivi/blob/main/license-code
 
[ATE Coder]: https://www.IntegratedScientificResources.com
[dn.core]: https://www.bitbucket.org/davidhary/dn.core

